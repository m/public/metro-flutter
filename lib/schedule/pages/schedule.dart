import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/search/widgets/search_bar.dart';

import 'schedule_lines.dart';
import 'schedule_plans.dart';

class SchedulePage extends TraceableStatefulWidget {
  SchedulePage({Key key}) : super(key: key, name: MatomoBloc.screen_schedules);

  @override
  SchedulePageState createState() => SchedulePageState();
}

class SchedulePageState extends State<SchedulePage> with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(text: translations.text('schedule.LINES')),
    Tab(text: translations.text('schedule.PLANS')),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    
    _tabController = TabController(
      length: myTabs.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.menu, color: globalElevations.fstTextColor.backgroundColor),
            onPressed: () => Scaffold.of(context).openDrawer()),
            backgroundColor: globalElevations.e16dp.backgroundColor,
            title: Text(translations.text('schedule.title_appbar'), style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),),
            iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
            bottom: TabBar(
              controller: _tabController,
              tabs: myTabs,
              unselectedLabelColor: globalElevations.fstTextColor.backgroundColor,
              labelColor: Theme.of(context).indicatorColor,
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: <Widget>[
              ScheduleLinesPage(),
              SchedulePlansPage(),
            ],
          ),
        ),
      ],
    );
  }
}
