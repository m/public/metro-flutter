import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/widgets/line_card.dart';
import 'package:url_launcher/url_launcher.dart';

class ScheduleLinesPage extends StatefulWidget {
  @override
  ScheduleLinesPageState createState() => ScheduleLinesPageState();
}

class ScheduleLinesPageState extends State<ScheduleLinesPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 16),
      child: Align(
        alignment: Alignment.topLeft,
        child: StreamBuilder<List<ReadyLine>>(
          stream: BlocProvider.of<LinesBloc>(context).onDownloadLinesCompleted,
          initialData: BlocProvider.of<LinesBloc>(context).lines,
          builder: (context, snapshot) {
            // If there is no data to display, we show a progress indicator
            if (snapshot.data == null || snapshot.data.isEmpty) return const AnimatedLoadingLogo();

            // Group Structurantes & Secondaires under the same type 'GRESIVAUDAN'
            // Group Urbaines & Interurbaines under the same type 'VOIRONAIS'
            List<ReadyLine> rebuildedLines =
            List.from(snapshot.data.where((x) => x.line.type != 'SCOL' && x.line.type != 'TAD' && x.line.type != 'SNC').toList());
            List<ReadyLine> tmp = [];
            for (int i = 0; i < rebuildedLines.length; i++) {
              if (rebuildedLines[i].line.type == 'Structurantes' || rebuildedLines[i].line.type == 'Secondaires') {
                final l = ReadyLine();
                l.line = Line.copy(rebuildedLines[i].line, 'GRESIVAUDAN');
                l.details = rebuildedLines[i].details;
                l.polyline = rebuildedLines[i].polyline;
                tmp.add(l);
              } else if (rebuildedLines[i].line.type == 'Urbaines' || rebuildedLines[i].line.type == 'Interurbaines') {
                final l = ReadyLine();
                l.line = Line.copy(rebuildedLines[i].line, 'VOIRONAIS');
                l.details = rebuildedLines[i].details;
                l.polyline = rebuildedLines[i].polyline;
                tmp.add(l);
              } else {
                tmp.add(rebuildedLines[i]);
              }
            }

            // Otherwise, we build lines views
            Map<String, List<ReadyLine>> linesGrouped = groupBy(tmp, (line) => line.line.type);
            return AnimationLimiter(
              child: ListView.builder(
                itemCount: linesGrouped.length,
                padding: const EdgeInsets.only(top: 8),
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  String type = linesGrouped.keys.elementAt(index);
                  return AnimationConfiguration.staggeredList(
                    position: index,
                    duration: const Duration(milliseconds: 375),
                    child: SlideAnimation(
                      verticalOffset: 50.0,
                      child: FadeInAnimation(
                        child: Container(
                          child: LineCard(
                            type: type,
                            line: linesGrouped,
                            onLineTap: (line) {
                              if (line.line.shortName == 'GRES') {
                                _openUrl("http://www.lignesplus-m.fr/lignes-mcovoit-gresivaudan/");
                              } else if (line.line.shortName == 'VOIR') {
                                _openUrl("http://www.lignesplus-m.fr/lignes-mcovoit-voironnais/");
                              } else {
                                BlocProvider.of<ScheduleBloc>(context).loadLineDetails(line);
                                BlocProvider.of<NavigationBloc>(context).changeTab(NavigationCategory.scheduleDetails);
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  _openUrl(String url) {
    launch(url);
  }
}
