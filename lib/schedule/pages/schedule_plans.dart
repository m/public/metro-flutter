import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:http/http.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/schedule/blocs/plans.dart';

import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/pdf_screen.dart';

class SchedulePlansPage extends StatefulWidget {
  @override
  SchedulePlansPageState createState() => SchedulePlansPageState();
}

class SchedulePlansPageState extends State<SchedulePlansPage> {
  bool _loadingPdf = false;
  static List<PlanData> plans = BlocProvider.master<PlanBloc>().plan;

  static List<Map<String, dynamic>> allNetworks = [
    {
      'src': 'assets/logos/SEM.png',
      'items': [
        {
          'name': translations.text('schedule_plans.urban_area'),
          'link': 'https://www.tag.fr/cms_viewFile.php?idtf=1888&path=Plan-lignes-essentielles_2018-10.pdf'
        },
        {
          'name': translations.text('schedule_plans.essential_lines'),
          'link': 'https://www.tag.fr/cms_viewFile.php?idtf=1888&path=Plan-lignes-essentielles_2018-10.pdf'
        },
        {
          'name': translations.text('schedule_plans.metropole'),
          'link': 'https://www.tag.fr/cms_viewFile.php?idtf=1887&path=Plan-Metropole-grenobloise-2018-10.pdf'
        }
      ]
    },
    {
      'src': 'assets/logos/C38.png',
      'items': [
        {
          'name': translations.text('schedule_plans.transisere'),
          'link': 'https://www.transisere.fr/ftp/document/plan-de-poche-reseau-2018-2019.pdf'
        }
      ]
    },
    {
      'src': 'assets/logos/GSV.png',
      'items': [
        {
          'name': translations.text('schedule_plans.tougo'),
          'link': 'https://www.tougo.fr/content/download/8634/112068/2019-04%20Abribus%20Plan%20Sch%C3%A9ma_A3-BD-compress%C3%A9.pdf'
        }
      ]
    },
    {
      'src': globalElevations.iconVoironnais,
      'items': [
        {
          'name': translations.text('schedule_plans.urban'),
          'link':
              'http://www.paysvoironnais.com/documents/Documents/TRANSPORT/Plan_Voiron_urbain_2019.pdf'
        },
        {
          'name': translations.text('schedule_plans.long_distance_plan'),
          'link':
              'http://www.paysvoironnais.com/documents/Documents/TRANSPORT/Interurbain_spider_2019.pdf'
        }
      ]
    },
    {
      'src': 'assets/logos/SMMAG.png',
      'items': [
        {
          'name': 'Aire Grenobloise',
          'link': 'https://www.tougo.fr/content/download/8634/112068/2019-04%20Abribus%20Plan%20Sch%C3%A9ma_A3-BD-compress%C3%A9.pdf'
        }
      ]
    }
  ];

  Future<void> _pdfLoader(fileUrl, String title) async {
    setState(() => _loadingPdf = true);
    String filename = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
    if (!filename.endsWith('.pdf')) filename += '.pdf';
    var request = await HttpClient().getUrl(Uri.parse(fileUrl));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    setState(() => _loadingPdf = false);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PDFScreen(file.path, title, bytes)));
  }

  static String getUrlPlan(String name){
    for (PlanData p in plans){
      if (p.nom == name) {
        return p.url;
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: <Widget>[
        AnimationLimiter(
          child: ListView.builder(
              itemCount: allNetworks.length,
              padding: const EdgeInsets.only(bottom: 8),
              itemBuilder: (context, index) {

                var network = allNetworks[index];
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 50.0,
                    child: FadeInAnimation(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: globalElevations.e02dp.backgroundColor,
                        ),
                        margin: EdgeInsets.only(left: 8, top: 8, right: 8),
                        child: Column(
                          children: <Widget>[
                            // Logo
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, top: 16, bottom: 16),
                              child: Container(
                                height: 40,
                                alignment: Alignment.centerLeft,
                                child: Image.asset(
                                  network['src'],
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ),
                            // Download links
                            ...(network['items'] as List).map((item) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                    left: 8, right: 8, bottom: 8),
                                child: FlatButton(
                                  color: globalElevations
                                      .expandPanelColor.backgroundColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(7.0)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16),
                                        child: Text(
                                          item['name'],
                                          style: TextStyle(
                                              color: globalElevations
                                                  .fstTextColor.backgroundColor,
                                              fontWeight: FontWeight.w300),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            translations
                                                .text('schedule_plans.pdf'),
                                            style: TextStyle(
                                                color: globalElevations
                                                    .fstTextColor
                                                    .backgroundColor,
                                                fontWeight: FontWeight.w300),
                                          ),
                                          SizedBox(
                                            width: 4,
                                          ),
                                          Icon(
                                            Icons.file_download,
                                            color: globalElevations
                                                .fstTextColor.backgroundColor,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  onPressed: () {
                                    _pdfLoader(getUrlPlan(item['name']), item['name']);
                                  },
                                ),
                              );
                            }).toList(),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
        Visibility(visible: _loadingPdf, child: AnimatedLoadingLogo()),
      ],
    );
  }
}
