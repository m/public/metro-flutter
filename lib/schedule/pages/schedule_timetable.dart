import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/widgets/always_disabled_focus.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/route_utils.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/models/schedule.dart';

class ScheduleTimetablePage extends TraceableStatefulWidget {
  final Line line;

  ScheduleTimetablePage({Key key, this.line}) : assert(line != null), super(key: key, name: MatomoBloc.screen_timetables);

  @override
  _ScheduleTimetablePageState createState() => _ScheduleTimetablePageState();
}

class _ScheduleTimetablePageState extends State<ScheduleTimetablePage> {
  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  int currentDir = 0;
  String dirFrom = '';
  String dirTo = '';

  @override
  void initState() {
    super.initState();

    _setDate(DateTime.now());
    _setTime(TimeOfDay.now());

    _requestTimetable(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(translations.text('schedule_detail.time_sheet')),
        leading: new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () => Navigator.pop(context)),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: StreamBuilder<Timetable>(
            stream: BlocProvider.of<ScheduleBloc>(context).onTimetableChanged,
            builder: (context, snapshot) {
              Widget list;
              List<bool> phone = [];
              if(!snapshot.hasData || snapshot.data == null)
                list =  const AnimatedLoadingLogo();
              else if(snapshot.data.dirs[0].stops.length == 0)
                list =  Text(translations.text('schedule_timetable.no_times'));
              else {
                lookupForDirectionNames(snapshot.data);
                int nbTrips = BlocProvider.of<ScheduleBloc>(context).nbTripsForTimetable;

                for (Trips t in snapshot.data.dirs[currentDir].trips){
                    if (t.pickupType == "2") {
                      phone.add(true);
                    }else{
                      phone.add(false);
                    }
                  }

                if (phone.length == 1){
                  phone.add(false);
                  phone.add(false);
                } else if (phone.length == 2){
                  phone.add(false);
                }else if (phone.length == 0){
                  phone.add(false);
                  phone.add(false);
                  phone.add(false);
                }

                list =  Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          (phone[0] != null && phone[0] != false)
                              ? Icon(
                                  Icons.phone,
                                  color: globalElevations.fstTextColor.backgroundColor,
                                  size: 26,
                                )
                              : SizedBox(width: 26,),
                          SizedBox(
                            width: 30,
                          ),
                          (phone[1] != null && phone[1] != false)
                              ? Icon(
                            Icons.phone,
                            color: globalElevations.fstTextColor.backgroundColor,
                            size: 26,
                          )
                              : SizedBox(width: 26,),
                          SizedBox(
                            width: 30,
                          ),
                          (phone[2] != null && phone[2] != false)
                              ? Icon(
                            Icons.phone,
                            color: globalElevations.fstTextColor.backgroundColor,
                            size: 26,
                          )
                              : SizedBox(width: 26,),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) => TimetableItem(stopPassage: snapshot.data.dirs[currentDir].stops[index], nbTrips: nbTrips,),
                        separatorBuilder: (context, index) => const Divider(),
                        itemCount: snapshot.data.dirs[currentDir].stops.length
                      ),
                    ),
                  ],
                );
              }

              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  // Header
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: LineDisplay(
                          line: widget.line,
                          size: 50,
                          isSchedule: true,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Text(
                            widget.line.directions,
                            textAlign: TextAlign.start,
                            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                  // Date & time pickers
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: 130,
                        child: Stack(
                          children: <Widget>[
                            TextField(
                              focusNode: AlwaysDisabledFocusNode(),
                              maxLines: 1,
                              controller: _dateController,
                              onTap: () => _onDateTap(),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                labelText: translations.text('route.date'),
                                labelStyle: TextStyle(
                                  color: Colors.black54,
                                ),
                              ),
                            ),
                            Positioned(
                              top: 26,
                              right: 0,
                              child: IgnorePointer(child: Icon(Icons.event)),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                      ),
                      Container(
                        width: 130,
                        child: Stack(
                          children: <Widget>[
                            TextField(
                              focusNode: AlwaysDisabledFocusNode(),
                              maxLines: 1,
                              controller: _timeController,
                              onTap: () => _onTimeTap(),
                              enableInteractiveSelection: false,
                              decoration: InputDecoration(
                                alignLabelWithHint: true,
                                labelText: translations.text('route.time'),
                                labelStyle: TextStyle(
                                  color: Colors.black54,
                                ),
                              ),
                            ),
                            Positioned(
                              top: 26,
                              right: 0,
                              child: IgnorePointer(child: Icon(Icons.access_time)),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  // Swap button
                  Center(
                    child: GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Icons.swap_vert,
                              size: 32,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(dirFrom),
                                Text(dirTo),
                              ],
                            )
                          ],
                        ),
                      ),
                      onTap: () {
                        _swapDirection();
                      },
                    ),
                  ),
                  // Previous & next button
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            _previousTimetable(snapshot.data);
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.arrow_back_ios),
                              Text(
                                translations.text('schedule_timetable.previous'),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            _nextTimetable(snapshot.data);
                          },
                          child: Row(
                            children: <Widget>[
                              Text(
                                translations.text('schedule_timetable.next'),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Icon(Icons.arrow_forward_ios),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8,),
                  // Timetable
                  Flexible(
                    fit: FlexFit.loose,
                    child: list,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  void _onDateTap() async {
    DateTime current = DateTime.now();

    DateTime date = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(current.year - 1, current.month, current.day),
      lastDate: DateTime(current.year + 1, current.month, current.day),
    );

    if (date != null && date != _selectedDate) {
      _setDate(date);
      _requestTimetable(context);
    }
  }

  void _setDate(DateTime date) {
    setState(() {
      _selectedDate = date;
      _dateController.text = dateFormat.format(date);
    });
  }

  void _onTimeTap() async {
    TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
    );
    if (time != null && time != _selectedTime) {
      _setTime(time);
      _requestTimetable(context);
    }
  }

  void _setTime(TimeOfDay time) {
    DateTime now = DateTime.now();
    setState(() {
      _selectedTime = time;
      _timeController.text = timeFormat.format(DateTime(now.year, now.month, now.day, time.hour, time.minute));
    });
  }

  void _swapDirection() {
    setState(() {
      currentDir = (currentDir + 1) % 2;
      lookupForDirectionNames(BlocProvider.of<ScheduleBloc>(context).timetable);
    });
  }

  void _previousTimetable(Timetable timetable) {
    _previousNextTimetable(timetable, false);
  }

  void _nextTimetable(Timetable timetable) {
    _previousNextTimetable(timetable, true);
  }

  void _previousNextTimetable(Timetable timetable, bool next) {
    if(timetable == null)
      return;

    DateTime requestedDate;
    if(timetable.dirs[currentDir] == null || (next ? timetable.dirs[currentDir].nextTime : timetable.dirs[currentDir].previousTime) == null) {
      requestedDate = DateTime(_selectedDate.year, _selectedDate.month, _selectedDate.day, _selectedTime.hour, _selectedTime.minute);
      if(next)
        requestedDate.add(Duration(minutes: 30));
      else
        requestedDate.add(Duration(minutes: -30));
    } else {
      requestedDate = next ? timetable.dirs[currentDir].nextTime : timetable.dirs[currentDir].previousTime;
    }

    DateTime requestedDateInLocal = DateTime(requestedDate.year, requestedDate.month, requestedDate.day, requestedDate.hour, requestedDate.minute);

    _setDate(requestedDateInLocal);
    _setTime(TimeOfDay.fromDateTime(requestedDateInLocal));
    _requestTimetable(context);
  }

  void _requestTimetable(BuildContext context) {
    // TODO : tell metro to correct the way they send the date to WS (it's in UTC even thought it's a locale time !)
    DateTime date = DateTime.utc(_selectedDate.year, _selectedDate.month, _selectedDate.day, _selectedTime.hour, _selectedTime.minute);
    BlocProvider.of<ScheduleBloc>(context).requestTimetable(lineCode: widget.line.id, date: date);
  }

  void lookupForDirectionNames(Timetable timetable) {
    if(timetable == null || timetable.dirs.length == 0 || timetable.dirs[currentDir].stops.length == 0) {
      dirFrom = '';
      dirTo = '';
    } else {
      dirFrom = timetable.dirs[currentDir].stops.first.name.replaceAll(',', ' -');
      dirTo = timetable.dirs[currentDir].stops.last.name.replaceAll(',', ' -');
    }
  }
}

class TimetableItem extends StatelessWidget {
  final StopPassage stopPassage;
  final int nbTrips;

  const TimetableItem({Key key, this.stopPassage, this.nbTrips}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(removeCityFromName(stopPassage.name), maxLines: 1, overflow: TextOverflow.ellipsis,),
        ),
        if(stopPassage != null) ...stopPassage.trips.map((t) {
          return Padding(
            padding: const EdgeInsets.only(left:4.0),
            child: ConstrainedBox(
              constraints: BoxConstraints(minWidth: 50, maxWidth: double.infinity, minHeight: 20, maxHeight: 20),
              child: FittedBox(
                child: Text(t),
                fit: BoxFit.contain,
              ),
            ),
          );
        }),
      ],
    );
  }
  
}