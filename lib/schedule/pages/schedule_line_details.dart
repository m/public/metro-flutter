import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:expandable/expandable.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/pages/schedule_timetable.dart';
import 'package:metro/schedule/widgets/schedule_list_item.dart';
import 'package:metro/schedule/widgets/schedule_of_passage.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/disturbances/pages/disturbances_filters.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:metro/global/widgets/pdf_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class ScheduleLineDetailsPage extends StatefulWidget {
  final bool isBottomsheet;
  final double paddingTop;

  ScheduleLineDetailsPage({Key key, this.isBottomsheet = false, this.paddingTop = 0.0}) : super(key: key);

  @override
  _ScheduleLineDetailsPageState createState() => _ScheduleLineDetailsPageState();
}

class _ScheduleLineDetailsPageState extends State<ScheduleLineDetailsPage> with WidgetsBindingObserver {
  bool _loadingPdf = false;
  Line _line;
  Color _lineColor;
  Color _textColor;
  Color _itemColor;
  Color _backgroundColor;
  String _assetsNsv;
  String _itemText;

  List<Point> _localListPoint = List<Point>();
  PointsBloc _pointsBloc;
  double bottomMarge = 160;

  // Data download and search
  StreamSubscription<ReadyLine> _onLineDetailsChanged;
  Stream<List<Point>> _onListDataChanged;
  BehaviorSubject<String> _onSearchValueChanged = BehaviorSubject<String>.seeded(null);

  List<ExpandableController> _controllers = List();
  Listenable listenableControllers;
  Function() controllersListener;
  StreamSubscription _onSelectedLineStopChanged;

  Color _overrideBorderColor;
  TextEditingController _textFieldController = TextEditingController();
  FocusNode focusNode = FocusNode();

  String hintText = '';

  Color labelInputColor = Colors.grey;
  bool isFocused = false;

  _onClear() {
    setState(() {
      _textFieldController.clear();
    });
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    _pointsBloc = BlocProvider.of<PointsBloc>(context);

    // Triggered when the user tap on a line display within Schedule page
    _onLineDetailsChanged = BlocProvider.of<ScheduleBloc>(context).onLineDetailsChanged.listen((readyLine) {
      _line = readyLine.line;
      _lineColor = Utils.colorFromHex(readyLine.line.color);
      _textColor = Utils.colorFromHex(_line.textColor);
      _pointsBloc.requestListPoints(readyLine.line.id);
    });

    // Triggered when the list is done downloading
    _onListDataChanged = Rx.combineLatest2(_pointsBloc.onRequestedPointsLineDownloaded, _onSearchValueChanged, (List<Point> points, String search) {
      if (search == null || search.isEmpty) return points;

      return points.where((Point point) => point.properties.name.toLowerCase().contains(search.toLowerCase())).toList();
    });

    // To close the last selected controller whenever an other one is tapped
    _onSelectedLineStopChanged = BlocProvider.of<ScheduleBloc>(context).onSelectedLineStopChanged.listen((data) {
      if (data != null && _controllers != null && _controllers.length > 0) {
        var selectedLineStopController = _controllers[data];
        int indexLastSelected = BlocProvider.of<ScheduleBloc>(context).lastSelectedLineStop;
        var lastSelectedLineStopController = indexLastSelected == null ? null : _controllers[indexLastSelected];
        if (selectedLineStopController != null) {
          if (lastSelectedLineStopController != null) {
            if (selectedLineStopController == lastSelectedLineStopController)
              selectedLineStopController.expanded = true;
            else
              lastSelectedLineStopController.expanded = false;
          }
        }
      }
    });

    // Listener that is called every time the list of controllers value changed to know if all ExpandablePanels are to show the line correctly
    controllersListener = () {
      if (_controllers.every((controller) => !controller.value)) BlocProvider.master<ScheduleBloc>().onTappedLineStop(null);
    };
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    _onLineDetailsChanged.cancel();
    _onSearchValueChanged.close();
    _onSelectedLineStopChanged.cancel();

    if (listenableControllers != null) listenableControllers.removeListener(controllersListener);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    // If the keyboard is open, the panel is move up (cover the picture)
    final renderBox = context.findRenderObject() as RenderBox;
    final offset = renderBox.localToGlobal(Offset.zero);
    final widgetRect = Rect.fromLTWH(offset.dx, offset.dy, renderBox.size.width, renderBox.size.height);
    final keyboardTopPixels = window.physicalSize.height - window.viewInsets.bottom;
    final keyboardTopPoints = keyboardTopPixels / window.devicePixelRatio;
    final overlap = widgetRect.bottom - keyboardTopPoints;
    setState(() {
      if (overlap > 0)
        bottomMarge = 0;
      else
        bottomMarge = 160;
    });
  }

  void _requestFocus() {
    setState(() {
      labelInputColor = Theme.of(context).indicatorColor;
    });
  }

  unfocus() {
    FocusScope.of(context).unfocus();
    labelInputColor = Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globalElevations.e00dp.backgroundColor,
      body: Padding(
        padding: EdgeInsets.only(top : widget.paddingTop),
        child: GestureDetector(
          onTap: () => this.unfocus(),
          child: Padding(
            padding: widget.isBottomsheet ? EdgeInsets.only(top : 10.0) : EdgeInsets.all(0.0) ,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 20,
                  left: 0,
                  right: 0,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () => widget.isBottomsheet ? Navigator.pop(context) : BlocProvider.of<NavigationBloc>(context).backNavigation(),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 22.0),
                        child: Icon(
                          Icons.arrow_back,
                          color: Theme.of(context).indicatorColor,
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 60,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    margin: const EdgeInsets.only(left: 5, top: 5, right: 5, bottom: 0),
                    decoration: BoxDecoration(
                      color: globalElevations.e02dp.backgroundColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(14),
                        topRight: Radius.circular(14),
                      ),
                    ),
                    child: StreamBuilder<Object>(
                      stream: _pointsBloc.onRequestedPointsLineDownloaded,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return const AnimatedLoadingLogo();

                        int nsv = BlocProvider.master<NsvBloc>().getNsvId(_line.id.replaceAll(':', '_'));
                        String asset = nsv == 4
                            ? globalElevations.perturbation_1
                            : (nsv == 2 || nsv == 3)
                                ? globalElevations.perturbation_2
                                : '';

                        if (nsv == 2) {
                          _assetsNsv = 'assets/icons/ic_smiley_service_legerement_perturbe.svg';
                          _itemColor = globalElevations.fstTextColor.backgroundColor;
                          _backgroundColor = globalElevations.nsvBackgroundColor_2.backgroundColor;
                          _itemText = 'Service légèrement perturbé';
                        } else if (nsv == 3) {
                          _assetsNsv = 'assets/icons/ic_smiley_service_tres_perturbe.svg';
                          _itemColor = Colors.white;
                          _backgroundColor = globalElevations.nsvBackgroundColor_3.backgroundColor;
                          _itemText = 'Service très perturbé';
                        } else if (nsv == 4) {
                          _assetsNsv = 'assets/icons/ic_smiley_hors_service.svg';
                          _itemColor = Colors.white;
                          _backgroundColor = globalElevations.nsvBackgroundColor_4.backgroundColor;
                          _itemText = 'Hors service';
                        } else {
                          _assetsNsv = 'assets/icons/ic_smiley_service_normal.svg';
                          _itemColor = globalElevations.fstTextColor.backgroundColor;
                          _backgroundColor = globalElevations.nsvBackgroundColorNormal.backgroundColor;
                          _itemText = 'Service normal';
                        }

                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Stack(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: _lineColor,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(14),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(22.0),
                                        child: Text(
                                          _line.shortName,
                                          style: TextStyle(color: _textColor, fontSize: 24, fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: (nsv == 2 || nsv == 3 || nsv == 4),
                                      child: Positioned(
                                        bottom: 50,
                                        left: 50,
                                        child: SizedBox(
                                          height: 20,
                                          width: 20,
                                          child: SvgPicture.asset(
                                            asset,
                                            height: 20,
                                            semanticsLabel: 'icon',
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Container(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 14.0),
                                      child: Text(
                                        _line.directions,
                                        textAlign: TextAlign.start,
                                        maxLines: 3,
                                        style: TextStyle(fontSize: 16,),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6.0),
                              child: FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    if (_line?.id?.startsWith('SEM'))
                                      FlatButton(
                                        onPressed: () {
                                          _pdfLoader(BlocProvider.master<WebServices>().getHost() + 'planligne/pdf?route=${_line.id}', _line.longName);
                                        },
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(16)),
                                          side: BorderSide(color: Theme.of(context).indicatorColor, width: 1),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Icon(
                                              Icons.map,
                                              color: Theme.of(context).indicatorColor,
                                            ),
                                            SizedBox(width: 6),
                                            Text(
                                              translations.text('schedule_detail.plan'),
                                              style: TextStyle(color: Theme.of(context).indicatorColor),
                                            ),
                                          ],
                                        ),
                                      ),
                                    SizedBox(width: 8,),
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ScheduleTimetablePage(
                                              line: _line,
                                            ),
                                          ),
                                        );
                                      },
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(color: Theme.of(context).indicatorColor, width: 1),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(16),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            translations.text('schedule_detail.time_sheet'),
                                            style: TextStyle(color: Theme.of(context).indicatorColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 8,),
                                    FlatButton(
                                      onPressed: () {
                                        _openUrl("tagandpasssemitag://home");
                                      },
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(color: Theme.of(context).indicatorColor, width: 1),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(16),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            translations.text('schedule_detail.ticket'),
                                            style: TextStyle(color: Theme.of(context).indicatorColor),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Visibility(visible: _loadingPdf, child: AnimatedLoadingLogo()),
                            SizedBox(
                              height: 6,
                            ),
                            GestureDetector(
                              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DisturbancesFilters(lineText: _line.shortName,))),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  padding: const EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    color: _backgroundColor,
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8.0, right: 16.0),
                                        child: SizedBox(
                                          height: 34,
                                          width: 34,
                                          child: SvgPicture.asset(
                                            _assetsNsv,
                                            color: _itemColor,
                                            semanticsLabel: 'icon',
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              _itemText,
                                              style: TextStyle(fontSize: 16, color: _itemColor),
                                            ),
                                            Visibility(
                                              visible: BlocProvider.of<EventsBloc>(context).getCountEvent(_line.shortName) != '' ? true : false,
                                              child: Text(
                                                BlocProvider.of<EventsBloc>(context).getCountEvent(_line.shortName),
                                                style: TextStyle(fontSize: 14, color: globalElevations.sndTextColor.backgroundColor),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Icon(
                                        Icons.chevron_right,
                                        color: _itemColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
                              child: Container(
                                child: TextFormField(
                                    onTap: _requestFocus,
                                    controller: _textFieldController,
                                    onChanged: (value) {
                                      _onSearchValueChanged.sink.add(value.replaceAll("’", "'"));
                                      setState(() {
                                        if (value != '') {
                                          isFocused = true;
                                        } else {
                                          isFocused = false;
                                        }
                                      });
                                    },
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.search, color: Colors.grey),
                                      labelText: translations.text("schedule_of_passage.search_stop"),
                                      labelStyle: TextStyle(color: labelInputColor),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8),
                                        borderSide: BorderSide(
                                          color: _overrideBorderColor != null ? _overrideBorderColor : Color(0xFF646267),
                                        ),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8), borderSide: BorderSide(color: Theme.of(context).indicatorColor)),
                                      suffixIcon: isFocused
                                          ? IconButton(
                                              icon: Icon(Icons.close, color: Colors.grey),
                                              onPressed: () {
                                                _onClear();
                                                isFocused = false;
                                                _onSearchValueChanged.sink.add("");
                                              })
                                          : null,
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(6),
                                        borderSide: BorderSide(
                                          color: _overrideBorderColor != null ? _overrideBorderColor : Color(0xFF646267),
                                        ),
                                      ),
                                    )),
                              ),
                            ),
                            // build result for the stream builder
                            _buildResults(context),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _openUrl(String url) {
    launch(url);
  }

  Widget _buildLinks() {
    return Padding(
      padding: const EdgeInsets.only(top: 28.0, right: 8.0, left: 8.0),
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            child: FlatButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0), side: BorderSide(color: Theme.of(context).colorScheme.primary)),
              onPressed: () {
                Utils.launchUrl('https://www.tag.fr/175-tag.fr-reclamations.htm');
              },
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Icons.public,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  Center(
                    child: Text("CONTACT",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold, fontSize: 16)),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            child: FlatButton(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0), side: BorderSide(color: Theme.of(context).colorScheme.primary)),
              onPressed: () {
                Utils.launchUrl('https://www.tag.fr/71-objet-trouves.htm');
              },
              color: Colors.transparent,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Icons.public,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  Center(
                    child: Text("OBJET PERDU / TROUVE ?",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold, fontSize: 16)),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Image.asset(globalElevations.footerAsset),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildResults(BuildContext context) {
    return StreamBuilder<List<Point>>(
      stream: _onListDataChanged,
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(12),
              ),
              Center(
                child: const AnimatedLoadingLogo(),
              ),
            ],
          );
        } else if (snapshot.data != null && snapshot.data.isEmpty) {
          return Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(12),
              ),
              Center(
                child: Text(translations.text('schedule_detail.no_line_found')),
              ),
            ],
          );
        } else {
          _controllers.clear();
          _localListPoint = snapshot.data;
          if (_localListPoint.length > 0) {
            for (var _ in _localListPoint) _controllers.add(ExpandableController());

            listenableControllers = Listenable.merge(_controllers);
            listenableControllers.addListener(controllersListener);
          }

          return _lineTimeListView();
        }
      },
    );
  }

  // Draw the line time for the list View
  Widget _lineTimeListView() {
    return Expanded(
      child: ListView.builder(
        padding: const EdgeInsets.only(top: 0, bottom: 0),
        itemCount: _localListPoint.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: ExpandableNotifier(
                          controller: _controllers[index],
                          child: ScheduleListItem(
                            index: index,
                            point: _localListPoint.elementAt(index),
                            line: _line,
                            stringLine: _line.shortName,
                          )),
                    ),
                    // TODO : handle this in a better way by having the stop circles in the expandable panel and the controlling the state with the controller to know if we had to show the line
                    Positioned(
                      child: Padding(
                        padding: EdgeInsets.only(left: 20, top: 4),
                        child: Container(
                          height: 50,
                          width: 25,
                          decoration: BoxDecoration(shape: BoxShape.circle, color: _lineColor),
                        ),
                      ),
                    ),
                    // Draws the lines between circles.
                    StreamBuilder<int>(
                        stream: BlocProvider.of<ScheduleBloc>(context).onSelectedLineStopChanged,
                        initialData: BlocProvider.of<ScheduleBloc>(context).selectedLineStop,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData || snapshot.data != index)
                            return _separatorLine(index);
                          else
                            return SizedBox.shrink();
                        }),
                    //Draw the emty circle in the principal circle.
                    _emptyCircle(index),
                  ],
                ),
              ),
              (index == _localListPoint.length - 1) ? _buildLinks() : Container()
            ],
          );
        },
      ),
    );
  }

//Draw the emty circle in the principal circle.
  Widget _emptyCircle(int index) {
    if (index != 0 && index != _localListPoint.length - 1) {
      return Positioned(
        child: Padding(
          padding: EdgeInsets.only(left: 25, top: 4),
          child: Container(
            height: 50.0,
            width: 15.0,
            decoration: BoxDecoration(shape: BoxShape.circle, color: _textColor),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

// Draws the lines between circles.
  Widget _separatorLine(int index) {
    if (index == 0) {
      if (_localListPoint.length != 1)
        return Positioned(
          top: 25.0,
          left: 30.0,
          child: Container(
            width: 5.0,
            height: 40.0,
            color: _lineColor,
          ),
        );
    } else if (index == _localListPoint.length - 1) {
      return Positioned(
        bottom: 25.0,
        left: 30.0,
        child: Container(
          width: 5.0,
          height: 40.0,
          color: _lineColor,
        ),
      );
    } else {
      return Positioned(
        left: 30.0,
        child: Container(
          width: 5.0,
          height: 70.0,
          color: _lineColor,
        ),
      );
    }

    return Container();
  }

  Widget _headerDetails(int index) {
    Point point = _localListPoint.elementAt(index);
    ClustersProperties clusterProperties = point.properties as ClustersProperties;
    return ScheduleOfPassage(
      point: _localListPoint.elementAt(index),
      code: clusterProperties.code,
      routeName: _line.id,
    );
  }

  Future<void> _pdfLoader(fileUrl, String title) async {
    setState(() {
      _loadingPdf = true;
    });
    String filename = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
    if (!filename.endsWith('.pdf')) filename += '.pdf';
    var request = await HttpClient().getUrl(Uri.parse(fileUrl));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    setState(() {
      _loadingPdf = false;
    });
    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFScreen(file.path, title, bytes)));
  }
}
