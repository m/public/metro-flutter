import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/helpers/time_helper.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/schedule/models/schedule.dart';
import 'package:metro/global/models/times.dart';
import 'package:metro/global/utils.dart';
import 'package:rxdart/rxdart.dart';

class ScheduleBloc extends BlocBase {
  BehaviorSubject<List<FormattedScheduleModel>> _scheduleController =
      BehaviorSubject<List<FormattedScheduleModel>>.seeded(List<FormattedScheduleModel>());
  Stream<List<FormattedScheduleModel>> get onSchedulesChanged => _scheduleController.stream;
  List<FormattedScheduleModel> get schedules => _scheduleController.value;

  BehaviorSubject<List<FormattedScheduleModel>> _favoriteScheduleController =
      BehaviorSubject<List<FormattedScheduleModel>>.seeded(List<FormattedScheduleModel>());
  Stream<List<FormattedScheduleModel>> get onFavoriteScheduleChanged => _favoriteScheduleController.stream;
  List<FormattedScheduleModel> get favoriteSchedules => _favoriteScheduleController.value;

  BehaviorSubject<List<FormattedScheduleModel>> _favoriteSchedulesController = BehaviorSubject<List<FormattedScheduleModel>>.seeded(List<FormattedScheduleModel>());
  Stream<List<FormattedScheduleModel>> get onFavoriteSchedulesChanged => _favoriteSchedulesController.stream;
  List<FormattedScheduleModel> get favoriteSchedulesValues => _favoriteSchedulesController.value;

  BehaviorSubject<bool> _scheduleDetailController = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get onScheduleDetailChanged => _scheduleDetailController.stream;
  bool get scheduleDetailIsVisible => _scheduleDetailController.value;

  BehaviorSubject<ReadyLine> _lineDetailsController = BehaviorSubject<ReadyLine>();
  Stream<ReadyLine> get onLineDetailsChanged => _lineDetailsController.stream;
  ReadyLine get lineDetails => _lineDetailsController.value;

  BehaviorSubject<Timetable> _timetableController = BehaviorSubject<Timetable>.seeded(null);
  Stream<Timetable> get onTimetableChanged => _timetableController.stream;
  Timetable get timetable => _timetableController.value;

  int nbTripsForTimetable = 3;

  BehaviorSubject<int> _selectedLineStopController = BehaviorSubject<int>.seeded(null);
  Stream<int> get onSelectedLineStopChanged => _selectedLineStopController.stream;

  int get selectedLineStop => _selectedLineStopController.value;
  int lastSelectedLineStop;

  BehaviorSubject<int> _selectedScheduleController = BehaviorSubject<int>.seeded(null);

  Stream<int> get onSelectedScheduleChanged => _selectedScheduleController.stream;

  int get selectedSchedule => _selectedScheduleController.value;
  int lastSelectedSchedule;

  int firstPassage;
  int secondPassage;

  @override
  void initState() {
    _scheduleController.sink.add(null);
  }

  @override
  void dispose() {
    _scheduleController.close();
    _scheduleDetailController.close();
    _lineDetailsController.close();
    _timetableController.close();
    _selectedLineStopController.close();
    _selectedScheduleController.close();
    _favoriteScheduleController.close();
    _favoriteSchedulesController.close();
  }

  void loadLineDetails(ReadyLine line) {
    _lineDetailsController.sink.add(line);
    resetSelectedLineStop();
  }

  /// Search schedules for a cluster from [code] value.
  ///
  /// [context] must be not null. Its used for request [LinesBloc].
  void searchCluster(String code, String routeName, List<ReadyLine> lines) async {
    _scheduleController.sink.add(null);
    assert(code != null && code.isNotEmpty);
    List<ScheduleModel> results = await _fetchSearchCluster(code, routeName);

    if (results == null) {
      // no result due to timeout
      _scheduleController.sink.add(List());
    } else {
      _scheduleController.sink.add(_buildFormattedSchedules(results, lines));
    }
  }

  void detailsSearchCluster(String code, String routeName, List<ReadyLine> lines) async {
    assert(code != null && code.isNotEmpty);
    List<ScheduleModel> results = await _fetchSearchCluster(code, routeName);

    if (results == null)
      _scheduleController.sink.add(List());
    else
      _scheduleController.sink.add(_buildFormattedSchedules(results, lines));
  }

  Future<List<FormattedScheduleModel>> loadClusters(String code, String routeName, List<ReadyLine> lines) async {
    assert(code != null && code.isNotEmpty);

    List<ScheduleModel> results = await _fetchSearchCluster(code, routeName);

    if (results != null) {
      List<FormattedScheduleModel> tmp = _buildFormattedSchedules(results, lines);
      _favoriteSchedulesController.sink.add(tmp);
      return tmp;
    }
    return null;
  }

  void updateFavClusters(String code, String routeName, List<ReadyLine> lines) async {
    assert(code != null && code.isNotEmpty);

    List<ScheduleModel> results = await _fetchSearchCluster(code, routeName);

    if (results != null) {
      List<FormattedScheduleModel> tmp = _buildFormattedSchedules(results, lines);
      _favoriteScheduleController.sink.add(tmp);
    }
  }

  void requestTimetable({@required String lineCode, @required DateTime date}) async {
    _timetableController.sink.add(null);
    Timetable timetable = await _fetchTimetable(_timetableUrl(lineCode, date.toUtc(), nbTripsForTimetable));
    _timetableController.sink.add(timetable);
  }

  Future<Timetable> _fetchTimetable(String urlRequest) async {
    final response = await BlocProvider.master<WebServices>().get(urlRequest);

    if (response.statusCode == 200)
      return _parseTimetable(response.body);
    else
      throw Exception('Failed to fetch timetable');
  }

  Timetable _parseTimetable(String responseBody) {
    final parsed = json.decode(responseBody);
    return Timetable.fromJson(parsed);
  }

  List<ScheduleModel> _parseSearchCluster(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<ScheduleModel>.from(parsed.map((json) => ScheduleModel.fromJson(json)));
  }

  Future<List<ScheduleModel>> _fetchSearchCluster(String code, String routeName) async {
    assert(code != null && code.isNotEmpty);

    // Building url
    String url = 'routers/default/index/clusters/$code/stoptimes';
    if (routeName != null && routeName.isNotEmpty) url += '?route=$routeName';

    final response = await BlocProvider.master<WebServices>().get(url, onTimeOut: () {
      return null;
    });

    if (response == null) {
      // timeout
      return null;
    } else {
      if (response.statusCode == 200)
        return _parseSearchCluster(response.body);
      else
        throw Exception('Failed to fetch search cluster');
    }
  }

  String _timetableUrl(String linecode, DateTime date, int nbTrips) =>
      Intl.message('ficheHoraires/json?route=$linecode&time=${date.millisecondsSinceEpoch}&nbTrips=$nbTrips',
          name: 'urlTimetable', args: [linecode, date, nbTrips], desc: 'Construct url for requesting timetables');

  /// Build the list of [FormattedScheduleModel] from [ScheduleModel] list.
  ///
  /// [context] must be not null. Its used for request [LinesBloc].
  List<FormattedScheduleModel> _buildFormattedSchedules(List<ScheduleModel> schedules, List<ReadyLine> lines) {
    List<FormattedScheduleModel> formattedSchedules = List();

    // List to
    List<int> timeList = List();

    List<String> tempListString = [];

    schedules.forEach((u) {
      if (!tempListString.contains(u.pattern.lastStop)) {
        tempListString.add(u.pattern.name);
      }
    });

    // First loop in order to put in a tab 4 values in minutes (provides to 2 patterns)
    for (int i = 0; i < schedules.length; i++) {
      if (schedules[i].pattern.name == tempListString[0]) {
        for (TimeModel t in schedules[i].times){
          timeList.add(t.realtimeArrival);
        }
      }
    }

    // If tab contains value(s) we can do a sort and set our variables
    if (timeList.isNotEmpty && timeList.length > 2) {
      timeList.sort();
      firstPassage = timeList.first;
      secondPassage = timeList[1];
    } else if (timeList.isNotEmpty && timeList.length >= 2) {
      timeList.sort();

      firstPassage = timeList.first;
      secondPassage = timeList[1];
    }

    // Second loop, if we have pattern with "Verdun - Préfecture" name and "SEM:15" code
    //  => we can set the two realtime fields
    int temp = 0;
    for (int i = 0; i < schedules.length; i++) {
      if (schedules[i].pattern.name == tempListString[0]) {
        if (temp == 0) {
          schedules[i].times.first.realtimeArrival = firstPassage;
          if (schedules[i].times.length > 1) schedules[i].times[1].realtimeArrival = timeList[1];
          temp++;

          List<ReadyLine> matchingLines = lines.where((x) => x.line.shortName == schedules[i].pattern.code.split(':')[1]).toList();

          FormattedScheduleModel model = _buildFormattedScheduleModel(matchingLines, schedules[i]);

          if (!formattedSchedules.map((schedule) => schedule.name).toList().contains(schedules[i].pattern.name)) formattedSchedules.add(model);
        }
      } else {
        List<ReadyLine> matchingLines = lines.where((x) => x.line.shortName == schedules[i].pattern.code.split(':')[1]).toList();

        FormattedScheduleModel model = _buildFormattedScheduleModel(matchingLines, schedules[i]);

        if (!formattedSchedules.map((schedule) => schedule.name).toList().contains(schedules[i].pattern.name)) formattedSchedules.add(model);
      }
    }

    return formattedSchedules;
  }

  /// Build FormattedScheduleModel.
  FormattedScheduleModel _buildFormattedScheduleModel(List<ReadyLine> lines, ScheduleModel schedule) {
    Color color = lines.length > 0 ? Utils.colorFromHex(lines[lines.length-1].line.color) : Colors.black45;
    Color textColor = lines.length > 0 ? Utils.colorFromWS(lines[0].details.properties.couleurTexte) : Colors.white;

    TimeModel time0 = schedule.times.length > 0 ? schedule.times[0] : null;
    TimeModel time1 = schedule.times.length > 1 ? schedule.times[1] : null;
    TimeModel time2 = schedule.times.length > 2 ? schedule.times[2] : null;
    TimeModel time3 = schedule.times.length > 3 ? schedule.times[3] : null;

    return FormattedScheduleModel(
        schedule.pattern.code,
        schedule.pattern.code.split(':')[1],
        schedule.pattern.name,
        schedule.pattern.dir,
        schedule.times[0].stopId,
        schedule.times[0].stopName,
        time0?.getTimeToDisplay() ?? '',
        time1?.getTimeToDisplay() ?? '',
        time2?.getTimeToDisplay() ?? '',
        time3?.getTimeToDisplay() ?? '',
        TimeHelper.getTimeIsRealtime(time0) ? Colors.transparent : Color(0xFFCACACA),
        TimeHelper.getTimeIsRealtime(time1) ? Colors.transparent : Color(0xFFCACACA),
        TimeHelper.getTimeIsRealtime(time2) ? Colors.transparent : Color(0xFFCACACA),
        TimeHelper.getTimeIsRealtime(time3) ? Colors.transparent : Color(0xFFCACACA),
        schedule.times[0].realtime,
        color,
        textColor,
        lines[lines.length - 1]);
  }

  /// Change visibilty of Schedule detail widget from [isVisible].
  /// Used by ScheduleOfPassage Widget.
  void setDetailVisibility(bool isVisible) {
    _scheduleDetailController.sink.add(isVisible);
  }

  void onTappedLineStop(int index) {
    lastSelectedLineStop = selectedLineStop;
    _selectedLineStopController.sink.add(index);
  }

  void resetSelectedLineStop() {
    _selectedLineStopController.sink.add(null);
    lastSelectedLineStop = null;
  }

  void onTappedSchedule(int index) {
    lastSelectedSchedule = selectedSchedule;
    _selectedScheduleController.sink.add(index);
  }

  void resetSelectedSchedule() {
    _selectedScheduleController.sink.add(null);
    lastSelectedSchedule = null;
  }
}
