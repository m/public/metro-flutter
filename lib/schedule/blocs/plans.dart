import 'dart:async';
import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';


class PlanBloc extends BlocBase {
  BehaviorSubject<List<PlanData>> _downloadController;
  Stream<List<PlanData>> get onDownloadLinesCompleted => _downloadController.stream;
  List<PlanData> get plan => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<PlanData>>.seeded(List<PlanData>());
    _downloadLines();
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadLines() {
    _downloadAndProcessData();
  }

  List<PlanData> _parsePlan(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<PlanData>((json) => PlanData.fromJson(json)).toList();
  }

  Future<List<PlanData>> _fetchPlan() async {
    final response = await BlocProvider.master<WebServices>().get('plansreseaux/json');

    if (response.statusCode == 200)
      return _parsePlan(response.body);
    else
      throw Exception('Failed to fetch plan');
  }

  void _downloadAndProcessData() async {

    List<PlanData> allPlan = await _fetchPlan();

    _downloadController.sink.add(allPlan);
  }

}

class PlanData {
  final String reseau;
  final String nom;
  final String url;

  PlanData({this.reseau, this.nom, this.url});

  PlanData.fromStandard(this.reseau, this.nom, this.url);

  factory PlanData.fromJson(Map<String, dynamic> json) {

    return PlanData(
      reseau: json['reseau'] as String,
      nom: json['nom'] as String,
      url: json['url'] as String,
    );
  }
}
