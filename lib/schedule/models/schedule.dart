import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/models/pattern.dart';
import 'package:metro/global/models/times.dart';

/// Schedule of passage model.
class ScheduleModel {
  final PatternModel pattern;
  final List<TimeModel> times;

  ScheduleModel({this.pattern, this.times});

  factory ScheduleModel.fromJson(Map<String, dynamic> json){
    List list = json['times'];
    List<TimeModel> timesList = list.map((i) => TimeModel.fromJson(i)).toList();

    return ScheduleModel(pattern: PatternModel.fromJson(json['pattern']),times:timesList);  
  }
}

class Timetable {
  final List <Direction> dirs;

  Timetable(this.dirs);

  factory Timetable.fromJson(Map<String, dynamic> json){
    if(json == null)
      return null;

    List<Direction> dirs = List<Direction>(json.length);
    for(var i = 0; i < json.length; i++)
      dirs[i] = Direction.fromJson(json['$i']);

    return Timetable(dirs);
  }
}

class Direction {
  final List<StopPassage> stops;
  final DateTime nextTime;
  final DateTime previousTime;
  final List<Trips> trips;

  Direction({this.nextTime, this.previousTime, this.stops,this.trips});

  factory Direction.fromJson(Map<String, dynamic> json){
    List<StopPassage> stops = (json['arrets'] as List)?.map((l) => StopPassage.fromJson(l))?.toList();
    List<Trips> trips = (json['trips'] as List)?.map((l) => Trips.fromJson(l))?.toList();
    return Direction(
      stops: stops,
      nextTime: json['nextTime'] != null ? DateTime.fromMillisecondsSinceEpoch(int.parse(json['nextTime'].toString()), isUtc: true) : null,
      previousTime: json['prevTime'] != null ? DateTime.fromMillisecondsSinceEpoch(int.parse(json['prevTime'].toString()), isUtc: true) : null,
      trips: trips
    );
  }
}

class Trips{
  final String tripId;
  final String pickupType;

  Trips(this.tripId, this.pickupType);

  factory Trips.fromJson(Map<String, dynamic> parsedJson) {
    return Trips(parsedJson['tripId'], parsedJson['pickupType']);
  }
}

class StopPassage {
  final String name;
  final List<String> trips;

  StopPassage({this.name, this.trips});

  factory StopPassage.fromJson(Map<String, dynamic> json){
    List<String> trips = (json['trips'] as List)?.map((trip) {
      if(trip is num) {
        DateTime date = getDateFromSecondsFromMidnight(trip);
        return date != null ? formatTimeFromDateTime(date) : 'N/A';
      }

      if(trip is String)
        return trip;

      return 'err';
    })?.toList();

    return StopPassage(
      name: json['stopName'].toString(),
      trips: trips,
    );
  }
}