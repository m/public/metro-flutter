import 'dart:async';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/agenceMetro.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/parking.dart';
import 'package:metro/global/blocs/parkingData.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/list_item.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/schedule/blocs/schedule.dart';

/// Widget which display schedules for a cluster.
///
/// [code] used to call API for schedules.
/// [routeName] used with [code] to call API for schedules for a precise route.
class FavoriteDynamicData extends StatefulWidget {
  final Favorite favorite;

  FavoriteDynamicData({Widget child, this.favorite});

  @override
  _FavoriteDynamicDataState createState() => _FavoriteDynamicDataState();
}

class _FavoriteDynamicDataState extends State<FavoriteDynamicData> {
  List<ExpandableController> _controllers = List();

  StreamSubscription _onSelectedScheduleChanged;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    // To close the last selected controller whenever an other one is tapped
    _onSelectedScheduleChanged = BlocProvider.of<ScheduleBloc>(context).onSelectedScheduleChanged.listen((data) {
      if (data != null && _controllers != null && _controllers.length > 0) {
        var selectedScheduleController = _controllers[data];
        int indexLastSelected = BlocProvider.of<ScheduleBloc>(context).lastSelectedSchedule;
        var lastSelectedScheduleController = indexLastSelected == null ? null : _controllers[indexLastSelected];
        if (selectedScheduleController != null) {
          if (lastSelectedScheduleController != null) {
            if (selectedScheduleController == lastSelectedScheduleController)
              selectedScheduleController.expanded = true;
            else
              lastSelectedScheduleController.expanded = false;
          }
        }
      }
    });

    return widget.favorite.point1.properties.type == PropertiesType.irve
        ? ListItem.normal(
            iconData: widget.favorite?.point1?.properties?.iconData(),
            text1: widget.favorite.point1.properties.name,
            text2: (widget.favorite.point1.properties as IrveProperties).pdcNb + " bornes",
            text3: (widget.favorite.point1.properties as IrveProperties).pdcTypes,
          )
        : _buildView(widget.favorite);
  }

  Widget _buildView(Favorite favorite) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            color: globalElevations.e08dp.backgroundColor,
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              ),
              _buildBody(favorite),
            ]),
          ),
        ),
      ],
    );
  }

  Widget _buildBody(Favorite favorite) {
    Future<void> futureData;
    List<String> favoriteData = [];

    switch (favorite.point1.properties.type) {
      case PropertiesType.pkg:
      case PropertiesType.par:
        futureData = BlocProvider.master<ParkingDataBloc>().loadParking();
        break;
      case PropertiesType.agenceM:
      case PropertiesType.mva:
        futureData = BlocProvider.master<AgenceMetro>().loadAgency();
        break;
      default:
        break;
    }

    return StreamBuilder(
        stream: BlocProvider.master<ParkingDataBloc>().onDownloadparkingDataInfoCompleted,
        builder: (context, snapshot) {
          if (snapshot.data != null)
            return FutureBuilder(
                future: futureData,
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data == null)
                    return SizedBox(
                      height: 5,
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
                      ),
                    );

                  switch (favorite.point1.properties.type) {
                    case PropertiesType.pkg:
                    case PropertiesType.par:
                      favoriteData.add(
                          BlocProvider.master<ParkingDataBloc>().getTypeParking(widget.favorite.point1.properties.id).tarif_pmr.toString() ?? '');
                      favoriteData.add((BlocProvider.master<ParkingBloc>().countAvailable(widget.favorite.point1.properties.id).toString() != "-1" &&
                              BlocProvider.master<ParkingBloc>().countAvailable(widget.favorite.point1.properties.id).toString() != 'null')
                          ? BlocProvider.master<ParkingBloc>().countAvailable(widget.favorite.point1.properties.id).toString() +
                              ' ' +
                              translations.text('bottomsheet_content.parking.places')
                          : null);
                      break;
                    case PropertiesType.agenceM:
                      favoriteData.add(translations.text('agency.week') +
                          BlocProvider.master<AgenceMetro>().getTypeMetro(favorite?.point1?.properties?.name).HORAIRES_LUNDI);
                      favoriteData.add(translations.text('agency.saturday') +
                          BlocProvider.master<AgenceMetro>().getTypeMetro(favorite?.point1?.properties?.name).HORAIRES_SAMEDI);
                      break;
                    case PropertiesType.mva:
                      favoriteData.add(PropertiesTypeExtension.getTypeName(favorite.point1.properties.type));
                      favoriteData.add(PropertiesTypeExtension.getPoiAddress(favorite.point1));
                      break;
                    default:
                      break;
                  }

                  return Container(
                      color: globalElevations.e01dp.backgroundColor,
                      child: ListItem.normal(
                        iconData: widget.favorite?.point1?.properties?.iconData(),
                        text1: widget.favorite.point1.properties.name,
                        text2: favoriteData[0],
                        text3: favoriteData[1],
                      ));
                });

          return Container();
        });
  }
}
