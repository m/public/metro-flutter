import 'dart:async';
import 'dart:io';
import 'package:device_apps/device_apps.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/disturbances/pages/disturbances_filters.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/pages/schedule_line_details.dart';
import 'package:metro/schedule/pages/schedule_timetable.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomModule extends StatefulWidget {
  final FormattedScheduleModel schedule;
  final Point point;
  final ReadyLine readyLine;

  const BottomModule({Key key, this.schedule, this.point, this.readyLine}) : super(key: key);

  @override
  _BottomModuleState createState() => _BottomModuleState();
}

class _BottomModuleState extends State<BottomModule> {
  dynamic data;
  Future<bool> isFavorite;
  bool hasBeenTaped;
  bool isInstalled;
  String _itemText;
  String linkStore = '';

  @override
  void initState() {
    super.initState();
    getApps();
  }

  @override
  Widget build(BuildContext context) {
    String txt = widget.schedule.name + ', ';
    txt += widget.readyLine.line.directions.replaceAll("\n", " ");
    _setData(widget.schedule);

    int nsv = BlocProvider.master<NsvBloc>().getNsvId(widget.readyLine.line.id.replaceAll(':', '_'));
    String asset = nsv == 4
        ? globalElevations.perturbation_1
        : (nsv == 2 || nsv == 3)
            ? globalElevations.perturbation_2
            : '';

    if (nsv == 2) {
      _itemText = 'Service légèrement perturbé';
    } else if (nsv == 3) {
      _itemText = 'Service très perturbé';
    } else if (nsv == 4) {
      _itemText = 'Hors service';
    }

    return Wrap(
      children: [
        FutureBuilder<bool>(
          future: isFavorite,
          builder: (context, favsnapshot) {
            bool value = false;
            if (!favsnapshot.hasError && favsnapshot.hasData) {
              hasBeenTaped = false;
              value = favsnapshot.data;
            }
            bool b = value;

            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 66,
                        width: 66,
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child: LineDisplay(
                            line: widget.readyLine.line,
                            isBottomSheet: true,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width - 106,
                        padding: const EdgeInsets.only(top: 4.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.arrow_forward_rounded,
                                  size: 20,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Expanded(
                                  child: Text(
                                    widget.schedule.name.toLowerCase().replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" "),
                                    maxLines: null,
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[..._buildAllLineContent(widget.schedule)],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    (nsv == 2 || nsv == 3 || nsv == 4)
                        ? ListTile(
                            leading: SvgPicture.asset(
                              asset,
                              width: 30,
                            ),
                            title: Text(
                              _itemText,
                              style: TextStyle(color: globalElevations.perturbationRedColor.backgroundColor),
                            ),
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DisturbancesFilters(
                                  lineText: widget.readyLine.line.shortName,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    ListTile(
                      leading: Icon(Icons.schedule),
                      title: Text("Fiche horaire"),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) => ScheduleTimetablePage(
                              line: widget.readyLine.line,
                            ),
                          ),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.credit_card_rounded),
                      title: Text("Ticket"),
                      onTap: () {
                        (isInstalled != null && isInstalled == true) ? _openUrl("tagandpasssemitag://home") : _openUrl(linkStore);
                      },
                    ),
                    ListTile(
                      leading: RotatedBox(quarterTurns: 1, child: Icon(Icons.linear_scale)),
                      title: Text("Détail de la ligne"),
                      onTap: () {
                        if (widget.readyLine.line.shortName == 'GRES') {
                          _openUrl("http://www.lignesplus-m.fr/lignes-mcovoit-gresivaudan/");
                        } else if (widget.readyLine.line.shortName == 'VOIR') {
                          _openUrl("http://www.lignesplus-m.fr/lignes-mcovoit-voironnais/");
                        } else {
                          BlocProvider.of<ScheduleBloc>(context).loadLineDetails(widget.readyLine);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (ctx) => ScheduleLineDetailsPage(
                                isBottomsheet: true,
                                paddingTop: 12.0,
                              ),
                            ),
                          );
                        }
                      },
                    ),
                    ListTile(
                      leading: value ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
                      title: Text(b ? 'Retirer des favoris' : 'Ajouter aux favoris'),
                      onTap: () {
                        setState(() {
                          b = !b;
                        });
                        if (hasBeenTaped) return;
                        value ? _removeFavorite(widget.schedule) : _addFavorite(widget.schedule);
                      },
                    ),
                    SizedBox(height: 20,)
                  ],
                ),
              ],
            );
          },
        ),
      ],
    );
  }

  Future<void> getApps() async {
    if (Platform.isAndroid) {
      // print(await AppAvailability.checkAvailability("com.android.chromeette"));
      // Returns: Map<String, String>{app_name: Chrome, package_name: com.android.chrome, versionCode: null, version_name: 55.0.2883.91}
      isInstalled = await DeviceApps.isAppInstalled('fr.semitag.tagandpass');
      linkStore = "https://play.google.com/store/apps/details?id=fr.semitag.tagandpass&hl=fr&gl=US";
      // Returns: true
    } else if (Platform.isIOS) {
      // iOS doesn't allow to get installed apps.
      isInstalled = false;
      linkStore = "https://apps.apple.com/fr/app/passmobilit%C3%A9s/id1031595009";
      // Returns: Map<String, String>{app_name: , package_name: calshow://, versionCode: , version_name: }
    } else {
      isInstalled = false;
      linkStore = "https://apps.apple.com/fr/app/passmobilit%C3%A9s/id1031595009";
    }
  }

  _setData(FormattedScheduleModel schedule) {
    if (data == null || data != widget.point) {
      FavoritesBloc favoritesBloc = BlocProvider.of<FavoritesBloc>(context);
      data = widget.point;
      if (data is FavoriteItinerary)
        isFavorite = favoritesBloc.isFavoriteItinerary(data);
      else if (data is FavoriteCluster)
        isFavorite = favoritesBloc.isFavoriteClusterLines(data, widget.readyLine.line.shortName);
      else
        isFavorite = favoritesBloc.isFavoriteClusterLines(data, widget.readyLine.line.shortName);
    }
  }

  _addFavorite(FormattedScheduleModel schedule) async {
    if (hasBeenTaped || !mounted) return;

    setState(() => hasBeenTaped = true);

    if (data is FavoriteItinerary) {
      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteItinerary(data);
    } else if ((data as Point).properties is ClustersProperties) {
      FavoriteCluster favoriteCluster = FavoriteCluster(stop: widget.point, lines: [widget.readyLine.line]);
      await BlocProvider.of<FavoritesBloc>(context).addFavorite(favoriteCluster);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavorite(data);
    } else {
      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);
    }

    setState(() {
      isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => true);
    });
  }

  _removeFavorite(FormattedScheduleModel schedule) async {
    if (hasBeenTaped || !mounted) return;

    setState(() => hasBeenTaped = true);

    await BlocProvider.of<FavoritesBloc>(context).deleteFavoriteClusterLines(widget.point, widget.readyLine.line.shortName);

    setState(() {
      isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => false);
    });
  }

  _openUrl(String url) {
    launch(url);
  }

  List<Widget> _buildAllLineContent(FormattedScheduleModel schedule) {
    String idLine = getLineInFunctionOfSchedule(schedule).replaceAll(':', '_');
    int nsv = BlocProvider.master<NsvBloc>().getNsvId(idLine);

    List<Widget> widgetsFirstNextArrival = _buildAllNextArrivalWidget(schedule.firstNextArrival, true);
    List<Widget> widgetsSecondNextArrival = _buildAllNextArrivalWidget(schedule.secondNextArrival, false);
    List<Widget> widgetsThirdNextArrival = _buildAllNextArrivalWidget(schedule.thirdNextArrival, false);
    List<Widget> widgetsFourthNextArrival = _buildAllNextArrivalWidget(schedule.fourthNextArrival, false);

    return <Widget>[
      nsv == 4
          ? Padding(
              padding: const EdgeInsets.only(right: 8.0, top: 10, bottom: 10),
              child: Container(
                decoration: BoxDecoration(
                  color: globalElevations.e12dp.backgroundColor,
                  borderRadius: BorderRadius.circular(12),
                ),
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Hors service",
                  style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
            )
          : Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                widgetsFirstNextArrival.isNotEmpty
                    ? Row(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: widgetsFirstNextArrival,
                          ),
                        ],
                      )
                    : Container(),
                widgetsSecondNextArrival.isNotEmpty
                    ? Row(
                        children: [
                          _circle(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: widgetsSecondNextArrival,
                          ),
                        ],
                      )
                    : Container(),
                widgetsThirdNextArrival.isNotEmpty
                    ? Row(
                        children: [
                          _circle(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: widgetsThirdNextArrival,
                          ),
                        ],
                      )
                    : Container(),
                widgetsFourthNextArrival.isNotEmpty
                    ? Row(
                        children: [
                          _circle(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: widgetsFourthNextArrival,
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
    ];
  }

  Widget _circle() {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: 5,
        height: 5,
        decoration: BoxDecoration(
          color: globalElevations.fstTextColor.backgroundColor,
          borderRadius: BorderRadius.circular(2.5),
        ),
      ),
    );
  }

  String getLineInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String idLine = BlocProvider.master<LinesBloc>().lines.singleWhere((readyLine) => readyLine.line.id == lineId).line.id;
    return idLine;
  }

  List<Widget> _buildAllNextArrivalWidget(String nextArrival, bool textIsBold) {
    List<Widget> list = List();
    if (nextArrival != null && nextArrival.isNotEmpty) {
      if (nextArrival.contains(':')) {
        list.add(Center(
          child: Text(nextArrival, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17)),
        ));
      } else {
        list.add(Text(nextArrival, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17)));
        list.add(Text(
            nextArrival.contains('>')
                ? 'h'
                : nextArrival.contains(':')
                    ? ''
                    : 'min',
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17)));
      }
    }
    return list;
  }
}

extension CapExtension on String {
  String get inCaps => this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';

  String get allInCaps => this.toUpperCase();

  String get capitalizeFirstofEach => this.replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" ");
}
