import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/new_expandable_panel.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/widgets/schedule_of_passage.dart';
import 'package:metro/schedule/widgets/schedule_bottom_module.dart';

class ScheduleListItem extends StatefulWidget {
  final Point point;
  final Line line;
  final int index;
  final String stringLine;

  ScheduleListItem({
    @required this.point,
    @required this.line,
    @required this.index,
    @required this.stringLine,
  });

  @override
  State<StatefulWidget> createState() => _ScheduleListItemState();
}

class _ScheduleListItemState extends State<ScheduleListItem> {
  Future<bool> isFavorite;
  bool hasBeenTaped;
  dynamic data;

  Timer _timer;

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 45, top: 5, bottom: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    widget.point.properties.name,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  widget.point.properties.city,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: globalElevations.sndTextColor.backgroundColor,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildHeaderDetails() {
    ClustersProperties clustersProperties =
        widget.point.properties as ClustersProperties;
    return ScheduleOfPassage(
      point: widget.point,
      code: clustersProperties.code,
      routeName: widget.line.id,
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null && _timer.isActive) _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NewExpandablePanel(
      header: _buildHeader(),
      details: _buildHeaderDetails(),
      onOpenOrClose: (bool expanded) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (expanded) {
          ScheduleBloc scheduleBloc = BlocProvider.of<ScheduleBloc>(context);
          scheduleBloc.onTappedLineStop(widget.index);
          scheduleBloc.searchCluster(
            (widget.point.properties as ClustersProperties).code,
            widget.line.id,
            BlocProvider.of<LinesBloc>(context).lines,
          );
          _timer = Timer.periodic(Duration(seconds: 20), (timer) {
            scheduleBloc.detailsSearchCluster(
              (widget.point.properties as ClustersProperties).code,
              widget.line.id,
              BlocProvider.of<LinesBloc>(context).lines,
            );
          });
        } else {
          if (_timer != null && _timer.isActive) _timer.cancel();
        }
      },
      actions: [
        Expanded(child: Container()),
        ExpandablePanelAction("AUTRES ACTIONS", () {
          openBottomSheet(context, widget.line, widget.point);
        }),
      ],
    );
  }

  openBottomSheet(BuildContext context, Line line, Point point) async {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return ScheduleBottomModule(
            line: line,
            point: point
        );
      },
    );
  }

  _removeFavorite() async {
    if (hasBeenTaped || !mounted) return;

    setState(() => hasBeenTaped = true);

    await BlocProvider.of<FavoritesBloc>(context)
        .deleteFavoriteClusterLines(widget.point, widget.stringLine);

    setState(() {
      isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => false);
    });
  }

  List<String> getChoices() {
    return [
      translations.text('schedule_detail.go_to'),
      translations.text('schedule_detail.locate_stop')
    ];
  }

  _goToRoute() {
    RoutesBloc routesBloc = BlocProvider.master<RoutesBloc>();
    routesBloc.setDeparture(null);
    routesBloc.setDestination(widget.point);
    BlocProvider.master<NavigationBloc>().changeTab(NavigationCategory.route);
  }

  void choiceAction(String choice) {
    if (choice == translations.text('schedule_detail.go_to'))
      _goToRoute();
    else if (choice == translations.text('schedule_detail.locate_stop'))
      _locateStop();
  }

  void _locateStop() {
    NavigationBloc navigationBloc = BlocProvider.of<NavigationBloc>(context);
    MapBloc mapBloc = BlocProvider.of<MapBloc>(context);
    navigationBloc.changeTab(NavigationCategory.scheduleDetailsAll);
    mapBloc.setBottomSheetContent(BottomSheetCategory.schedule, arguments: widget.point);
    mapBloc.moveTo(MapPositionAndZoom.withLatLng(Utils.coordinatesFromArray(widget.point.geometry.coordinates), 17), pointId: widget.point.properties.id);
    if (!widget.point.properties.id.contains("MCO")) mapBloc.selectPOI(widget.point.properties.id);
  }
}
