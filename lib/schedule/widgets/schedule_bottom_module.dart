import 'dart:async';
import 'dart:io';
import 'package:device_apps/device_apps.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/line.dart';

class ScheduleBottomModule extends StatefulWidget {
  final Line line;
  final Point point;

  const ScheduleBottomModule({Key key, this.line, this.point}) : super(key: key);

  @override
  _ScheduleBottomModuleState createState() => _ScheduleBottomModuleState();
}

class _ScheduleBottomModuleState extends State<ScheduleBottomModule> {
  dynamic data;
  Future<bool> isFavorite;
  bool hasBeenTaped;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _setData();

    return FutureBuilder<bool>(
      future: isFavorite,
      builder: (context, snapshot) {
        bool value = false;
        if (!snapshot.hasError && snapshot.hasData) {
          hasBeenTaped = false;
          value = snapshot.data;
        }
        bool b = value;
        if (snapshot.data == null || !snapshot.hasData)
          return Container(
            child: AnimatedLoadingLogo(
              height: 60,
            ),
          );

        return Wrap(
          children: [
            Container(
              color: globalElevations.e24dp.backgroundColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 66,
                          width: 66,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: LineDisplay(
                              line: widget.line,
                              isBottomSheet: true,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 106,
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(
                                      'Arrêt ' + widget.point.properties.name.toLowerCase().replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" "),
                                      maxLines: null,
                                      style: TextStyle(fontSize: 20, color: globalElevations.fstTextColor.backgroundColor),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 4,),
                              Text(
                                widget.point.properties.city,
                                maxLines: null,
                                style: TextStyle(fontSize: 14, color: globalElevations.fstTextColor.backgroundColor),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Visibility(
                        visible: widget.line.type == "FLEXO",
                        child: ListTile(
                          leading: Icon(Icons.local_phone_outlined),
                          title: Text("Réserver"),
                          onTap: () {
                            _launchCaller();
                          },
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.location_pin),
                        title: Text("Localisation de l'arrêt"),
                        onTap: () {
                          Navigator.pop(context);
                          _locateStop();
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.near_me_outlined),
                        title: Text('Itinéraire vers ce point'),
                        onTap: () {
                          Navigator.pop(context);
                          _goToRoute();
                        },
                      ),
                      ListTile(
                        leading: value ? Icon(Icons.favorite) : Icon(Icons.favorite_border),
                        title: Text(b ? 'Retirer des favoris' : 'Ajouter aux favoris'),
                        onTap: () {
                          setState(() {
                            b = !b;
                          });
                          if (hasBeenTaped) return;
                          value ? _removeFavorite() : _addFavorite();
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(height: 20.0,)
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  _setData() {
    if (data == null || data != widget.point) {
      FavoritesBloc favoritesBloc = BlocProvider.of<FavoritesBloc>(context);
      data = widget.point;
        isFavorite =
            favoritesBloc.isFavoriteClusterLines(data, widget.line.shortName);
    }
  }

  _addFavorite() async {
    if (hasBeenTaped || !mounted) return;

    setState(() => hasBeenTaped = true);

    if (data is FavoriteItinerary) {
      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite =
          BlocProvider.of<FavoritesBloc>(context).isFavoriteItinerary(data);
    } else if ((data as Point).properties is ClustersProperties) {
      FavoriteCluster favoriteCluster =
      FavoriteCluster(stop: widget.point, lines: [widget.line]);
      await BlocProvider.of<FavoritesBloc>(context)
          .addFavorite(favoriteCluster);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavorite(data);
    } else {
      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite =
          BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);
    }

    setState(() {
      isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => true);
    });
  }


    _removeFavorite() async {
    if (hasBeenTaped || !mounted) return;

    setState(() => hasBeenTaped = true);

    await BlocProvider.of<FavoritesBloc>(context).deleteFavoriteClusterLines(widget.point, widget.line.shortName);

    setState(() {
      isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => false);
    });
  }

  _launchCaller() async {
    const url = "tel:0438703870";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _goToRoute() {
    RoutesBloc routesBloc = BlocProvider.master<RoutesBloc>();
    routesBloc.setDeparture(null);
    routesBloc.setDestination(widget.point);
    BlocProvider.master<NavigationBloc>().changeTab(NavigationCategory.route);
  }

  void _locateStop() {
    NavigationBloc navigationBloc = BlocProvider.of<NavigationBloc>(context);
    MapBloc mapBloc = BlocProvider.of<MapBloc>(context);
    navigationBloc.changeTab(NavigationCategory.scheduleDetailsAll);
    mapBloc.setBottomSheetContent(BottomSheetCategory.schedule, arguments: widget.point);
    mapBloc.moveTo(MapPositionAndZoom.withLatLng(Utils.coordinatesFromArray(widget.point.geometry.coordinates), 17), pointId: widget.point.properties.id);
    if (!widget.point.properties.id.contains("MCO")) mapBloc.selectPOI(widget.point.properties.id);
  }

  String getLineInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String idLine = BlocProvider.master<LinesBloc>().lines.singleWhere((readyLine) => readyLine.line.id == lineId).line.id;
    return idLine;
  }
}

extension CapExtension on String {
  String get inCaps => this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';

  String get allInCaps => this.toUpperCase();

  String get capitalizeFirstofEach => this.replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" ");
}
