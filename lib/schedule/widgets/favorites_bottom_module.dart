import 'dart:async';
import 'dart:io';
import 'package:device_apps/device_apps.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/disturbances/pages/disturbances_filters.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/pages/schedule_line_details.dart';
import 'package:metro/schedule/pages/schedule_timetable.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';

class FavoritesBottomModule extends StatefulWidget {
  final Favorite favorite;
  final VoidCallback onTapRemoveFav;
  final VoidCallback onTapAddFav;

  const FavoritesBottomModule(
      {Key key, this.favorite, this.onTapRemoveFav, this.onTapAddFav})
      : super(key: key);

  @override
  _FavoritesBottomModuleState createState() => _FavoritesBottomModuleState();
}

class _FavoritesBottomModuleState extends State<FavoritesBottomModule> {
  dynamic data;
  Future<bool> isFavorite;
  bool hasBeenTaped;
  bool isInstalled;
  String _itemText;
  String linkStore = '';

  @override
  void initState() {
    super.initState();
    getApps();
  }

  @override
  Widget build(BuildContext context) {
    Future<List<FormattedScheduleModel>> testListFormated = BlocProvider.of<ScheduleBloc>(context)
        .loadClusters((widget.favorite.point1.properties as ClustersProperties).code, null, BlocProvider.master<LinesBloc>().lines);
    int nsv;
    String asset;
    List<FormattedScheduleModel> newList = [];

    ReadyLine readyLine;

    return FutureBuilder<List<FormattedScheduleModel>>(
      future: testListFormated,
      builder: (context, snapshot) {
        if (snapshot.data == null || !snapshot.hasData)
          return Container(
            child: AnimatedLoadingLogo(
              height: 60,
            ),
          );
        List<FormattedScheduleModel> snap = snapshot.data;
        for (var x in snap) {
          if (x.code == widget.favorite.lines[0]) {
            newList.add(x);
            String idLine = getLineInFunctionOfSchedule(x).replaceAll(':', '_');
            nsv = BlocProvider.master<NsvBloc>().getNsvId(idLine);

            asset = nsv == 4
                ? globalElevations.perturbation_1
                : (nsv == 2 || nsv == 3)
                ? globalElevations.perturbation_2
                : '';

            if (nsv == 2) {
              _itemText = 'Service légèrement perturbé';
            } else if (nsv == 3) {
              _itemText = 'Service très perturbé';
            } else if (nsv == 4) {
              _itemText = 'Hors service';
            }
          }
        }

        if (newList.length != 0) {
          readyLine = BlocProvider.master<LinesBloc>().lines.where((line) => line.line.shortName == newList[0].code).first;
        }

        return Wrap(
          children: [
            Container(
              color: globalElevations.e24dp.backgroundColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(14.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 66,
                          width: 66,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: LineDisplay(
                              line: readyLine.line,
                              isBottomSheet: true,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 106,
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text(
                                      'Arrêt ' +
                                          widget.favorite.point1.properties.name
                                              .toLowerCase()
                                              .replaceAll(RegExp(' +'), ' ')
                                              .split(" ")
                                              .map((str) => str.inCaps)
                                              .join(" "),
                                      maxLines: null,
                                      style: TextStyle(
                                          fontSize: 20,
                                          color: globalElevations
                                              .fstTextColor.backgroundColor),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                widget.favorite.point1.properties.city,
                                maxLines: null,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: globalElevations
                                        .fstTextColor.backgroundColor),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      (nsv == 2 || nsv == 3 || nsv == 4)
                          ? ListTile(
                              leading: SvgPicture.asset(
                                asset,
                                width: 30,
                              ),
                              title: Text(
                                _itemText,
                                style: TextStyle(
                                    color: globalElevations
                                        .perturbationRedColor.backgroundColor),
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DisturbancesFilters(
                                      lineText: readyLine.line.shortName,
                                    ),
                                  ),
                                );
                              })
                          : Container(),
                      Visibility(
                        visible: readyLine.line.type == "FLEXO",
                        child: Material(
                          color: globalElevations.e24dp.backgroundColor,
                          child: InkWell(
                            splashColor: Colors.grey,
                            child: ListTile(
                              leading: Icon(Icons.local_phone_outlined),
                              title: Text("Réserver"),
                              onTap: () {
                                Navigator.pop(context);
                                _launchCaller();
                              },
                            ),
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.schedule),
                            title: Text("Fiche horaire"),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (ctx) => ScheduleTimetablePage(
                                    line: readyLine.line,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.credit_card_rounded),
                            title: Text("Ticket"),
                            onTap: () {
                              Navigator.pop(context);
                              (isInstalled != null && isInstalled == true)
                                  ? _openUrl("tagandpasssemitag://home")
                                  : _openUrl(linkStore);
                            },
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: RotatedBox(
                                quarterTurns: 1,
                                child: Icon(Icons.linear_scale)),
                            title: Text("Détail de la ligne"),
                            onTap: () {
                              if (readyLine.line.shortName == 'GRES') {
                                _openUrl(
                                    "http://www.lignesplus-m.fr/lignes-mcovoit-gresivaudan/");
                              } else if (readyLine.line.shortName == 'VOIR') {
                                _openUrl(
                                    "http://www.lignesplus-m.fr/lignes-mcovoit-voironnais/");
                              } else {
                                Navigator.pop(context);
                                BlocProvider.of<ScheduleBloc>(context)
                                    .loadLineDetails(readyLine);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (ctx) => ScheduleLineDetailsPage(
                                      isBottomsheet: true,
                                      paddingTop: 12.0,
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.location_pin),
                            title: Text("Localisation de l'arrêt"),
                            onTap: () {
                              Navigator.pop(context);
                              _locateStop();
                            },
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.near_me_outlined),
                            title: Text('Itinéraire vers ce point'),
                            onTap: () {
                              Navigator.pop(context);
                              _goToRoute();
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: 0.6,
                        color: Colors.grey,
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.add),
                            title: Text('Nouvelle ligne favorite à cet arrêt'),
                            onTap: () {
                              Navigator.pop(context);
                              widget.onTapAddFav();
                            },
                          ),
                        ),
                      ),
                      Material(
                        color: globalElevations.e24dp.backgroundColor,
                        child: InkWell(
                          splashColor: Colors.grey,
                          child: ListTile(
                            leading: Icon(Icons.delete_outline_outlined),
                            title: Text('Retirer ce favoris'),
                            onTap: () {
                              Navigator.pop(context);
                              widget.onTapRemoveFav();
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  _launchCaller() async {
    const url = "tel:0438703870";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _goToRoute() {
    RoutesBloc routesBloc = BlocProvider.master<RoutesBloc>();
    routesBloc.setDeparture(null);
    routesBloc.setDestination(widget.favorite.point1);
    BlocProvider.master<NavigationBloc>().changeTab(NavigationCategory.route);
  }

  void _locateStop() {
    NavigationBloc navigationBloc = BlocProvider.of<NavigationBloc>(context);
    MapBloc mapBloc = BlocProvider.of<MapBloc>(context);
    navigationBloc.changeTab(NavigationCategory.scheduleDetailsAll);
    mapBloc.setBottomSheetContent(BottomSheetCategory.schedule,
        arguments: widget.favorite.point1);
    mapBloc.moveTo(
        MapPositionAndZoom.withLatLng(
            Utils.coordinatesFromArray(
                widget.favorite.point1.geometry.coordinates),
            17),
        pointId: widget.favorite.point1.properties.id);
    if (!widget.favorite.point1.properties.id.contains("MCO"))
      mapBloc.selectPOI(widget.favorite.point1.properties.id);
  }

  Future<void> getApps() async {
    if (Platform.isAndroid) {
      // print(await AppAvailability.checkAvailability("com.android.chromeette"));
      // Returns: Map<String, String>{app_name: Chrome, package_name: com.android.chrome, versionCode: null, version_name: 55.0.2883.91}
      isInstalled = await DeviceApps.isAppInstalled('fr.semitag.tagandpass');
      linkStore =
          "https://play.google.com/store/apps/details?id=fr.semitag.tagandpass&hl=fr&gl=US";
      // Returns: true
    } else if (Platform.isIOS) {
      // iOS doesn't allow to get installed apps.
      isInstalled = false;
      linkStore =
          "https://apps.apple.com/fr/app/passmobilit%C3%A9s/id1031595009";
      // Returns: Map<String, String>{app_name: , package_name: calshow://, versionCode: , version_name: }
    } else {
      isInstalled = false;
      linkStore =
          "https://apps.apple.com/fr/app/passmobilit%C3%A9s/id1031595009";
    }
  }

  _openUrl(String url) {
    launch(url);
  }

  String getLineInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String idLine = BlocProvider.master<LinesBloc>()
        .lines
        .singleWhere((readyLine) => readyLine.line.id == lineId)
        .line
        .id;
    return idLine;
  }
}

extension CapExtension on String {
  String get inCaps =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';

  String get allInCaps => this.toUpperCase();

  String get capitalizeFirstofEach => this
      .replaceAll(RegExp(' +'), ' ')
      .split(" ")
      .map((str) => str.inCaps)
      .join(" ");
}
