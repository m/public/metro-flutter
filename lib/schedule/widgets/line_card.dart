import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:url_launcher/url_launcher.dart';

class LineCard extends StatelessWidget {
  final String type;
  final Map<dynamic, List<ReadyLine>> line;
  final Function(ReadyLine line) onLineTap;
  final List<ReadyLine> lineNavette;

  const LineCard({Key key, this.type, this.line, this.onLineTap, this.lineNavette})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String cat = type[0] + type.substring(1).toLowerCase();
    String imgLink = '';
    String schedule = '';
    if (type == 'TRAM') {
      imgLink = 'assets/vehicules/Tram.png';
      schedule = '4h30 à 1h';
    } else if (type == 'NAVETTE') {
      cat = 'Navette Relais';
      schedule = '4h30 à 1h';
      imgLink = 'assets/vehicules/Chronos.png';
    } else if (type == 'CHRONO') {
      schedule = '5h à 1h';
      imgLink = 'assets/vehicules/Chronos.png';
    } else if (type == 'PROXIMO') {
      imgLink = 'assets/vehicules/Proximos.png';
      schedule = '5h à 21h';
    } else if (type == 'FLEXO') {
      imgLink = 'assets/vehicules/Flexos.png';
      schedule = '6h30 à 20h';
    } else if (type == 'C38') {
      cat = 'Transisère';
      imgLink = 'assets/vehicules/transisere_car.png';
    } else if (type == 'GRESIVAUDAN') {
      cat = 'TouGo';
      imgLink = 'assets/vehicules/TouGo.png';
    } else if (type == 'VOIRONAIS') {
      cat = 'Pays Voironnais';
      imgLink = 'assets/vehicules/Voironnais.png';
    } else if (type == 'MCO') {
      cat = "M'Covoit-Lignes+";
      imgLink = 'assets/vehicules/m_covoit_logo.png';
    }


    return Column(
      children: <Widget>[
        Card(
          color: globalElevations.e02dp.backgroundColor,
          margin: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
          semanticContainer: false,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8),),
          elevation: 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(top: 8, bottom: 8, left: 8),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                cat,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                schedule,
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: type == 'MCO' ? 36 : 28,
                    padding: const EdgeInsets.only(right: 8),
                    child: Image.asset(imgLink),
                  ),
                ],
              ),
              if (type == 'MCO')
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 0, 16, 8),
                  child: Text(
                      "Covoiturez avec M'Covoit - Lignes+ pour vos trajets quotidiens sur le Voironnais, le Grésivaudan et la métropole Grenobloise"),
                ),
              Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12),
                    child: Wrap(
                      spacing: 12,
                      runSpacing: 16,
                      children: (line[type]).map((line) => LineDisplay(
                        size: 40,
                        line: line.line,
                        onTap: () => onLineTap(line),
                      ))
                          .toList(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}
