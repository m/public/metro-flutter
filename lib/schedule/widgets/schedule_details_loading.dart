import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/list_item.dart';

class ScheduleDetailsLoading extends StatelessWidget {
  final String linesFavorite;
  final Favorite favorite;

  ScheduleDetailsLoading(
      {@required this.linesFavorite, @required this.favorite});

  Widget _buildBody() {
    return Container(
      child: Column(
        children: <Widget>[
          ListItem.normal(
            iconData: favorite.point1.properties.iconData(),
            text1: favorite.point1.properties.name,
          ),
        ],
      ),
    );
  }

  Widget _buildLineCode() {
    return Column(
      children: <Widget>[
        Container(
          width: 55,
          child: Center(
              child: Text(
            linesFavorite,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: globalElevations.fstTextColor.backgroundColor,
            ),
          )),
        ),
      ],
    );
  }

  Widget _buildLoadingIndicator(BuildContext context) {
    return SizedBox(
      height: 5,
      child: LinearProgressIndicator(
        backgroundColor: Colors.white,
        valueColor:
            AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                _buildLineCode(),
                Expanded(
                  child: Container(
                    color: globalElevations.e08dp.backgroundColor,
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 10.0, right: 10.0),
                          ),
                          _buildBody(),
                        ]),
                  ),
                ),
              ],
            ),
          ),
          _buildLoadingIndicator(context),
        ],
      ),
    );
  }
}
