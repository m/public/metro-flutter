import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/widgets/animated_realtime.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/schedule/blocs/schedule.dart';

/// Widget which display schedules for a cluster.
///
/// [point] must be not null.
/// [code] used to call API for schedules.
/// [routeName] used with [code] to call API for schedules for a precise route.
class ScheduleOfPassage extends StatelessWidget {
  final Point point;
  final String code;
  final String routeName;

  // Used to display or not the detail bloc.
  String name;

  ScheduleOfPassage({this.point, this.code, this.routeName});

  @override
  Widget build(BuildContext context) {
    ScheduleBloc bloc = BlocProvider.of<ScheduleBloc>(context);

    return StreamBuilder<List<FormattedScheduleModel>>(
      stream: bloc.onSchedulesChanged,
      builder: (context, snapshot) {
        if (!snapshot.hasData) return _buildLoadingView(context);

        return Container(
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                height: 0.3,
                color: Colors.grey,
              ),
            ),
            _buildBody(bloc, snapshot.data)
          ]),
        );
      },
    );
  }

  /// Build next arrival bloc.
  List<Widget> _buildNextArrivalWidget(String nextArrival, bool textIsBold) {
    List<Widget> list = List();
    if (nextArrival != null && nextArrival.isNotEmpty) {
      if (nextArrival.contains(':')) {
        list.add(Center(
          child: textIsBold
              ? Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold))
              : Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        ));
      } else {
        list.add(textIsBold
            ? Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
            : Text(nextArrival, style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.bold, fontSize: 20)));
        list.add(Text(
            nextArrival.contains('>')
                ? 'h'
                : nextArrival.contains(':')
                ? ''
                : 'min',
            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 11)));
      }
    } else {
      list.add(Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Container(height: 22, width: 3, color: globalElevations.sndTextColor.backgroundColor,),
      ));
    }
    return list;
  }

  /// Build the body bloc which display schedules ListView from [schedules].
  ///
  /// [bloc] must be not null.
  Widget _buildBody(ScheduleBloc bloc, List<FormattedScheduleModel> schedules) {
    if (schedules.length == 0)
      return Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: globalElevations.e08dp.backgroundColor,
                border: Border(
                  left: BorderSide(color: Colors.black12),
                  right: BorderSide(color: Colors.black12),
                  bottom: BorderSide(color: Colors.black12),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Center(
                    child: Text(translations
                        .text('schedule_of_passage.data_not_available'))),
              ),
            ),
          ],
        ),
      );

    return Container(
      child: Column(
        children: schedules.map((schedule) {
          return Column(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: Container(
                  child: Row(
                    children: <Widget>[
                      ..._buildLineContent(schedule),
                    ],
                  ),
                ),
                onTap: () {
                  name = name != schedule.name ? name = schedule.name : '';
                  name.isNotEmpty
                      ? bloc.setDetailVisibility(true)
                      : bloc.setDetailVisibility(false);
                },
              ),
            ],
          );
        }).toList(),
      ),
    );
  }

  /// Build the line display widget that contain background color and a text.
  Widget _buildLineDisplay(
      String code, Color backgroundColor, Color textColor) {
    // If the text is too long, we need to rotate it to fit
    bool isVertically = code.length > 2 ? true : false;

    return Container(
      width: 40,
      height: 75,
      color: backgroundColor,
      child: Center(
        child: RotatedBox(
          quarterTurns: isVertically ? 3 : 0, //3 = rotate to 90 degree
          child: Text(
            code,
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: textColor),
          ),
        ),
      ),
    );
  }

  /// Build the line of ListView from [schedule].
  List<Widget> _buildLineContent(FormattedScheduleModel schedule) {
    List<Widget> widgetsFirstNextArrival =
    _buildNextArrivalWidget(schedule.firstNextArrival, true);
    List<Widget> widgetsSecondNextArrival =
    _buildNextArrivalWidget(schedule.secondNextArrival, false);

    return <Widget>[
      Expanded(
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 14.0),
            child: Text(
              schedule.name,
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
          ),
        ),
      ),
      Padding(
          padding: const EdgeInsets.only(bottom: 12.0),
          child: Transform.rotate(
            angle: -12.0,
            child:
            schedule.realtime ? AnimatedRealtime(height: 15) : EmptyCell(),
          )),
      Container(
        height: 50,
        width: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: widgetsFirstNextArrival,
        ),
      ),
      Center(
        child: Container(
          width: 0.5,
          height: 40,
          color: Colors.grey,
        ),
      ),
      Container(
        width: 83,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0, left: 4),
              child: Transform.rotate(
                angle: -12.0,
                child: schedule.realtime && schedule.secondNextArrival.isNotEmpty
                    ? AnimatedRealtime(height: 15)
                    : EmptyCell(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 14.0),
              child: Container(
                height: 50,
                width: 50,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: widgetsSecondNextArrival,
                ),
              ),
            )
          ],
        ),
      ),
    ];
  }

  /// Build the loading view.
  Widget _buildLoadingView(BuildContext context) {
    return SizedBox(
      height: 5,
      child: LinearProgressIndicator(
        backgroundColor: Colors.white,
        valueColor:
        AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
      ),
    );
  }
}
