import 'dart:async';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/widgets/animated_realtime.dart';
import 'package:metro/global/widgets/new_expandable_panel.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/pages/schedule_timetable.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

/// Widget which display schedules for a cluster.
///
/// [code] used to call API for schedules.
/// [routeName] used with [code] to call API for schedules for a precise route.
class ScheduleDetailsOfPassage extends StatefulWidget {
  final String code;
  final String routeName;
  final bool isBottomSheet;

  ScheduleDetailsOfPassage({Widget child, this.code, this.routeName, this.isBottomSheet = true});

  @override
  _ScheduleDetailsOfPassageState createState() => _ScheduleDetailsOfPassageState();
}

class _ScheduleDetailsOfPassageState extends State<ScheduleDetailsOfPassage> {
  BuildContext ctx;

  List<ExpandableController> _controllers = List();

  StreamSubscription _onSelectedScheduleChanged;

  String name;
  Timer _timer;
  ScheduleBloc bloc;
  final String lessThanOneMin = "<1";
  final String moreThanOneHour = ">1";

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ScheduleBloc>(context);
    bloc.searchCluster(widget.code, widget.routeName, BlocProvider.of<LinesBloc>(context).lines);
    _timer = new Timer.periodic(new Duration(seconds: 20), (timer) {
      if (widget.isBottomSheet) {
        bloc.detailsSearchCluster(widget.code, widget.routeName, BlocProvider.of<LinesBloc>(context).lines);
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // To close the last selected controller whenever an other one is tapped
    _onSelectedScheduleChanged = BlocProvider.of<ScheduleBloc>(context).onSelectedScheduleChanged.listen((data) {
      if (data != null && _controllers != null && _controllers.length > 0) {
        var selectedScheduleController = _controllers[data];
        int indexLastSelected = BlocProvider.of<ScheduleBloc>(context).lastSelectedSchedule;
        var lastSelectedScheduleController = indexLastSelected == null ? null : _controllers[indexLastSelected];
        if (selectedScheduleController != null) {
          if (lastSelectedScheduleController != null) {
            if (selectedScheduleController == lastSelectedScheduleController)
              selectedScheduleController.expanded = true;
            else
              lastSelectedScheduleController.expanded = false;
          }
        }
      }
    });

    ScheduleBloc bloc = BlocProvider.of<ScheduleBloc>(context);
    if (widget.isBottomSheet) bloc.searchCluster(widget.code, widget.routeName, BlocProvider.of<LinesBloc>(context).lines);
    ctx = context;

    return StreamBuilder<List<FormattedScheduleModel>>(
      stream: bloc.onSchedulesChanged,
      builder: (context, snapshot) {
        if (!snapshot.hasData) return _buildLoadingView(context);

        return Container(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Padding(padding: const EdgeInsets.symmetric(horizontal: 10)), _buildBody(bloc, snapshot.data)]),
        );
      },
    );
  }

  /// Build next arrival bloc.
  List<Widget> _buildNextArrivalWidget(String nextArrival, bool textIsBold) {
    List<Widget> list = List();
    if (nextArrival != null && nextArrival.isNotEmpty) {
      if (nextArrival.contains(':')) {
        list.add(Center(
          child: textIsBold
              ? Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold))
              : Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold)),
        ));
      } else {
        list.add(textIsBold
            ? Text(nextArrival, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
            : Text(nextArrival, style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.bold, fontSize: 20)));
        list.add(Text(
            nextArrival.contains('>')
                ? 'h'
                : nextArrival.contains(':')
                ? ''
                : 'min',
            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 11)));
      }
    } else {
      list.add(Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Container(
          height: 22,
          width: 3,
          color: globalElevations.sndTextColor.backgroundColor,
        ),
      ));
    }
    return list;
  }

  /// Build the body bloc which display schedules ListView from [schedules].
  ///
  /// [bloc] must be not null.
  Widget _buildBody(ScheduleBloc bloc, List<FormattedScheduleModel> schedules) {
    if (schedules.length == 0)
      return Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: globalElevations.e08dp.backgroundColor,
                border: Border(
                  left: BorderSide(color: Colors.black12),
                  right: BorderSide(color: Colors.black12),
                  bottom: BorderSide(color: Colors.black12),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Center(child: Text(translations.text('schedule_of_passage.data_not_available'))),
              ),
            ),
          ],
        ),
      );

    List<FormattedScheduleModel> schedulesFiltered = schedules.length > 1 ? getSchedulesFiltered(schedules) : schedules;

    return Container(
      child: Column(
        children: schedulesFiltered.map((schedule) {
          ExpandableController controller = ExpandableController();
          _controllers.add(controller);
          return Column(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(6),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 5.0, 0.0, 5.0),
                    child: ExpandableNotifier(
                      controller: controller,
                      child: NewExpandablePanel(
                        showArrow: false,
                        controller: controller,
                        header: Container(
                          color: widget.isBottomSheet ? globalElevations.e08dp.backgroundColor : globalElevations.e16dp.backgroundColor,
                          child: Row(
                            children: <Widget>[..._buildLineContent(schedule)],
                          ),
                        ),
                        details: Container(
                          color: globalElevations.e24dp.backgroundColor,
                          child: Row(
                            children: [
                              Container(
                                color: globalElevations.e24dp.backgroundColor,
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10.0),
                                      child: Icon(
                                        Icons.launch,
                                        color: Theme.of(context).indicatorColor,
                                        size: 22,
                                      ),
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        var splitCode = schedule.id.split(':');
                                        String lineId = splitCode[0] + ':' + splitCode[1];

                                        Navigator.push(
                                          ctx,
                                          MaterialPageRoute(
                                              builder: (ctx) => ScheduleTimetablePage(
                                                line: BlocProvider.master<LinesBloc>()
                                                    .lines
                                                    .singleWhere((readyLine) => readyLine.line.id == lineId)
                                                    .line,
                                              )),
                                        );
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(12, 16, 8, 16),
                                        child: Text(
                                          translations.text('schedule_detail.time_sheet'),
                                          textAlign: TextAlign.start,
                                          maxLines: 1,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                            color: Theme.of(context).indicatorColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                color: globalElevations.e24dp.backgroundColor,
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10.0),
                                      child: Icon(
                                        Icons.credit_card,
                                        color: Theme.of(context).indicatorColor,
                                        size: 22,
                                      ),
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.opaque,
                                      onTap: () {
                                        _openUrl("tagandpasssemitag://home");
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(12, 16, 16, 16),
                                        child: Text(
                                          translations.text('schedule_detail.ticket'),
                                          textAlign: TextAlign.start,
                                          maxLines: 1,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15,
                                            color: Theme.of(context).indicatorColor,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        onOpenOrClose: (expanded) {
                          if (expanded) {
                            ScheduleBloc scheduleBloc = BlocProvider.master<ScheduleBloc>();
                            scheduleBloc.onTappedSchedule(schedules.indexOf(schedule));
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        }).toList(),
      ),
    );
  }

  _openUrl(String url) {
    launch(url);
  }

  /// Build the line display widget that contain background color and a text.
  Widget _buildLineDisplay(String code, Color backgroundColor, Color textColor) {
    // If the text is too long, we need to rotate it to fit
    bool isVertically = code.length > 2 ? true : false;

    return Container(
      width: 40,
      height: 75,
      color: backgroundColor,
      child: Center(
        child: RotatedBox(
          quarterTurns: isVertically ? 3 : 0, //3 = rotate to 90 degree
          child: Text(
            code,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: textColor),
          ),
        ),
      ),
    );
  }

  /// Build the line of ListView from [schedule].
  List<Widget> _buildLineContent(FormattedScheduleModel schedule) {
    String idLine = getLineInFunctionOfSchedule(schedule).replaceAll(':', '_');
    int nsv = BlocProvider.master<NsvBloc>().getNsvId(idLine);
    String asset = nsv == 4
        ? globalElevations.perturbation_1
        : (nsv == 2 || nsv == 3)
        ? globalElevations.perturbation_2
        : '';
    List<Widget> widgetsFirstNextArrival = _buildNextArrivalWidget(schedule.firstNextArrival, true);
    List<Widget> widgetsSecondNextArrival = _buildNextArrivalWidget(schedule.secondNextArrival, false);

    return <Widget>[
      Container(
        color: nsv != 4 ? schedule.color : schedule.color.withOpacity(0.4),
        width: 60,
        height: 68,
        child: Stack(
          children: [
            Center(
              child: Text(
                schedule.code,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: nsv != 4 ? schedule.textColor : schedule.textColor.withOpacity(0.4),
                ),
              ),
            ),
            Visibility(
              visible: (nsv == 2 || nsv == 3 || nsv == 4),
              child: Positioned(
                bottom: 44,
                left: 38,
                child: SizedBox(
                  height: 17,
                  width: 17,
                  child: SvgPicture.asset(
                    asset,
                    height: 17,
                    semanticsLabel: 'icon',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      Expanded(
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: Text(
              "${schedule.name[0].toUpperCase()}${schedule.name.substring(1).toLowerCase()}",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ),
      nsv == 4
          ? Padding(
        padding: const EdgeInsets.only(right: 8.0, top: 10, bottom: 10),
        child: Container(
          decoration: BoxDecoration(
            color: globalElevations.e12dp.backgroundColor,
            borderRadius: BorderRadius.circular(12),
          ),
          padding: EdgeInsets.all(8.0),
          child: Text(
            "Hors service",
            style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
      )
          : Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 12.0),
            child: Transform.rotate(
              angle: -11.8,
              child: schedule.realtime && schedule.firstNextArrival.isNotEmpty
                  ? AnimatedRealtime(
                height: 10,
              )
                  : SizedBox(
                width: 8,
                height: 8,
              ),
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: widgetsFirstNextArrival,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0, right: 8),
            child: Center(
              child: Container(
                width: 0.5,
                height: 30,
                color: Colors.grey,
              ),
            ),
          ),
          Container(
            width: schedule.secondNextArrival.contains(':') ? 50 : 46,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 12.0),
                  child: Transform.rotate(
                    angle: -11.8,
                    child: schedule.realtime && schedule.secondNextArrival.isNotEmpty
                        ? AnimatedRealtime(
                      height: 10,
                    )
                        : SizedBox(
                      width: schedule.secondNextArrival.contains(':') ? 0 : 8,
                      height: schedule.secondNextArrival.contains(':') ? 0 : 8,
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: widgetsSecondNextArrival,
                ),
              ],
            ),
          ),
        ],
      ),
    ];
  }

  /// Build the loading view.
  Widget _buildLoadingView(BuildContext context) {
    return SizedBox(
      height: 5,
      child: LinearProgressIndicator(
        backgroundColor: Colors.white,
        valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
      ),
    );
  }

  String getLineTypeInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String type = BlocProvider.master<LinesBloc>().lines.singleWhere((readyLine) => readyLine.line.id == lineId).line.type;

    return type;
  }

  String getLineInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String idLine = BlocProvider.master<LinesBloc>().lines.singleWhere((readyLine) => readyLine.line.id == lineId).line.id;
    return idLine;
  }

  List<FormattedScheduleModel> getSchedulesFiltered(List<FormattedScheduleModel> schedules) {
    // Sort schedules by line type
    schedules.sort((a, b) {
      return a.id.toLowerCase().compareTo(b.id.toLowerCase());
    });

    List<FormattedScheduleModel> schedulesFiltered = new List<FormattedScheduleModel>();

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "TRAM") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "CHRONO") {
        schedulesFiltered.add(schedule);
      }
    }
    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "PROXIMO") {
        schedulesFiltered.add(schedule);
      }
    }
    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "FLEXO") {
        schedulesFiltered.add(schedule);
      }
    }
    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "C38") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "Structurantes") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "Secondaires") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "Urbaines") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "Interurbaines") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "SCOL") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "TAD") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "SNC") {
        schedulesFiltered.add(schedule);
      }
    }

    for (FormattedScheduleModel schedule in schedules) {
      if (getLineTypeInFunctionOfSchedule(schedule) == "MCO") {
        schedulesFiltered.add(schedule);
      }
    }

    // Sort by next passages
    schedulesFiltered.sort((a, b) {
      String idA = a.id.split(":")[1];
      String idB = b.id.split(":")[1];

      if (idA == idB) {
        String timeA = a.firstNextArrival.toString();
        String timeB = b.firstNextArrival.toString();
        int timeAint;
        int timeBint;

        if (timeA.contains(':') || timeA.contains('>')) {
          timeAint = 60;
        } else if (timeA == lessThanOneMin) {
          timeAint = 0;
        } else {
          timeAint = int.parse(timeA);
        }

        if (timeB.contains(':') || timeB.contains('>')) {
          timeBint = 60;
        } else if (timeB == lessThanOneMin) {
          timeBint = 0;
        } else {
          timeBint = int.parse(timeB);
        }

        return timeAint.compareTo(timeBint);
      } else {
        return 0;
      }
    });
    return schedulesFiltered;
  }
}
