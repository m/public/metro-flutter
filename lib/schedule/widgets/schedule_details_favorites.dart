import 'dart:async';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/animated_realtime.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/schedule/widgets/schedule_details_loading.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/global/utils.dart';

/// Widget which display schedules for a cluster.
///
/// [code] used to call API for schedules.
/// [routeName] used with [code] to call API for schedules for a precise route.
class ScheduleDetailsOfPassageFavorites extends StatefulWidget {
  final String code;
  final String routeName;
  final bool isBottomSheet;
  final List<String> linesFavorites;
  final Favorite favorite;
  final ExpandableController controller;

  List<FormattedScheduleModel> loadedData;

  ScheduleDetailsOfPassageFavorites(
      {Widget child, this.code, this.routeName, this.isBottomSheet = true, this.linesFavorites, this.favorite, this.controller});

  @override
  _ScheduleDetailsOfPassageFavoritesState createState() => _ScheduleDetailsOfPassageFavoritesState();
}

extension CapExtension on String {
  String get inCaps => this.length > 0 ?'${this[0].toUpperCase()}${this.substring(1)}':'';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" ");
}

class _ScheduleDetailsOfPassageFavoritesState extends State<ScheduleDetailsOfPassageFavorites> {
  List<ExpandableController> _controllers = List();

  StreamSubscription _onSelectedScheduleChanged;

  String name;

  Timer _timer;
  int testInt = 0;
  bool testBool;
  bool isCompleted = false;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 40), (timer) {
      BlocProvider.of<ScheduleBloc>(context).updateFavClusters(
        (widget.favorite.point1.properties as ClustersProperties).code,
        null,
        BlocProvider.master<LinesBloc>().lines,
      );
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    // To close the last selected controller whenever an other one is tapped
    _onSelectedScheduleChanged = BlocProvider.of<ScheduleBloc>(context).onSelectedScheduleChanged.listen((data) {
      if (data != null && _controllers != null && _controllers.length > 0) {
        var selectedScheduleController = _controllers[data];
        int indexLastSelected = BlocProvider.of<ScheduleBloc>(context).lastSelectedSchedule;
        var lastSelectedScheduleController = indexLastSelected == null ? null : _controllers[indexLastSelected];
        if (selectedScheduleController != null) {
          if (lastSelectedScheduleController != null) {
            if (selectedScheduleController == lastSelectedScheduleController)
              selectedScheduleController.expanded = true;
            else
              lastSelectedScheduleController.expanded = false;
          }
        }
      }
    });

    if (widget.loadedData == null) {
      return StreamBuilder<List<FormattedScheduleModel>>(
        stream: BlocProvider.of<ScheduleBloc>(context).onFavoriteScheduleChanged,
        builder: (context, favSnapshot) {
          return StreamBuilder<bool>(
            stream: BlocProvider.master<LinesBloc>().onIsLinesDownloaded,
            builder: (context, snapshot) {
              if (snapshot.data == null || snapshot.data == false)
                return SizedBox(
                  height: 5,
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
                  ),
                );

              Future<List<FormattedScheduleModel>> testListFormated = BlocProvider.of<ScheduleBloc>(context)
                  .loadClusters((widget.favorite.point1.properties as ClustersProperties).code, null, BlocProvider.master<LinesBloc>().lines);

              return StreamBuilder<List<FormattedScheduleModel>>(
                stream: BlocProvider.master<ScheduleBloc>().onFavoriteSchedulesChanged,
                builder: (context, snapshotSchedules) {
                  if (snapshotSchedules.data == null || snapshotSchedules.data.isEmpty)
                    return SizedBox(
                      height: 5,
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).indicatorColor),
                      ),
                    );

                  return FutureBuilder<List<FormattedScheduleModel>>(
                    future: testListFormated,
                    builder: (context, snapshot) {
                      if (snapshot.data == null || snapshot.data.isEmpty || !snapshot.hasData)
                        return ScheduleDetailsLoading(linesFavorite: widget.linesFavorites.first, favorite: widget.favorite);

                      widget.loadedData = snapshot.data;
                      return _buildView(snapshot.data, context);
                    },
                  );
                },
              );
            },
          );
        },
      );
    } else {
      return StreamBuilder(
        stream: BlocProvider.of<ScheduleBloc>(context).onFavoriteScheduleChanged,
        builder: (context, snapshot) {
          return _buildView(widget.loadedData, context);
        },
      );
    }
  }

  String getLineInFunctionOfSchedule(FormattedScheduleModel schedule) {
    List<String> splitCode = schedule.id.split(':');
    String lineId = splitCode[0] + ':' + splitCode[1];
    String idLine = BlocProvider.master<LinesBloc>().lines.singleWhere((readyLine) => readyLine.line.id == lineId).line.id;
    return idLine;
  }

  Widget _buildView(List<FormattedScheduleModel> data, BuildContext context) {
    ScheduleBloc bloc = BlocProvider.of<ScheduleBloc>(context);
    int nsv = 0;
    String asset = '';
    List<FormattedScheduleModel> newList = [];
    for (var x in data) {
      if (x.code == widget.linesFavorites[0]) {
        newList.add(x);
        String idLine = getLineInFunctionOfSchedule(x).replaceAll(':', '_');
        nsv = BlocProvider.master<NsvBloc>().getNsvId(idLine);
        asset = nsv == 4
            ? globalElevations.perturbation_1
            : (nsv == 2 || nsv == 3)
                ? globalElevations.perturbation_2
                : '';
      }
    }
    ReadyLine lines;
    if (newList.length != 0) {
      lines = BlocProvider.master<LinesBloc>().lines.where((line) => line.line.shortName == newList[0].code).first;
    }

    return Column(
      children: <Widget>[
        Container(
          color: globalElevations.e00dp.backgroundColor,
          child: Row(
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    width: 66,
                    height: 50,
                    decoration: BoxDecoration(
                      color: newList.length != 0 ? Utils.colorFromHex(lines.line.color) : Colors.grey,
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(6.0),
                        topLeft: Radius.circular(6.0),
                        // bottomLeft: Radius.circular(6.0),
                        // topRight: Radius.circular(6.0),
                      ),
                    ),
                    child: Center(
                        child: Text(
                      widget.linesFavorites[0],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: newList.length != 0 ? newList[0].textColor : Colors.white,
                      ),
                    )),
                  ),
                  Visibility(
                    visible: (nsv == 2 || nsv == 3 || nsv == 4),
                    child: Positioned(
                      bottom: 30,
                      left: 46,
                      child: SizedBox(
                        height: 17,
                        width: 17,
                        child: SvgPicture.asset(
                          asset,
                          height: 17,
                          semanticsLabel: 'icon',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 14,
              ),
              Expanded(
                child: Text(
                  widget.favorite.point1.properties.name.toLowerCase().replaceAll(RegExp(' +'), ' ').split(" ").map((str) => str.inCaps).join(" "),
                  style: TextStyle(fontSize: 18),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: Icon(
                  widget.controller.expanded ? Icons.unfold_less : Icons.unfold_more,
                  color: Colors.grey,
                ),
              )
            ],
          ),
        ),
        Container(
          color: globalElevations.e08dp.backgroundColor,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            ),
            _buildHeader(bloc, newList)
          ]),
        ),
      ],
    );
  }


  /// Build next arrival bloc.
  List<Widget> _buildNextArrivalWidget(String nextArrival, bool textIsBold) {
    List<Widget> list = List();
    if (nextArrival.isNotEmpty) {
      String t = nextArrival.contains('>')
          ? 'h'
          : nextArrival.contains(':')
              ? ''
              : ' min';

      list.add(textIsBold
          ? Text(nextArrival + t, textAlign: TextAlign.end, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))
          : Row(
              children: <Widget>[
                Text(nextArrival,
                    textAlign: TextAlign.end,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: globalElevations.sndTextColor.backgroundColor)),
                Text(t, textAlign: TextAlign.end, style: TextStyle(fontSize: 16, color: globalElevations.sndTextColor.backgroundColor))
              ],
            ));
    } else {
      list.add(Center(
          child: Padding(
        padding: const EdgeInsets.only(left: 18.0, right: 10),
        child: Text(' N/A ', style: TextStyle(fontSize: 16, color: globalElevations.sndTextColor.backgroundColor)),
      )));
    }
    return list;
  }

  /// Build the body bloc which display schedules ListView from [schedules].
  ///
  /// [bloc] must be not null.
  Widget _buildBody(ScheduleBloc bloc, List<FormattedScheduleModel> schedules) {
    if (schedules.isEmpty)
      return Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: globalElevations.e00dp.backgroundColor,
                border: Border(
                  left: BorderSide(color: Colors.black12),
                  right: BorderSide(color: Colors.black12),
                  bottom: BorderSide(color: Colors.black12),
                ),
              ),
              child: Text(
                widget.favorite.point1.properties.name,
                style: TextStyle(fontSize: 18),
              ),
            ),
          ],
        ),
      );

    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.favorite.point1.properties.name,
            style: TextStyle(fontSize: 18),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Icon(
              widget.controller.expanded ? Icons.unfold_less : Icons.unfold_more,
              color: Colors.grey,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildHeader(ScheduleBloc bloc, List<FormattedScheduleModel> schedules) {
    if (schedules.isEmpty)
      return Flexible(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: globalElevations.e01dp.backgroundColor,
                border: Border(
                  left: BorderSide(color: Colors.black12),
                  right: BorderSide(color: Colors.black12),
                  bottom: BorderSide(color: Colors.black12),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Center(child: Text(translations.text('schedule_of_passage.data_not_available'))),
              ),
            ),
          ],
        ),
      );
    testInt = 0;
    Comparator<FormattedScheduleModel> comparatorTram = (a, b) => a.name.compareTo(b.name);
    schedules.sort(comparatorTram);
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 16, 0, 6),
      child: Container(
        child: Column(
          children: schedules.map((schedule) {
            testInt += 1;
            testBool = testInt == schedules.length ? false : true;
            return Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Row(
                children: <Widget>[
                  ..._buildLineContent(schedule, testBool),
                ],
              ),
            );
          }).toList(),
        ),
      ),
    );
  }

  /// Build the line of ListView from [schedule].
  List<Widget> _buildLineContent(FormattedScheduleModel schedule, bool end) {
    List<Widget> widgetsFirstNextArrival = _buildNextArrivalWidget(schedule.firstNextArrival, true);
    List<Widget> widgetsSecondNextArrival = _buildNextArrivalWidget(schedule.secondNextArrival, false);
    List<Widget> widgetsThirdNextArrival = _buildNextArrivalWidget(schedule.thirdNextArrival, false);
    List<Widget> widgetsFourthNextArrival = _buildNextArrivalWidget(schedule.fourthNextArrival, false);

    return <Widget>[
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 14.0, right: 30.0),
                              child: Text(
                                "${schedule.name[0].toUpperCase()}${schedule.name.substring(1).toLowerCase()}",
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      realTimeWidget(schedule, schedule.firstNextArrival),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: widgetsFirstNextArrival,
                                      ),
                                    ],
                                  ),
                                  Visibility(
                                    visible: widget.controller.expanded,
                                    child: Row(
                                      children: [
                                        realTimeWidget(schedule, schedule.thirdNextArrival),
                                        Row(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: widgetsThirdNextArrival,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                            width: 16.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  realTimeWidget(schedule, schedule.secondNextArrival),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: widgetsSecondNextArrival,
                                  ),
                                ],
                              ),
                              Visibility(
                                visible: widget.controller.expanded,
                                child: Row(
                                  children: <Widget>[
                                    realTimeWidget(schedule, schedule.fourthNextArrival),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: widgetsFourthNextArrival,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: (widget.controller.expanded && end),
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Container(
                  height: 1.6,
                  color: globalElevations.separator.backgroundColor,
                ),
              ),
            )
          ],
        ),
      ),
    ];
  }

  Widget realTimeWidget(FormattedScheduleModel schedule, String hour) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Transform.rotate(
        angle: -12.2,
        child: (schedule.realtime && hour.isNotEmpty)
            ? AnimatedRealtime(height: 15)
            : hour != ""
                ? Container(
                    height: 15,
                    width: 15,
                  )
                : Container(
                    height: 15,
                  ),
      ),
    );
  }
}
