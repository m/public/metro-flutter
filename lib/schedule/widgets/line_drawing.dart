import 'package:flutter/material.dart';

enum LineDrawingPosition { start, middle, end }

class LineDrawing extends StatelessWidget {
  final Color color;
  final LineDrawingPosition position;

  const LineDrawing(
      {Key key,
      this.color = Colors.white,
      this.position = LineDrawingPosition.middle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 9.0),
          child: _createLineSegments(),
        ),
        _createCenterCircle(),
      ],
    );
  }

  Widget _createLineSegments() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: position == LineDrawingPosition.middle ||
                    position == LineDrawingPosition.end
                ? _createLineSegment()
                : Container(),
          ),
          Expanded(
            child: position == LineDrawingPosition.middle ||
                    position == LineDrawingPosition.start
                ? _createLineSegment()
                : Container(),
          ),
        ],
      ),
    );
  }

  Widget _createLineSegment() {
    return Container(
      width: 6,
      color: color,
    );
  }

  Widget _createCenterCircle() {
    return Center(
      child: Container(
        height: 24,
        width: 24,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: color,
        ),
        child: Center(
          child: Container(
            height: 14,
            width: 14,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
