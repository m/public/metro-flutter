import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/atmo/blocs/atmo.dart';
import 'package:metro/atmo/blocs/atmo_bloc.dart';
import 'package:metro/atmo/models/atmo_info.dart';
import 'package:metro/atmo/widgets/atmo_widget.dart';
import 'package:metro/atmo/widgets/atmo_search.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:metro/search/widgets/search_bar.dart';

class AtmoPage extends TraceableStatefulWidget {
  AtmoPage({Key key}) : super(key: key, name: MatomoBloc.screen_atmo);

  @override
  _AtmoPageSate createState() => _AtmoPageSate();
}

class _AtmoPageSate extends State<AtmoPage> {
  final _baseSize = 16.0;
  SearchBarController _departureSearchBarController = SearchBarController();

  @override
  Widget build(BuildContext context) {
    // BlocProvider.master<IndiceAtmoBloc>().downloadAtmoInfo();

    return Scaffold(
      backgroundColor: globalElevations.e00dp.backgroundColor,
      appBar: AppBar(
        backgroundColor: globalElevations.e16dp.backgroundColor,
        iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
        title: Text(
          translations.text('atmo.index'),
          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return StreamBuilder<AtmoInfo>(
            stream: BlocProvider.of<AtmoBloc>(context).onDownloadAtmoInfoCompleted,
            initialData: BlocProvider.of<AtmoBloc>(context).atmoInfo,
            builder: (context, snapshot2) {
              if (!snapshot2.hasData || snapshot2.data == null) return Text(translations.text('generic.loading'));

              return SingleChildScrollView(
                padding: EdgeInsets.all(16.0),
                child: ConstrainedBox(
                  constraints: constraints.copyWith(minHeight: constraints.maxHeight - _baseSize * 4.125, maxHeight: double.infinity),
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        AtmoSearchInput(
                          label: "Rechercher une commune...",
                          color: globalElevations.e16dp.backgroundColor,
                          controller: _departureSearchBarController,
                          isSchedulePage: false,
                          onSearchComplete: (city) async {
                            // departure = point.properties.name;
                            _departureSearchBarController.changeText(city.commune);
                            _departureSearchBarController.changeBorderColor(Theme.of(context).indicatorColor);
                            // BlocProvider.of<RoutesBloc>(context)
                            //     .setDeparture(point);
                            // _setFavData();
                          },
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 0.0),
                            child: Container(
                              width: constraints.maxWidth,
                              decoration: BoxDecoration(
                                color: globalElevations.e01dp.backgroundColor,
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(
                                  width: 1.0,
                                  color: Color(0xFF646267),
                                ),
                              ),
                              child: StreamBuilder<IndiceAtmoInfo>(
                                stream: BlocProvider.master<IndiceAtmoBloc>().onDownloadAtmoInfoCompleted,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData || snapshot.data == null) return Container();
                                  IndiceAtmoInfo indiceAtmo = snapshot.data;

                                  return Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(20, 32, 20, 0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Indice atmo ",
                                                style: TextStyle(
                                                    fontSize: 24, color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.w300),
                                              ),
                                              SizedBox(
                                                height: 16.0,
                                              ),
                                              Text(
                                                indiceAtmo.indices[0].qualificatif +
                                                    " (" +
                                                    indiceAtmo.indices[0].indice.toString() +
                                                    ")",
                                                style: TextStyle(
                                                    fontSize: 26, color: globalElevations.sndTextColor.backgroundColor, fontWeight: FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 16.0,
                                              ),
                                              Text(
                                                "Polluant majoritaire : " + indiceAtmo.indices[0].firstPolluant,
                                                style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 24, right: 24, bottom: 8.0),
                                        child: SvgPicture.asset(
                                          getAssetsAtmo(context, indiceAtmo.indices[0].indice.toString()),
                                          width: constraints.maxWidth,
                                          placeholderBuilder: (context) => Container(
                                            height: _baseSize * 11.25,
                                            child: AnimatedLoadingLogo(),
                                          ),
                                          semanticsLabel: 'Atmo image',
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          color: globalElevations.e16dp.backgroundColor,
                                          borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0),
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(20.0),
                                          child: Row(
                                            children: [
                                              _buildLink(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget _buildLink() {
    return GestureDetector(
      onTap: () => launch('https://www.atmo-auvergnerhonealpes.fr/'),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "En savoir plus",
            style: TextStyle(
              color: globalElevations.sndTextColor.backgroundColor,
              decoration: TextDecoration.underline,
              fontSize: 16,
            ),
          ),
          SizedBox(width: _baseSize / 4),
        ],
      ),
    );
  }

  String getAssetsAtmo(BuildContext context, String indice) {
    final String basePath = 'assets/indice_atmo' + (Theme.of(context).brightness == Brightness.dark ? '/dark' : '/light');
    if (indice != null) {
      if (indice == "1") {
        return '$basePath/atmo_bon.svg';
      } else if (indice == "2") {
        return '$basePath/atmo_moyen.svg';
      } else if (indice == "3") {
        return '$basePath/atmo_degrade.svg';
      } else if (indice == "4") {
        return '$basePath/atmo_mauvais.svg';
      } else if (indice == "5") {
        return '$basePath/atmo_tres_mauvais.svg';
      } else if (indice == "6") {
        return '$basePath/atmo_bon.svg';
      }
    } else {
      return '$basePath/atmo_extremement_mauvais.svg';
    }
  }
}
