import 'dart:ui' show Color;

import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/utils.dart';

class AtmoInfo {
  final DateTime date;
  final DateTime lastModification;
  final String ongoingMeasures;
  final String majorityPollutant;
  final String comment;
  final List<AtmoValue> values;
  final List<AtmoActivation> activations;
  final List<AtmoAction> actions;

  AtmoInfo({this.date, this.lastModification, this.ongoingMeasures, this.majorityPollutant, this.comment, this.values, this.activations, this.actions});

  factory AtmoInfo.fromJson(Map<String, dynamic> json) {
    if(json == null ||json.length == 0)
      return null;

    HtmlUnescape htmlUnescape = HtmlUnescape();
    List<AtmoValue> values = (json['indice_exposition_sensible'] as List)?.map((l) => AtmoValue.fromJson(l))?.toList();
    List<AtmoActivation> activations = (json['activation'] as List)?.map((l) => AtmoActivation.fromJson(l))?.toList();
    List<AtmoAction> actions = (json['action'] as List)?.map((l) => AtmoAction.fromJson(l))?.toList();

    return AtmoInfo(
      date: DateTime.parse(json['date']),
      lastModification: DateTime.parse(json['date_modification']),
      ongoingMeasures: json['dispositif_en_cours'] as String,
      majorityPollutant: json['polluant_majoritaire'] as String,
      comment: htmlUnescape.convert(json['commentaire'].toString()).replaceAll('<br />', ''),
      values: values,
      activations: activations,
      actions: actions
    );
  }

  AtmoValue get todayValue {
    //TODO: check if it is always the 2 elements in the list
    if(this == null)
      return null;
    if(values == null || values.length == 0)
      return null;

    DateTime today = DateTime.now();
    for(var i = 0; i < values.length; i++) {
      if(isSameDay(values[i].date, today))
        return values[i];
    }

    return null;
  }
}

class AtmoValue {
  final DateTime date;
  final int value;
  final Color color;
  final String descriptive;

  const AtmoValue({this.date, this.value, this.color, this.descriptive});

  factory AtmoValue.fromJson(Map<String, dynamic> json) {
    HtmlUnescape htmlUnescape = HtmlUnescape();
    int value = double.tryParse(json['valeur'].toString().replaceAll(',', '.')).round();
    return AtmoValue(
      date: DateTime.parse(json['date']),
      value: value,
      color: Utils.colorFromHex(json['couleur_html']?.toString()?.substring(1)),
      descriptive: htmlUnescape.convert(json['qualificatif'])?.toString(),
    );
  }

  String getAsset(BuildContext context, {bool header = false}) {
    assert(this != null && value != null);
    final String basePath = 'assets/atmo' + (Theme.of(context).brightness == Brightness.dark ? '/dark' : '/light');
    final String drawer = 'drawer.svg';

    if(value < 20)
      return header ? '$basePath/0_$drawer' : '$basePath/0.svg';
    if(value < 30)
      return header ? '$basePath/0_$drawer' : '$basePath/20.svg';
    if(value < 40)
      return header ? '$basePath/30_$drawer' : '$basePath/30.svg';
    if(value < 50)
      return header ? '$basePath/30_$drawer' : '$basePath/40.svg';
    if(value < 60)
      return header ? '$basePath/50_$drawer' : '$basePath/50.svg';
    if(value < 70)
      return header ? '$basePath/60_$drawer' : '$basePath/60.svg';
    if(value < 80)
      return header ? '$basePath/60_$drawer' : '$basePath/70.svg';
    if(value < 90)
      return header ? '$basePath/60_$drawer' : '$basePath/80.svg';
    if(value < 100)
      return header ? '$basePath/90_$drawer' : '$basePath/90.svg';
    return header ? '$basePath/100_$drawer' : '$basePath/100.svg';
  }

}

class AtmoActivation {
  final DateTime date;
  final String value;
  final String descriptive;

  AtmoActivation({this.date, this.value, this.descriptive});

  factory AtmoActivation.fromJson(Map<String, dynamic> json) {
    if(json['date'] == null)
      return null;
    return AtmoActivation(
      date: DateTime.parse(json['date']?.toString()),
      value: json['valeur']?.toString(),
      descriptive: json['texte']?.toString(),
    );
  }
}

class AtmoAction {
  final DateTime date;
  final String value;
  final String comment;
  final String descriptive;

  AtmoAction({this.date, this.value, this.comment, this.descriptive});

  factory AtmoAction.fromJson(Map<String, dynamic> json) {
    if(json['date'] == null)
      return null;
    return AtmoAction(
      date: DateTime.parse(json['date']),
      value: json['valeur']?.toString(),
      comment: json['commentaire']?.toString(),
      descriptive: json['qualificatif']?.toString(),
    );
  }
}