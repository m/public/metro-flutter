import 'dart:math';

class CityInfo {
  final int gid;
  final String commune;
  final String ref_insee;
  final String ref_postal;
  final String epci;
  final String bassin_air;
  final bool rectangle;

  CityInfo({this.gid, this.commune, this.ref_insee, this.ref_postal, this.epci, this.bassin_air, this.rectangle});

  factory CityInfo.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) return null;

    return CityInfo(
        gid: json['gid'] as int,
        commune: json['commune'] as String,
        ref_insee: json['ref_insee'] as String,
        ref_postal: json['ref_postal'] as String,
        epci: json['epci'] as String,
        bassin_air: json['bassin_air'] as String,
        rectangle: json['rectangle'] as bool);
  }
}

class IndiceAtmoInfo {
  final String commentaire;
  final List<DefinitionsAtmo> definitions;
  final List<IndicesAtmo> indices;

  IndiceAtmoInfo({this.commentaire, this.definitions, this.indices});

  factory IndiceAtmoInfo.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) return null;

    List<DefinitionsAtmo> defAtmo = (json['definitions'] as List)?.map((l) => DefinitionsAtmo.fromJson(l))?.toList();
    List<IndicesAtmo> indiceAtmo = (json['indices'] as List)?.map((l) => IndicesAtmo.fromJson(l))?.toList();
    String atmoComment = json['commentaire'] as String;
    if (atmoComment == null || atmoComment.isEmpty) atmoComment = "Aucun commentaire";

    return IndiceAtmoInfo(
      commentaire: atmoComment,
      definitions: defAtmo,
      indices: indiceAtmo,
    );
  }
}

class DefinitionsAtmo {
  final int indice;
  final String qualificatif;
  final String couleur;
  final String message_sanitaire;
  final String message_population_sensible;
  final String picto_url;

  DefinitionsAtmo({this.indice, this.qualificatif, this.couleur, this.message_sanitaire, this.message_population_sensible, this.picto_url});

  factory DefinitionsAtmo.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) return null;

    return DefinitionsAtmo(
        indice: json['indice'] as int,
        qualificatif: json['qualificatif'] as String,
        couleur: json['couleur'] as String,
        message_sanitaire: json['message_sanitaire'] as String,
        message_population_sensible: json['message_population_sensible'] as String,
        picto_url: json['picto_url'] as String);
  }
}

class IndicesAtmo {
  final int echeance;
  final String date_echeance;
  final int indice;
  final String qualificatif;
  final String couleur_html;
  final String date_calcul;
  final String commune_insee;
  final String commune_nom;
  final String type_valeur;
  final List<SousIndices> sous_indices;
  final List<dynamic> polluants_majoritaires;
  final String firstPolluant;

  IndicesAtmo(
      {this.echeance,
      this.date_echeance,
      this.indice,
      this.qualificatif,
      this.couleur_html,
      this.date_calcul,
      this.commune_insee,
      this.commune_nom,
      this.type_valeur,
      this.sous_indices,
      this.polluants_majoritaires,
      this.firstPolluant});

  factory IndicesAtmo.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) return null;

    List<SousIndices> sousIndicesAtmo = (json['sous_indices'] as List)?.map((l) => SousIndices.fromJson(l))?.toList();
    String polluant = "";

    if (sousIndicesAtmo != null && sousIndicesAtmo.isNotEmpty) {
      sousIndicesAtmo.sort((a, b) => a.concentration.compareTo(b.concentration));
    }

    polluant = sousIndicesAtmo[sousIndicesAtmo.length - 1].polluant_nom;

    return IndicesAtmo(
      echeance: json['echeance'] as int,
      date_echeance: json['date_echeance'] as String,
      indice: json['indice'] as int,
      qualificatif: json['qualificatif'] as String,
      couleur_html: json['couleur_html'] as String,
      date_calcul: json['date_calcul'] as String,
      commune_insee: json['commune_insee'] as String,
      commune_nom: json['commune_nom'] as String,
      type_valeur: json['type_valeur'] as String,
      sous_indices: sousIndicesAtmo,
      polluants_majoritaires: json['polluants_majoritaires'] as List<dynamic>,
      firstPolluant: polluant,
    );
  }
}

class SousIndices {
  final String polluant_nom;
  final double concentration;
  final int indice;

  SousIndices({this.polluant_nom, this.concentration, this.indice});

  factory SousIndices.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) return null;

    var atmoConcentration = json['concentration'];

    return SousIndices(
      polluant_nom: json['polluant_nom'] as String,
      concentration: atmoConcentration.toDouble(),
      indice: json['indice'] as int,
    );
  }
}
