import 'dart:async';
import 'dart:convert';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/database/database.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

/// This bloc helps in downloading points based on their name.
/// Used by the [CustomSearchDelegate] to display the suggestions
/// in a search screen.
class AtmoSearchBloc extends BlocBase {
  /// The controller object which contain the last added value in the stream.
  BehaviorSubject<List<Point>> _searchController = BehaviorSubject<List<Point>>.seeded(List<Point>());
  /// Event called when search has finish downloading.
  Stream<List<Point>> get onSearchFinish => _searchController.stream;
  /// The current search value.
  List<Point> get searchResult => _searchController.value;

  final MyDatabase database;
  AtmoSearchBloc(this.database) : assert(database != null);

  @override
  void dispose() {
    _searchController.close();
  }

  @override
  void initState() {
  }

  List<Point> _parseSearch(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<Point>.from(parsed['features'].map((json) => Point.fromJson(json)));
  }

  /// Call API endpoint for given [text].
  ///
  /// ```
  /// fetchSearch('cha');
  /// Call "https://datatest.metromobilite.fr/api/find/json?query=cha&types=clusters,lieux,rue"
  /// ```
  Future<List<Point>> _fetchSearch(String text, bool isSchedulePage) async {
    if (text == null || text.isEmpty) return null;

    var response;

    response = await BlocProvider.master<WebServices>().get('city/json');

    if (response.statusCode == 200) {
      print(response.body);
      return _parseSearch(response.body);
    } else
      return null;
  }

  /// Search points with name similar to [text].
  void search(String text, bool isSchedulePage) async {
    _searchController.sink.add(null);
    List<Point> results = await _fetchSearch(text, isSchedulePage);

    if (results == null)
      return;

    // Remove all unvisible points
    results.removeWhere((point) {
      if (point.properties.type == PropertiesType.clusters)
        return !(point.properties as ClustersProperties).visible;
      return false;
    });

    _searchController.sink.add(results);
  }

  Future<int> addLastSearch(Point point) async {
    assert(point != null);
    return await database.lastSearchDao.addResearch(point);
  }

  Future<List<LastSearchPoint>> getLastSearchPoints() {
    return database.lastSearchDao.getLastResearches;
  }

}
