import 'dart:convert';

import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';

class IndiceAtmoBloc extends BlocBase {
  BehaviorSubject<IndiceAtmoInfo> _downloadController;

  Stream<IndiceAtmoInfo> get onDownloadAtmoInfoCompleted => _downloadController.stream;

  IndiceAtmoInfo get atmoInfo => _downloadController.value;

  BehaviorSubject<List<CityInfo>> _downloadCityController;

  Stream<List<CityInfo>> get onDownloadAtmoCityInfoCompleted => _downloadCityController.stream;

  List<CityInfo> get cityInfo => _downloadCityController.value;

  BehaviorSubject<List<CityInfo>> _searchCityController;

  Stream<List<CityInfo>> get onSearchAtmoCityInfoCompleted => _searchCityController.stream;

  List<CityInfo> get searchCityInfo => _searchCityController.value;

  BehaviorSubject<List<CityInfo>> _lastSearchCityController;

  Stream<List<CityInfo>> get onLastSearchAtmoCityInfoCompleted => _lastSearchCityController.stream;

  List<CityInfo> get lastSearchCityInfo => _lastSearchCityController.value;

  @override
  void initState() {
    String preferencesCommune = PrefService.getString("pref_commune");
    if (preferencesCommune == null) PrefService.setString("pref_commune", "38185");
    _downloadController = BehaviorSubject<IndiceAtmoInfo>.seeded(null);
    _downloadCityController = BehaviorSubject<List<CityInfo>>.seeded(null);
    _searchCityController = BehaviorSubject<List<CityInfo>>.seeded(null);
    _lastSearchCityController = BehaviorSubject<List<CityInfo>>.seeded(null);
    downloadAtmoInfo(PrefService.getString("pref_commune"));
    downloadCityInfo();
    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      downloadAtmoInfo(PrefService.getString("pref_commune"));
      downloadCityInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
    _downloadCityController.close();
    _searchCityController.close();
    _lastSearchCityController.close();
  }

  void downloadAtmoInfo(String commune) async {
    IndiceAtmoInfo info = await _fetchAtmoInfo(commune);

    _downloadController.sink.add(info);
  }

  IndiceAtmoInfo _parseAtmoInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return IndiceAtmoInfo.fromJson(parsed);
  }

  Future<IndiceAtmoInfo> _fetchAtmoInfo(String commune) async {
    final response = await BlocProvider.master<WebServices>().get('dyn/indiceAtmoCommunal/' + commune + '/json');

    if (response.statusCode == 200) {
      return _parseAtmoInfo(response.body);
    } else
      throw Exception('Failed to fetch atmo data');
  }

  void downloadCityInfo() async {
    List<CityInfo> info = await _fetchAtmoCityInfo();
    _downloadCityController.sink.add(info);
    getRecentResearch();
  }

  List<CityInfo> _parseAtmoCityInfo(String responseBody) {
    final jsonItems = json.decode(responseBody).cast<Map<String, dynamic>>();

    List<CityInfo> usersList = jsonItems.map<CityInfo>((json) {
      return CityInfo.fromJson(json);
    }).toList();

    return usersList;
  }

  Future<List<CityInfo>> _fetchAtmoCityInfo() async {
    final response = await BlocProvider.master<WebServices>().get('city/json');

    if (response.statusCode == 200) {
      return _parseAtmoCityInfo(response.body);
    } else
      throw Exception('Failed to fetch atmo data');
  }

  void onCityChanged(String value) {
    addNewCityList(cityInfo
        .where((city) => city.commune
            .replaceAll("É", "e")
            .replaceAll("é", "e")
            .replaceAll("è", "e")
            .replaceAll("-", " ")
            .toUpperCase()
            .contains(value.replaceAll("É", "e").replaceAll("é", "e").replaceAll("è", "e").replaceAll("-", " ").toUpperCase()))
        .toList());
  }

  void addNewCityList(List<CityInfo> newList) {
    _searchCityController.sink.add(newList);
  }

  addRecentResearch(String searchAtmo) {
    List<String> finalListAtmo = [];
    List<CityInfo> finalListAtmoInfo = [];
    List<String> savedList = PrefService.getStringList('recentSearchAtmo');
    if (savedList != null && savedList.isNotEmpty) {
      finalListAtmo = savedList;
    }
    if (finalListAtmo.length >= 3) {
      finalListAtmo.removeLast();
    }
    finalListAtmo.add(searchAtmo);
    PrefService.setStringList("recentSearchAtmo", finalListAtmo);
    for (CityInfo c in cityInfo) {
      if (c.commune == searchAtmo) finalListAtmoInfo.add(c);
    }
    _lastSearchCityController.sink.add(finalListAtmoInfo.toList());
  }

  getRecentResearch() {
    List<CityInfo> finalListAtmoInfo = [];
    List<String> savedList = PrefService.getStringList('recentSearchAtmo');
    if (savedList != null) {
      for (CityInfo c in cityInfo) {
        for (String s in savedList) {
          if (s == c.commune) finalListAtmoInfo.add(c);
        }
      }
    }
    if (finalListAtmoInfo != null && finalListAtmoInfo.isNotEmpty)
      _lastSearchCityController.sink.add(
        finalListAtmoInfo.toSet().toList(),
      );
  }
}
