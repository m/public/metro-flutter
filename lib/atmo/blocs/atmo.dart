import 'dart:convert';

import 'package:metro/atmo/models/atmo_info.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class AtmoBloc extends BlocBase {
  BehaviorSubject<AtmoInfo> _downloadController;
  Stream<AtmoInfo> get onDownloadAtmoInfoCompleted => _downloadController.stream;
  AtmoInfo get atmoInfo => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<AtmoInfo>.seeded(null);
    _downloadAtmoInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _downloadAtmoInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadAtmoInfo() async {
    AtmoInfo info = await _fetchAtmoInfo();

    _downloadController.sink.add(info);
  }

  AtmoInfo _parseAtmoInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return AtmoInfo.fromJson(parsed);
  }

  Future<AtmoInfo> _fetchAtmoInfo() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/indiceAtmoFull/json/');

    if (response.statusCode == 200)
      return _parseAtmoInfo(response.body);
    else
      throw Exception('Failed to fetch atmo data');
  }
}
