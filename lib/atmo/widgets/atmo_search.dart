import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/always_disabled_focus.dart';
import 'package:metro/search/widgets/search_bar.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/atmo/blocs/atmo_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';


class AtmoSearchInput extends StatefulWidget {
  final String label;
  final Function(CityInfo) onSearchComplete;
  final bool isSchedulePage;
  final Color color;
  final SearchBarController controller;

  AtmoSearchInput({
    @required this.label,
    @required this.onSearchComplete,
    this.controller,
    this.isSchedulePage,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _AtmoSearchInputState();
}

class _AtmoSearchInputState extends State<AtmoSearchInput> {
  Color _overrideBorderColor;
  TextEditingController _textController;

  void _changeBorderColor(Color color) {
    if (mounted) setState(() => _overrideBorderColor = color);
  }

  void _changeText(String text) {
    if (mounted) setState(() => _textController.text = text.toUpperCase());
  }

  @override
  void initState() {
    super.initState();

    _textController = TextEditingController();

    widget.controller?.onBorderColorChanged =
        (color) => _changeBorderColor(color);

    widget.controller?.onTextChanged = (text) => _changeText(text.toUpperCase());
    BlocProvider.of<IndiceAtmoBloc>(context).onDownloadAtmoInfoCompleted.listen((event) {
      _changeText(event.indices[0].commune_nom);
    });
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      child: TextField(
        focusNode: AlwaysDisabledFocusNode(),
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w300,
          color: _textController != null
              ? globalElevations.fstTextColor.backgroundColor
              : globalElevations.sndTextColor.backgroundColor,
        ),
        controller: _textController,
        onTap: () async {
          BlocProvider.master<IndiceAtmoBloc>().getRecentResearch();
          widget.onSearchComplete(
              await Utils.openAtmoSearch(context));
        },
        decoration: InputDecoration(
          suffixIcon: Icon(Icons.search, size: 30,),
          labelText: "Commune",
          labelStyle: TextStyle(fontSize: 18),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: BorderSide(
              color: _overrideBorderColor != null
                  ? _overrideBorderColor
                  : Color(0xFF646267),
            ),
          ),
        ),
      ),
    );
  }
}
