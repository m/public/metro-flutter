import 'dart:async';

import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/search/blocs/search.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/atmo/blocs/atmo_bloc.dart';
import 'package:preferences/preference_service.dart';

/// The [CustomAtmoSearchDelegate] will handle the display and sending search
/// request to [SearchBloc] based on the used input.
///
/// Use `Utils.showSearch(context)` to launch a search page.
///
/// To get the selected point from it use `Point p = await Utils.showSearch(context);`.
class CustomAtmoSearchDelegate extends SearchDelegate<CityInfo> {
  CustomAtmoSearchDelegate({Key key}) : super();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: globalElevations.fstTextColor.backgroundColor,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  String get searchFieldLabel => "Rechercher une commune";

  /// Override the theme of the search input.
  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);

    final ThemeData base = theme;
    return base.copyWith(
      primaryColor: globalElevations.e02dp.backgroundColor,
      textTheme: _buildDefaultTextTheme(base.textTheme),
      iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
    );
  }

  /// Resize the displayed title.
  ///
  /// The displayed label value "Chercher un lieu, un arrêt..." is edited in CustomLocalization in main.dart.
  TextTheme _buildDefaultTextTheme(TextTheme base) {
    return base.copyWith(
      title: base.title.copyWith(
        fontSize: 18,
        fontWeight: FontWeight.w300,
        color: globalElevations.fstTextColor.backgroundColor,
        decorationColor: globalElevations.e02dp.backgroundColor,
      ),
    );
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
        color: globalElevations.fstTextColor.backgroundColor,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  /// Called when the used press enter on the keyboard.
  @override
  Widget buildResults(BuildContext context) {
    // Simply build view from previous suggestion
    return _buildResultView(context);
  }

  /// Called every time the search input change.
  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty /* || query.length < 3 || query.length > 50*/) return _buildEmptyView(context);

    BlocProvider.master<IndiceAtmoBloc>().onCityChanged(query);

    // Request for suggestions if the current query is different from the previous one
    // if (query != _previousQuery) {
    //   BlocProvider.of<SearchBloc>(context).search(query.replaceAll("’", "'"), isSchedulePage);
    //   _previousQuery = query;
    // }
    // And build the view
    return _buildResultView(context);
  }

  Widget _buildEmptyView(BuildContext context) {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: StreamBuilder<List<CityInfo>>(
                stream: BlocProvider.of<IndiceAtmoBloc>(context).onLastSearchAtmoCityInfoCompleted,
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data.isEmpty) return Container();

                  return ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      final city = snapshot.data[index];
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(6.0, 1.5, 8.0, 1.5),
                        child: Container(
                          color: globalElevations.e02dp.backgroundColor,
                          child: ListTile(
                              leading: Icon(
                                Icons.schedule,
                                color: globalElevations.fstTextColor.backgroundColor,
                                size: 30,
                              ),
                              title: Text(
                                city.commune.toUpperCase(),
                                style: TextStyle(fontWeight: FontWeight.w300),
                              ),
                              // subtitle: Text("Commune"),
                              onTap: () {
                                PrefService.setString("pref_commune", city.ref_insee);
                                BlocProvider.of<IndiceAtmoBloc>(context).downloadAtmoInfo(city.ref_insee);
                                close(context, city);
                              }),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SimpleDialog(key: key, children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16),
              child: Center(
                child: Row(children: [
                  AnimatedLoadingLogo(
                    height: 60,
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Text(
                    translations.text('search.location_loading_message'),
                    softWrap: true,
                  ))
                ]),
              ),
            )
          ]);
        });
  }

  Widget _buildLoadingView() {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: const AnimatedLoadingLogo(),
      ),
    );
  }

  Widget _buildNoResultView() {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Text(
            translations.text('search.no_result'),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
          ),
        ),
      ),
    );
  }

  Widget _buildResultView(BuildContext context) {
    return StreamBuilder<List<CityInfo>>(
      stream: BlocProvider.of<IndiceAtmoBloc>(context).onSearchAtmoCityInfoCompleted,
      builder: (context, snapshot) {
        // If search hasn't occured yet (not enought/too many characters), loading...
        if (snapshot.data == null)
          return _buildLoadingView();
        // Else if there is no match for the given input
        else if (snapshot.data.isEmpty) return _buildNoResultView();

        List<CityInfo> cityInfo = snapshot.data;

        return Container(
          color: globalElevations.e00dp.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ListView.builder(
              itemCount: cityInfo.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(6.0, 1.5, 8.0, 1.5),
                  child: Container(
                    color: globalElevations.e02dp.backgroundColor,
                    child: ListTile(
                        // leading: Padding(
                        //   padding: const EdgeInsets.only(top: 8.0),
                        //   child: Icon(
                        //     Icons.schedule,
                        //     color: globalElevations.fstTextColor.backgroundColor,
                        //   ),
                        // ),
                        title: Text(
                          cityInfo[index].commune.toUpperCase(),
                          style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
                        ),
                        onTap: () {
                          PrefService.setString("pref_commune", cityInfo[index].ref_insee);
                          BlocProvider.of<IndiceAtmoBloc>(context).addRecentResearch(cityInfo[index].commune);
                          BlocProvider.of<IndiceAtmoBloc>(context).downloadAtmoInfo(cityInfo[index].ref_insee);
                          close(context, cityInfo[index]);
                        }),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
