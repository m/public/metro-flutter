import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';

class AtmoWidget extends StatelessWidget {
  final IndiceAtmoInfo atmoInfo;
  final bool header;

  final _baseSize = 16.0;

  const AtmoWidget({Key key, @required this.atmoInfo, this.header = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IndiceAtmoInfo todayValue = atmoInfo;
    return Material(
      type: MaterialType.transparency,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: _baseSize * 1.25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              fit: FlexFit.loose,
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return SvgPicture.asset(
                    getAssetsAtmo(context, todayValue.indices[0].indice.toString()),
                    width: constraints.maxWidth,
                    placeholderBuilder: (context) => Container(
                      height: _baseSize * 11.25,
                      child: AnimatedLoadingLogo(),
                    ),
                    semanticsLabel: 'Atmo image',
                  );
                },
              ),
            ),
            if (header) SizedBox(height: _baseSize),
            if (header)
              Column(
                children: [
                  Text(todayValue.indices[0].commune_nom, style: TextStyle(fontWeight: FontWeight.bold),),
                  SizedBox(height: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '${translations.text('atmo.index')} - ${todayValue.indices[0].qualificatif} (${todayValue.indices[0].indice.toString()})',
                        style: TextStyle(fontSize: 14),
                      ),
                      SizedBox(width: _baseSize / 2),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  String getAssetsAtmo(BuildContext context, String indice) {
    final String basePath = 'assets/indice_atmo' + (Theme.of(context).brightness == Brightness.dark ? '/dark' : '/light');
    if (indice != null) {
      if (indice == "1") {
        return '$basePath/atmo_bon.svg';
      } else if (indice == "2") {
        return '$basePath/atmo_moyen.svg';
      } else if (indice == "3") {
        return '$basePath/atmo_degrade.svg';
      } else if (indice == "4") {
        return '$basePath/atmo_mauvais.svg';
      } else if (indice == "5") {
        return '$basePath/atmo_tres_mauvais.svg';
      } else if (indice == "6") {
        return '$basePath/atmo_bon.svg';
      }
    } else {
      return '$basePath/atmo_extremement_mauvais.svg';
    }
  }

}
