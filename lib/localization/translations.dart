import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:preferences/preference_service.dart';

const List<String> _kSupportedLanguages = ['en', 'fr'];
const String _defaultFRLanguageCode = 'fr';
const String _defaultWorldLanguageCode = 'en';
const String _defaultFRLanguageName = 'Français';
const String _defaultWorldLanguageName = 'English';

GlobalTranslations translations = GlobalTranslations();

class GlobalTranslations {
  Locale _locale;
  Map<dynamic, dynamic> _localizedValues;
  Map<String, String> _cache = {};

  /// Singleton Factory.
  static final GlobalTranslations _translations = GlobalTranslations._internal();

  factory GlobalTranslations() {
    return _translations;
  }

  GlobalTranslations._internal();

  /// Returns the list of supported locales.
  Iterable<Locale> supportedLocales() => _kSupportedLanguages.map<Locale>((lang) => Locale(lang, ''));

  /// Return the translation that corresponds to the [key].
  ///
  /// The [key] might be a sequence of [key].[sub-key].[sub-key].
  String text(String key) {
    // Return the requested string
    String string = '/!\\ missing "$key" translation /!\\';

    if (_localizedValues != null) {
      // Check if the requested [key] is in the cache
      if (_cache[key] != null) {
        return _cache[key];
      }

      // Iterate the key until found or not
      bool found = true;
      Map<dynamic, dynamic> _values = _localizedValues;
      List<String> _keyParts = key.split('.');
      int _keyPartsLen = _keyParts.length;
      int index = 0;
      int lastIndex = _keyPartsLen - 1;

      while (index < _keyPartsLen && found) {
        var value = _values[_keyParts[index]];

        if (value == null) {
          // Not found => STOP
          found = false;
          break;
        }

        // Check if we found the requested key
        if (value is String && index == lastIndex) {
          string = value;

          // Add to cache
          _cache[key] = string;
          break;
        }

        // go to next subKey
        _values = value;
        index++;
      }
    }
    return string;
  }

  String get currentLanguage => _locale == null ? '' : _locale.languageCode;

  Locale get locale => _locale;

  /// One-time initialization.
  init() async {
    // Si la préférence de langue est nulle, on prends la langue du téléphone si cette dernière est dans le périmétre { Francais, Anglais }
    // Si la langue du tel n'est pas dans le périmètre défini, l'Anglais est le language par default
    String language = PrefService.getString("preferences_language");

    if (language == null) {
      language = getDeviceLanguage(0);
    }

    await setNewLanguage(language);
  }

  String getDeviceLanguage(int choice) {
    String lang = '';
    String currentLocale = Platform?.localeName?.toLowerCase();
    if (currentLocale != null) {
      if (currentLocale.length > 2) {
        currentLocale.substring(0, 2) == _defaultFRLanguageCode ? lang = _defaultFRLanguageCode : lang = _defaultWorldLanguageCode;
      } else {
        lang = _defaultFRLanguageCode;
      }
    } else {
      lang = _defaultFRLanguageCode;
    }

    if (choice == 1) {
      if (lang == _defaultFRLanguageCode) {
        lang = _defaultFRLanguageName;
      } else if (lang == _defaultWorldLanguageCode) {
        lang = _defaultWorldLanguageName;
      }
    }

    return lang;
  }

  /// Routine to change the language.
  setNewLanguage([String newLanguage]) async {
    // Load the language strings
    String jsonContent = await rootBundle.loadString('assets/locales/locale_'+ newLanguage +'.json');
    _localizedValues = json.decode(jsonContent.toString());

    // Clear the cache
    _cache = {};
  }
}
