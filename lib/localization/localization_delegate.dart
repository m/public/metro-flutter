import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;
import 'package:intl/date_symbols.dart' as intl;
import 'package:flutter_localizations/src/utils/date_localizations.dart'
    show loadDateIntlDataIfNotLoaded;
import 'package:metro/localization/translations.dart';

/// To change the default placeholder of search delegate, we have to override a specific
/// label from the default LocalizationDelegate. To do so, I've created a custom Localisation
/// delegate that is feeded with custom [MaterialLocalization] that override the desired label
/// from the originals [MaterialLocalization].
class CustomLocalizationDelegate extends LocalizationsDelegate<MaterialLocalizations> {
  const CustomLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => translations.supportedLocales().contains(locale);

  static final Map<Locale, Future<MaterialLocalizations>> _loadedTranslations = <Locale, Future<MaterialLocalizations>>{};

  @override
  Future<MaterialLocalizations> load(Locale locale) {
    assert(isSupported(locale));
    return _loadedTranslations.putIfAbsent(locale, () {
      loadDateIntlDataIfNotLoaded();

      final String localeName = intl.Intl.canonicalizedLocale(locale.toString());
      assert(
        locale.toString() == localeName,
        'Flutter does not support the non-standard locale form $locale (which '
        'might be $localeName',
      );

      intl.DateFormat fullYearFormat;
      intl.DateFormat mediumDateFormat;
      intl.DateFormat longDateFormat;
      intl.DateFormat yearMonthFormat;
      intl.DateFormat compactDateFormat;
      intl.DateFormat shortDateFormat;
      intl.DateFormat shortMonthDayFormat;
      if (intl.DateFormat.localeExists(localeName)) {
        fullYearFormat = intl.DateFormat.y(localeName);
        mediumDateFormat = intl.DateFormat.MMMEd(localeName);
        longDateFormat = intl.DateFormat.yMMMMEEEEd(localeName);
        yearMonthFormat = intl.DateFormat.yMMMM(localeName);
        compactDateFormat = intl.DateFormat.yM(localeName);
        shortDateFormat = intl.DateFormat.yMd(localeName);
        shortMonthDayFormat = intl.DateFormat.MMMd(localeName);
      } else if (intl.DateFormat.localeExists(locale.languageCode)) {
        fullYearFormat = intl.DateFormat.y(locale.languageCode);
        mediumDateFormat = intl.DateFormat.MMMEd(locale.languageCode);
        longDateFormat = intl.DateFormat.yMMMMEEEEd(locale.languageCode);
        yearMonthFormat = intl.DateFormat.yMMMM(locale.languageCode);
        compactDateFormat = intl.DateFormat.yM(locale.languageCode);
        shortDateFormat = intl.DateFormat.yMd(locale.languageCode);
        shortMonthDayFormat = intl.DateFormat.MMMd(locale.languageCode);
      } else {
        fullYearFormat = intl.DateFormat.y();
        mediumDateFormat = intl.DateFormat.MMMEd();
        longDateFormat = intl.DateFormat.yMMMMEEEEd();
        yearMonthFormat = intl.DateFormat.yMMMM();
        compactDateFormat = intl.DateFormat.yM();
        shortDateFormat = intl.DateFormat.yMd();
        shortMonthDayFormat = intl.DateFormat.MMMd();
      }

      intl.NumberFormat decimalFormat;
      intl.NumberFormat twoDigitZeroPaddedFormat;
      if (intl.NumberFormat.localeExists(localeName)) {
        decimalFormat = intl.NumberFormat.decimalPattern(localeName);
        twoDigitZeroPaddedFormat = intl.NumberFormat('00', localeName);
      } else if (intl.NumberFormat.localeExists(locale.languageCode)) {
        decimalFormat = intl.NumberFormat.decimalPattern(locale.languageCode);
        twoDigitZeroPaddedFormat = intl.NumberFormat('00', locale.languageCode);
      } else {
        decimalFormat = intl.NumberFormat.decimalPattern();
        twoDigitZeroPaddedFormat = intl.NumberFormat('00');
      }

      switch (locale.languageCode) {
        case 'fr':
          return SynchronousFuture<MaterialLocalizations>(
              CustomMaterialLocalizationFr(
                  fullYearFormat: fullYearFormat,
                  mediumDateFormat: mediumDateFormat,
                  longDateFormat: longDateFormat,
                  yearMonthFormat: yearMonthFormat,
                  decimalFormat: decimalFormat,
                  twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
                  compactDateFormat: compactDateFormat,
                  shortDateFormat: shortDateFormat,
                  shortMonthDayFormat: shortMonthDayFormat));
        case 'en':
        default:
          return SynchronousFuture<MaterialLocalizations>(
              CustomMaterialLocalizationEn(
                  fullYearFormat: fullYearFormat,
                  mediumDateFormat: mediumDateFormat,
                  longDateFormat: longDateFormat,
                  yearMonthFormat: yearMonthFormat,
                  decimalFormat: decimalFormat,
                  twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
                  compactDateFormat: compactDateFormat,
                  shortDateFormat: shortDateFormat,
                  shortMonthDayFormat: shortMonthDayFormat));
      }
    });
  }

  @override
  bool shouldReload(CustomLocalizationDelegate old) => false;

  @override
  String toString() => 'CustomLocalization.delegate(${translations.locale.languageCode})';
}

/// Override of the default [MaterialLocalization] for the English language
class CustomMaterialLocalizationEn extends MaterialLocalizationEn {
  const CustomMaterialLocalizationEn({
    @required intl.DateFormat fullYearFormat,
    @required intl.DateFormat mediumDateFormat,
    @required intl.DateFormat longDateFormat,
    @required intl.DateFormat yearMonthFormat,
    @required intl.NumberFormat decimalFormat,
    @required intl.NumberFormat twoDigitZeroPaddedFormat,
    @required intl.DateFormat shortDateFormat,
    @required intl.DateFormat shortMonthDayFormat,
    @required intl.DateFormat compactDateFormat,
  }) : super(
          fullYearFormat: fullYearFormat,
          mediumDateFormat: mediumDateFormat,
          longDateFormat: longDateFormat,
          yearMonthFormat: yearMonthFormat,
          decimalFormat: decimalFormat,
          twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
          shortDateFormat: shortDateFormat,
          shortMonthDayFormat: shortMonthDayFormat,
          compactDateFormat: compactDateFormat,
        );
  @override
  String get searchFieldLabel => translations.text('search_bar.text');
}

/// Override of the default [MaterialLocalization] for the French language
class CustomMaterialLocalizationFr extends MaterialLocalizationFr {
  const CustomMaterialLocalizationFr({
    @required intl.DateFormat fullYearFormat,
    @required intl.DateFormat mediumDateFormat,
    @required intl.DateFormat longDateFormat,
    @required intl.DateFormat yearMonthFormat,
    @required intl.NumberFormat decimalFormat,
    @required intl.NumberFormat twoDigitZeroPaddedFormat,
    @required intl.DateFormat shortDateFormat,
    @required intl.DateFormat shortMonthDayFormat,
    @required intl.DateFormat compactDateFormat,
  }) : super(
          fullYearFormat: fullYearFormat,
          mediumDateFormat: mediumDateFormat,
          longDateFormat: longDateFormat,
          yearMonthFormat: yearMonthFormat,
          decimalFormat: decimalFormat,
          twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
          shortDateFormat: shortDateFormat,
          shortMonthDayFormat: shortMonthDayFormat,
          compactDateFormat: compactDateFormat,
        );
  @override
  String get searchFieldLabel => translations.text('search_bar.text');
}
