import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:location/location.dart';
import 'package:map_controller/map_controller.dart';
import 'package:metro/bottomsheet/contents/bottom_filters.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/bottomsheet/contents/cam.dart';
import 'package:metro/bottomsheet/contents/citiz.dart';
import 'package:metro/bottomsheet/contents/empty.dart';
import 'package:metro/bottomsheet/contents/event.dart';
import 'package:metro/bottomsheet/contents/irve.dart';
import 'package:metro/bottomsheet/contents/new_poi_around.dart';
import 'package:metro/bottomsheet/contents/poi_details.dart';
import 'package:metro/bottomsheet/contents/parking.dart';
import 'package:metro/bottomsheet/contents/bike_service.dart';
import 'package:metro/bottomsheet/contents/pony.dart';
import 'package:metro/bottomsheet/contents/route.dart';
import 'package:metro/bottomsheet/contents/route_details.dart';
import 'package:metro/bottomsheet/contents/schedule_details.dart';
import 'package:metro/bottomsheet/contents/tier.dart';
import 'package:metro/bottomsheet/contents/yea.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/disturbances/models/icon_data_model.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/filters_type.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/agenceM.dart';
import 'package:metro/global/blocs/bike_bloc.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/blocs/traficolor_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/linePopUp.dart';
import 'package:metro/map/widgets/line_selection_dialog.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/route/route_utils.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:rxdart/rxdart.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:intl/intl.dart' show DateFormat;

import 'package:metro/main.dart';

class MapBloc implements BlocBase, TickerProvider {
  BehaviorSubject<MapPositionAndZoom> _positionController = BehaviorSubject<MapPositionAndZoom>.seeded(MapPositionAndZoom.initial);

  /// Event called whenever an actor asked for position or zoom to be changed.
  Stream<MapPositionAndZoom> get onPositionChanged => _positionController.stream;

  /// The current position and zoom of the map.
  MapPositionAndZoom get positionAndZoom => _positionController.value;

  BehaviorSubject<List<LayerOptions>> _layerController = BehaviorSubject<List<LayerOptions>>();

  /// Event called whenever layers of the map changed.
  Stream<List<LayerOptions>> get onLayersChanged => _layerController.stream;

  /// The current layers of the map.
  List<LayerOptions> get layers => _layerController.value;

  /// The current position and zoom of the map.
  MapPositionAndZoom get currentPositionAndZoom {
    LatLng tmpLatLng = (_mapController.ready ?? false) ? _mapController?.center : LatLng(Consts.placeVictorHugoLat, Consts.placeVictorHugoLng);
    double tmpZoom = (_mapController.ready ?? false) ? _mapController?.zoom : Consts.defaultZoom;
    return MapPositionAndZoom.withLatLng(tmpLatLng, tmpZoom);
  }

  BehaviorSubject<BottomSheetContent> _bottomSheetContentController = BehaviorSubject<BottomSheetContent>();

  /// Event called whenever the tab index changed.
  Stream<BottomSheetContent> get onBottomSheetContentChanged => _bottomSheetContentController.stream;

  /// The current tab index.
  BottomSheetContent get bottomSheetContent => _bottomSheetContentController.value;

  BehaviorSubject<bool> _searchBarController = BehaviorSubject<bool>.seeded(true);

  /// Event called whenever an actor asked to change the visibility state of the search
  /// bar. Triggered by `showSearchBar` and `hideSearchBar`.
  Stream<bool> get onSearchBarVisibilityChanged => _searchBarController.stream;

  /// Current search bar visibility
  bool get searchBarVisibility => _searchBarController.value;

  /// Point selected to search bar
  BehaviorSubject<Point> _searchPointController = BehaviorSubject<Point>.seeded(null);
  Stream<Point> get onSearchPointChanged => _searchPointController.stream;
  Function(Point) get setSearchPoint => _searchPointController.sink.add;
  Point get searchPoint => _searchPointController.stream.value;

  // Used by animation to smooth the map move function
  AnimationController _animationController;
  Tween _tween;
  Animation _animation;

  // Map components
  MapController _mapController = MapController();

  MapController get mapController => _mapController;
  Map<MapLayerIndex, LayerOptions> _mapLayers;
  Map<MapLayerIndex, bool> _mapLayersVisibility;

  StatefulMapController _statefulMapController;

  StatefulMapController get statefulMapController => _statefulMapController;

  Map<MapLayerIndex, Set<String>> _markersUids = Map<MapLayerIndex, Set<String>>();
  String _selectedMarkerId;

  static PopupController popupLayerController = PopupController();

  // Stream listeners
  StreamSubscription _onLinesDownloaded;
  StreamSubscription _onPOIAroundChanged;
  StreamSubscription _onUserLocationChanged;
  StreamSubscription _onScheduleChanged;
  StreamSubscription _onSelectedRouteChanged;
  StreamSubscription _onFiltersValuesChanged;
  StreamSubscription _onTraficolorChanged;
  StreamSubscription _onEventsChanged;
  StreamSubscription _onBikeChanged;

  // Bottomsheet contents
  BottomSheetCategory currentBottomSheetCategory;
  BottomSheetContent _contentPOIAround;
  BottomSheetContent _contentFilters;

  MapPositionAndZoom _previousMapPositionAndZoom;

  // Key to get BottomSheet's size
  final bottomSheetKey = GlobalKey();

  static List<ReadyLine> lines;
  bool popUpRefresh = false;
  Point markerPoint;

  @override
  void initState() {
    // Initialize animation controller
    _animationController = AnimationController(duration: Duration(milliseconds: 500), vsync: this);

    _statefulMapController = StatefulMapController(mapController: _mapController);

    // Initial all default layers to empty and invisible
    _mapLayers = Map<MapLayerIndex, LayerOptions>();
    _mapLayersVisibility = Map<MapLayerIndex, bool>();
    for (MapLayerIndex x in MapLayerIndex.values) {
      _mapLayers[x] = MarkerLayerOptions(markers: []);
      _mapLayersVisibility[x] = false;
      _markersUids[x] = Set<String>();
    }
    // Then set the base layer to display the map
    TileLayerOptions backgroundLayer = TileLayerOptions(
        urlTemplate: Elevations.darkModeEnabled(false).test, backgroundColor: Elevations.darkModeEnabled(false).e02dp.backgroundColor);
    _mapLayers[MapLayerIndex.background] = backgroundLayer;
    _mapLayersVisibility[MapLayerIndex.background] = true;

    // Finaly, send layers to the stream
    _updateLayers();

    // Once the lines have been downloaded, display the major lines
    _onLinesDownloaded = BlocProvider.master<LinesBloc>().onDownloadLinesCompleted.listen((data) {
      _setLayerVisibility(MapLayerIndex.lines, true);
      _updateLinesLayer();
    });

    // Handle marker to display
    _onPOIAroundChanged = BlocProvider.master<PointsBloc>().onRequestedPointsDownloaded.listen((points) {
      // Update the around markers layout with the list of points
      _updatePOIAroundMarkersLayout(points, Elevations.darkModeEnabled(false));
    });

    // Handle user position
    _mapLayersVisibility[MapLayerIndex.location] = true;
    _onUserLocationChanged = BlocProvider.master<LocationBloc>().onLocationChanged.listen((locationData) {
      // Update the location marker layout with the location
      _updateUserLocationMarkerLayer(locationData);
    });

    _onScheduleChanged = BlocProvider.master<ScheduleBloc>().onSchedulesChanged.listen((schedule) {
      if (schedule != null) _updateDetailLinesLayer(schedule.map<String>((schedule) => schedule.id.split(':')[0] + ':' + schedule.code).toList());
    });

    _onSelectedRouteChanged = BlocProvider.master<RoutesBloc>().onSelectedRouteChanged.listen((selectedRoute) {
      _updateRouteLayer(selectedRoute);
    });

    _onFiltersValuesChanged = BlocProvider.master<FiltersBloc>().onFiltersValueChanged.listen((data) {
      _mapController.onReady.then((_) {
        BlocProvider.master<PointsBloc>().requestPointsInZone(
          _mapController.bounds.west,
          _mapController.bounds.east,
          _mapController.bounds.south,
          _mapController.bounds.north,
        );
        _updateLinesLayer();
      });

      // I update the traficolor layer's visibility based on its filter's value
      if (data.entries.firstWhere((x) => x.key.type == FiltersType.trafic_state).value)
        _setLayerVisibility(MapLayerIndex.traficolor, true);
      else
        _setLayerVisibility(MapLayerIndex.traficolor, false);

      final trueList = data.entries.where((e) => e.value != null && e.value).toList();

      // I update the bike layer's visibility based on its filter's value
      if (trueList.firstWhere((x) => x.key.type == FiltersType.bikes_chrono, orElse: () => null) != null ||
          trueList.firstWhere((x) => x.key.type == FiltersType.bikes_amenage, orElse: () => null) != null ||
          trueList.firstWhere((x) => x.key.type == FiltersType.bikes_difficile, orElse: () => null) != null ||
          trueList.firstWhere((x) => x.key.type == FiltersType.bikes_nonamenage, orElse: () => null) != null ||
          trueList.firstWhere((x) => x.key.type == FiltersType.bikes_tempo, orElse: () => null) != null ||
          trueList.firstWhere((x) => x.key.type == FiltersType.bikes_verte, orElse: () => null) != null) {
        BlocProvider.master<BikeBloc>().downloadBike();
        _setLayerVisibility(MapLayerIndex.bike, true);
      } else
        _setLayerVisibility(MapLayerIndex.bike, false);

      // I update events layer's visibility based on its filter's value
      if (data.entries.firstWhere((x) => x.key.type == FiltersType.trafic_event).value)
        _setLayerVisibility(MapLayerIndex.events, true);
      else
        _setLayerVisibility(MapLayerIndex.events, false);
    });

    _onTraficolorChanged = BlocProvider.master<TraficolorBloc>().onPolylinesChanged.listen((data) {
      _updateTraficolorLayer(data);
    });

    _onBikeChanged = BlocProvider.master<BikeBloc>().onPolylinesChanged.listen((data) {
      if (data != null) {
        _updateBikeLayer(data);
      }
    });

    _onEventsChanged = BlocProvider.master<EventsBloc>().onEventsChanged.listen((data) {
      if (data != null) _updateEventsLayer(data);
    });

    // By default set the content of the bottom sheet to display POI around the user
    setBottomSheetContent(BottomSheetCategory.POIAroundMe);
  }

  @override
  void dispose() {
    // Subscriptions
    _onLinesDownloaded.cancel();
    _onPOIAroundChanged.cancel();
    _onUserLocationChanged.cancel();
    _onScheduleChanged.cancel();
    _onSelectedRouteChanged.cancel();
    _onFiltersValuesChanged.cancel();
    _onTraficolorChanged.cancel();
    _onEventsChanged.cancel();
    _onBikeChanged.cancel();
    _searchPointController.close();

    // Streams
    _positionController.close();
    _layerController.close();
    _bottomSheetContentController.close();
    _searchBarController.close();
  }

  @override
  Ticker createTicker(onTick) {
    return Ticker(onTick);
  }

  void updateMapTheme(Elevations themedElevations) {
    TileLayerOptions backgroundLayer = TileLayerOptions(urlTemplate: themedElevations.test, backgroundColor: themedElevations.e02dp.backgroundColor);
    _mapLayers[MapLayerIndex.background] = backgroundLayer;
    _mapLayersVisibility[MapLayerIndex.background] = true;
    _updateLayers();
  }

  void updateMarkersTheme(Elevations themedElevations) {
    _onPOIAroundChanged = BlocProvider.master<PointsBloc>().onRequestedPointsDownloaded.listen((points) {
      // Update the around markers layout with the list of points
      _updatePOIAroundMarkersLayout(points, themedElevations);
    });
  }

  void updateLieuxMarkersTheme(Elevations themedElevations) {
    onSearchPointChanged.listen((event) {
      updateLieuxLayout(themedElevations, event);
    });
  }

  // ------------ //
  // MAP CONTROLS //
  // ------------ //

  /// Smoothly move the view from the start [MapPositionAndZoom] to the end [MapPositionAndZoom].
  void moveFromTo(MapPositionAndZoom from, MapPositionAndZoom to, {String pointId}) {
    _tween = Tween<MapPositionAndZoom>(begin: from, end: to);
    _animation = _tween.animate(CurvedAnimation(parent: _animationController, curve: Curves.easeInOutQuad))
      ..addListener(() {
        _moveMapView(_animation.value);
        _positionController.sink.add(_animation.value);
      });
    _animationController.reset();
    _animationController.forward().then((_) {
      if (pointId != null) {
        if (!pointId.contains("MCO")) selectPOI(pointId);
      }
    });
  }

  /// Smoothly move the view from the current [MapPositionAndZoom] to the given one.
  void moveTo(MapPositionAndZoom to, {String pointId}) {
    moveFromTo(currentPositionAndZoom, to, pointId: pointId);
  }

  /// Smoothly move the view to the initial [MapPositionAndZoom]. If the user has allowed
  /// the app to use his GPS, the initial [MapPositionAndZoom] is his current GPS position.
  /// The [BuildContext] is used to get the user location from [LocationBloc].
  void resetPositionAndZoom() {
    // Get the destination, by default set to the initial map position and zoom
    MapPositionAndZoom destination = MapPositionAndZoom.initial;

    LocationData location = BlocProvider.master<LocationBloc>().location;
    // If the user has allowed us to use his GPS, we change the destination to
    // his real position if he's inside Grenoble
    if (location != null && Utils.isPositionInGrenoble(location.latitude, location.longitude)) {
      destination = MapPositionAndZoom(location.latitude, location.longitude, Consts.defaultZoom);
    }

    moveFromTo(currentPositionAndZoom, destination);
  }

  /// Save the current [MapPositionAndZoom] in order to be set back later on.
  void saveCurrentPositionAndZoom() {
    _previousMapPositionAndZoom = currentPositionAndZoom;
  }

  /// Restore previously saved [MapPositionAndZoom].
  void restorePreviousSavedPositionAndZoom() {
    if (_previousMapPositionAndZoom == null) return;

    moveFromTo(currentPositionAndZoom, _previousMapPositionAndZoom);
  }

  void _moveMapView(MapPositionAndZoom mapPositionAndZoom) {
    if (!_mapController.ready)
      _mapController.onReady.whenComplete(() {
        _mapController.move(LatLng(mapPositionAndZoom.lat, mapPositionAndZoom.lng), mapPositionAndZoom.zoom);
      });
    else
      _mapController.move(LatLng(mapPositionAndZoom.lat, mapPositionAndZoom.lng), mapPositionAndZoom.zoom);
  }

  // ------------------- //
  // BOTTOMSHEET CONTENT //
  // ------------------- //

  /// Change the content of the [BottomSheet] based on the category. Depending of the category,
  /// you might want to pass some arguments to populate the [bottomSheetContent], such as
  /// a point, a collection of points, a route...
  void setBottomSheetContent(BottomSheetCategory category, {Object arguments}) {
    if (category == BottomSheetCategory.POIAroundMe && category == currentBottomSheetCategory) return;

    currentBottomSheetCategory = category;
    _setLayersVisibility(category);
    switch (category) {
      case BottomSheetCategory.POIAroundMe:
        _setBottomSheetContentToPOIAround();
        break;
      case BottomSheetCategory.schedule:
        // We make sure the given arguments is not null and of type Point
        assert(arguments != null && arguments is Point);
        _setBottomSheetContentToSchedule(arguments);
        break;
      case BottomSheetCategory.POIDetails:
        assert(arguments != null && arguments is Point);
        _setBottomSheetContentToPOIDetails(arguments);
        break;
      case BottomSheetCategory.routes:
        _setBottomSheetContentToRoutes();
        break;
      case BottomSheetCategory.routeDetails:
        assert(arguments != null && arguments is Itinerary);
        _setBottomSheetContentToRouteDetails(arguments);
        break;
      case BottomSheetCategory.traficolor:
        _setBottomSheetContentToNull();
        break;
      case BottomSheetCategory.filters:
        _setBottomSheetContentToFilters();
        break;
      case BottomSheetCategory.events:
        assert(arguments != null && arguments is EventModel);
        _setBottomSheetContentToEvents(arguments);
        break;
    }
  }

  /// Set the bottom sheet of the map to display POI around the user.
  void _setBottomSheetContentToPOIAround() {
    _showSearchBar();
    _bottomSheetContentController.sink.add(_getPOIAroundBottomSheetContent());
  }

  void _setBottomSheetContentToFilters() {
    _showSearchBar();
    _bottomSheetContentController.sink.add(_getFiltersBottomSheetContent());
  }

  void _setBottomSheetContentToSchedule(Point point) {
    _hideSearchBar();
    _bottomSheetContentController.sink.add(_getScheduleDetailsBottomSheetContent(point));
  }

  Future _setBottomSheetContentToPOIDetails(Point point) async {
    _hideSearchBar();

    switch (point.properties.runtimeType) {
      case ClustersProperties:
        _bottomSheetContentController.sink.add(_getScheduleDetailsBottomSheetContent(point));
        break;
      case ParkingProperties:
        _bottomSheetContentController.sink.add(_getParkingBottomSheetContent(point));
        break;
      case CitizProperties:
        _bottomSheetContentController.sink.add(_getCitizBottomSheetContent(point));
        break;
      case CamProperties:
        BlocProvider.master<NavigationBloc>().eventSelected = true;
        _bottomSheetContentController.sink.add(_getCamBottomSheetContent(point));
        break;
      case CitizyeaProperties:
        _bottomSheetContentController.sink.add(_getYeaBottomSheetContent(point));
        break;
      case TierProperties:
        _bottomSheetContentController.sink.add(_getTierBottomSheetContent(point));
        break;
      case PonyProperties:
        _bottomSheetContentController.sink.add(_getPonyBottomSheetContent(point));
        break;
      case AgencyProperties:
        _bottomSheetContentController.sink.add(_getAgencyBottomSheetContent(point));
        break;
      case EnergyProperties:
      case CodeProperties:
      case CarpoolingProperties:
        _bottomSheetContentController.sink.add(_getEnergyBottomSheetContent(point));
        break;
      case IrveProperties:
        _bottomSheetContentController.sink.add(_getIrveBottomSheetContent(point));
        break;
      case BikeServiceProperties:
        _bottomSheetContentController.sink.add(_getIBikeServiceBottomSheetContent(point));
        break;
      default:
        _bottomSheetContentController.sink.add(_getPOIDetailsBottomSheetContent(point));
        break;
    }
  }

  void _setBottomSheetContentToRoutes() {
    _hideSearchBar();
    _bottomSheetContentController.sink.add(_getRouteBottomSheetContent());
  }

  void _setBottomSheetContentToRouteDetails(Itinerary itinerary) {
    _hideSearchBar();
    _bottomSheetContentController.sink.add(_getRouteDetailsBottomSheetContent(itinerary));
  }

  void _setBottomSheetContentToNull() {
    _hideSearchBar();
    _bottomSheetContentController.sink.add(EmptyBottomSheetContent());
  }

  void _setBottomSheetContentToEvents(EventModel event) {
    BlocProvider.master<NavigationBloc>().eventSelected = true;
    _hideSearchBar();
    _bottomSheetContentController.sink.add(_getEventsDetailsBottomSheetContent(event));
  }

  /// Give the content to be displayed inside the bottom sheet if POI around are requested.
  BottomSheetContent _getPOIAroundBottomSheetContent() {
    // If the view has never been created, we create it
    if (_contentPOIAround == null) _contentPOIAround = NewBottomSheetPOIAroundContent(_mapController);
    return _contentPOIAround;
  }

  BottomSheetContent _getFiltersBottomSheetContent() {
    // If the view has never been created, we create it
    if (_contentFilters == null) _contentFilters = FiltersContent(_mapController);
    return _contentFilters;
  }

  /// Give the content to be displayed inside the bottom sheet if schedule are requested.
  BottomSheetContent _getScheduleDetailsBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetScheduleDetailsContent(point);
  }

  BottomSheetContent _getParkingBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetParkingContent(point);
  }

  BottomSheetContent _getAgencyBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetAgencyContent(point);
  }

  BottomSheetContent _getCitizBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetCitizContent(point);
  }

  BottomSheetContent _getYeaBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetYeaContent(point);
  }

  BottomSheetContent _getTierBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetTierContent(point);
  }

  BottomSheetContent _getPonyBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetPonyContent(point);
  }

  BottomSheetContent _getEnergyBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetPoiDetailsContent(point);
  }

  BottomSheetContent _getIrveBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetIrveContent(point);
  }

  BottomSheetContent _getIBikeServiceBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetBikeServiceContent(point);
  }

  BottomSheetContent _getPOIDetailsBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetPoiDetailsContent(point);
  }

  BottomSheetContent _getCamBottomSheetContent(Point point) {
    // We make sure the given point is not null
    assert(point != null);
    return BottomSheetCamContent(point);
  }

  /// Give the content to be displayed inside the bottom sheet if routes is requested.
  BottomSheetContent _getRouteBottomSheetContent() {
    return BottomSheetRouteContent();
  }

  /// Give the content to be displayed inside the bottom sheet if routes is requested.
  BottomSheetContent _getRouteDetailsBottomSheetContent(Itinerary itinerary) {
    assert(itinerary != null);
    return BottomSheetRouteDetailsContent(itinerary);
  }

  /// Give the content to be displayed inside the bottom sheet if events is requested.
  BottomSheetContent _getEventsDetailsBottomSheetContent(EventModel event) {
    assert(event != null);
    return BottomSheetEventContent(event);
  }

  // ----------------- //
  // LAYERS VISIBILITY //
  // ----------------- //

  void _setLayerVisibility(MapLayerIndex index, bool visibility) {
    _mapLayersVisibility[index] = visibility;
    _updateLayers();
  }

  void _setLayersVisibility(BottomSheetCategory category) {
    // TODO : change all map layers according to the requested category
    switch (category) {
      case BottomSheetCategory.POIAroundMe:
        //_onPOIAroundChanged.resume();
        _setLayerVisibility(MapLayerIndex.lieux, true);
        _setLayerVisibility(MapLayerIndex.lines, true);
        _setLayerVisibility(MapLayerIndex.detailLines, false);
        _setLayerVisibility(MapLayerIndex.marker, true);
        _setLayerVisibility(MapLayerIndex.route, false);
        _setLayerVisibility(MapLayerIndex.routeMarkers, false);
        _setLayerVisibility(MapLayerIndex.traficolor,
            BlocProvider.master<FiltersBloc>().filtersValue.entries.firstWhere((x) => x.key.type == FiltersType.trafic_state).value);
        _setLayerVisibility(MapLayerIndex.events,
            BlocProvider.master<FiltersBloc>().filtersValue.entries.firstWhere((x) => x.key.type == FiltersType.trafic_event).value);
        _setLayerVisibility(MapLayerIndex.cam, false);
        break;
      case BottomSheetCategory.schedule:
        //_onPOIAroundChanged.resume();
        _setLayerVisibility(MapLayerIndex.lines, false);
        _setLayerVisibility(MapLayerIndex.detailLines, true);
        _setLayerVisibility(MapLayerIndex.marker, true);
        _setLayerVisibility(MapLayerIndex.route, false);
        _setLayerVisibility(MapLayerIndex.routeMarkers, false);
        _setLayerVisibility(MapLayerIndex.traficolor, false);
        _setLayerVisibility(MapLayerIndex.events, false);
        _setLayerVisibility(MapLayerIndex.cam, false);
        break;
      case BottomSheetCategory.POIDetails:
      case BottomSheetCategory.events:
        // TODO: Handle this case.

        break;
      case BottomSheetCategory.routes:
        //_onPOIAroundChanged.pause();
        _setLayerVisibility(MapLayerIndex.lines, false);
        _setLayerVisibility(MapLayerIndex.detailLines, false);
        _setLayerVisibility(MapLayerIndex.marker, false);
        _setLayerVisibility(MapLayerIndex.route, true);
        _setLayerVisibility(MapLayerIndex.routeMarkers, true);
        _setLayerVisibility(MapLayerIndex.traficolor, false);
        _setLayerVisibility(MapLayerIndex.events, false);
        _setLayerVisibility(MapLayerIndex.cam, false);
        _setLayerVisibility(MapLayerIndex.bike, false);
        break;
      case BottomSheetCategory.routeDetails:
        //_onPOIAroundChanged.pause();
        _setLayerVisibility(MapLayerIndex.lines, false);
        _setLayerVisibility(MapLayerIndex.detailLines, false);
        _setLayerVisibility(MapLayerIndex.marker, false);
        _setLayerVisibility(MapLayerIndex.route, true);
        _setLayerVisibility(MapLayerIndex.routeMarkers, true);
        _setLayerVisibility(MapLayerIndex.traficolor, false);
        _setLayerVisibility(MapLayerIndex.events, false);
        _setLayerVisibility(MapLayerIndex.cam, false);
        break;
      case BottomSheetCategory.traficolor:
        // case BottomSheetCategory.events:
        //_onPOIAroundChanged.pause();
        _setLayerVisibility(MapLayerIndex.lines, false);
        _setLayerVisibility(MapLayerIndex.detailLines, false);
        _setLayerVisibility(MapLayerIndex.marker, false);
        _setLayerVisibility(MapLayerIndex.route, false);
        _setLayerVisibility(MapLayerIndex.routeMarkers, false);
        _setLayerVisibility(MapLayerIndex.traficolor, true);
        _setLayerVisibility(MapLayerIndex.events, true);
        _setLayerVisibility(MapLayerIndex.cam, true);
        _setLayerVisibility(MapLayerIndex.bike, false);
        break;
    }
  }

  // -------------- //
  // LAYERS UPDATES //
  // -------------- //

  /// Send all visible layers to the Stream.
  void _updateLayers() {
    List<LayerOptions> displayedLayers = List();

    for (int i = 0; i < _mapLayers.entries.length; i++) {
      if (_mapLayersVisibility[MapLayerIndex.values[i]] != null && _mapLayersVisibility[MapLayerIndex.values[i]])
        displayedLayers.add(_mapLayers[MapLayerIndex.values[i]]);
    }
    _layerController.sink.add(displayedLayers);
  }

  /// Explicitly set a specific layer of the map.
  void _setLayer(MapLayerIndex index, LayerOptions layer, {bool notifyStream = true}) {
    _mapLayers[index] = layer;
    _updateLayers();
  }

  Map<String, StatefulMarker> _buildMarkers(List<Point> pointData, Elevations themedElevations) {
    Map<String, StatefulMarker> markers = {};
    for (final point in pointData) {
      markers[point.properties.id] = StatefulMarker(
        height: 60,
        width: 60,
        anchorAlign: AnchorAlign.top,
        point: Utils.coordinatesFromArray(point.geometry.coordinates),
        builder: (context, state) {
          double orientation;
          if (point.properties.type == PropertiesType.cam) {
            if ((point.properties as CamProperties).code == 'RondeauNord') {
              orientation = math.pi / 180 * 270;
            } else if ((point.properties as CamProperties).code == 'RondeauEst') {
              orientation = math.pi / 260 * 0;
            } else if ((point.properties as CamProperties).code == 'RondeauSud') {
              orientation = math.pi / 360 * (point.properties as CamProperties).cap.toDouble();
            } else {
              orientation = math.pi / 300 * (point.properties as CamProperties).cap.toDouble();
            }
          }
          bool isSelected = state['isSelected'];
          return GestureDetector(
            child: point.properties.type == PropertiesType.cam
                ? Transform.rotate(angle: orientation, child: Image.asset(point.properties.markerPath(themedElevations, isSelected: isSelected)))
                : Image.asset(point.properties.markerPath(themedElevations, isSelected: isSelected)),
            onTap: () {
              lines = null;
              markerPoint = point;
              // popupLayerController.hidePopup();
              setBottomSheetContent(BottomSheetCategory.POIDetails, arguments: point);
              moveTo(MapPositionAndZoom.withLatLng(Utils.coordinatesFromArray(point.geometry.coordinates), 17));
              selectPOI(point.properties.id);
              // popupLayerController.togglePopup(_statefulMapController.getMarker(point.properties.id));
            },
          );
        },
        state: <String, dynamic>{"isSelected": (_selectedMarkerId != null && _selectedMarkerId == point.properties.id)},
      );
    }
    return markers;
  }

  void _updatePOIAroundMarkersLayout(List<Point> points, Elevations themedElevations) {
    if (points == null) return;

    // I select only visible points (based on zoom level) and sort them by
    // latitude to avoid pins being on top of an other one
    List<Point> sortedVisiblePoints = points.where((point) => point.properties.isVisible(_mapController.zoom)).toList()
      ..sort((a, b) =>
          Utils.coordinatesFromArray(b.geometry.coordinates).latitude.compareTo(Utils.coordinatesFromArray(a.geometry.coordinates).latitude));

    _markersUids[MapLayerIndex.marker].clear();
    sortedVisiblePoints.forEach((e) => _markersUids[MapLayerIndex.marker].add(e.properties.id));

    _statefulMapController.onReady.then((_) async {
      final markers = _buildMarkers(sortedVisiblePoints, themedElevations);
      _statefulMapController.addStatefulMarkers(markers);
      // I create the layer that will hold markers for each nearby points
      final layer = MarkerLayerOptions(
        markers: _statefulMapController.getMarkers(_markersUids[MapLayerIndex.marker].toList()),
      );

//      final layer = PopupMarkerLayerOptions(
//          markers: _statefulMapController.getMarkers(_markersUids[MapLayerIndex.marker].toList()),
//          popupSnap: PopupSnap.top,
//          popupController: popupLayerController,
//          popupBuilder: (BuildContext _, Marker marker) {
//
//            return Padding(
//              padding: const EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 8.0),
//              child: Container(
//                padding: const EdgeInsets.all(8.0),
//                  decoration: BoxDecoration(
//                    borderRadius: BorderRadius.circular(6),
//                    color: Colors.white,
//                  ),
//                  child: markersPopUp(marker, markerPoint)),
//            );
//          });
      // And ask the MapBloc to add it to the map layers

      _setLayer(MapLayerIndex.marker, layer);
    });
  }

  Widget markersPopUp(Marker marker, Point point) {
    return FutureBuilder<List<ReadyLine>>(
        future: BlocProvider.master<LinesBloc>().nearbyLinesPopUp(marker.point),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container(height: 40, width: 40, color: Colors.transparent, child: AnimatedLoadingLogo());

          return Wrap(
            alignment: WrapAlignment.start,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Text(
                      point.properties.name.toUpperCase(),
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Wrap(
                    alignment: WrapAlignment.center,
                    spacing: 8,
                    runSpacing: 8,
                    children: snapshot.data
                        .map((line) => LineDisplayPopUp(
                              size: 20,
                              line: line.line,
                            ))
                        .toList(),
                  ),
                ],
              ),
            ],
          );
        });
  }

  Future<void> updateCamMarkersLayout(Elevations themedElevations) async {
    List<Point> points = await BlocProvider.master<PointsBloc>().requestCam();

    if (points == null) return;

    _markersUids[MapLayerIndex.cam].clear();
    points.forEach((e) => _markersUids[MapLayerIndex.cam].add(e.properties.id));
    _statefulMapController.onReady.then((_) {
      final markers = _buildMarkers(points, themedElevations);
      _statefulMapController.addStatefulMarkers(markers);
      // I create the layer that will hold markers for each nearby points
      final layer = MarkerLayerOptions(
        markers: _statefulMapController.getMarkers(_markersUids[MapLayerIndex.cam].toList()),
      );
      // And ask the MapBloc to add it to the map layers
      _setLayer(MapLayerIndex.cam, layer);
    });
  }

  Future<void> updateLieuxLayout(Elevations themedElevations, Point point) async {
    List<Point> listPoints = await BlocProvider.master<PointsBloc>().requestLieux();
    if (point == null) return;
    List<Point> points = [];

    for (Point p in listPoints){
      if (p.properties.id == point.properties.id){
        points.add(p);
      }
    }

    if (points == null) return;

    _markersUids[MapLayerIndex.lieux].clear();
    points.forEach((e) => _markersUids[MapLayerIndex.lieux].add(e.properties.id));
    _statefulMapController.onReady.then((_) {
      final markers = _buildMarkers(points, themedElevations);
      _statefulMapController.addStatefulMarkers(markers);
      // I create the layer that will hold markers for each nearby points
      final layer = MarkerLayerOptions(
        markers: _statefulMapController.getMarkers(_markersUids[MapLayerIndex.lieux].toList()),
      );
      // And ask the MapBloc to add it to the map layers
      _setLayer(MapLayerIndex.lieux, layer);
    });
  }

  void _updateUserLocationMarkerLayer(LocationData locationData) {
    final markerId = 'locationData';
    _statefulMapController.onReady.then((_) {
      final marker = StatefulMarker(
        height: 64,
        width: 64,
        point: LatLng(locationData.latitude, locationData.longitude),
        builder: (_, __) => Container(
          height: 64,
          width: 64,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(64),
            color: Colors.blue.withAlpha(80),
          ),
          child: Center(
            child: Container(
              height: 12,
              width: 12,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(color: Colors.white, width: 2),
                color: Colors.blue,
              ),
            ),
          ),
        ),
        state: {},
      );

      _statefulMapController.addStatefulMarker(name: markerId, statefulMarker: marker);

      // I create the layer that will hold a marker for the user position
      final layer = MarkerLayerOptions(markers: [_statefulMapController.getMarker(markerId)]);

      // And ask the MapBloc to add it to the map layers
      _setLayer(MapLayerIndex.location, layer);
    });
  }

  /// Update `DetailLine` layer with a set of data.
  void _updateDetailLinesLayer(List<String> linesIds) {
    List<ReadyLine> lines = BlocProvider.master<LinesBloc>().lines.where((line) => linesIds.contains(line.line.id)).toList();

    List<Polyline> polylines = [];
    for (final line in lines) if (line.polyline != null) polylines.addAll(line.polyline);

    PolylineLayerOptions layer = PolylineLayerOptions(polylines: polylines, polylineCulling: true);
    _setLayer(MapLayerIndex.detailLines, layer);
  }

  void _updateLinesLayer() {
    List<ReadyLine> selectedLines = List();
    for (MapEntry<Filter, bool> filtersValue in BlocProvider.master<FiltersBloc>().filtersValue.entries) {
      if (filtersValue.value != null && filtersValue.value) {
        final lines = BlocProvider.master<LinesBloc>().lines.where((readyLine) => readyLine.line.type == filtersValue.key.requestId).toList();
        if (lines != null) selectedLines.addAll(lines);
      }
    }

    List<Polyline> polylines = [];
    for (final line in selectedLines) if (line.polyline != null) polylines.addAll(line.polyline);

    PolylineLayerOptions layer = PolylineLayerOptions(polylines: polylines, polylineCulling: true);
    _setLayer(MapLayerIndex.lines, layer);
  }

  double _getBottomSheetSize() {
    final keyContext = bottomSheetKey.currentContext;
    if (keyContext != null) {
      final box = keyContext.findRenderObject() as RenderBox;
      if (box != null) return box.size.height;
    }
    return null;
  }

  void _updateRouteLayer(ItineraryRow selectedRoute) {
    PolylineLayerOptions routeLayer;
    MarkerLayerOptions markerLayer;
    LatLngBounds bounds;
    if (selectedRoute == null) {
      routeLayer = PolylineLayerOptions(polylineCulling: true);
      markerLayer = MarkerLayerOptions();
    } else {
      bounds = getItineraryFittingBounds(selectedRoute.itinerary);
      routeLayer = PolylineLayerOptions(polylines: selectedRoute.itinerary.polylines, polylineCulling: true);
      markerLayer = MarkerLayerOptions(markers: [
        Marker(
          height: 32,
          width: 47,
          anchorPos: AnchorPos.align(AnchorAlign.top),
          point: selectedRoute.itinerary.departure,
          builder: (context) => Image.asset(globalElevations.assetsPath + 'start.png'),
        ),
        Marker(
          height: 32,
          width: 47,
          anchorPos: AnchorPos.align(AnchorAlign.top),
          point: selectedRoute.itinerary.destination,
          builder: (context) => Image.asset(globalElevations.assetsPath + 'end.png'),
        )
      ]);
    }

    _setLayer(MapLayerIndex.route, routeLayer);
    _setLayer(MapLayerIndex.routeMarkers, markerLayer);

    if (bounds != null) {
      _mapController.fitBounds(
        bounds,
        options: FitBoundsOptions(padding: EdgeInsets.only(top: 40, bottom: _getBottomSheetSize() + 20 ?? 300, left: 50, right: 50)),
      );
    }
  }

  void _updateTraficolorLayer(List<Polyline> data) {
    PolylineLayerOptions layer = PolylineLayerOptions(polylines: data, polylineCulling: true);
    _setLayer(MapLayerIndex.traficolor, layer,
        notifyStream: BlocProvider.master<FiltersBloc>().filtersValue.entries.firstWhere((x) => x.key.type == FiltersType.trafic_state).value);
  }

  void _updateEventsLayer(List<EventModel> data) {
    DateFormat dateFormat = DateFormat("dd/MM/yyyy HH:mm");
    Function(EventModel) eventWithLatLng = (event) => event.latitude > 0 && event.longitude > 0 && event.visibleVoiture;
    Function(EventModel) eventWithinDate =
        (event) => dateFormat.parse(event.dateDebut).isBefore(DateTime.now()) && dateFormat.parse(event.dateFin).isAfter(DateTime.now());
    Function(EventModel) eventAlmostThere = (event) =>
        dateFormat.parse(event.dateDebut).isAfter(DateTime.now().add(Duration(days: 1))) &&
        !DateTime.now().isAfter(dateFormat.parse(event.dateDebut));

    List<EventModel> events = data.where((event) => eventWithLatLng(event) && (eventWithinDate(event) || eventAlmostThere(event))).toList();
    events.sort((a, b) => b.latitude.compareTo(a.latitude));

    MarkerLayerOptions layer = MarkerLayerOptions(
        markers: events.map((event) {
      LatLng position = LatLng(event.latitude, event.longitude);
      return Marker(
        height: 60,
        width: 60,
        anchorPos: AnchorPos.align(AnchorAlign.top),
        point: position,
        builder: (context) => GestureDetector(
          child: Image.asset(event.markerPath()),
          onTap: () {
            setBottomSheetContent(BottomSheetCategory.events, arguments: event);
            moveTo(MapPositionAndZoom.withLatLng(position, 17));
          },
        ),
      );
    }).toList());
    _setLayer(MapLayerIndex.events, layer,
        notifyStream: BlocProvider.master<FiltersBloc>().filtersValue.entries.firstWhere((x) => x.key.type == FiltersType.trafic_event).value);
  }

  void _updateBikeLayer(List<Polyline> data) {
    PolylineLayerOptions layer = PolylineLayerOptions(polylines: data, polylineCulling: true);
    _setLayer(MapLayerIndex.bike, layer,
        notifyStream: BlocProvider.master<FiltersBloc>().filtersValue.entries.firstWhere((x) => x.key.type == FiltersType.bikes).value);
  }

  // ------------- //
  // MISCELLANEOUS //
  // ------------- //

  bool onBackButtonPress() {
    switch (currentBottomSheetCategory) {
      case BottomSheetCategory.schedule:
      case BottomSheetCategory.POIDetails:
      case BottomSheetCategory.traficolor:
      case BottomSheetCategory.events:
        setBottomSheetContent(BottomSheetCategory.POIAroundMe);
        break;
      case BottomSheetCategory.routes:
        setBottomSheetContent(BottomSheetCategory.POIAroundMe);
        BlocProvider.master<NavigationBloc>().changeTab(NavigationCategory.route);
        break;
      case BottomSheetCategory.routeDetails:
        setBottomSheetContent(BottomSheetCategory.routes);
        break;
      case BottomSheetCategory.POIAroundMe:
      default:
        return false;
    }
    return true;
  }

  void _showSearchBar() {
    _searchBarController.sink.add(true);
  }

  void _hideSearchBar() {
    _searchBarController.sink.add(false);
  }

  // Récupérer les lines d'un arret
  Future<List<Line>> getLinesFromStop(longitude, latitude, code) async {
    List<Line> resultIconList = [];
    Dio dio = Dio();

    if (code != '') {
      var allRoutes = await BlocProvider.master<WebServices>().get('routers/default/index/clusters/$code/routes');
      List<IconDataModel> iconDataList = (jsonDecode(allRoutes.body) as List).map<IconDataModel>((data) => IconDataModel.fromJson(data)).toList();
      int tempIndex = -1;
      final response =
          await dio.get('https://datatest.metromobilite.fr/api/linesNear/json?x=${longitude.toString()}&y=${latitude.toString()}&dist=500');
      if (response.statusCode == 200) {
        for (var i in response.data) {
          tempIndex = iconDataList.indexWhere((el) => el.id.contains(i));

          if (tempIndex != -1) {
            resultIconList.add(Line.fromStandard(
              iconDataList[tempIndex].id,
              iconDataList[tempIndex].gtfsId,
              iconDataList[tempIndex].shortName,
              iconDataList[tempIndex].longName,
              iconDataList[tempIndex].color,
              iconDataList[tempIndex].textColor,
              iconDataList[tempIndex].mode,
              iconDataList[tempIndex].type,
            ));
          }
        }

        return resultIconList;
      }

      return [];
    } else {
      var allRoutes = await BlocProvider.master<WebServices>().get('routers/default/index/routes');
      List<IconDataModel> iconDataList = (jsonDecode(allRoutes.body) as List).map<IconDataModel>((data) => IconDataModel.fromJson(data)).toList();
      int tempIndex = -1;
      print('https://datatest.metromobilite.fr/api/linesNear/json?x=${longitude.toString()}&y=${latitude.toString()}&dist=500');
      final response =
          await dio.get('https://datatest.metromobilite.fr/api/linesNear/json?x=${longitude.toString()}&y=${latitude.toString()}&dist=500');
      if (response.statusCode == 200) {
        for (var i in response.data) {
          tempIndex = iconDataList.indexWhere((el) => el.id.contains(i));

          if (tempIndex != -1) {
            resultIconList.add(Line.fromStandard(
              iconDataList[tempIndex].id,
              iconDataList[tempIndex].gtfsId,
              iconDataList[tempIndex].shortName,
              iconDataList[tempIndex].longName,
              iconDataList[tempIndex].color,
              iconDataList[tempIndex].textColor,
              iconDataList[tempIndex].mode,
              iconDataList[tempIndex].type,
            ));
          }
        }

        return resultIconList;
      }

      return [];
    }
  }

  Future<void> deleteFavoriteCluster(Favorite fav) async {
    database.favoriteDao.deleteFavorite(fav);
  }

  Future<void> addLinesToFavorites(BuildContext context, String longitude, String latitude, dynamic point, Function(bool) update) async {
    List<Line> lines = [];

    if (point is Point){
      lines = await getLinesFromStop(longitude, latitude, (point.properties as ClustersProperties).code);
    }else {
      lines = await getLinesFromStop(longitude, latitude, '');
    }


    //Condition si il y a qu'une seule ligne ne pas afficher la modale.
    if (lines.length < 2) {
      FavoriteCluster favoriteCluster = FavoriteCluster(stop: point, lines: lines);
      //Ajouter le favoris de type cluster
      database.favoriteDao.addFavorite(favoriteCluster);
    } else {
      showDialog<SelectionData>(barrierDismissible: false, context: context, builder: (BuildContext context) => LineSelectionDialog(lines, point))
          .then((value) {
        List<String> tempFav = [];

        if (value.value) {
          if (value.selectAll)
            lines.forEach((j) => value.selectedLines.add(j));
          else
            value.selectedLines.forEach((j) => tempFav.add(j.shortName));

          if (point is Point) {
          } else {
            for (Favorite fav in point) {
              if (!tempFav.contains(fav.lines[0])) {
                database.favoriteDao.deleteFavorite(fav);
              }
            }
          }

          value.selectedLines.forEach((line) {
            Point clusterPoint;
            if (point is Point) {
              clusterPoint = point;
            } else {
              List<Favorite> temp = point;
              clusterPoint = temp[0].point1;
            }
            List<Line> test = [];
            test.add(line);

            List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;
            List<String> tempLines = [];
            if (listFavs != null) {
              bool isFav = true;
              for (Favorite favs in listFavs) {
                if (point is Point) {
                  (favs.point1.properties.name == point.properties.name) ? isFav = true : isFav = false;
                }
                if (point is Favorite) {
                  (favs.point1.properties.name == point.point1.properties.name) ? isFav = true : isFav = false;
                }

                if (favs.details.type == FavoriteType.cluster && favs.lines[0] != null && isFav) {
                  tempLines.add(favs.lines[0]);
                }
              }
            }

            if (!tempLines.contains(test[0].shortName)) {
              FavoriteCluster favoriteCluster = FavoriteCluster(stop: clusterPoint, lines: test);
              database.favoriteDao.addFavorite(favoriteCluster);
            }
          });
        } else if (update != null) update(false);
      });
    }
  }

  Future<void> addLinesDeleteOldFavs(BuildContext context, String longitude, String latitude, Favorite fav, Point p) async {
    List<Line> lines = await getLinesFromStop(longitude, latitude, (p.properties as ClustersProperties).code);

    Point point = fav.point1;

    showDialog<SelectionData>(barrierDismissible: false, context: context, builder: (BuildContext context) => LineSelectionDialog(lines, point))
        .then((value) {
      if (value.value) {
        if (value.selectAll) lines.forEach((j) => value.selectedLines.add(j));

        for (Line line in value.selectedLines) {
          List<Line> test = [];
          test.add(line);
          FavoriteCluster favoriteCluster = FavoriteCluster(stop: point, lines: test);
          database.favoriteDao.addFavorite(favoriteCluster);
        }
        database.favoriteDao.deleteFavorite(fav);
      }
    });
  }

  void selectPOI(String pointId) {
    if (_selectedMarkerId != null && _selectedMarkerId != pointId) {
      _statefulMapController.mutateMarker(
        name: _selectedMarkerId,
        property: 'isSelected',
        value: false,
      );
    }
    _selectedMarkerId = pointId;
    _statefulMapController.mutateMarker(
      name: pointId,
      property: 'isSelected',
      value: true,
    );
  }

  void unselectPOI() {
    if (_selectedMarkerId != null)
      _statefulMapController.mutateMarker(
        name: _selectedMarkerId,
        property: 'isSelected',
        value: false,
      );
    _selectedMarkerId = null;
  }
}

/// Class used to specify position and zoom of the map.
class MapPositionAndZoom {
  final double lat;
  final double lng;
  final double zoom;

  MapPositionAndZoom(this.lat, this.lng, this.zoom);

  factory MapPositionAndZoom.withLatLng(LatLng latlng, double zoom) => MapPositionAndZoom(latlng.latitude, latlng.longitude, zoom);

  MapPositionAndZoom operator -(MapPositionAndZoom other) => MapPositionAndZoom(this.lat - other.lat, this.lng - other.lng, this.zoom - other.zoom);

  MapPositionAndZoom operator +(MapPositionAndZoom other) => MapPositionAndZoom(this.lat + other.lat, this.lng + other.lng, this.zoom + other.zoom);

  MapPositionAndZoom operator *(double other) => MapPositionAndZoom(this.lat * other, this.lng * other, this.zoom * other);

  static MapPositionAndZoom initial = MapPositionAndZoom(Consts.placeVictorHugoLat, Consts.placeVictorHugoLng, Consts.defaultZoom);
}

/// Order of MapLayerIndex's value determine how layers will be displayed on the map.
enum MapLayerIndex {
  background, // 1st to be displayed
  lines, // containes tram lines
  detailLines, // used to display lines of a selected stop
  traficolor,
  bike,
  route, // used to display the drawing of the route
  routeMarkers, // used to display Start and End markers of the route
  marker, // display all the points around the user location
  events,
  cam,
  location, // display the user location
  lieux
}

enum BottomSheetCategory { POIAroundMe, schedule, POIDetails, routes, routeDetails, traficolor, events, filters }
