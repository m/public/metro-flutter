import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:location/location.dart';
import 'package:map_controller/map_controller.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/bottomsheet/contents/empty.dart';
import 'package:metro/bottomsheet/contents/route.dart';
import 'package:metro/bottomsheet/widgets/half_bottom_sheet.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/disturbances/pages/disturbances.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/bottomsheet/widgets/bottom_sheet.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/search/widgets/search_bar.dart';
import 'package:latlong/latlong.dart';
import 'package:preferences/preference_service.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Timer _mapMovementTimer;
  StreamSubscription _onFiltersChanged;
  CustomBottomSheetController _bottomSheetController;
  int i = 0;
  MapBloc _mapBloc;
  StatefulMapController _statefulMapController;

  StatefulMapController get statefulMapController => _statefulMapController;
  final PopupController _popupLayerController = PopupController();


  @override
  void initState() {
    super.initState();
    _mapBloc = BlocProvider.of<MapBloc>(context);

    _bottomSheetController = CustomBottomSheetController();

    bool preferencesHomeBool = PrefService.getBool("preferences_home_bool");
    if (preferencesHomeBool != null) {
      if (preferencesHomeBool) {
        int _page = PrefService.getInt("preferences_home");
        _page != null
            ? BlocProvider.master<NavigationBloc>().changeTabByIndex(_page)
            : BlocProvider.master<NavigationBloc>().changeTabByIndex(0);
        PrefService.setBool("preferences_home_bool", false);
      }
    }
  }

  @override
  void dispose() {
    _onFiltersChanged?.cancel();
    _mapBloc.unselectPOI();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        // Map Widget
        StreamBuilder<List<LayerOptions>>(
          stream: _mapBloc.onLayersChanged,
          builder: (context, snapshot) {
            // If there is no layer to display, a loading circle is returned
            if (snapshot.data == null || snapshot.data.isEmpty)
              return const AnimatedLoadingLogo();
            return FlutterMap(
              mapController: _mapBloc.mapController,
              layers: snapshot.data,
              options: MapOptions(
                onTap: (_) {
                  //MapBloc.popupLayerController.hidePopup();
                },
                // plugins: [PopupMarkerPlugin()],
                center: moveToPosition(),
                zoom: Consts.defaultZoom,
                minZoom: Consts.minZoom,
                maxZoom: Consts.maxZoom,
                swPanBoundary: LatLng(Consts.grenobleMinLat, Consts.grenobleMinLng),
                nePanBoundary: LatLng(Consts.grenobleMaxLat, Consts.grenobleMaxLng),
                // bounds: bounds,
                onPositionChanged: (_, __) {
                  // Using a Timer, I can detect when onPositionChanged has ended
                  // and avoid getting callback on every single frame
                  if (_mapMovementTimer != null) _mapMovementTimer.cancel();

                  final bounds = _mapBloc.mapController.bounds;

                  Consts.refreshLat = _mapBloc.mapController.center.latitude;
                  Consts.refreshLng = _mapBloc.mapController.center.longitude;

                  _mapMovementTimer = Timer(Duration(milliseconds: 100), () {
                    // Here, the map didn't moved for 250ms. I request points in the view bounds
                    BlocProvider.master<PointsBloc>().requestPointsInZone(
                        bounds.west, bounds.east, bounds.south, bounds.north);
                  });
                },
              ),
            );
          },
        ),
        // Search bar / back button
        StreamBuilder<bool>(
          stream: _mapBloc.onSearchBarVisibilityChanged,
          initialData: _mapBloc.searchBarVisibility,
          builder: (context, snapshot) {
            Widget displayedWidget;

            if (snapshot.data)
              displayedWidget = _buildSearchBar();
            else
              displayedWidget = _buildSearchBarBack();

            return AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (child, animation) {
                // Create an animation for both the SearchBar and the back button.
                if (child is SearchBar) {
                  // Sliding from/to the top
                  return SlideTransition(
                      child: child,
                      position:
                      Tween<Offset>(begin: Offset(0, -2), end: Offset(0, 0))
                          .animate(animation));
                } else {
                  // Sliding from/to the left
                  return SlideTransition(
                      child: child,
                      position: Tween<Offset>(
                          begin: Offset(-0.25, 0), end: Offset(0, 0))
                          .animate(animation));
                }
              },
              child: displayedWidget,
            );
          },
        ),
        // Floating action buttons
        Positioned(
          top: 68,
          right: 8,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 36,
                width: 36,
                child: FloatingActionButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  heroTag: 'resetPosition',
                  onPressed: () => _mapBloc.resetPositionAndZoom(),
                  child: Icon(Icons.my_location),
                ),
              ),
            ],
          ),
        ),
        StreamBuilder<List<EventModel>>(
            stream: BlocProvider
                .of<EventsBloc>(context)
                .onExceptionnalEventsChanged,
            builder: (context, AsyncSnapshot<List<EventModel>> snapshot) {
              bool showBadge;
              if (snapshot.hasData && snapshot.data.isNotEmpty) {
                showBadge = true;
              } else {
                showBadge = false;
              }
              return Visibility(
                visible: showBadge,
                child: Positioned(
                  top: 116,
                  right: 8,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 36,
                        width: 36,
                        child: FloatingActionButton(
                          backgroundColor:
                          globalElevations.e01dp.backgroundColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(
                                  color: Color(0xFFf96c6c), width: 0.8)),
                          onPressed: () =>
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DisturbancesPage(
                                            0,
                                            importantEvent: true,
                                          ))),
                          child: Text(
                            "!",
                            style: TextStyle(
                                color: Color(0xFFf96c6c),
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
        // BottomSheet
        StreamBuilder<BottomSheetContent>(
          key: _mapBloc.bottomSheetKey,
          stream: _mapBloc.onBottomSheetContentChanged,
          initialData: _mapBloc.bottomSheetContent,
          builder: (context, snapshot) {
            if (!snapshot.hasData || snapshot.data == null)
              return const AnimatedLoadingLogo();

            if (snapshot.data is EmptyBottomSheetContent) {
              // If we want to hide the bottomsheet
              return Container();
            } else if (snapshot.data is BottomSheetRouteContent) {
              // Or if the bottomsheet should be fixed at 50% of the screen
              return HalfBottomSheet(
                header: snapshot.data.header,
                body: snapshot.data.body,
              );
            } else {
              // Otherwise, we simply display the default bottomsheet's behaviour
              _bottomSheetController.resizeHeader();

              return CustomBottomSheet(
                controller: _bottomSheetController,
                headerKey: snapshot.data.headerKey,
                header: snapshot.data.header,
                body: snapshot.data.body,
              );
            }
          },
        )
      ],
    );
  }

  LatLng moveToPosition() {
    LocationData location = BlocProvider
        .master<LocationBloc>()
        .location;
    return(Consts.refreshLng == null || Consts.refreshLat == null) ? (location != null &&
        Utils.isPositionInGrenoble(location.latitude, location.longitude))
        ? LatLng(location.latitude, location.longitude)
        : LatLng(Consts.placeVictorHugoLat, Consts.placeVictorHugoLng)
        : LatLng(Consts.refreshLat, Consts.refreshLng);
  }

  /// Return the SearchBar widget that should sits on top of the screen
  Widget _buildSearchBar() {
    return SearchBar(
      text: translations.text('search_bar.text'),
      hasShadow: true,
      isSchedulePage: false,
      isMapPage: true,
      onSearchComplete: (point) {
        // Once the user has selected a point from the search screen
        if (point != null) {
          // Display POI layers
          _updatePOILayers(point);
          // Center the point on the map
          _mapBloc.moveTo(
              MapPositionAndZoom.withLatLng(
                  Utils.coordinatesFromArray(point.geometry.coordinates), 17),
              pointId: point.properties.id);
          // And show the details of the point
          _mapBloc.setBottomSheetContent(BottomSheetCategory.POIDetails,
              arguments: point);
        }
      },
    );
  }

  Widget _buildSearchBarBack() {
    return SearchBar(
      onlyBackButton: true,
      text: translations.text('search_bar.text'),
      hasShadow: true,
      isSchedulePage: true,
      leading: SearchBarLeading.back,
      onSearchComplete: (point) {
        // Once the user has selected a point from the search screen
        if (point != null) {
          // Center the point on the map
          _mapBloc.moveTo(MapPositionAndZoom.withLatLng(
              Utils.coordinatesFromArray(point.geometry.coordinates), 17));
          // And show the details of the point
          _mapBloc.setBottomSheetContent(BottomSheetCategory.POIDetails,
              arguments: point);
        }
      },
    );
  }

  /// Return a generic back button that should sits on top at the screen
  /// when the search bar is not visible
  Widget _buildBackButton() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        height: 42,
        width: 42,
        margin: EdgeInsets.all(12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(21),
            color: globalElevations.e02dp.backgroundColor,
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black45,
                offset: Offset(0, 3),
                blurRadius: 6,
              )
            ]),
        child: Center(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              BlocProvider.of<NavigationBloc>(context).backNavigation();
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.red,
              size: 24,
            ),
          ),
        ),
      ),
    );
  }

  void _updatePOILayers(Point point) {
    Filter filter = BlocProvider.of<FiltersBloc>(context)
        .getFilterFromPropType(point.properties.type);
    if (filter != null &&
        !BlocProvider.of<FiltersBloc>(context).getFilterValue(filter.type)) {
      _updateFilters(filter);
    }
  }

  void _updateFilters(Filter filter) {
    BlocProvider.of<FiltersBloc>(context).setFilterValue(filter, true);
    if (filter.subFilters != null) {
      filter.subFilters.forEach((Filter subFilter) {
        BlocProvider.of<FiltersBloc>(context).setFilterValue(subFilter, true);
      });
    }
  }
}
