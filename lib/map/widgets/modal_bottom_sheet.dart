import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/localization/translations.dart';

class CustomModalBottomSheet {
  static show(BuildContext context, Point point, bool isPointFavorited,
      {VoidCallback onItineraryTap,
      VoidCallback onNextDeapartureTap,
      VoidCallback onVisualizeOnMapTap,
      VoidCallback onAddFavoriteTap}) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          List<Widget> tiles = List<Widget>();
          // Header
          tiles.add(Container(
            padding: EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                point.properties.name,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ));

          // Commun tiles
          tiles.add(ListTile(
            leading: Icon(Icons.directions),
            title: Text(translations.text('modal_bottom_sheet.itinerary')),
            onTap: () {
              Navigator.pop(context);
              if (onItineraryTap != null) onItineraryTap();
            },
          ));

          // Additionnal tiles based on point type
          if (point.properties.type == PropertiesType.clusters) {
            tiles.add(ListTile(
              leading: Icon(Icons.departure_board),
              title: Text(translations.text('modal_bottom_sheet.schedule')),
              onTap: () {
                Navigator.pop(context);
                if (onNextDeapartureTap != null) onNextDeapartureTap();
              },
            ));
          }

          // Commun tiles
          tiles.add(ListTile(
            leading: Icon(Icons.pin_drop),
            title: Text(translations.text('modal_bottom_sheet.visualize')),
            onTap: () {
              Navigator.pop(context);
              if (onVisualizeOnMapTap != null) onVisualizeOnMapTap();
            },
          ));

          // Additionnal tiles based on point type
          if (point.properties.type == PropertiesType.clusters) {
            tiles.add(ListTile(
              leading: Icon(
                isPointFavorited ? Icons.favorite_border : Icons.favorite
              ),
              title: Text(
                isPointFavorited ? translations.text('modal_bottom_sheet.unfavorite') : translations.text('modal_bottom_sheet.favorite')
              ),
              onTap: () {
                Navigator.pop(context);
                if (onAddFavoriteTap != null) onAddFavoriteTap();
              },
            ));
          }

          // We need this empty GestureDetector to avoid mobalBottomSheet
          // from closing when tapping on the header
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {},
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: tiles,
            ),
          );
        });
  }
}
