import 'package:flutter/material.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/localization/translations.dart';

class SelectionData {
  final bool value;
  final bool selectAll;
  final List<Line> selectedLines;

  SelectionData({
    @required this.value,
    @required this.selectAll,
    @required this.selectedLines,
  });
}

class LineSelectionDialog extends StatefulWidget {
  final List<Line> lines;
  final dynamic point;
  final List<Line> previousSelection;

  LineSelectionDialog(this.lines, this.point, {this.previousSelection});

  @override
  State<StatefulWidget> createState() => _LineSelectionDialogState();
}

class _LineSelectionDialogState extends State<LineSelectionDialog> {
  bool _selectAll = false;
  List<Line> selectedLines = [];

  Widget _buildTitle() {
    return Text(
      translations.text('favorites.modal_line_selection'),
      style: TextStyle(
        fontFamily: 'Roboto',
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  List<Widget> getlinesLogoWidgetList(selectAll) {

    if (selectAll == false) selectedLines = [];
    List<Widget> logoList = [];
    for (Line item in widget.lines) {
      bool _selected = false;
      if (widget.previousSelection != null && widget.previousSelection.contains(item)) {
        _selected = true;
        if (!selectedLines.contains(item)) selectedLines.add(item);
      }

      List<String> tempFav = [];

      if (widget.point is Point || widget.point is Favorite){
      }else{
        for (Favorite fav in widget.point){
          tempFav.add(fav.lines[0]);
        }

        for (String lines in tempFav){
          if (item.shortName == lines) {
            _selected = true;
            selectedLines.add(item);
          }
        }
      }

      logoList.add(StatefulBuilder(builder: (context, setState) {
        return InkWell(
          onTap: () {
            _selected ? selectedLines.remove(item) : selectedLines.add(item);
            setState(() => _selected = !_selected);
          },
          child: Opacity(
              opacity: selectAll ? 1 : (_selected ? 1 : 0.5),
              child: LineDisplay(line: item, isFavoriteAction: true,)),
        );
      }));
    }
    return logoList;
  }

  Widget _buildContent() {
    return Container(
      color: Color(0x636066),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Theme(
                  data: ThemeData(unselectedWidgetColor: globalElevations.fstTextColor.backgroundColor),
                  child: Checkbox(
                    checkColor: Colors.white, // color of tick Mark
                    activeColor: Theme.of(context).indicatorColor,
                    value: _selectAll,
                    onChanged: (bool value) => setState(() => _selectAll = value),
                  ),
                ),
                GestureDetector(
                  child: Text(
                    translations.text('favorites.modal_all_lines'),
                    style: TextStyle(
                        fontFamily: 'Roboto', fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  onTap: () => setState(() => _selectAll = !_selectAll),
                )
              ],
            ),
            SizedBox(height: 15),
            Wrap(
              spacing: 8,
              runSpacing: 8,
              children: getlinesLogoWidgetList(_selectAll),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildActions() {
    return <Widget>[
      FlatButton(
        child: Text(
          translations.text('favorites.modal_cancel'),
          style: TextStyle(
              fontFamily: 'Roboto', fontSize: 16, color: Color(0xffff6e6e)),
          textAlign: TextAlign.center,
        ),
        onPressed: () => Navigator.of(context).pop(SelectionData(
            value: false, selectAll: _selectAll, selectedLines: selectedLines)),
      ),
      FlatButton(
        child: Text(
          translations.text('favorites.modal_validate'),
          style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              color: Theme.of(context).indicatorColor),
          textAlign: TextAlign.center,
        ),
        onPressed: () => Navigator.of(context).pop(SelectionData(
            value: true, selectAll: _selectAll, selectedLines: selectedLines)),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: _buildTitle(),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      content: _buildContent(),
      actions: _buildActions(),
    );
  }
}
