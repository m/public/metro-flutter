import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:metro/atmo/blocs/atmo.dart';
import 'package:metro/atmo/blocs/atmo_bloc.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/global/blocs/agenceMetro.dart';
import 'package:metro/global/blocs/bike_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/citiz.dart';
import 'package:metro/global/blocs/consignesData.dart';
import 'package:metro/global/blocs/irve_bloc.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/parking.dart';
import 'package:metro/global/blocs/parkingData.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/blocs/traficolor_bloc.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/localization/localization_delegate.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/pages/navigation_page.dart';
import 'package:metro/preferences/bloc/preferences.dart';
import 'package:metro/preferences/theme.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/schedule/blocs/plans.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/atmo/blocs/search_atmo.dart';
import 'package:metro/search/blocs/search.dart';
import 'package:preferences/preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'global/database/database.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'dart:async';

MyDatabase database;

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarIconBrightness: Brightness.light,
    statusBarBrightness: Brightness.dark,
  ));

  await PrefService.init(prefix: 'pref_');
  await translations.init();
  database = MyDatabase();

  FlutterError.onError = (FlutterErrorDetails details) {
    Crashlytics.instance.recordError(details, StackTrace.fromString("StackTrace"));
  };

  Crashlytics.instance.setUserEmail("test_email");
  Crashlytics.instance.setUserName("test_name");
  Crashlytics.instance.setUserIdentifier("test_identifier");

  // Crashlytics.instance.enableInDevMode = true;

//  final iTunes = ITunesSearchAPI();
//  final resultsFuture = iTunes.lookupByBundleId('org.lametro.metromobilite');
//  resultsFuture.then((results) {
//    print('results: $results');
//  });

  runApp(BlocProvider(key: GlobalKey(),
      blocs: [
        WebServices(),
        MatomoBloc(),
        SearchBloc(database),
        LinesBloc(),
        ScheduleBloc(),
        PointsBloc(),
        FavoritesBloc(database),
        ParkingBloc(),
        ParkingDataBloc(),
        RoutesBloc(),
        LocationBloc(),
        NavigationBloc(),
        MapBloc(),
        AtmoBloc(),
        FiltersBloc(),
        EventsBloc(),
        TraficolorBloc(),
        BikeBloc(),
        CitizBloc(),
        PreferencesBloc(),
        AgenceMetro(),
        ConsignesDataBloc(),
        PlanBloc(),
        IrveBloc(),
        IndiceAtmoBloc(),
        AtmoSearchBloc(database),
        NsvBloc()
      ],
      child: MyApp()));

  PrefService.setBool("preferences_home_bool", true);
  if (PrefService.getString('preferences_filters') == null) PrefService.setString('preferences_filters', 'bus');
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.dark,
      data: (brightness) =>
          brightness == Brightness.light ? lightTheme : darkTheme,
      themedWidgetBuilder: (context, theme) {
        globalElevations = theme.brightness == Brightness.light
            ? Elevations.darkModeEnabled(false)
            : Elevations.darkModeEnabled(true);
        BlocProvider.of<MapBloc>(context).updateMapTheme(globalElevations);
        BlocProvider.of<MapBloc>(context).updateMarkersTheme(globalElevations);
        BlocProvider.of<MapBloc>(context).updateCamMarkersLayout(globalElevations);
        BlocProvider.of<MapBloc>(context).updateLieuxMarkersTheme(globalElevations);
        BlocProvider.of<PreferencesBloc>(context).initThemeValue(theme == lightTheme ? false : true);
        return MaterialApp(
          theme: theme,
          title: translations.text('generic.app_name'),
          locale: translations.locale,
          localizationsDelegates: [
            CustomLocalizationDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          supportedLocales: translations.supportedLocales(),
          initialRoute: '/',
          routes: {
            '/': (context) => NavigationPage(),
          },
          debugShowCheckedModeBanner: false,
          navigatorKey: GlobalVariable.navState,
        );
      },
    );
  }
}

class GlobalVariable {
  static final GlobalKey<NavigatorState> navState = GlobalKey<NavigatorState>();
}
