import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:rxdart/rxdart.dart';

class LocationBloc extends BlocBase {
  BehaviorSubject<LocationData> _locationController = BehaviorSubject<LocationData>();

  /// Event called whenever the user location change.
  Stream<LocationData> get onLocationChanged => _locationController.stream;

  /// Current location of the user.
  LocationData get location => _locationController.value;

  Location _location;
  StreamSubscription _subscription;
  BuildContext context;

  @override
  void dispose() {
    _subscription?.cancel();
    _locationController.close();
  }

  @override
  void initState() async {
    _location = Location();

    if (await _location.serviceEnabled()) {
      _location.getLocation().then((data) {
        _locationController.sink.add(data);
        BlocProvider.master<MapBloc>().resetPositionAndZoom();
      });

      _subscription = _location.onLocationChanged().listen((data) {
        _locationController.sink.add(data);
      });
    }
  }

  Future<bool> getLocation() async {
    _location = Location();

    if (await _location.hasPermission() == PermissionStatus.DENIED){
      return false;
    }else if(await _location.hasPermission() == PermissionStatus.GRANTED){
      await _location.getLocation();
      return true;
    }else if(await _location.hasPermission() == PermissionStatus.DENIED_FOREVER){
      return false;
    }

  }
}
