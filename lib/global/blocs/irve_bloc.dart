import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class IrveBloc extends BlocBase {
  BehaviorSubject<List<IrveDynamicData>> _downloadController;
  Stream<List<IrveDynamicData>> get onDownloadIrveInfoCompleted => _downloadController.stream;
  List<IrveDynamicData> get irveInfo => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<IrveDynamicData>>.seeded(null);
    _downloadIrveInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _downloadIrveInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadIrveInfo() async {
    List<IrveDynamicData> irveDetails = await _fetchIrveInfo();

    _downloadController.sink.add(irveDetails);
  }

  List<IrveDynamicData> _parseIrveInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return IrveDynamicData.fromJson(parsed);
  }

  Future<List<IrveDynamicData>> _fetchIrveInfo() async {
    final response = await BlocProvider.master<WebServices>().get('bbox/json?types=irve');

    if (response.statusCode == 200)
      return _parseIrveInfo(response.body);
    else
      throw Exception('Failed to fetch borne de recharge IRVE data');
  }

  List<IrvePdcData> pdcIrve(String codeIrve) {

    if(irveInfo == null || irveInfo.length == 0)
      return null;

    List<IrvePdcData> pdc = [];

    for (IrveDynamicData d in irveInfo){
      if (d.code == codeIrve){
        pdc = d.pdc;
      }
    }

    return pdc;

  }

}

class IrveDynamicData {
  final String code;
  final String amenageur;
  final List<IrvePdcData> pdc;
  final String pdcTypes;
  final String pdcNb;

  IrveDynamicData(this.code, this.amenageur, this.pdc, this.pdcTypes, this.pdcNb);

  static List<IrveDynamicData> fromJson(Map<String, dynamic> parsedJson) {

    List<IrveDynamicData> values = List<IrveDynamicData>();

    parsedJson['features'].map<IrveDynamicData>((e) {

      List<IrvePdcData> irvePdc = List<IrvePdcData>.from((e['properties']['pdc'] as List).map((json) => IrvePdcData.fromJson(json)));
      String irveTypes = "";
      List<String> irveTypesList = [];

      for (IrvePdcData irvePdc in irvePdc){
        irveTypesList.add(irvePdc.type_prise);
      }

      if (irveTypesList.contains("CHADEMO - COMBO") && irveTypesList.contains("EF - T2")){
        irveTypes = "E/F + T2 • CHAdeMO";
      } else if (irveTypesList.contains("CHADEMO - COMBO")){
        irveTypes = "CHAdeMO";
      } else if (irveTypesList.contains("EF - T2")){
        irveTypes = "E/F + T2";
      }else{
        irveTypes = "null";
      }

      values.add(IrveDynamicData(e['properties']['id_station'], e['properties']['n_amenageur'], irvePdc, irveTypes, irvePdc.length.toString()));

    }).toList();

    return values;

  }
}

class IrvePdcData {
  final String id_pdc;
  final String puiss_max;
  final String type_prise;

  IrvePdcData(this.id_pdc, this.puiss_max, this.type_prise);

  factory IrvePdcData.fromJson(Map<String, dynamic> parsedJson) {
    return IrvePdcData(parsedJson['id_pdc'], parsedJson['name'], parsedJson['type_prise']);
  }

}
