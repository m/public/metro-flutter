import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/route/route_utils.dart';
import 'package:rxdart/rxdart.dart';
import 'package:latlong/latlong.dart';

class TraficolorBloc extends BlocBase {
  BehaviorSubject<List<TraficolorFeature>> _shapeController =
      BehaviorSubject<List<TraficolorFeature>>();
  Stream<List<TraficolorFeature>> get onShapesChanged =>
      _shapeController.stream;
  List<TraficolorFeature> get shapes => _shapeController.value;

  BehaviorSubject<List<TraficolorState>> _colorController =
      BehaviorSubject<List<TraficolorState>>();
  Stream<List<TraficolorState>> get onColorsChanged => _colorController.stream;
  List<TraficolorState> get colors => _colorController.value;

  BehaviorSubject<List<Polyline>> _polylinesController =
      BehaviorSubject<List<Polyline>>();
  Stream<List<Polyline>> get onPolylinesChanged => _polylinesController.stream;
  List<Polyline> get polylines => _polylinesController.value;

  @override
  Future initState() async {
    Rx.combineLatest2(onShapesChanged, onColorsChanged,
        (List<TraficolorFeature> shapes, List<TraficolorState> colors) {
      return shapes.map((shape) {
        return Polyline(
          color: colors
              .singleWhere((color) => color.id == shape.id)
              .associatedColor(),
          points: shape.points,
          strokeWidth: 4.0,
        );
      }).toList();
    }).listen((data) => _polylinesController.sink.add(data));

    downloadTraficolor();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      downloadTraficolor();
    });
  }

  @override
  void dispose() {
    _shapeController.close();
    _colorController.close();
    _polylinesController.close();
  }

  Future downloadTraficolor() async {
    _shapeController.sink.add(await _fetchTraficolorFeatures());
    _colorController.sink.add(await _fetchTraficolorStates());
  }

  List<TraficolorFeature> _parseTraficolorFeatures(String responseBody) {
    final parsed = json.decode(responseBody);
    return parsed['features'].map<TraficolorFeature>((j) {
      return TraficolorFeature(j['properties']['id'],
          decodeEncodedPolyline(j['properties']['shape']));
    }).toList();
  }

  Future<List<TraficolorFeature>> _fetchTraficolorFeatures() async {
    final response =
        await BlocProvider.master<WebServices>().get('troncons/poly?niveau=0');

    if (response.statusCode == 200)
      return _parseTraficolorFeatures(response.body);
    else
      throw Exception('Failed to fetch traficolor features');
  }

  List<TraficolorState> _parseTraficolorStates(String responseBody) {
    final parsed = json.decode(responseBody);

    List<TraficolorState> states = List();
    for (var x in parsed.entries) {
      states.add(TraficolorState(x.key, (x.value as List).last['nsv_id']));
    }
    return states;
  }

  Future<List<TraficolorState>> _fetchTraficolorStates() async {
    final response =
        await BlocProvider.master<WebServices>().get('dyn/trr/json');

    if (response.statusCode == 200)
      return _parseTraficolorStates(response.body);
    else
      throw Exception('Failed to fetch traficolor states');
  }
}

class TraficolorFeature {
  final String id;
  final List<LatLng> points;

  TraficolorFeature(this.id, this.points);
}

class TraficolorState {
  final String id;
  final int level;

  TraficolorState(this.id, this.level);

  Color associatedColor() {
    switch (level) {
      case 1:
        return Color(0x66CC33).withOpacity(1);
      case 2:
        return Color(0xFFA500).withOpacity(1);
      case 3:
        return Color(0xFF0000).withOpacity(1);
      case 4:
        return Color(0x000000).withOpacity(1);
      default:
        return Color(0xA9A9A9).withOpacity(1);
    }
  }
}
