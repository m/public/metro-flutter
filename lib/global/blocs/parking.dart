import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class ParkingBloc extends BlocBase {
  BehaviorSubject<List<ParkingDynamicData>> _downloadController;
  Stream<List<ParkingDynamicData>> get onDownloadParkingInfoCompleted => _downloadController.stream;
  List<ParkingDynamicData> get parkingInfo => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<ParkingDynamicData>>.seeded(null);
    _downloadParkingInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _downloadParkingInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadParkingInfo() async {
    List<ParkingDynamicData> parkingDetails = await _fetchParkingInfo();

    _downloadController.sink.add(parkingDetails);
  }

  List<ParkingDynamicData> _parseParkingInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return ParkingDynamicData.fromJson(parsed);
  }

  Future<List<ParkingDynamicData>> _fetchParkingInfo() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/parking/json/');

    if (response.statusCode == 200)
      return _parseParkingInfo(response.body);
    else
      throw Exception('Failed to fetch Parking data');
  }

  /// Return true if at least one vehicle is available (based on the given parking station id),
  /// false otherwise. Return null if data hasn't been loaded up yet.
  int countAvailable(String parkingId) {

    if(parkingInfo == null || parkingInfo.length == 0)
      return null;

    int vehicles = parkingInfo.firstWhere((v) => v.id == parkingId, orElse: () => ParkingDynamicData(parkingId, 0)).dispo;

    return vehicles;

  }

}

class ParkingDynamicData {
  final String id;
  final int dispo;

  ParkingDynamicData(this.id, this.dispo);

  static List<ParkingDynamicData> fromJson(Map<String, dynamic> parsedJson) {

    List<ParkingDynamicData> values = List<ParkingDynamicData>();

    for(var x in parsedJson.entries){
      values.add(ParkingDynamicData(x.key, parsedJson[x.key]["nb_places_libres"]));
    }
    return values;
  }
}