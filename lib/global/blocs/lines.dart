import 'dart:async';
import 'dart:convert';

import 'package:metro/global/utils.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/line_details.dart';

import 'bloc_provider.dart';

class LinesBloc extends BlocBase {
  BehaviorSubject<List<ReadyLine>> _downloadController;
  Stream<List<ReadyLine>> get onDownloadLinesCompleted => _downloadController.stream;
  List<ReadyLine> get lines => _downloadController.value;

  BehaviorSubject<bool> _isLinesDownloaded;
  Stream<bool> get onIsLinesDownloaded => _isLinesDownloaded.stream;
  bool get isLinesDownloaded => _isLinesDownloaded.value;

  bool _linesDownloaded = false;
  bool _isDownloading = false;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<ReadyLine>>.seeded(List<ReadyLine>());
    _isLinesDownloaded = BehaviorSubject<bool>.seeded(false);
    _downloadLines();
  }

  @override
  void dispose() {
    _downloadController.close();
    _isLinesDownloaded.close();
  }

  void _downloadLines() {
    if (_linesDownloaded || _isDownloading) return;

    _downloadAndProcessData();
  }

  List<Line> _parseLines(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Line>((json) => Line.fromJson(json)).toList();
  }

  Future<List<Line>> _fetchLines() async {
    final response = await BlocProvider.master<WebServices>().get('routers/default/index/routes');

    if (response.statusCode == 200)
      return _parseLines(response.body);
    else
      throw Exception('Failed to fetch lines');
  }

  Future<LinesDetails> _fetchLinesDetails(String linesIds) async {
    final response = await BlocProvider.master<WebServices>().get('lines/json?types=ligne&codes=' + linesIds);
    if (response.statusCode == 200)
      return LinesDetails.fromJson(json.decode(response.body));
    else
      throw Exception('Failed to fetch lines details');
  }

  Future<List<String>> _fetchNearbyLines(LatLng position) async {
    final response = await BlocProvider.master<WebServices>().get('linesNear/json?x=${position.longitude}&y=${position.latitude}&dist=500');
    
    if (response.statusCode == 200)
      return json.decode(response.body).cast<String>();
    else
      throw Exception('Failed to fetch nearby lines');
  }

  void _downloadAndProcessData() async {
    _isDownloading = true;

    // Get all lines
    List<Line> allLines = await _fetchLines();

    // Get all lines IDs
    String legacyIds = '';
    String gresivaudanIds = '';
    String voironaisIds = '';

    allLines.forEach((line) {
      // We only take care of line we will be using in the app
      if (line.type == 'TRAM' || line.type == 'MCO'|| line.type == 'NAVETTE'|| line.type == 'CHRONO' || line.type == 'PROXIMO' || line.type == 'FLEXO' || line.type == 'C38') {
        legacyIds += line.id + ',';
      }

      if (line.type == 'Structurantes' || line.type == 'Secondaires') {
        gresivaudanIds += line.id + ',';
      }

      if (line.type == 'Urbaines' || line.type == 'Interurbaines') {
        voironaisIds += line.id + ',';
      }
    });

    // Remove the last ',' from the string
    legacyIds = legacyIds.substring(0, legacyIds.length - 1);
    gresivaudanIds = gresivaudanIds.substring(0, gresivaudanIds.length - 1);
    voironaisIds = voironaisIds.substring(0, voironaisIds.length - 1);

    // Fetch details of all lines
    LinesDetails allLinesDetails = await _fetchLinesDetails(legacyIds.replaceAll(':', '_'));
    LinesDetails gresivaudanDetails = await _fetchLinesDetails(gresivaudanIds.replaceAll(':', '_'));
    LinesDetails voironaisDetails = await _fetchLinesDetails(voironaisIds.replaceAll(':', '_'));

    // Create readylines for each line with associated line and details
    List<ReadyLine> allReadyLines = List<ReadyLine>();
    allLines.forEach((line) {
      if (line.type == 'TRAM' ||
          line.type == 'NAVETTE' ||
          line.type == 'CHRONO' ||
          line.type == 'PROXIMO' ||
          line.type == 'FLEXO' ||
          line.type == 'C38' ||
          line.type == 'Structurantes' ||
          line.type == 'Secondaires' ||
          line.type == 'Urbaines' ||
          line.type == 'Interurbaines' || 
          line.type == 'SCOL' || 
          line.type == 'MCO' ||
          line.type == 'TAD' ||
          line.type == 'SNC' ) {
        ReadyLine readyline = ReadyLine();
        // Creation of our own type to merge Metro's multi types
        if (line.type == 'Structurantes' || line.type == 'Secondaires') {
          readyline.line = line;
          readyline.details = gresivaudanDetails.getFeature(line.id);
        } else if (line.type == 'Urbaines' || line.type == 'Interurbaines') {
          readyline.line = line;
          readyline.details = voironaisDetails.getFeature(line.id);
        } else {
          readyline.line = line;
          readyline.details = allLinesDetails.getFeature(line.id);
        }
        if (readyline.details != null) allReadyLines.add(readyline);
      }
    });

    // Building polylines for each readylines
    allReadyLines.forEach((readyline) {
      // If the line has no geometry we skip it
      if (readyline.details.geometry != null) {
        readyline.polyline = List();
        List<List<List<double>>> coordinates = readyline.details.geometry.coordinates;

        for (int j = 0; j < coordinates.length; j++) {
          // Set all points of the line
          List<LatLng> points = List<LatLng>();
          for (int i = 0; i < coordinates[j].length; i++) {
            points.add(new LatLng(coordinates[j][i][1], coordinates[j][i][0]));
          }

          readyline.polyline.add(Polyline(
            color: Utils.colorFromWS(readyline.details.properties.couleur),
            points: points,
            strokeWidth: 4.0,
          ));
        }
      }
    });

    _isDownloading = false;
    _linesDownloaded = true;
    _downloadController.sink.add(allReadyLines);
    _isLinesDownloaded.sink.add(true);
  }

  Future<List<String>> nearbyLinesId(LatLng position) async {
    return await _fetchNearbyLines(position);
  }

  Future<List<ReadyLine>> nearbyLines(LatLng position) async {
    List<String> ids = await nearbyLinesId(position);
    return lines.where((line) => ids.contains(line.line.id)).toList();
  }

  Future<List<ReadyLine>> nearbyLinesPopUp(LatLng position) async {
    List<String> ids = await nearbyLinesId(position);
    return lines.where((line) => ids.contains(line.line.id)).toList();
  }

}

class ReadyLine {
  Line line;
  Feature details;
  List<Polyline> polyline;
}
