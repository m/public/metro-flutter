import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/agenceMetro.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/localization/translations.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/properties.dart';

class BottomSheetAgencyContent extends BottomSheetContent {
  BottomSheetAgencyContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetAgencyBody(point: point);
  }
}

class BottomSheetAgencyBody extends StatelessWidget {
  final Point point;

  const BottomSheetAgencyBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MetroData agencyData = BlocProvider.master<AgenceMetro>().getTypeMetro(point.properties.name);
    return point.properties.type == PropertiesType.agenceM
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                BottomSheetBodyItineraryButton(point: point),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Text(
                    translations.text('agency.info'),
                    style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
                  ),
                ),
                agencyData.VENTES_DE_TICKETS == true
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.purchase'), style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                agencyData.ABONNEMENTS == true
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.abonnement'), style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                agencyData.ABONNEMENT_METROVELO == true
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.abonnement_velo'), style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                agencyData.CONSEIL == true
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.advice'), style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                agencyData.OBJETS_TROUVES == true
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.objects_founds'), style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Text(
                    translations.text('agency.hours'),
                    style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
                  ),
                ),
                agencyData.HORAIRES_LUNDI != null
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.week') + agencyData.HORAIRES_LUNDI, style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                agencyData.HORAIRES_SAMEDI != null
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                        child: Text(translations.text('agency.saturday') + agencyData.HORAIRES_SAMEDI, style: TextStyle(fontSize: 16)),
                      )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text(translations.text('agency.public_holidays'), style: TextStyle(fontSize: 16)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Container(
                    width: double.infinity,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0), side: BorderSide(color: Theme.of(context).colorScheme.primary)),
                      onPressed: () {
                        Utils.launchUrl('https://www.mobilites-m.fr/pages/DecouvrirM.html#Agencesmetro');
                      },
                      color: Colors.transparent,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: Icon(
                                  Icons.public,
                                  color: Theme.of(context).colorScheme.primary,
                                )),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              translations.text('agency.more_info'),
                              style: TextStyle(
                                color: Theme.of(context).colorScheme.primary,
                                fontSize: 16,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  child: FlatButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0), side: BorderSide(color: Theme.of(context).colorScheme.primary)),
                    onPressed: () {
                      _makePhoneCall('tel:' + agencyData.TELEPHONE);
                    },
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            translations.text('agency.call'),
                            style: TextStyle(
                              color: Theme.of(context).colorScheme.primary,
                              fontSize: 16,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 32),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: BottomSheetBodyNearbyLines(
                      position: Utils.coordinatesFromArray(point.geometry.coordinates),
                    ),
                  ),
                ),
              ],
            ),
          )
        : Column(
          children: <Widget>[
            BottomSheetBodyItineraryButton(point: point),
            Padding(
              padding: const EdgeInsets.only(top: 32),
              child: Align(
                alignment: Alignment.centerLeft,
                child: BottomSheetBodyNearbyLines(
                  position: Utils.coordinatesFromArray(point.geometry.coordinates),
                ),
              ),
            ),
          ],
        );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
