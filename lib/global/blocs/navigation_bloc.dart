import 'package:flutter/material.dart';
import 'package:metro/favorites/pages/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/map/pages/map_page.dart';
import 'package:metro/preferences/pages/preferences.dart';
import 'package:metro/route/pages/route.dart';
import 'package:metro/schedule/pages/schedule.dart';
import 'package:metro/schedule/pages/schedule_line_details.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:latlong/latlong.dart';

import 'matomo.dart';

class NavigationBloc implements BlocBase {
  BehaviorSubject<NavigationCategory> _categoryController = BehaviorSubject<NavigationCategory>.seeded(NavigationCategory.map);
  /// Event called whenever the tab index changed.
  Stream<NavigationCategory> get onNavigationCategoryChanged => _categoryController.stream;
  /// The current tab index.
  NavigationCategory get navigationCategory => _categoryController.value;

  BehaviorSubject<Widget> _pageController = BehaviorSubject<Widget>.seeded(MapPage());
  /// Event called whenever the tab index changed.
  Stream<Widget> get onPageChanged => _pageController.stream;
  /// The current tab index.
  Widget get page => _pageController.value;

  List<Widget> _pages;
  GlobalKey _safeAreaKey;
  BehaviorSubject<bool> _infoPopupController;
  Stream<bool> get infoPopupController => _infoPopupController.stream;
  bool get showPopup => _infoPopupController.value;

  BehaviorSubject<bool> _infoEventsPopupController = BehaviorSubject<bool>.seeded(true);
  bool get showEventsPopup => _infoEventsPopupController.value;

  BehaviorSubject<bool> _lightThemePopupController = BehaviorSubject<bool>.seeded(true);
  bool get showLightThemePopup => _lightThemePopupController.value;

  List<NavigationCategory> _bottomBarPilePage = [];

  bool _eventSelected = false;
  set eventSelected(bool value) => _eventSelected = value;

  @override
  void initState() {
    _infoPopupController = BehaviorSubject<bool>.seeded(PrefService.getBool(Consts.preferencesInfoModal) ?? true);
    // Must be ordened based on NavigationCategory enum
    _pages = [
      MapPage(),
      FavoritesPage(),
      RoutePage(),
      SchedulePage(),
      ScheduleLineDetailsPage(),
      PreferencesPage()
    ];
    BlocProvider.master<MatomoBloc>().trackScreen(MatomoBloc.screen_home, 'test');
  }

  @override
  void dispose() {
    _categoryController.close();
    _pageController.close();
    _infoPopupController.close();
    _infoEventsPopupController.close();
    _lightThemePopupController.close();
  }

  switchInfoPopup(){
    _infoPopupController.sink.add(!showPopup);
  }

  switchLightThemePopup() => _lightThemePopupController.sink.add(!showLightThemePopup);

  switchEventPopup() => _infoEventsPopupController.sink.add(!showEventsPopup);

  void changeTabByIndex(int index) {
    assert(index >= 0 && index < 4);
    changeTab(NavigationCategory.values[index]);
  }

  void changeTab(NavigationCategory page, {bool updatePile = true}) {
    if(navigationCategory == NavigationCategory.traficolor)
        BlocProvider.master<MapBloc>().restorePreviousSavedPositionAndZoom();

    _categoryController.sink.add(page);
    switch (page) {
//      case NavigationCategory.preferences:
//    _pageController.sink.add(_pages[NavigationCategory.values.indexOf(NavigationCategory.preferences)]);
//    break;
      case NavigationCategory.map:
      case NavigationCategory.favorites:
      case NavigationCategory.route:
      case NavigationCategory.schedule:
        if (updatePile) _bottomBarPilePage.add(page);
        if(page == NavigationCategory.map)
          BlocProvider.master<MatomoBloc>().trackScreen(MatomoBloc.screen_home, 'test');
        _pageController.sink.add(_pages[NavigationCategory.values.indexOf(page)]);
        BlocProvider.master<MapBloc>().setBottomSheetContent(BottomSheetCategory.POIAroundMe);
        break;
      case NavigationCategory.routeResult:
      case NavigationCategory.routeResultDetails:
        _pageController.sink.add(_pages[NavigationCategory.values.indexOf(NavigationCategory.map)]);
        break;
      case NavigationCategory.scheduleDetails:
        _pageController.sink.add(_pages[NavigationCategory.values.indexOf(NavigationCategory.scheduleDetails)]);
        break;
      case NavigationCategory.scheduleDetailsAll:
        _pageController.sink.add(_pages[NavigationCategory.values.indexOf(NavigationCategory.map)]);
        break;
      case NavigationCategory.traficolor:
        BlocProvider.master<MatomoBloc>().trackScreen(MatomoBloc.screen_traffic, 'test');
        _pageController.sink.add(_pages[NavigationCategory.values.indexOf(NavigationCategory.map)]);
        BlocProvider.master<MapBloc>().setBottomSheetContent(BottomSheetCategory.traficolor);
        break;
      default:
        throw Exception('Navigation to {$page} is not handle. Please fix it.');
    }
  }

  NavigationCategory getMajorCategory(NavigationCategory category) {
    switch (category) {
      case NavigationCategory.map:
      case NavigationCategory.favorites:
      case NavigationCategory.route:
      case NavigationCategory.schedule:
        return category;
//      case NavigationCategory.preferences:

      case NavigationCategory.routeResult:
      case NavigationCategory.routeResultDetails:
        return NavigationCategory.route;
      case NavigationCategory.scheduleDetails:
      case NavigationCategory.scheduleDetailsAll:
        return NavigationCategory.schedule;
        break;
      case NavigationCategory.traficolor:
        return NavigationCategory.map;
      default:
        throw Exception(
            'Major category for {$category} is not specify. Please fix it.');
    }
  }

  bool backNavigation() {
    switch (navigationCategory) {
//      case NavigationCategory.preferences:
//        changeTab(NavigationCategory.preferences);
//        break;
      case NavigationCategory.map:
        if (!BlocProvider.master<MapBloc>().onBackButtonPress()) {
          if (_bottomBarPilePage.isNotEmpty) {
            _bottomBarPilePage.removeLast();
            changeTab(_bottomBarPilePage.last, updatePile: false);
          } else
            return true;
        }
        break;
      case NavigationCategory.favorites:
      case NavigationCategory.route:
      case NavigationCategory.schedule:
        if (_bottomBarPilePage.isEmpty) return true;
        _bottomBarPilePage.removeLast();
        changeTab(_bottomBarPilePage.last, updatePile: false);
        break;
      case NavigationCategory.routeResult:
        changeTab(NavigationCategory.route);
        break;
      case NavigationCategory.routeResultDetails:
        changeTab(NavigationCategory.routeResult);
        BlocProvider.master<MapBloc>().onBackButtonPress();
        break;
      case NavigationCategory.scheduleDetails:
        changeTab(NavigationCategory.schedule);
        break;
      case NavigationCategory.scheduleDetailsAll:
        changeTab(NavigationCategory.scheduleDetails);
        BlocProvider.master<MapBloc>().onBackButtonPress();
        break;
      case NavigationCategory.traficolor:
        if (_eventSelected) {
          changeTab(NavigationCategory.traficolor);
          BlocProvider.master<MapBloc>().moveTo(MapPositionAndZoom.withLatLng(
           LatLng((Consts.grenobleMaxLat - Consts.grenobleMinLat) / 2 + Consts.grenobleMinLat,
               (Consts.grenobleMaxLng - Consts.grenobleMinLng) / 1.89 + Consts.grenobleMinLng),
           13));
           _eventSelected = false;
        }
        else changeTab(NavigationCategory.map);
        break;

      default:
        throw Exception('Back navigation from {$navigationCategory} is not handle. Please fix it.');
    }

    return false;
  }

  GlobalKey getSafeAreaKey() {
    if(_safeAreaKey == null)
      _safeAreaKey = GlobalKey();
    return _safeAreaKey;
  }
}

getCurrentIndex(){
  int currentIndex = PrefService.getInt('preferences_home');

  if(currentIndex==null) currentIndex = 0;

  return currentIndex;
}

enum NavigationCategory {
  map,
//  preferences,
  favorites,
  route,
  schedule,
  scheduleDetails,
  routeResult,
  routeResultDetails,
  scheduleDetailsAll,
  traficolor,
}
