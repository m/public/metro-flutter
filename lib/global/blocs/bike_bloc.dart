import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/route/route_utils.dart';
import 'package:rxdart/rxdart.dart';
import 'package:latlong/latlong.dart';

class BikeBloc extends BlocBase {
  BehaviorSubject<List<BikeFeature>> _shapeController = BehaviorSubject<List<BikeFeature>>();
  Stream<List<BikeFeature>> get onShapesChanged => _shapeController.stream;
  List<BikeFeature> get shapes => _shapeController.value;

  BehaviorSubject<List<Polyline>> polylinesController = BehaviorSubject<List<Polyline>>();
  Stream<List<Polyline>> get onPolylinesChanged => polylinesController.stream;
  List<Polyline> get polylines => polylinesController.value;

  Map<String, List<BikeFeature>> _bikeFeats = {};

  Polyline _buildPolyline(BikeFeature shape) {
    switch (shape.type) {
      case "tempovelo":
        return Polyline(
          borderColor: Colors.black,
          color: Color(0xFFFDEA00),
          points: shape.points,
          strokeWidth: 4.0,
        );
      case "chronovelo":
        return Polyline(
          color: Color(0xFF8154C0),
          points: shape.points,
          strokeWidth: 4.0,
        );
      case "voieverte":
        return Polyline(
          color: Color(0xFF498D60),
          points: shape.points,
          strokeWidth: 4.0,
        );
      case "veloamenage":
        return Polyline(
          color: Color(0xFF498D60),
          points: shape.points,
          strokeWidth: 4.0,
        );
      case "velononamenage":
        return Polyline(
          color: Color(0xFF498D60),
          points: shape.points,
          strokeWidth: 4.0,
          isDotted: true,
        );
      default:
        return Polyline(
          color: Color(0xFFDF866B),
          points: shape.points,
          strokeWidth: 4.0,
          isDotted: true,
        );
    }
  }

  @override
  void initState() {
    onShapesChanged.listen((shapes) => polylinesController.sink.add(shapes.map((shape) => _buildPolyline(shape)).toList()));
    downloadBike();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      downloadBike();
    });
  }

  @override
  void dispose() {
    _shapeController.close();
    polylinesController.close();
  }

  void downloadBike({bool isBottomFilter = false, List<String> filtersList = const []}) async {
    List<BikeFeature> bikeInfo = await _fetchBikeFeatures(isBottomFilters: isBottomFilter, filtersList: filtersList);
    _shapeController.sink.add(bikeInfo);
  }

  Future<List<BikeFeature>> _fetchBikeFeatures({bool isBottomFilters = false, List<String> filtersList = const []}) async {
    List<String> _filtersList = List<String>.from(filtersList);
    FiltersBloc filtersBloc = BlocProvider.master<FiltersBloc>();
    filtersBloc.filters[FiltersPosition.bikes.index].subFilters.forEach((filter) {
        if (filtersBloc.filtersValue[filter]) {
          _filtersList.add(filter.requestId);
        }
    });

    List<BikeFeature> _feats = [];
    for (final id in _filtersList) {
      if (!_bikeFeats.containsKey(id)) {
        final response = await BlocProvider.master<WebServices>().get('lines/poly?types=$id');
        if (response.statusCode == 200) {
          _bikeFeats[id] = await _parseBikeFeatures(response.body);
        } else
          throw Exception("Failed to fetch bike features");
      }
      _feats.addAll(_bikeFeats[id]);
    }
    return _feats;
  }

  Future<List<BikeFeature>> _parseBikeFeatures(String responseBody) async {
    final parsed = jsonDecode(responseBody);
    List<BikeFeature> elems = [];
    (parsed['features'] as List).forEach((e) {
      final shape = e['properties']['shape'];
      if (shape is List)
        shape.forEach((i) => elems.add(BikeFeature.fromJson(i, e['properties']['type'])));
      else
        elems.add(BikeFeature.fromJson(shape, e['properties']['type']));
    });
    return elems;
  }
}

class BikeFeature {
  final List<LatLng> points;
  final String type;

  BikeFeature({@required this.points, @required this.type});

  factory BikeFeature.fromJson(dynamic poly, String type) => BikeFeature(points: decodeEncodedPolyline(poly), type: type);
}
