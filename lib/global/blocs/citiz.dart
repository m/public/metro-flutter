import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class CitizBloc extends BlocBase {
  BehaviorSubject<List<CitizDynamicData>> _downloadController;
  Stream<List<CitizDynamicData>> get onDownloadCitizInfoCompleted => _downloadController.stream;
  List<CitizDynamicData> get citizInfo => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<CitizDynamicData>>.seeded(null);
    _downloadCitizInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _downloadCitizInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadCitizInfo() async {
    List<CitizDynamicData> citizDetails = await _fetchCitizInfo();

    _downloadController.sink.add(citizDetails);
  }

  List<CitizDynamicData> _parseCitizInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return CitizDynamicData.fromJson(parsed);
  }

  Future<List<CitizDynamicData>> _fetchCitizInfo() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/citiz/json/');

    if (response.statusCode == 200)
      return _parseCitizInfo(response.body);
    else
      throw Exception('Failed to fetch Citiz data');
  }

  /// Return true if at least one vehicle is available (based on the given citiz station id),
  /// false otherwise. Return null if data hasn't been loaded up yet.
  bool isCitizVehicleAvailable(String citizId) {
    if(citizInfo == null || citizInfo.length == 0)
      return null;

    List<CitizVehicle> vehicles = citizInfo.firstWhere((v) => v.id == citizId).vehicles;

    for(var vehicle in vehicles) {
        if(vehicle.availabilityStart.isBefore(DateTime.now()))
          return true;
    }

    return false;
  }
}

class CitizDynamicData {
  final String id;
  final List<CitizVehicle> vehicles;

  CitizDynamicData(this.id, this.vehicles);

  static List<CitizDynamicData> fromJson(Map<String, dynamic> parsedJson) {
    List<CitizDynamicData> values = List<CitizDynamicData>();
    for(var x in parsedJson.entries){
      values.add(CitizDynamicData(x.key, (x.value['vehicles'] as Map<String, dynamic>).entries.map((l) => CitizVehicle.fromJson(l))?.toList()));
    }
    return values;
  }
}

class CitizVehicle {
  final String id;
  final int fuelLevel;
  final DateTime availabilityStart;
  final DateTime availabilityEnd;

  CitizVehicle(this.id, this.fuelLevel, this.availabilityStart, this.availabilityEnd);

  factory CitizVehicle.fromJson(MapEntry<String, dynamic> parsedJson) {
    String id = parsedJson.key;
    DateTime start = DateTime.tryParse(parsedJson.value['availabilityStart'].split('.')[0]);
    DateTime end =  DateTime.tryParse(parsedJson.value['availabilityEnd'].split('.')[0]);
    return CitizVehicle(parsedJson.key, parsedJson.value['fuelLevel'], start, end);
  }
}