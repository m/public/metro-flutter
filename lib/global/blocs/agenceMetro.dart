import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class AgenceMetro extends BlocBase {
  BehaviorSubject<List<MetroData>> _downloadController;
  Stream<List<MetroData>> get onDownloadMetroDataInfoCompleted => _downloadController.stream;
  List<MetroData> get metroDataInfo => _downloadController.value;

  bool _isDownloading = false;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<MetroData>>.seeded(null);
    _downloadMetroDataInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _isDownloading = false;
      _downloadMetroDataInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadMetroDataInfo() async {
    if (_isDownloading) return;
    _isDownloading = true;
    List<MetroData> agencyDataDetails = await _fetchMetroDataInfo();
    _downloadController.sink.add(agencyDataDetails);
  }

  List<MetroData> _parseMetroDataInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return MetroData.fromJson(parsed);
  }

  Future<List<MetroData>> _fetchMetroDataInfo() async {

    final response = await BlocProvider.master<WebServices>().get('findType/json?types=agenceM');

    if (response.statusCode == 200)
      return _parseMetroDataInfo(response.body);
    else
      throw Exception('Failed to fetch agency data');
  }

  Future<bool> loadAgency() async {
    List<MetroData> results = await _fetchMetroDataInfo();

    if (results != null) {
      return true;
    }
    return false;
  }

  MetroData getTypeMetro(String agencyName) {
    if(metroDataInfo == null || metroDataInfo.length == 0)
      return null;

    for (MetroData x in metroDataInfo){
      if (x.nom == agencyName){
        return x;
      }
    }
  }

}

class MetroData {
  final String id;
  final String nom;
  final String HORAIRES_LUNDI;
  final String HORAIRES_MARDI;
  final String HORAIRES_MERCREDI;
  final String HORAIRES_JEUDI;
  final String HORAIRES_VENDREDI;
  final String HORAIRES_SAMEDI;
  final String HORAIRES_DIMANCHE;
  final String TELEPHONE;
  final bool GUICHETS;
  final bool VENTES_DE_TICKETS;
  final bool ABONNEMENTS;
  final bool CREATION_CARTE_OURA;
  final bool SAV_OURA;
  final bool ABONNEMENT_METROVELO;
  final bool PAIEMENT_ESPECES;
  final bool PAIEMENT_CHEQUE;
  final bool PAIEMENT_CB;
  final bool PAIEMENT_CAIRN;
  final bool CONSEIL;
  final bool OBJETS_TROUVES;


  MetroData(this.id, this.nom, this.HORAIRES_LUNDI, this.HORAIRES_MARDI, this.HORAIRES_MERCREDI,
      this.HORAIRES_JEUDI, this.HORAIRES_VENDREDI, this.HORAIRES_SAMEDI, this.HORAIRES_DIMANCHE,
      this.TELEPHONE, this.GUICHETS, this.VENTES_DE_TICKETS, this.ABONNEMENTS, this.CREATION_CARTE_OURA,
      this.SAV_OURA, this.ABONNEMENT_METROVELO, this.PAIEMENT_ESPECES, this.PAIEMENT_CHEQUE,
      this.PAIEMENT_CB, this.PAIEMENT_CAIRN, this.CONSEIL, this.OBJETS_TROUVES);

  static List<MetroData> fromJson(Map<String, dynamic> parsedJson) {

    List<MetroData> values = List<MetroData>();

    parsedJson['features'].map<MetroData>((e) {

      String id = e['properties']['id'].toString();

      values.add(MetroData(
        id,
        e['properties']['NOM'],
        e['properties']['HORAIRES_LUNDI'],
        e['properties']['HORAIRES_MARDI'],
        e['properties']['HORAIRES_MERCREDI'],
        e['properties']['HORAIRES_JEUDI'],
        e['properties']['HORAIRES_VENDREDI'],
        e['properties']['HORAIRES_SAMEDI'],
        e['properties']['HORAIRES_DIMANCHE'],
        e['properties']['TELEPHONE'],
        e['properties']['GUICHETS'],
        e['properties']['VENTES_DE_TICKETS'],
        e['properties']['ABONNEMENTS'],
        e['properties']['CREATION_CARTE_OURA'],
        e['properties']['SAV_OURA'],
        e['properties']['ABONNEMENT_METROVELO'],
        e['properties']['PAIEMENT_ESPECES'],
        e['properties']['PAIEMENT_CHEQUE'],
        e['properties']['PAIEMENT_CB'],
        e['properties']['PAIEMENT_CAIRN'],
        e['properties']['CONSEIL'],
        e['properties']['OBJETS_TROUVES'],
      ));

    }).toList();

    return values;
  }
}