import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

enum WebServiceHost {
  preprod,
  prod,
}

class WebServices extends BlocBase {
  static WebServiceHost _host = WebServiceHost.prod;

  BehaviorSubject<WebServiceHost> _hostController = BehaviorSubject<WebServiceHost>.seeded(_host);
  Stream<WebServiceHost> get onHostChanged => _hostController.stream;
  WebServiceHost get host => _host;

  void changeHost(WebServiceHost host) {
    _host = host;
    _hostController.sink.add(host);
  }

  String getHost() {
    switch (_host) {
      case WebServiceHost.preprod:
        return 'https://datatest.metromobilite.fr/api/';
      case WebServiceHost.prod:
        return 'https://data.mobilites-m.fr/api/';
    }

    return 'WebServiceHost not setup in ws.dart';
  }

  String getSivHost() {
    switch (_host) {
      case WebServiceHost.preprod:
        return 'https://sivtest.metromobilite.fr/datasiv/';
      case WebServiceHost.prod:
        return 'https://siv.metromobilite.fr/datasiv/';
    }

    return 'WebServiceHost not setup in ws.dart';
  }

  Future<http.Response> get(String endPoint, {VoidCallback onTimeOut}) async {
    return await http.get(getHost() + endPoint, headers: {'origin': 'FLUTTER'}).timeout(Duration(seconds: 60), onTimeout: onTimeOut);
  }

  @override
  void dispose() {
    _hostController.close();
  }

  @override
  void initState() {
    // TODO: implement initState
  }
}
