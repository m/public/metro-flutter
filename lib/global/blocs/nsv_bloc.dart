import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class NsvBloc extends BlocBase {
  BehaviorSubject<List<NsvData>> _downloadController;
  Stream<List<NsvData>> get onDownloadNsvInfoCompleted => _downloadController.stream;
  List<NsvData> get nsvInfo => _downloadController.value;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<NsvData>>.seeded(null);
    _downloadNsvInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _downloadNsvInfo();
    });

  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadNsvInfo() async {
    List<NsvData> nsvDetails = await _fetchNsvInfo();

    _downloadController.sink.add(nsvDetails);
  }

  List<NsvData> _parseNsvInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return NsvData.fromJson(parsed);
  }

  Future<List<NsvData>> _fetchNsvInfo() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/ligne/json');

    if (response.statusCode == 200)
      return _parseNsvInfo(response.body);
    else
      throw Exception('Failed to fetch Nsv data');
  }

  int getNsvId(String lineId) {

    if(nsvInfo == null || nsvInfo.length == 0)
      return null;

    int nsv_id = nsvInfo.firstWhere((v) => v.id == lineId, orElse: () => NsvData(lineId, 0)).nsv_id;

    return nsv_id;
  }

}

class NsvData {
  final String id;
  final int nsv_id;

  NsvData(this.id, this.nsv_id);

  static List<NsvData> fromJson(Map<String, dynamic> parsedJson) {

    List<NsvData> values = List<NsvData>();

    for(var x in parsedJson.entries){
      values.add(NsvData(x.key, parsedJson[x.key][0]["nsv_id"]));
    }

    return values;
  }
}