import 'package:flutter_matomo/flutter_matomo.dart';

import 'bloc_provider.dart';

class MatomoBloc extends BlocBase {
  @override
  void initState() async {
    await FlutterMatomo.initializeTracker(site_url, site_id);
  }

  @override
  void dispose() {}

  void trackScreen(String name, String eventName) async {
    await FlutterMatomo.trackScreenWithName(name, eventName);
  }

  static const int site_id = 14;
  static const String site_url = 'https://www.mobilites-m.fr/stats/piwik.php';

  static const String screen_home = 'Accueil';
  static const String screen_route_details = 'Itinéraire détails';
  static const String screen_route_results = 'Itinéraire résultats';
  static const String screen_route_search = 'Recherche itinéraires';
  static const String screen_traffic = 'Traficolor';
  static const String screen_disturbances_all = 'Perturbations tout';
  static const String screen_disturbances_my_alerts = 'Perturbations Mes alertes';
  static const String screen_schedules = 'Horaires';
  static const String screen_timetables = 'Fiches horaires';
  static const String screen_atmo = 'Atmo';
  static const String screen_favorites = 'Favoris';
  static const String screen_informations = 'Informations';
}