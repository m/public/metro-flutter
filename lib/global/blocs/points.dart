import 'dart:async';
import 'dart:convert';

import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/filters_type.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_provider.dart';

class PointsBloc extends BlocBase {
  BehaviorSubject<List<Point>> _downloadController = BehaviorSubject<List<Point>>.seeded(List<Point>());
  /// Event called whenever an actor asked to download points in a given zone. Triggered by
  /// `requestPointsInZone` after download is complete.
  Stream<List<Point>> get onRequestedPointsDownloaded => _downloadController.stream;
  /// Current points in the requested zone.
  List<Point> get points => _downloadController.value;

  BehaviorSubject<List<Point>> _downloadPointController = BehaviorSubject<List<Point>>.seeded(List<Point>());
  /// Event called whenever an actor asked to download bus stop in a line.
  /// `_getPointsListWithIdLine` after download is complete.
  Stream<List<Point>> get onRequestedPointsLineDownloaded => _downloadPointController.stream;
  /// Current points in the requested zone.
  List<Point> get pointsList => _downloadPointController.value;

  BehaviorSubject<int> _updateDetailsTimeController = BehaviorSubject<int>.seeded(-1);
  /// Event called whenever an actor asked to download bus stop in a line.
  /// `_getPointsListWithIdLine` after download is complete.
  Stream<int> get onDetailsLineTimeUpdatedController => _updateDetailsTimeController.stream;
  /// Current points in the requested zone.
  int get indexTapped => _updateDetailsTimeController.value;

  StreamSubscription _onFilterChanged;
  String _filterRequestIds = '';

  @override
  void initState() {
    _onFilterChanged = BlocProvider.master<FiltersBloc>().onFiltersValueChanged.listen((data) {
      _filterRequestIds = '';
      for (int i = 0; i < data.entries.length; i++) {
        MapEntry<Filter, bool> entry = data.entries.elementAt(i);
        if (entry.value != null && entry.key.requestId != '' && entry.value) {

          if (entry.key.type == FiltersType.trams ||
              entry.key.type == FiltersType.buses ||
              entry.key.type == FiltersType.buses_chrono ||
              entry.key.type == FiltersType.buses_proximo ||
              entry.key.type == FiltersType.buses_flexo ||
              entry.key.type == FiltersType.c38 ||
              entry.key.type == FiltersType.voironnais ||
              entry.key.type == FiltersType.voironnais_interurbaines ||
              entry.key.type == FiltersType.voironnais_urbaines ||
              entry.key.type == FiltersType.gresivaudan ||
              entry.key.type == FiltersType.gresivaudan_structurante ||
              entry.key.type == FiltersType.gresivaudan_secondaires ||
              entry.key.type == FiltersType.trafic ||
              entry.key.type == FiltersType.trafic_event ||
              entry.key.type == FiltersType.trafic_state ||
              entry.key.type == FiltersType.bikes ||
              entry.key.type == FiltersType.bikes_amenage ||
              entry.key.type == FiltersType.bikes_chrono ||
              entry.key.type == FiltersType.bikes_difficile ||
              entry.key.type == FiltersType.bikes_nonamenage ||
              entry.key.type == FiltersType.bikes_tempo ||
              entry.key.type == FiltersType.bikes_verte ||
              entry.key.type == FiltersType.bikes_parking) continue;
          // Only add associated filters
          _filterRequestIds += entry.key.requestId + ',';
        }
      }_filterRequestIds = _filterRequestIds.substring(0, _filterRequestIds.length);
    });
  }

  @override
  void dispose() {
    _onFilterChanged.cancel();

    _downloadController.close();
    _downloadPointController.close();
    _updateDetailsTimeController.close();
  }

  /// Ask the bloc to fetch points in the given zone. Example of usage :
  /// ```
  /// requestPointsInZone(5.70470, 5.72656, 45.17004, 45.18523, 'PAR,PKG,agenceM,clusters');
  /// ```
  void requestPointsInZone(double xMin, double xMax, double yMin, double yMax) async {
    cleanPreviousRequest();
    List<Point> allPoints = await _fetchPoints(xMin, xMax, yMin, yMax, _filterRequestIds);

    if (_filterRequestIds.contains(PropertiesTypeExtension.getStringFromType(PropertiesType.citizyea))) allPoints.addAll(await _fetchCitizyea());
    if (_filterRequestIds.contains(PropertiesTypeExtension.getStringFromType(PropertiesType.tier))) allPoints.addAll(await requestPointsInZoneForTier(xMin, xMax, yMin, yMax));
    if (_filterRequestIds.contains(PropertiesTypeExtension.getStringFromType(PropertiesType.pony))) allPoints.addAll(await requestPointsInZoneForPony(xMin, xMax, yMin, yMax));

    // Remove all unvisible points
    allPoints.removeWhere((point) {
      if (point.properties.type == PropertiesType.clusters) return !(point.properties as ClustersProperties).visible;
      return false;
    });

    _downloadController.sink.add(allPoints);
  }

  Future<List<Point>> requestPointsInZoneForImport(double xMin, double xMax, double yMin, double yMax, String types) async {
    List<Point> allPoints = await _fetchPoints(xMin, xMax, yMin, yMax, types);

    // Remove all unvisible points
    allPoints.removeWhere((point) {
      if (point.properties.type == PropertiesType.clusters) return !(point.properties as ClustersProperties).visible;
      return false;
    });

    return allPoints;
  }

  Future<List<Point>> requestPointsInZoneForTier(double xMin, double xMax, double yMin, double yMax) async {
    List<Point> allPoints = await _fetchTier();
    List<Point> allPointsAdded = [];

    for (Point p in allPoints){
      if (p.geometry.coordinates[1] > yMin
          && p.geometry.coordinates[1] < yMax
          && p.geometry.coordinates[0] > xMin
          && p.geometry.coordinates[0] < yMax) allPointsAdded.add(p);
    }

    return allPointsAdded;
  }

  Future<List<Point>> requestPointsInZoneForPony(double xMin, double xMax, double yMin, double yMax) async {
    List<Point> allPoints = await _fetchPony();
    List<Point> allPointsAdded = [];

    for (Point p in allPoints){
      if (p.geometry.coordinates[1] > yMin
          && p.geometry.coordinates[1] < yMax
          && p.geometry.coordinates[0] > xMin
          && p.geometry.coordinates[0] < yMax) allPointsAdded.add(p);
    }

    return allPointsAdded;
  }

  Future<List<Point>> requestClustersWithQuery(String query) async {
    List<Point> allPoints = await _fetchWithFindType('arret', query);

    // Remove all unvisible points
    allPoints.removeWhere((point) {
      if (point.properties.type == PropertiesType.clusters) return !(point.properties as ClustersProperties).visible;
      return false;
    });

    return allPoints;
  }

  Future<List<Point>> requestPointsWithQuery(PropertiesType type, String query) {
    if (type == null) return null;

    return _fetchWithFindType(PropertiesTypeExtension.getStringFromType(type), query);
  }

  Future<List<Point>> requestCam() {
    return requestPointsWithQuery(PropertiesType.cam, '');
  }

  Future<List<Point>> requestLieux() {
    return requestPointsWithQuery(PropertiesType.lieux, '');
  }

  void cleanPreviousRequest() {
    _downloadController.sink.add(null);
  }

  void cleanPreviousRequestList() {
    _downloadPointController.sink.add(null);
  }

  void cleanPreviousRequestDetailsTime() {
    _updateDetailsTimeController.sink.add(null);
  }

  List<Point> _parsePoints(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<Point>.from(parsed['features'].map((json) => Point.fromJson(json)));
  }

  List<Point> _parseCitizyea(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<Point>.from(parsed['features'].map((json) => Point.fromJsonWithType(json, PropertiesType.citizyea)));
  }

  List<Point> _parseTier(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<Point>.from(parsed['features'].map((json) => Point.fromJsonWithType(json, PropertiesType.tier)));
  }

  List<Point> _parsePony(String responseBody) {
    final parsed = json.decode(responseBody);
    return List<Point>.from(parsed['data']['bikes'].map((json) => Point.fromJsonWithType(json, PropertiesType.pony)));
  }

  Future<List<Point>> _fetchPoints(double xMin, double xMax, double yMin, double yMax, String types) async {
    //Delete comma from request
    if (types.length > 0 && types[types.length -1 ] == ',') types = types.substring(0, types.length -1);

    final response = await BlocProvider.master<WebServices>().get('bbox/json?ymax=$yMax&xmin=$xMin&ymin=$yMin&xmax=$xMax&types=$types');

    if (response.statusCode == 200)
      return _parsePoints(response.body);
    else
      throw Exception('Failed to fetch points');
  }

  Future<List<Point>> _fetchWithFindType(String types, String query) async {
    final response = await BlocProvider.master<WebServices>().get('findType/json?types=$types&query=$query');

    if (response.statusCode == 200)
      return _parsePoints(response.body);
    else
      throw Exception('Failed to fetch points');
  }

  Future<List<Point>> _fetchCitizyea() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/citizyea/json');

    if (response.statusCode == 200)
      return _parseCitizyea(response.body);
    else
      throw Exception('Failed to fetch citizyea');
  }

  Future<List<Point>> _fetchTier() async {
    final response = await BlocProvider.master<WebServices>().get('dyn/tierVehicle/json');

    if (response.statusCode == 200)
      return _parseTier(response.body);
    else
      throw Exception('Failed to fetch tierVehicle');
  }

  Future<List<Point>> _fetchPony() async {
    final response = await BlocProvider.master<WebServices>().get('gbfs/pony_grenoble/free_bike_status');

    if (response.statusCode == 200)
      return _parsePony(response.body);
    else
      throw Exception('Failed to fetch ponyVehicle');
  }

  /// request to get list of point in a line
  void requestListPoints(String idLine) async {
    cleanPreviousRequestList();
    List<Point> pointsList = await getPointsListWithIdLine(idLine);
    _downloadPointController.sink.add(pointsList);
  }

  /// Get list of points in Json and convert it to lines.
  Future<List<Point>> getPointsListWithIdLine(String idLine) async {
    final response = await BlocProvider.master<WebServices>().get('routers/default/index/routes/$idLine/clusters');

    if (response.statusCode == 200)
      return _parseListPointsWithLine(response.body);
    else
      throw Exception('Failed to fetch Points');
  }

  /// Get list of points with line Id.
  List<Point> _parseListPointsWithLine(String responseBody) {
    final parsed = json.decode(responseBody);
    List<Point> _gettedList = List();
    if (parsed != null) {
      for (var item in parsed) {
        _gettedList.add(Point(
            properties: ClustersProperties(PropertiesType.clusters, item['id'], item['name'], item['city'], item['visible'], 1, item['code']),
            geometry: Geometry(coordinates: [
              double.tryParse(item['lon'].toString()),
              double.tryParse(item['lat'].toString()),
            ])));
      }
    }
    return _gettedList;
  }

  /// request to update Time line details
  void requestUpdateTime(int index) {
    cleanPreviousRequestDetailsTime();
    _updateDetailsTimeController.sink.add(index);
  }
}
