import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class ConsignesDataBloc extends BlocBase {
  BehaviorSubject<List<ConsignesData>> _downloadController;
  Stream<List<ConsignesData>> get onDownloadconsignesDataInfoCompleted => _downloadController.stream;
  List<ConsignesData> get consignesDataInfo => _downloadController.value;

  bool _isDownloading = false;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<ConsignesData>>.seeded(null);
    _downloadConsignesDataInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _isDownloading = false;
      _downloadConsignesDataInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadConsignesDataInfo() async {
    if (_isDownloading) return;
    _isDownloading = true;
    List<ConsignesData> consignesDataDetails = await _fetchConsignesDataInfo();
    _downloadController.sink.add(consignesDataDetails);
  }

  List<ConsignesData> _parseConsignesDataInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return ConsignesData.fromJson(parsed);
  }

  Future<List<ConsignesData>> _fetchConsignesDataInfo() async {

    final response = await BlocProvider.master<WebServices>().get('findType/json?types=MVC');

    if (response.statusCode == 200)
      return _parseConsignesDataInfo(response.body);
    else
      throw Exception('Failed to fetch CONSIGNES data');
  }

  Future<bool> loadConsignes() async {
    List<ConsignesData> results = await _fetchConsignesDataInfo();
    if (results != null) {
      return true;
    }
    return false;
  }

  ConsignesData getTypeConsignes(String idConsignes) {
    if(consignesDataInfo == null || consignesDataInfo.length == 0)
      return null;

    for (ConsignesData x in consignesDataInfo){
      if (x.id == idConsignes){
        return x;
      }
    }
  }

}

class ConsignesData {
  final String id;
  final String TYPE;
  final int TOTAL;

  ConsignesData(this.id, this.TYPE, this.TOTAL);

  static List<ConsignesData> fromJson(Map<String, dynamic> parsedJson) {

    List<ConsignesData> values = List<ConsignesData>();

    parsedJson['features'].map<ConsignesData>((e) {
        values.add(ConsignesData(
          e['properties']['id'].toString(),
          e['properties']['TYPE'],
          e['properties']['TOTAL'],
        ));

    }).toList();

    return values;
  }
}