import 'dart:convert';

import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:rxdart/rxdart.dart';

class ParkingDataBloc extends BlocBase {
  BehaviorSubject<List<ParkingData>> _downloadController;
  Stream<List<ParkingData>> get onDownloadparkingDataInfoCompleted => _downloadController.stream;
  List<ParkingData> get parkingDataInfo => _downloadController.value;

  bool _isDownloading = false;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<ParkingData>>.seeded(null);
    _downloadParkingDataInfo();

    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      _isDownloading = false;
      _downloadParkingDataInfo();
    });
  }

  @override
  void dispose() {
    _downloadController.close();
  }

  void _downloadParkingDataInfo() async {
    if (_isDownloading) return;
    _isDownloading = true;
    List<ParkingData> parkingDataDetails = await _fetchParkingDataInfo();
    _downloadController.sink.add(parkingDataDetails);
  }

  List<ParkingData> _parseParkingDataInfo(String responseBody) {
    final parsed = json.decode(responseBody);
    return ParkingData.fromJson(parsed);
  }

  Future<List<ParkingData>> _fetchParkingDataInfo() async {

    final response = await BlocProvider.master<WebServices>().get('findType/json?types=parking');

    if (response.statusCode == 200)
      return _parseParkingDataInfo(response.body);
    else
      throw Exception('Failed to fetch PARKING data');
  }

  Future<bool> loadParking() async {
    if (_isDownloading) return true;

    List<ParkingData> results = await _fetchParkingDataInfo();

    if (results != null) {
      return true;
    }
    return false;
  }

  ParkingData getTypeParking(String idParking) {
    if(parkingDataInfo == null || parkingDataInfo.length == 0)
      return null;

    for (ParkingData x in parkingDataInfo){
      if (x.id == idParking){
        return x;
      }
    }
  }

}

class ParkingData {
  final String id;
  final String nom;
  final String type_ouvrage;
  final String tarif_pmr;
  final int hauteur_max;
  final String info;
  final String tarif_1h;
  final String tarif_3h;
  final String tarif_4h;
  final String tarif_24h;

  ParkingData(this.id, this.nom, this.type_ouvrage, this.tarif_pmr, this.hauteur_max, this.info,
      this.tarif_1h, this.tarif_3h, this.tarif_4h, this.tarif_24h);

  static List<ParkingData> fromJson(Map<String, dynamic> parsedJson) {

    List<ParkingData> values = List<ParkingData>();

    parsedJson['features'].map<ParkingData>((e) {
      String free = e['properties']['tarif_pmr'];
      String ouvrage = e['properties']['type_ouvrage'];

      String tarif1 = e['properties']['tarif_1h'].toString();
      String tarif3 = e['properties']['tarif_3h'].toString();
      String tarif4 = e['properties']['tarif_4h'].toString();
      String tarif24 = e['properties']['tarif_24h'].toString();


      if (free == 'normal_payant'){
        free = "Parking payant";
      }else{
        free = "Parking gratuit";
      }

      if (ouvrage == 'ouvrage'){
        ouvrage = "Couvert";
      }else{
        ouvrage = "Non couvert";
      }

      values.add(ParkingData(
        e['properties']['id'],
        e['properties']['nom'],
        ouvrage,
        free,
        e['properties']['hauteur_max'],
        e['properties']['info'],
        tarif1,
        tarif3,
        tarif4,
        tarif24,
      ));

    }).toList();

    return values;
  }
}