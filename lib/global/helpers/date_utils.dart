import 'package:intl/intl.dart' show DateFormat;

const String _timeFormat = 'HH:mm';
const String _dateFormat = 'dd/MM/yyyy';
const String _dateFormatWithHyphens ='yyyy-MM-dd';

String formatDuration(Duration duration) {
  if (duration == null) return null;

  if (duration.inHours == 0)
    // duration < 1h
    return '${duration.inMinutes} min';

  List<String> splits = duration.toString().split(':');
  return '${duration.inHours} h ${splits[1]} min';
}

String formatTimeFromMilliseconds(int milliseconds) {
  try {
    return formatTimeFromDateTime(DateTime.fromMillisecondsSinceEpoch(milliseconds));
  } catch (_) {
    return 'N/A';
  }
}

String formatTimeFromDateTime(DateTime time) {
  try {
    return timeFormat.format(time);
  } catch (_) {
    return 'N/A';
  }
}

DateTime getDateFromSecondsFromMidnight(int seconds) {
  if(seconds == null)
    return null;

  DateTime now = DateTime.now();
  DateTime today = DateTime(now.year, now.month, now.day);

  return today.add(Duration(seconds: seconds));
}

Duration calculateDuration(DateTime start, DateTime end) {
  if (start == null || end == null)
    return null;
  Duration duration = start.toUtc().difference(end.toUtc());
  return duration.abs();
}

bool isSameDay(DateTime first, DateTime second) {
  if(first == null || second == null)
  return false;

  return first.year == second.year && first.month == second.month && first.day == second.day;
}

DateFormat get dateFormat => DateFormat(_dateFormat);
DateFormat get dateFormatWithHyphens => DateFormat(_dateFormatWithHyphens);
DateFormat get timeFormat => DateFormat(_timeFormat);