import 'package:metro/global/models/times.dart';

/// Class which give methods for processing [TimeModel].
class TimeHelper {
  /// Get arrival time from [time]
  /// Return `time.realtimeArrival` if `time.realtime = true` else `time.scheduledArrival`
  static int getTimeArrival(TimeModel time) {
    if (time == null) return 0;

    return time.realtime ? time.realtimeArrival : time.scheduledArrival;
  }

  /// Get time is realtime from [time].
  static bool getTimeIsRealtime(TimeModel time) {
    if (time == null) return false;
    return time.realtime;
  }

}