class Consts {
  // GPS positions
  static double grenobleMinLat = 44.894901;
  static double grenobleMaxLat = 45.454413;
  static double grenobleMinLng = 5.361396;
  static double grenobleMaxLng = 6.041091;

  static double agenceMetroLat = 45.179974;
  static double agenceMetroLng = 5.7128053;
  static double placeVictorHugoLat = 45.188805;
  static double placeVictorHugoLng = 5.725065;

  static double refreshLat;
  static double refreshLng;

  // Zoom levels
  static double minZoom = 10;
  static double maxZoom = 18;
  static double defaultZoom = 16;

  // Route
  /// Penalty for parking in certain cities.
  static const int penaltyInSeconds = 10 * 60;
  static const List<String> townsWithParkPenalty = [
    'GRENOBLE',
    'SEYSSINET-PARISET',
    'ECHIROLLES',
    'SAINT-MARTIN-D\'HERES',
    'GIERES',
    'LA TRONCHE',
    'FONTAINE',
    'MEYLAN'
  ];
  static const int startingMinElevation = 4810;
  static const double minElevationPercentage = 0.01;
  static const int legWalkMinDurationInMinutesToShowInResults = 2;

  //CO2 Rejection
  static const double carRejection = 205;
  static const double railCableCarRejection = 30;
  static const double tramRejection = 6.62;
  static const double busRejection = 144;
  static const int activeMobility = 0;

  /// It is the max entries we can have for the last search functionality in the search bar.
  static const int maxLastSearches = 3;

  static const String preferencesInfoModal = "preferences_info_modal";
  static const String preferencesHome = "preferences_home";
  static const String preferencesLanguage = "preferences_language";
  static const String homeTitle = "home.title";
  static const String languageTitle = "language.title";
  static const String themeTitle = "preferences.theme_title";
  static const String preferencesTitle = "preferences.title";
  static const String drawerPreferences = "drawer.preferences";

  static const String defaultFRLanguage = 'fr';
  static const String defaultWorldLanguage = 'en';

  static const String preferencesMap = "preferences.map";
  static const String preferencesFavorite = "preferences.favorite";
  static const String preferencesRoute = "preferences.route";
  static const String preferencesSchedule = "preferences.lines";

  static const List<String> languages = ["Français", "English"];
  static const List<String> appThemes = ["Dark", "Light"];

  static const String preferencesEventsInfoModal =
      "preferences_events_info_modal";
  static const String preferencesLightThemeModal =
      "preferences_light_theme_modal";
  static const String preferencesUpdateModal = "preferences_update_modal";

  static const String preferencesFilterIdSelected = "sharedActuallyFilterId";
}
