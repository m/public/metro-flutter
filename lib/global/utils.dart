import 'dart:core';

import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/search/widgets/search_delegate.dart';
import 'package:metro/atmo/widgets/atmo_search_delegate.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  // ---------------------------------------------------
  // ------------------ Navigation ---------------------
  // ---------------------------------------------------

  /// Open a search page that will return the selected value.
  static Future<Point> openSearch(BuildContext context, bool isSchedulePage) async {
    return await showSearch(context: context, delegate: CustomSearchDelegate(isSchedulePage));
  }

  static Future<CityInfo> openAtmoSearch(BuildContext context) async {
    return await showSearch(context: context, delegate: CustomAtmoSearchDelegate());
  }

  static void launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  // ---------------------------------------------------
  // --------------------- Math ------------------------
  // ---------------------------------------------------

  /// Return the value clamped between a min and a max.
  static double clamp(double min, double max, double value) {
    if (value.compareTo(min) < 0)
      return min;
    else if (value.compareTo(max) > 0)
      return max;
    return value;
  }

  // ---------------------------------------------------
  // -------------------- Color ------------------------
  // ---------------------------------------------------
  /// Return a value from a HEX code (FF0000 -> Colors.red).
  static Color colorFromHex(String color) {
    if(color == null)
      return null;
    return Color(int.parse('0xFF' + color));
  }

  /// Return a value from 3 ints (255, 0, 0 -> Colors.red).
  static Color colorFromWS(String color) {
    var colorSplit = color.split(',');
    return Color.fromRGBO(int.parse(colorSplit[0]), int.parse(colorSplit[1]), int.parse(colorSplit[2]), 1);
  }

  // ---------------------------------------------------
  // ------------------ Coordinates --------------------
  // ---------------------------------------------------

  static bool isPositionInGrenoble(double latitude, double longitude) {
    if(latitude == null || longitude == null)
      return false;

    return latitude > Consts.grenobleMinLat &&
      latitude < Consts.grenobleMaxLat &&
      longitude > Consts.grenobleMinLng &&
      longitude < Consts.grenobleMaxLng;
  }

  /// Return a `LatLng` object to be used with flutter_map
  /// from an array of 2 double (same format from metro WS).
  static LatLng coordinatesFromArray(List<double> array) {
    return LatLng(array[1], array[0]);
  }

  static LatLng latLngFromString(String coords){
    if(coords == null || coords.isEmpty)
      return null;

    List<String> splits = coords.split(',');
    assert(splits.length == 2, 'Error while parsing coordinates : not formated properly.');
    double double1 = double.tryParse(splits[0]);
    double double2 = double.tryParse(splits[1]);
    assert(double1 != null, 'Error while parsing latitude.');
    assert(double2 != null, 'Error while parsing longitude.');

    return LatLng(double1, double2);
  }

  // ---------------------------------------------------
  // -------------------- String -----------------------
  // ---------------------------------------------------

  static String formatRessource(String ressourceName, List<dynamic> args) {
    String result = '';
    try{
      RegExp reg;
      String text = translations.text(ressourceName);
      if(text.startsWith('/!\\'))
        return null;

      result = text;

      int count = 0;
      for(var arg in args) {
        reg =  RegExp('\\{$count\\}');
        if(!reg.hasMatch(result))
          throw('Too much arguments');
        result = result.replaceAll(reg, arg);

        count++;
      }

      reg =  RegExp('\\{$count\\}');
      if(reg.hasMatch(result))
        throw('Not enough arguments');
      return result;
    } catch (e) {
      debugPrintStack();
      return null;
    }
  }
}
