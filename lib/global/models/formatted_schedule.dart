import 'dart:ui';

import 'package:metro/global/blocs/lines.dart';

class FormattedScheduleModel {
  final String id;
  final String code;
  final String name;
  final String dir;
  final String stopId;
  final String stopName;
  final String firstNextArrival;
  final String secondNextArrival;
  final String thirdNextArrival;
  final String fourthNextArrival;
  final Color firstNextArrivalColor;
  final Color secondNextArrivalColor;
  final Color thirdNextArrivalColor;
  final Color fourthNextArrivalColor;
  final bool realtime;
  final Color color;
  final Color textColor;
  final ReadyLine readyLine;

  FormattedScheduleModel(
      this.id,
      this.code,
      this.name,
      this.dir,
      this.stopId,
      this.stopName,
      this.firstNextArrival,
      this.secondNextArrival,
      this.thirdNextArrival,
      this.fourthNextArrival,
      this.firstNextArrivalColor,
      this.secondNextArrivalColor,
      this.thirdNextArrivalColor,
      this.fourthNextArrivalColor,
      this.realtime,
      this.color,
      this.textColor,
      this.readyLine);
}
