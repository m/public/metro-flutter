class Line {
  final String id;
  final String gtfsId;
  final String shortName;
  final String longName;
  final String color;
  final String textColor;
  final String mode;
  final String type;

  Line({this.id, this.gtfsId, this.shortName, this.longName, this.color, this.textColor, this.mode, this.type});

  Line.fromStandard(this.id, this.gtfsId, this.shortName, this.longName, this.color, this.textColor, this.mode, this.type);

  factory Line.fromJson(Map<String, dynamic> json) {

    return Line(
      id: json['id'] as String,
      gtfsId: json['gtfsId'] as String,
      shortName: json['shortName'] as String,
      longName: json['longName'] as String,
      color: json['color'] as String,
      textColor: json['textColor'] as String,
      mode: json['mode'] as String,
      type: json['type'] as String,
    );
  }

  factory Line.copy(Line line, String type) {
    return Line(
      id: line.id,
      gtfsId: line.gtfsId,
      shortName: line.shortName,
      longName: line.longName,
      color: line.color,
      textColor: line.textColor,
      mode: line.mode,
      type: type,
    );
  }

  String get directions {
    if(this == null)
      return null;

    if(type == null || type.isEmpty)
      return longName;

    StringBuffer buffer = StringBuffer();

    switch(type) {
      case 'TRAM':
      case 'NAVETTE':
      case 'CHRONO':
      case 'PROXIMO':
      case 'FLEXO':
      case 'MCO':
        List<String> splits = longName.split('/');
        for(var i = 0; i < splits.length; i++) {
          buffer.write(splits[i].trim());
          if(i + 1 != splits.length)
            buffer.write('\n');
        }

        return buffer.toString();
      case 'C38':
      case 'Structurantes':
      case 'Secondaires':
      case 'Urbaines':
      case 'Interurbaines':
        if(id.startsWith('C38:EXP')) {
          List<String> firstSplits = longName.replaceAll('EXPRESS', '').split('(');
          if(firstSplits.length != 2)
            return longName;

          List<String> splits = firstSplits[0].split('-');
          for(var i = 0; i < splits.length; i++) {
            buffer.write(splits[i].trim());
            if(i + 1 != splits.length)
              buffer.write('\n');
          }
        } else {
          List<String> splits = longName.split('-');
          for(var i = 0; i < splits.length; i++) {
            if(i == 0 || i == splits.length - 1) {
              buffer.write(splits[i].trim());
              if(i == 0)
                buffer.write('\n');
            }
          }
        }
        return buffer.toString();

      default:
        return longName;
    }
  }
}
