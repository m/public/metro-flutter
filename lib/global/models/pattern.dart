class PatternModel {
  final String code;
  final String shortCode;
  final String name;
  final String dir;
  final String lastStop;

  // final String id;
  // final String desc;
  // final int dir;

  PatternModel({this.code, this.shortCode, this.name, this.dir, this.lastStop});

  factory PatternModel.fromJson(Map<String, dynamic> json) {
    // Get the code of type of transport (example : 12)
    var id = json['id'];
    var shortCode = '';
    if (id != null && id.toString().isNotEmpty) {
      var strings = id.toString().split(':');
      if (strings.length > 1) shortCode = '${strings[0]}:${strings[1]}:${json['desc']}:${json['dir'].toString()}';
    }

    return PatternModel(
        code: json['id'],
        shortCode: shortCode,
        name: json['desc'],
        dir: json['dir'].toString(),
        lastStop: json['lastStop'].toString()
    );
  }
}
