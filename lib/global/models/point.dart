import 'package:metro/global/database/database.dart';
import 'package:metro/global/models/properties.dart';

class Point {
  final Properties properties;
  final Geometry geometry;

  Point({this.properties, this.geometry});

  factory Point.fromJson(Map<String, dynamic> parsedJson) {
    return Point(
      properties: Properties.fromJson(parsedJson['properties']),
      geometry: Geometry.fromJson(parsedJson['geometry']),
    );
  }

  factory Point.fromJsonWithType(Map<String, dynamic> parsedJson, PropertiesType type) {
    if (type == PropertiesType.pony) {
      return Point(
        properties: Properties.fromJsonWithType(parsedJson, type),
        geometry: Geometry(coordinates: [
          double.parse(parsedJson['lon'].toString()),
          double.parse(parsedJson['lat'].toString())
        ]),
      );
    } else
      return Point(
        properties: Properties.fromJsonWithType(parsedJson['properties'], type),
        geometry: Geometry.fromJson(parsedJson['geometry']),
      );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['properties'] = properties.toJson();
    json['geometry'] = geometry.toJson();
    return json;
  }

  bool operator ==(dynamic other) {
    if(other is! Point) 
      return false;

    return this.properties.type == other.properties.type && this.properties.id == other.properties.id && this.properties.name == other.properties.name;
  }

  @override
  int get hashCode {
    return this.properties.type.hashCode + this.properties.id.hashCode + this.properties.name.hashCode;
  }
}

class Geometry {
  List<double> coordinates;

  Geometry({this.coordinates});

  factory Geometry.fromJson(Map<String, dynamic> parsedJson) {
    //return Geometry(coordinates: parsedJson['coordinates'].cast<double>());
    return Geometry(coordinates: parsedJson['coordinates'].map<double>((value) => 
       double.parse(value.toString())
    ).toList());
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['coordinates'] = coordinates;
    return json;
  }
}

class LastSearchPoint {
  final LastSearch lastSearch;
  final Point point;

  LastSearchPoint({this.lastSearch, this.point});
}
