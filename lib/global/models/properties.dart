import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:metro/global/blocs/agenceMetro.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/citiz.dart';
import 'package:metro/global/blocs/parking.dart';
import 'package:metro/global/blocs/parkingData.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/localization/translations.dart';

enum PropertiesType { undefined, clusters, agenceM, par, pkg, lieux, rue, recharge, citiz, citizyea, cam, stops, parkingCov, pointCov, autostop, depositaire, pointService, mvc, mva, hydrogene, gpl, gnv, tier, pony, irve, veloservice }

class PropertiesTypeExtension {
  static String getStringFromType(PropertiesType type) {
    switch (type) {
      case PropertiesType.undefined:
        return '';
      case PropertiesType.clusters:
        return 'clusters';
      case PropertiesType.agenceM:
        return 'agenceM';
      case PropertiesType.par:
        return 'PAR';
      case PropertiesType.pkg:
        return 'PKG';
      case PropertiesType.lieux:
        return 'lieux';
      case PropertiesType.rue:
        return 'rue';
      case PropertiesType.recharge:
        return 'irve';
      case PropertiesType.veloservice:
        return 'veloservice';
      case PropertiesType.citiz:
        return 'citiz';
      case PropertiesType.citizyea:
        return 'citizyea';
      case PropertiesType.tier:
        return 'tier';
      case PropertiesType.pony:
        return 'ponyVehicle';
      case PropertiesType.cam:
        return 'CAM';
      case PropertiesType.stops:
        return 'stops';
      case PropertiesType.parkingCov:
        return 'parkingCov';
      case PropertiesType.pointCov:
        return 'pointCov';
      case PropertiesType.autostop:
        return 'autostop';
      case PropertiesType.depositaire:
        return 'depositaire';
      case PropertiesType.pointService:
        return 'pointService';
      case PropertiesType.mvc:
        return 'MVC';
      case PropertiesType.mva:
        return 'MVA';
      case PropertiesType.hydrogene:
        return 'hydrogene';
      case PropertiesType.gpl:
        return 'gpl';
      case PropertiesType.gnv:
        return 'gnv';
      case PropertiesType.irve:
        return 'irve';
        break;
    }

    return getStringFromType(PropertiesType.undefined);
  }

  static PropertiesType getTypeFromString(String type) {
    switch (type) {
      case '':
        return PropertiesType.undefined;
      case 'clusters':
      case 'arret':
        return PropertiesType.clusters;
      case 'agenceM':
        return PropertiesType.agenceM;
      case 'PAR':
        return PropertiesType.par;
      case 'PKG':
        return PropertiesType.pkg;
      case 'lieux':
        return PropertiesType.lieux;
      case 'rue':
        return PropertiesType.rue;
      case 'irve':
        return PropertiesType.irve;
      case 'veloservice':
        return PropertiesType.veloservice;
      case 'citiz':
        return PropertiesType.citiz;
      case 'citizyea':
        return PropertiesType.citizyea;
      case 'tier':
        return PropertiesType.tier;
      case 'ponyVehicle':
        return PropertiesType.pony;
      case 'CAM':
        return PropertiesType.cam;
      case 'stops':
        return PropertiesType.stops;
      case 'parkingCov':
        return PropertiesType.parkingCov;
      case 'pointCov':
        return PropertiesType.pointCov;
      case 'autostop':
        return PropertiesType.autostop;
      case 'depositaire':
        return PropertiesType.depositaire;
      case 'pointService':
        return PropertiesType.pointService;
      case 'MVC':
        return PropertiesType.mvc;
      case 'MVA':
        return PropertiesType.mva;
      case 'hydrogene':
        return PropertiesType.hydrogene;
      case 'gpl':
        return PropertiesType.gpl;
      case 'gnv':
        return PropertiesType.gnv;
    }

    return getTypeFromString('');
  }

  static String getTypeName(PropertiesType type) {
    switch (type) {
        case PropertiesType.undefined:
        case PropertiesType.cam:
        case PropertiesType.rue:
        return'';
        break;
      case PropertiesType.veloservice:
        return 'Aire de service';
        break;
      default:
        return translations.text('POI.names.' + type.toString().split('.').last);
    }
  }

  static String getPoiAddress(Point point) {
    switch (point.properties.type) {
      case PropertiesType.pkg:
      case PropertiesType.mva:
      case PropertiesType.par:
      case PropertiesType.mvc:
        return (point.properties as ParkingProperties).address;
        break;
      case PropertiesType.agenceM:
        return (point.properties as AgencyProperties).address;
        break;
      default:
        return '';
    }
  }

  static String getDetailsPoiType(Point point) {
    switch (point.properties.type) {
      case PropertiesType.citiz:
        bool available = BlocProvider.master<CitizBloc>().isCitizVehicleAvailable(point.properties.id);
        if(available == null)
          return '';  // TODO : gerer le cas où le bloc n'a pas encore chargé les données, il faut donc rappeler cette fonction par la suite (utilisation d'un future?)
        return translations.text(available ? 'citiz.available' : 'citiz.unavailable_citiz');
      case PropertiesType.citizyea:
        return '';
      case PropertiesType.tier:
        return "Batterie " + (point.properties as TierProperties).batteryLevel.toString() + "%";
      case PropertiesType.pony:
        return "Batterie " + (point.properties as PonyProperties).batteryLevel.toString() + "%";
      case PropertiesType.irve:
        return (point.properties as IrveProperties).pdcNb + " bornes";
      case PropertiesType.cam:
        return '';
      case PropertiesType.veloservice:
        return 'Aire de service';
      case PropertiesType.pkg:
        ParkingData parkingData = BlocProvider.master<ParkingDataBloc>().getTypeParking(point.properties.id);
        return parkingData.tarif_pmr.toString();
        break;
      default:
        return translations.text('POI.names.' + point.properties.type.toString().split('.').last);
    }
  }

  static String getThirdDetailsPoiType(Point point) {
    switch (point.properties.type) {
      case PropertiesType.citiz:
      case PropertiesType.citizyea:
      case PropertiesType.cam:
        return '';
      case PropertiesType.irve:
        return (point.properties as IrveProperties).pdcTypes;
      case PropertiesType.agenceM:
        return BlocProvider.master<AgenceMetro>().getTypeMetro(point.properties.name).HORAIRES_LUNDI != null ? "Lun - Ven : " + BlocProvider.master<AgenceMetro>().getTypeMetro(point.properties.name).HORAIRES_LUNDI : '';
      case PropertiesType.pkg:
      case PropertiesType.par:
        return BlocProvider.master<ParkingBloc>().countAvailable(point.properties.id).toString() != "null" ?
        BlocProvider.master<ParkingBloc>().countAvailable(point.properties.id).toString() + ' ' + translations.text('bottomsheet_content.parking.places')
            : '';
        break;
      default:
        return '';
    }
  }

}

abstract class Properties {
  final PropertiesType type;
  final String id;
  final String name;
  final String city;

  Properties(this.type, this.id, this.name, this.city);

  factory Properties.fromJson(Map<String, dynamic> parsedJson) {
    PropertiesType type = PropertiesTypeExtension.getTypeFromString(parsedJson['type']);
    return Properties.fromJsonWithType(parsedJson, type);
  }

  factory Properties.fromJsonWithType(Map<String, dynamic> parsedJson, PropertiesType type) {
    switch (type) {
      case PropertiesType.undefined:
        return null;
      case PropertiesType.clusters:
        return ClustersProperties.fromJson(parsedJson);
      case PropertiesType.agenceM:
      case PropertiesType.depositaire:
      case PropertiesType.pointService:
        return AgencyProperties.fromJson(parsedJson, type);
      case PropertiesType.par:
      case PropertiesType.pkg:
      case PropertiesType.mvc:
      case PropertiesType.mva:
        return ParkingProperties.fromJson(parsedJson, type);
      case PropertiesType.irve:
        return IrveProperties.fromJson(parsedJson, type);
      case PropertiesType.veloservice:
        return BikeServiceProperties.fromJson(parsedJson);
      case PropertiesType.lieux:
        return PlaceProperties.fromJson(parsedJson);
      case PropertiesType.rue:
        return StreetProperties.fromJson(parsedJson);
      case PropertiesType.citiz:
        return CitizProperties.fromJson(parsedJson);
      case PropertiesType.citizyea:
        return CitizyeaProperties.fromJson(parsedJson);
      case PropertiesType.tier:
        return TierProperties.fromJson(parsedJson);
      case PropertiesType.pony:
        return PonyProperties.fromJson(parsedJson);
      case PropertiesType.cam:
        return CamProperties.fromJson(parsedJson);
      case PropertiesType.stops:
        return StopsProperties.fromJson(parsedJson);
      case PropertiesType.parkingCov:
      case PropertiesType.pointCov:
        return CarpoolingProperties.fromJson(parsedJson, type);
      case PropertiesType.autostop:
        return AutostopProperties.fromJson(parsedJson);
      case PropertiesType.hydrogene:
      case PropertiesType.gpl:
      case PropertiesType.gnv:
        return EnergyProperties.fromJson(parsedJson, type);
    }

    return null;
  }

  Map<String, dynamic> toJson();

  /// This function will specify if a point is visible to the user based on the zoom level of the map.
  bool isVisible(double zoom) {
    // TODO: check why sometimes zoom is null when displaying poiaround after opening a line details in schedule once
    if(zoom == null) {
      return false;
    }
    switch (type) {
      case PropertiesType.clusters:
        return zoom > 14;
      case PropertiesType.agenceM:
        return true;
      case PropertiesType.par:
      case PropertiesType.pkg:
        return zoom > 14;
      default:  // TODO : add case for all properties
        return zoom > 14;
    }
  }

  String markerPath(Elevations themedElevations, {bool isSelected = false}) {
    if (isSelected) return themedElevations.assetsPath + 'p_selected.png';
    switch (type) {
      case PropertiesType.undefined:
        return '';
      case PropertiesType.clusters:
        return themedElevations.assetsPath + 'p_clusters.png';
      case PropertiesType.agenceM:
        return themedElevations.assetsPath + 'p_agenceM.png';
      case PropertiesType.par:
        return themedElevations.assetsPath + 'p_PR_0.png';
      case PropertiesType.pkg:
        return themedElevations.assetsPath + 'p_PKG_0.png';
      case PropertiesType.lieux:
        return themedElevations.assetsPath + 'p_lieux.png';
      case PropertiesType.rue:
        return '';
      case PropertiesType.irve:
        return themedElevations.assetsPath + 'p_recharge.png';
      case PropertiesType.veloservice:
        return themedElevations.assetsPath + 'p_bike_service.png';
      case PropertiesType.citiz:
        return themedElevations.assetsPath + 'p_citiz.png';
      case PropertiesType.citizyea:
        return themedElevations.assetsPath + 'p_citizyea.png';
      case PropertiesType.tier:
        return themedElevations.assetsPath + 'p_kick_scooter.png';
      case PropertiesType.pony:
        return themedElevations.assetsPath + 'p_bicycle_rental_pony.png';
      case PropertiesType.cam:
        return themedElevations.assetsPath + 'CAM_1.png';
      case PropertiesType.stops:
        return themedElevations.assetsPath + 'p_stops.png';
      case PropertiesType.parkingCov:
        return themedElevations.assetsPath + 'p_parkingCov.png';
      case PropertiesType.pointCov:
        return themedElevations.assetsPath + 'p_pointCov.png';
      case PropertiesType.autostop:
        return themedElevations.assetsPath + 'p_autostop.png';
      case PropertiesType.depositaire:
        return themedElevations.assetsPath + 'p_relaisTAG.png';
      case PropertiesType.pointService:
        return themedElevations.assetsPath + 'p_pointService.png';
      case PropertiesType.mvc:
        return themedElevations.assetsPath + 'p_MV_Consigne.png';
      case PropertiesType.mva:
        return themedElevations.assetsPath + 'p_MV_agence.png';
      case PropertiesType.hydrogene:
        return themedElevations.assetsPath + 'p_hydrogene.png';
      case PropertiesType.gpl:
        return themedElevations.assetsPath + 'p_gpl.png';
      case PropertiesType.gnv:
        return themedElevations.assetsPath + 'p_gnv.png';
    }

    return '';
  }

  String iconPath() {
    switch (type) {
      case PropertiesType.undefined:
        return '';
      case PropertiesType.clusters:
        return 'assets/icons/clusters.png';
      case PropertiesType.agenceM:
        return 'assets/icons/agenceM.png';
      case PropertiesType.par:
        return 'assets/icons/PR_0.png';
      case PropertiesType.pkg:
        return 'assets/icons/PKG_0.png';
      case PropertiesType.lieux:
        return 'assets/icons/agenceM.png';
      case PropertiesType.rue:
        return '';
      case PropertiesType.irve:
        return 'assets/icons/recharge.png';
      case PropertiesType.veloservice:
        return 'assets/icons/CAM_1.png';
      case PropertiesType.citiz:
        return 'assets/icons/citiz.png';
      case PropertiesType.citizyea:
        return 'assets/icons/citizyea.png';
      case PropertiesType.tier:
        return 'assets/icons/p_kick_scooter.png';
      case PropertiesType.pony:
        return 'assets/icons/p_bicycle_rental_pony.png';
      case PropertiesType.cam:
        return 'assets/icons/CAM_1.png';
      case PropertiesType.stops:
        return 'assets/icons/stops.png';
      case PropertiesType.parkingCov:
        return 'assets/icons/parkingCov.png';
      case PropertiesType.pointCov:
        return 'assets/icons/pointCov.png';
      case PropertiesType.autostop:
        return 'assets/icons/autostop.png';
      case PropertiesType.depositaire:
        return 'assets/icons/relaisTAG.png';
      case PropertiesType.pointService:
        return 'assets/icons/pointService.png';
      case PropertiesType.mvc:
        return 'assets/icons/MV_Consigne.png';
      case PropertiesType.mva:
        return 'assets/icons/MV_agence.png';
      case PropertiesType.hydrogene:
        return 'assets/icons/hydrogene.png';
      case PropertiesType.gpl:
        return 'assets/icons/gpl.png';
      case PropertiesType.gnv:
        return 'assets/icons/gnv.png';
    }

    return '';
  }

  dynamic iconData() {
    switch (type) {
      case PropertiesType.clusters:
        return MetroIcons.clusters;
      case PropertiesType.agenceM:
        return MetroIcons.agence_m;
      case PropertiesType.par:
        return MetroIcons.par;
      case PropertiesType.pkg:
        return MetroIcons.pkg;
      case PropertiesType.irve:
        return MetroIcons.recharge;
      case PropertiesType.veloservice:
        return 'assets/icons/bike_service.svg';
      case PropertiesType.citiz:
        return MetroIcons.citiz;
      case PropertiesType.citizyea:
        return MetroIcons.citiz_yea;
      case PropertiesType.tier:
        return 'assets/icons/kick-scooter-icon.svg';
      case PropertiesType.pony:
        return 'assets/icons/pony_logo.svg';
      case PropertiesType.stops:
        return MetroIcons.clusters;
      case PropertiesType.parkingCov:
        return MetroIcons.parking_cov;
      case PropertiesType.pointCov:
        return MetroIcons.point_cov;
      case PropertiesType.autostop:
        return MetroIcons.autostop;
      case PropertiesType.depositaire:
        return MetroIcons.relais_tag;
      case PropertiesType.pointService:
        return MetroIcons.point_service;
      case PropertiesType.mvc:
        return MetroIcons.mvc;
      case PropertiesType.mva:
        return MetroIcons.mva;
      case PropertiesType.hydrogene:
        return MetroIcons.hydrogene;
      case PropertiesType.gpl:
        return MetroIcons.gpl;
      case PropertiesType.gnv:
        return MetroIcons.gnv;
      case PropertiesType.lieux:
        return Icons.location_city;
      case PropertiesType.rue:
        return Icons.place;
      case PropertiesType.cam:
        return Icons.videocam;

      case PropertiesType.undefined:
      default:  // TODO : handle all other cases and remove the default value that way if there is new types, and error will show up
        return Icons.warning;
    }
  }
}

//Used by search delegate
class PlaceProperties extends Properties{
  final int dist;
  PlaceProperties(type, id, name, city, this.dist) : super(type, id, name, city);

  factory PlaceProperties.fromJson(Map<String, dynamic> parsedJson) {
    // String distance = parsedJson['dist'].toString();
    // int distanceAsInt = distance != null ? int.parse(distance) : -1;
    return PlaceProperties(PropertiesType.lieux, parsedJson['id'].toString(), parsedJson['LIBELLE'], parsedJson['COMMUNE'], -1);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = 'lieux';
    json['id'] = id;
    json['LIBELLE'] = name;
    json['COMMUNE'] = city;
    json['dist'] = dist;
    return json;
  }
}

//Used by search delegate
class StreetProperties extends Properties{
  final int dist;
  StreetProperties(type, id, name, city, this.dist) : super(type, id, name, city);

  factory StreetProperties.fromJson(Map<String, dynamic> parsedJson) {
    String distance = parsedJson['dist'].toString();
    int distanceAsInt = distance != null ? int.parse(distance) : -1;
    return StreetProperties(PropertiesType.rue, parsedJson['id'].toString(), parsedJson['LIBELLE'], parsedJson['COMMUNE'], distanceAsInt);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = 'rue';
    json['id'] = id;
    json['rue'] = name;
    json['COMMUNE'] = city;
    json['dist'] = dist;
    return json;
  }
}

/// Used by clusters.
class ClustersProperties extends Properties {
  final bool visible;
  final int dist;
  final String code;

  ClustersProperties(type, id, name, city, this.visible, this.dist, this.code) : super(type, id, name, city);

  factory ClustersProperties.fromJson(Map<String, dynamic> parsedJson) {
    bool isArret = parsedJson['type'] == 'arret';
    String name = isArret ? parsedJson['LIBELLE'] : parsedJson['name'];
    String city = isArret ? parsedJson['COMMUNE'] : parsedJson['city'];
    bool visible = isArret ? parsedJson['arr_visible'] == "1" ?? false : parsedJson['visible'];
    String distance = parsedJson['dist'].toString();
    String code = isArret ? (parsedJson['CODE'] as String)?.replaceFirst('_', ':') : parsedJson['code'];
    int distanceAsInt = distance == null || distance.isEmpty || distance == 'null' ? -1 : int.parse(distance);
    return ClustersProperties(PropertiesType.clusters, parsedJson['id'].toString(), name, city, visible, distanceAsInt, code);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = 'clusters';
    json['id'] = id;
    json['name'] = name;
    json['city'] = city;
    json['visible'] = visible;
    json['dist'] = dist;
    json['code'] = code;
    return json;
  }
}

/// Used by AgenceM & depositaire & pointService
class AgencyProperties extends Properties {
  final String code;
  final String address;

  AgencyProperties(type, id, name, city, this.code, this.address) : super(type, id, name, city);

  factory AgencyProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {
    return AgencyProperties(type, parsedJson['id'].toString(), parsedJson['NOM'], parsedJson['COMMUNE'], parsedJson['CODE'].toString(), parsedJson['RUE']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['NOM'] = name;
    json['COMMUNE'] = city;
    json['CODE'] = code;
    json['RUE'] = address;
    return json;
  }
}

/// Used by recharge.
class CodeProperties extends Properties {
  final String code;

  CodeProperties(type, id, name, city, this.code) : super(type, id, name, city);

  factory CodeProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {
    return CodeProperties(type, parsedJson['id'].toString(), parsedJson['LIBELLE'], '', parsedJson['CODE']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['LIBELLE'] = name;
    json['CODE'] = code;
    return json;
  }
}

class IrveProperties extends Properties {
  final String code;
  final String amenageur;
  final List<IrvePdc> pdc;
  final String pdcTypes;
  final String pdcNb;

  IrveProperties(type, id, name, city, this.code, this.amenageur, this.pdc, this.pdcTypes, this.pdcNb) : super(type, id, name, city);

  factory IrveProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {

    List<IrvePdc> irvePdc = List<IrvePdc>.from((parsedJson['pdc'] as List).map((json) => IrvePdc.fromJson(json)));
    String irveTypes = "";
    List<String> irveTypesList = [];

    for (IrvePdc irvePdc in irvePdc){
      irveTypesList.add(irvePdc.type_prise);
    }

    if (irveTypesList.contains("CHADEMO - COMBO") && irveTypesList.contains("EF - T2")){
      irveTypes = "E/F - T2 • CHAdeMO";
    }if (irveTypesList.contains("CHADEMO - COMBO") && irveTypesList.contains("EF + T2")){
      irveTypes = "E/F + T2 • CHAdeMO";
    } else if (irveTypesList.contains("CHADEMO - COMBO")){
      irveTypes = "CHAdeMO";
    } else if (irveTypesList.contains("EF - T2")){
      irveTypes = "E/F - T2";
    }else if (irveTypesList.contains("EF + T2")){
      irveTypes = "E/F + T2";
    }else{
      irveTypes = "E/F T2";
    }


    String irveName = parsedJson['n_station'].toString().replaceAll("GRENOBLE - ", "");

    return IrveProperties(type, parsedJson['id'].toString(), irveName, '', parsedJson['id_station'],
        parsedJson['n_amenageur'], irvePdc, irveTypes, irvePdc.length.toString());
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = 'irve';
    json['id'] = id;
    json['id_station'] = code;
    json['n_amenageur'] = amenageur;
    json['pdc'] = jsonEncode(pdc);
    return json;
  }
}

class BikeServiceProperties extends Properties {
  final String code;
  final String service;

  BikeServiceProperties(type, id, name, city, this.code, this.service) : super(type, id, name, city);

  factory BikeServiceProperties.fromJson(Map<String, dynamic> parsedJson) {
    return BikeServiceProperties(PropertiesType.veloservice, parsedJson['id'].toString(), parsedJson['nom'], '', parsedJson['CODE'].toString(), parsedJson['service']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = 'veloservice';
    json['id'] = id;
    json['nom'] = name;
    json['CODE'] = code;
    json['service'] = service;
    return json;
  }
}

class IrvePdc {
  final String id_pdc;
  final String puiss_max;
  final String type_prise;

  IrvePdc(this.id_pdc, this.puiss_max, this.type_prise);

  factory IrvePdc.fromJson(Map<String, dynamic> parsedJson) {
    return IrvePdc(parsedJson['id_pdc'], parsedJson['name'], parsedJson['type_prise']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['id_pdc'] = id_pdc;
    json['name'] = puiss_max;
    json['type_prise'] = type_prise;
    return json;
  }
}

class CamProperties extends CodeProperties {
  final int cap;
  final int nsv;

  CamProperties(type, id, name, city, String code, this.cap, this.nsv) : super(type, id, name, city, code);

  factory CamProperties.fromJson(Map<String, dynamic> parsedJson) {
    CodeProperties p = CodeProperties.fromJson(parsedJson, PropertiesType.cam);
    return CamProperties(p.type, p.id, p.name, p.city, p.code, parsedJson['CAP'], parsedJson['nsv']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson();
    json['CAP'] = cap;
    json['nsv'] = nsv;
    return json;
  }
}

class ParkingProperties extends Properties {
  final String code;
  final String address;
  final int total;
  final int dist;

  ParkingProperties(type, id, name, city, this.code, this.address, this.total, this.dist) : super(type, id, name, city);

  factory ParkingProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {
    String distance;
    if(parsedJson['dist'] != null)
      distance = parsedJson['dist'].toString();
    int distanceAsInt = distance != null ? int.parse(distance) : -1;
    return ParkingProperties(type, parsedJson['id'].toString(), parsedJson['LIBELLE'], parsedJson['COMMUNE'], parsedJson['CODE'].toString(), parsedJson['ADRESSE'], parsedJson['TOTAL'], distanceAsInt);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['LIBELLE'] = name;
    json['COMMUNE'] = city;
    json['CODE'] = code;
    json['ADRESSE'] = address;
    json['TOTAL'] = total;
    json['dist'] = dist.toString();
    return json;
  }
}

/// Used by autostop.
class AutostopProperties extends Properties {
  final String code;
  final String address;
  final String direction1;
  final String direction2;
  final String direction3;

  AutostopProperties(type, id, name, city, this.code, this.address, this.direction1, this.direction2, this.direction3) : super(type, id, name, city);

  factory AutostopProperties.fromJson(Map<String, dynamic> parsedJson) {
    return AutostopProperties(PropertiesType.autostop, parsedJson['id'].toString(), parsedJson['Nom du point'], parsedJson['Nom de la Commune'], parsedJson['CODE'].toString(), parsedJson['Adresse'], parsedJson['Direction 1'], parsedJson['Direction 2'], parsedJson['Direction 3']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['Nom du point'] = name;
    json['Nom de la Commune'] = city;
    json['CODE'] = code;
    json['Adresse'] = address;
    json['Direction 1'] = direction1;
    json['Direction 2'] = direction2;
    json['Direction 3'] = direction3;
    return json;
  }
}

class CitizProperties extends Properties {
  final String code;
  final List<CitizVehicule> vehicules;

  CitizProperties(type, id, name, city, this.code, this.vehicules) : super(type, id, name, city);

  factory CitizProperties.fromJson(Map<String, dynamic> parsedJson) {
    return CitizProperties(PropertiesType.citiz, parsedJson['id'].toString(), parsedJson['name'], parsedJson['city'], parsedJson['CODE'], List<CitizVehicule>.from((parsedJson['vehicles'] as List).map((json) => CitizVehicule.fromJson(json))));
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['name'] = name;
    json['city'] = city;
    json['CODE'] = code;
    json['vehicles'] = jsonEncode(vehicules);
    return json;
  }
}

class CitizVehicule {
  final int id;
  final String name;
  final bool electricEngine;

  CitizVehicule(this.id, this.name, this.electricEngine);

  factory CitizVehicule.fromJson(Map<String, dynamic> parsedJson) {
    return CitizVehicule(parsedJson['id'], parsedJson['name'], parsedJson['electricEngine']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['id'] = id;
    json['name'] = name;
    json['electricEngine'] = electricEngine;
    return json;
  }
}

class CitizyeaProperties extends Properties {
  final String licencePlate;
  final int fuelLevel;

  CitizyeaProperties(type, id, name, city, this.licencePlate, this.fuelLevel) : super(type, id, name, city);

  factory CitizyeaProperties.fromJson(Map<String, dynamic> parsedJson) {
    int fuel = parsedJson['fuelLevel'];
    return CitizyeaProperties(PropertiesType.citizyea, parsedJson['code'].toString(), parsedJson['name'], '', parsedJson['licencePlate'], fuel > 100 ? 100 : fuel);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['name'] = name;
    json['licencePlate'] = licencePlate;
    json['fuelLevel'] = fuelLevel;
    return json;
  }
}

class TierProperties extends Properties {
  final int batteryLevel;
  final int maxSpeed;
  final String deepLink;

  TierProperties(type, id, name, city, this.batteryLevel, this.maxSpeed, this.deepLink) : super(type, id, name, city);

  factory TierProperties.fromJson(Map<String, dynamic> parsedJson) {
    return TierProperties(PropertiesType.tier, parsedJson['code'].toString(), 'TIER', '', parsedJson['batteryLevel'], parsedJson['maxSpeed'], parsedJson['deepLink']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['batteryLevel'] = batteryLevel;
    json['maxSpeed'] = maxSpeed;
    json['deepLink'] = deepLink;
    return json;
  }
}

class PonyProperties extends Properties {
  final int batteryLevel;
  final String deepLink;

  PonyProperties(type, id, name, city, this.batteryLevel, this.deepLink) : super(type, id, name, city);

  factory PonyProperties.fromJson(Map<String, dynamic> parsedJson) {
    return PonyProperties(PropertiesType.pony, parsedJson['bike_id'].toString(), 'PONY', '', 1, "parsedJson['deepLink']");
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['bike_id'] = id;
    // json['batteryLevel'] = batteryLevel;
    // json['deepLink'] = deepLink;
    return json;
  }
}

class StopsProperties extends Properties {
  final String gtfsId;
  final String gtfsClusterId;

  StopsProperties(type, id, name, city, this.gtfsId, this.gtfsClusterId) : super(type, id, name, city);

  factory StopsProperties.fromJson(Map<String, dynamic> parsedJson) {
    return StopsProperties(PropertiesType.stops, parsedJson['id'].toString(), parsedJson['name'], parsedJson['city'], parsedJson['gtfsId'], parsedJson['clusterGtfsId']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['name'] = name;
    json['city'] = city;
    json['gtfsId'] = gtfsId;
    json['clusterGtfsId'] = gtfsClusterId;
    return json;
  }
}

// Used by parkingCov & pointCov
class CarpoolingProperties extends Properties {
  final String placeId;
  final bool isOpen;
  final int placeLeft;
  final List<String> nearbyLinesIds;
  final String typeOfParking;

  CarpoolingProperties(type, id, name, city, this.placeId, this.isOpen, this.placeLeft, this.nearbyLinesIds, this.typeOfParking) : super(type, id, name, city);

  factory CarpoolingProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {
    return CarpoolingProperties(type, parsedJson['id'].toString(), parsedJson['nom_lieu'], parsedJson['com_lieu'], parsedJson['id_lieu'], parsedJson['ouvert'] == 'true' ? true : false, parsedJson['nbre_pl'], parsedJson['Transport public'].toString().split(', '), parsedJson['typeCov']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['nom_lieu'] = name;
    json['com_lieu'] = city;
    json['id_lieu'] = placeId;
    json['ouvert'] = isOpen.toString();
    json['nbre_pl'] = placeLeft;

    String nearbyLinesIdsPlane = '';
    for(int i = 0; i < nearbyLinesIds.length; i++) {
      nearbyLinesIdsPlane += nearbyLinesIds[i];

      if(i < nearbyLinesIds.length - 1)
        nearbyLinesIdsPlane += ', ';
    }

    json['Transport public'] = nearbyLinesIdsPlane;
    json['typeCov'] = typeOfParking;
    return json;
  }
}

/// Used by hydrogen, gpl, gnv
class EnergyProperties extends Properties {
  final String address;

  EnergyProperties(type, id, name, city, this.address) : super(type, id, name, city);

  factory EnergyProperties.fromJson(Map<String, dynamic> parsedJson, PropertiesType type) {
    return EnergyProperties(type, parsedJson['id'], parsedJson['nom'], parsedJson['commune'], parsedJson['adresse']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = Map();
    json['type'] = PropertiesTypeExtension.getStringFromType(type);
    json['id'] = id;
    json['name'] = name;
    json['city'] = city;
    json['adresse'] = address;
    return json;
  }
}
