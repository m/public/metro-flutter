import 'package:intl/intl.dart';

class TimeModel {
  final String stopId;
  final String stopName;
  final int scheduledArrival;
  final int scheduledDeparture;
  int realtimeArrival;
  final int realtimeDeparture;
  final int arrivalDelay;
  final int departureDelay;
  final bool timepoint;
  final bool realtime;
  final int serviceDay;
  final dynamic tripId;

  TimeModel(
      {this.stopId,
        this.stopName,
        this.scheduledArrival,
        this.scheduledDeparture,
        this.realtimeArrival,
        this.realtimeDeparture,
        this.arrivalDelay,
        this.departureDelay,
        this.timepoint,
        this.realtime,
        this.serviceDay,
        this.tripId});

  factory TimeModel.fromJson(Map<String, dynamic> json) {
    return TimeModel(
      stopId: json['stopId'],
      stopName: json['stopName'],
      scheduledArrival: json['scheduledArrival'],
      scheduledDeparture: json['scheduledDeparture'],
      realtimeArrival: json['realtimeArrival'],
      realtimeDeparture: json['realtimeDeparture'],
      arrivalDelay: json['arrivalDelay'],
      departureDelay: json['departureDelay'],
      timepoint: json['timepoint'],
      realtime: json['realtime'],
      serviceDay: json['serviceDay'],
      tripId: json['tripId'],
    );
  }

  void setRealTime() {}

  /// Get time to display from [this].
  /// Formated as string :
  /// `X` min where `X` is a number
  /// Or `<1` min
  /// Or `>1` h
  String getTimeToDisplay() {
    String result = '';
    if (this == null) return '';

    final now = DateTime.now();
    final lastMidnight =
        DateTime.fromMillisecondsSinceEpoch(this.serviceDay * 1000);

    final lastGoodMidnight = DateTime(now.year, now.month, now.day);

    if (lastMidnight == lastGoodMidnight) {
      int timeToCalc =
          (this.realtime ? this.realtimeArrival : this.scheduledArrival);
      int diff = timeToCalc - now.difference(lastMidnight).inSeconds;

      if (diff >= 60 && diff < 3600)
        result = (diff / 60).toStringAsFixed(0); // min
      if (diff < 60) result = '<1'; //min
      if (diff >= 3600) {
        //Problème heure de décalage avec page horaire métro (- 3600)
        final formattedDate = DateFormat('HH:mm')
            .format(DateTime.fromMillisecondsSinceEpoch((timeToCalc - 3600) * 1000));
        result = '$formattedDate'; //h
      }
    } else
      result = '>1'; // next day

    return result;
  }
}
