import 'package:flutter/material.dart';

class LinesDetails {
  final List<Feature> features;

  LinesDetails({this.features});

  factory LinesDetails.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['features'] as List;
    List<Feature> featuresList = list.map((i) => Feature.fromJson(i)).toList();

    return LinesDetails(
      features: featuresList,
    );
  }

  Feature getFeature(String lineId) {
    // TODO : better solution when a line has no associated feature
    return features.firstWhere((v) {
      return v.properties.id == lineId.replaceAll(':', '_');
    }, orElse: () {
      return features.first;
    });
  }
}

class Feature {
  final Properties properties;
  final Geometry geometry;

  Feature({this.properties, this.geometry});

  factory Feature.fromJson(Map<String, dynamic> parsedJson) {
    return Feature(
      properties: Properties.fromJson(parsedJson['properties']),
      geometry: Geometry.fromJson(parsedJson['geometry']),
    );
  }
}

class Properties {
  final String id;
  final String numero;
  final String code;
  final String couleur;
  final String couleurTexte;
  final int pmr;
  final String libelle;
  final List<String> zonesArret;
  final String type;

  Properties({this.id, this.numero, this.code, this.couleur, this.couleurTexte, this.pmr, this.libelle, this.zonesArret, this.type});

  factory Properties.fromJson(Map<String, dynamic> parsedJson) {
    var zonesFromJson = parsedJson['ZONES_ARRET'];
    List<String> zonesList = zonesFromJson.cast<String>();

    return Properties(
      id: parsedJson['id'],
      numero: parsedJson['NUMERO'],
      code: parsedJson['CODE'],
      couleur: parsedJson['COULEUR'],
      couleurTexte: parsedJson['COULEUR_TEXTE'],
      pmr: parsedJson['PMR'],
      libelle: parsedJson['LIBELLE'],
      zonesArret: zonesList,
      type: parsedJson['type'],
    );
  }
}

class Geometry {
  final List<List<List<double>>> coordinates;

  Geometry({this.coordinates});

  factory Geometry.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null || parsedJson.length == 0) {
      // If the line has no geometry
      return null;
    }

    var coordinate;

    // I had to rebuild all the list because of an error:
    // 'List<dynamic>' is not a subtype of type 'List<List<List<double>>>'
    List<List<List<double>>> result = List<List<List<double>>>();
    for (var i = 0; i < parsedJson['coordinates'].length; i++) {
      List<List<double>> part = List<List<double>>();
      for (var j = 0; j < parsedJson['coordinates'][i].length; j++) {
        List<double> coordinates = List<double>();
        for (var k = 0; k < parsedJson['coordinates'][i][j].length; k++) {
          coordinate = parsedJson['coordinates'][i][j][k];
          coordinates.add(coordinate.toDouble());
        }
        part.add(coordinates);
      }
      result.add(part);
    }

    return Geometry(coordinates: result);
  }
}
