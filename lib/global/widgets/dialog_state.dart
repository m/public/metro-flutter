import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:preferences/preference_service.dart';
import 'package:metro/localization/translations.dart';
import '../consts.dart';

class MyDialog extends StatefulWidget{
  @override
  // TODO: implement createState
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog>{

  bool showInfoModal = PrefService.getBool(Consts.preferencesInfoModal) ?? true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AlertDialog(
      title: Text(translations.text("favorites.modal_title")),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SingleChildScrollView(
            child: Text(translations.text("favorites.modal_content")),
          ),
          Row(children: <Widget>[
            Checkbox(
              value: !showInfoModal,
              onChanged: (bool value) => setState(() {
                showInfoModal = !showInfoModal;
              }),
            ),
            Text(
                translations.text("favorites.modal_not_display")
            )
          ],),

        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(translations.text("favorites.modal_close")),
          onPressed: () {
            PrefService.setBool(Consts.preferencesInfoModal, showInfoModal);
            BlocProvider.master<NavigationBloc>().switchInfoPopup();
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }
}