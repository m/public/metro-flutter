import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';

class PDFScreen extends StatelessWidget {
  final String pathPDF;
  final String title;
  final bytes;

  PDFScreen(this.pathPDF, this.title, this.bytes);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () async {
                await Share.file(
                    title,
                    pathPDF.substring(pathPDF.lastIndexOf("/") + 1),
                    bytes.buffer.asUint8List(),
                    'application/pdf');
              },
            ),
          ],
        ),
        path: pathPDF);
  }
}
