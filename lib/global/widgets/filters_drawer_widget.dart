// import 'package:flutter/material.dart';
// import 'package:metro/filters/blocs/filters_bloc.dart';
// import 'package:metro/filters/filters_type.dart';
// import 'package:metro/filters/models/filter.dart';
// import 'package:metro/filters/pages/filters_page.dart';
// import 'package:metro/filters/widgets/ChipTile.dart';
// import 'package:metro/global/blocs/bloc_provider.dart';
// import 'package:metro/global/widgets/expansion_panel.dart';
// import 'package:metro/localization/translations.dart';
// import 'package:metro/global/elevation.dart';
// import 'package:metro/route/widgets/route_details.dart';
//
// class FiltersDrawerWidget extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => FiltersDrawerWidgetState();
// }
//
// class FiltersDrawerWidgetState extends State<FiltersDrawerWidget> {
//   List<Widget> _switches = List();
//   List<Widget> list = List();
//   Map<FiltersType, CheckboxTileController> _controllers = Map();
//   FiltersBloc _bloc;
//   List<TravelMode> _data = List<TravelMode>();
//   Color chipColor = globalElevations.blueColor.backgroundColor;
//   Color chipTextColor;
//   bool chipSelected;
//   List<Filter> filtersList = new List<Filter>();
//   bool checkBoxValue;
//
//   CheckboxTile _createCheckboxTile(Filter filter) {
//     return CheckboxTile(
//       controller: _bloc.createController(filter),
//       parameters: CheckboxTileParameters(
//         filter.title,
//         value: _bloc.filtersValue[filter],
//         filter: filter,
//         onChanged: (value) {
//           _bloc.onValueChange(filter, value, "_createCheckboxTile");
//           if (filter != null) {
//             _bloc.subfiltersValueController.sink.add(true);
//           }
//         },
//       ),
//     );
//   }
//
//   Widget _createChipTile(Filter filter, Filter filterParent) {
//     return ChipTile(
//       controller: _bloc.createController(filter),
//       parameters: ChipTileParameters(filter.title, chipColor,
//           value: _bloc.filtersValue[filter], haveIcon: filter.haveIcon, iconPath: filter.iconPath, filter: filter, filterParent: filterParent),
//     );
//   }
//
//   travelModesCreation() {
//     TravelMode publicTransport = new TravelMode(id: 1, code: "public_transport", value: translations.text("transport_type.public"));
//     TravelMode bike = new TravelMode(id: 2, code: "bike", value: translations.text("transport_type.bike"));
//     TravelMode tier = new TravelMode(id: 3, code: "tier", value: translations.text("transport_type.tier"));
//     TravelMode car = new TravelMode(id: 4, code: "car", value: translations.text("transport_type.car"));
//     _data.add(publicTransport);
//     _data.add(bike);
//     _data.add(tier);
//     _data.add(car);
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     travelModesCreation();
//     _bloc = BlocProvider.of<FiltersBloc>(context);
//     filtersList = _bloc.filters;
//     _switches = _bloc.filters.map((filter) {
//       checkBoxValue = _bloc.filtersValue[filter];
//       if (filter.hasSubFilters) {
//         chipSelected = _bloc.filtersValue[filter];
//
//         _bloc.subfiltersValueController.sink.add(chipSelected);
//
//         return CheckboxGroupTile(
//           choice: _createCheckboxTile(filter),
//           subChoices: filter.subFilters.map((subFilter) {
//             return _createChipTile(subFilter, filter);
//           }).toList(),
//         );
//       } else {
//         return _createCheckboxTile(filter);
//       }
//     }).toList();
//   }
//
//   Widget _buildPanel() {
//     return ExpansionPanelListTest(
//       expandedHeaderPadding: null,
//       expansionCallback: (int index, bool isExpanded) {
//         setState(() {
//           _data[index].isExpanded = !isExpanded;
//         });
//       },
//       children: _data.map<ExpansionPanelTest>((TravelMode tm) {
//         return ExpansionPanelTest(
//           canTapOnHeader: true,
//           headerBuilder: (BuildContext context, bool isExpanded) {
//             return Column(
//               children: <Widget>[
//                 Container(
//                   child: ListTile(
//                     title: Text(tm.value.toString(), style: TextStyle(fontSize: 16)),
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.only(left: 16.0, right: 16.0),
//                   child: Container(
//                     alignment: Alignment.centerLeft,
//                     child: Wrap(
//                       alignment: WrapAlignment.start,
//                       children: getFilterListInFunctionOfTravelMode(tm.code).map((filter) {
//                         checkBoxValue = _bloc.filtersValue[filter];
//                         return (_bloc.filtersValue[filter] == true || _bloc.filtersValue[filter] == null) ? Padding(
//                           padding: const EdgeInsets.only(right: 8.0, bottom: 16),
//                           child: showFilters(filter.title),
//                         ) : EmptyCell();
//                       }).toList(),
//                     ),
//                   ),
//                 )
//               ],
//             );
//           },
//           body: Column(
//             children: <Widget>[
//               Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: getFilterListInFunctionOfTravelMode(tm.code).map((filter) {
//                     checkBoxValue = _bloc.filtersValue[filter];
//                     if (filter.hasSubFilters) {
//                       return CheckboxGroupTile(
//                         choice: _createCheckboxTile(filter),
//                         subChoices: filter.subFilters.map((subFilter) {
//                           return _createChipTile(subFilter, filter);
//                         }).toList(),
//                       );
//                     } else {
//                       return _createCheckboxTile(filter);
//                     }
//                   }).toList(),),
//               Container(color: Colors.grey, height: 0.8,)
//             ],
//           ),
//           isExpanded: tm.isExpanded,
//         );
//       }).toList(),
//     );
//   }
//
//   Widget showFilters(String filter) {
//     return Padding(
//       padding: const EdgeInsets.only(right: 4.0),
//       child: Container(
//         decoration: BoxDecoration(color: globalElevations.e24dp.backgroundColor, borderRadius: BorderRadius.circular(2)),
//         child: Padding(
//           padding: const EdgeInsets.all(5.0),
//           child: Text(
//             filter,
//             style: TextStyle(fontSize: 12.0, color: globalElevations.fstTextColor.backgroundColor),
//           ),
//         ),
//       ),
//     );
//   }
//
//   List<Filter> getFilterListInFunctionOfTravelMode(String travelMode) {
//     List<Filter> list = _bloc.filters;
//     List<Filter> listResults = new List<Filter>();
//     for (Filter filter in list) {
//       if (filter.travelMode == travelMode) {
//         listResults.add(filter);
//       }
//     }
//     return listResults;
//   }
//
//   Widget _buildTitle() {
//     return Container(
//       height: 92,
//       color: Theme.of(context).buttonTheme.colorScheme.primary,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 16),
//         child: Column(
//           children: <Widget>[
//             Row(
//               children: <Widget>[
//                 Expanded(
//                     child: Text(
//                   translations.text('filter.title'),
//                   style: TextStyle(
//                       fontWeight: FontWeight.bold,
//                       fontSize: 20,
//                       color: (Theme.of(context).brightness == Brightness.light ? Colors.white : Colors.black)),
//                 )),
//                 IconButton(
//                   icon: Icon(Icons.close, color: (Theme.of(context).brightness == Brightness.light ? Colors.white : Colors.black)),
//                   onPressed: () => Navigator.pop(context),
//                 )
//               ],
//             ),
//             Row(
//               children: <Widget>[
//                 Expanded(
//                     child: Text(
//                   translations.text("filter.subtitle"),
//                   style: TextStyle(
//                       fontFamily: 'RobotoMono',
//                       fontSize: 12,
//                       color: (Theme.of(context).brightness == Brightness.light ? Colors.white : Colors.black)),
//                 )),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//         child: Column(
//       children: <Widget>[
//         _buildTitle(),
//         Expanded(
//           child: Container(
//             color: globalElevations.e02dp.backgroundColor,
//             child: ListView(
//               children: <Widget>[_buildPanel()],
//             ),
//           ),
//         ),
//       ],
//     ));
//   }
// }
//
// class TravelMode {
//   TravelMode({
//     @required this.id,
//     @required this.value,
//     @required this.code,
//     this.isExpanded = false,
//   });
//
//   int id;
//   String value;
//   String code;
//   bool isExpanded;
// }
