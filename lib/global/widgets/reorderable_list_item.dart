import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';

class ReorderableListItem extends StatelessWidget {
  final IconData iconData;
  final String text;
  final String code;

  final double iconPadding = 8.0;

  ReorderableListItem({
    @required this.iconData,
    this.text,
    @required this.code,
  });

  Widget _buildLineCode() {
    return Column(
      children: <Widget>[
        Container(
          width: 55,
          child: Center(
              child: Text(
            code ?? "",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: globalElevations.fstTextColor.backgroundColor,
            ),
          )),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var primaryStyle = const TextStyle(
      fontSize: 14,
    );
    return Row(
      children: <Widget>[
        _buildLineCode(),
        // Icon
        if (iconData != null)
          Center(
            child: Padding(
              padding: EdgeInsets.only(
                  left: 8, top: iconPadding, bottom: iconPadding),
              child: Icon(
                iconData,
                size: 32,
              ),
            ),
          ),
        const SizedBox(
          width: 8,
        ),
        // Text
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                text,
                style: primaryStyle,
              ),
            ],
          ),
        ),
        const SizedBox(
          width: 8,
        ),
        //Trailling
      ],
    );
  }
}
