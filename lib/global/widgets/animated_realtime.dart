import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/global/elevation.dart';

class AnimatedRealtime extends StatefulWidget {
  final double height;
  const AnimatedRealtime({this.height = 100});

  @override
  AnimatedRealtimeState createState() => AnimatedRealtimeState();
}

class AnimatedRealtimeState extends State<AnimatedRealtime> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> opacity0;
  Animation<double> opacity1;
  Animation<double> opacity2;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this)
      ..repeat();
    opacity0 = TestAnimation(controller: controller, changeAnimation: 0.5,
      first: Tween<double>(begin: 0, end: 1,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(0, 1/6, curve: Curves.ease,),
        )),
      next: Tween<double>(begin: 1, end: 0,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(3/6, 4/6, curve: Curves.ease,),
        ),
    ));
    opacity1 = TestAnimation(controller: controller, changeAnimation: 0.5,
      first: Tween<double>(begin: 0, end: 1,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(1/6, 2/6, curve: Curves.ease,),
        )),
      next: Tween<double>(begin: 1, end: 0,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(4/6, 5/6, curve: Curves.ease,),
        ),
      ));
    opacity2 = TestAnimation(controller: controller, changeAnimation: 0.5,
      first: Tween<double>(begin: 0, end: 1,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(2/6, 3/6, curve: Curves.ease,),
        )),
      next: Tween<double>(begin: 1, end: 0,).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(5/6, 1, curve: Curves.ease,),
        ),
      ));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          FadeTransition(
            opacity: opacity0,
            child: SvgPicture.asset(
              'assets/realtime_logo/realtime_0.svg',
              height: widget.height,
              semanticsLabel: 'Realtime logo',
              color: globalElevations.sndTextColor.backgroundColor,
            ),
          ),
          FadeTransition(
            opacity: opacity1,
            child: SvgPicture.asset(
              'assets/realtime_logo/realtime_1.svg',
              height: widget.height,
              semanticsLabel: 'Realtime logo',
              color: globalElevations.sndTextColor.backgroundColor,
            ),
          ),
          FadeTransition(
            opacity: opacity2,
            child: SvgPicture.asset(
              'assets/realtime_logo/realtime_2.svg',
              height: widget.height,
              semanticsLabel: 'Realtime logo',
              color: globalElevations.sndTextColor.backgroundColor,
            ),
          ),
        ]
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class TestAnimation extends CompoundAnimation<double> {
  final Animation<double> controller;
  final double changeAnimation;

  TestAnimation({this.controller, this.changeAnimation, Animation<double> first, Animation<double> next}) : super(first: first, next: next);

  @override
  double get value {
    if(controller.value <= changeAnimation) return first.value;
    return next.value;
  }
}
