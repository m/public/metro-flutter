import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AnimatedLoadingLogo extends StatefulWidget {
  final double height;
  const AnimatedLoadingLogo({this.height = 150});

  @override
  AnimatedLoadingLogoState createState() => AnimatedLoadingLogoState();
}

class AnimatedLoadingLogoState extends State<AnimatedLoadingLogo> with SingleTickerProviderStateMixin{
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this)..repeat();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          RotationTransition(
            turns: controller,
            child: SvgPicture.asset(
              'assets/logo_metro/colors.svg',
              height: widget.height,
              semanticsLabel: 'Colors of M logo',
            ),
          ),
          SvgPicture.asset(
            'assets/logo_metro/m.svg',
            height: widget.height,
            semanticsLabel: 'M logo',
          ),
        ]
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


}