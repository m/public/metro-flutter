import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';

class NewExpandablePanel extends StatefulWidget {
  final Widget header;
  final Widget details;
  final bool isExpanded;
  final bool showBorder;
  final bool showArrow;
  final Function(bool) onOpenOrClose;
  final List<dynamic> actions;
  final ExpandableController controller;

  final Stream stream;

  const NewExpandablePanel(
      {Key key,
      this.controller,
      @required this.header,
      this.details,
      this.isExpanded,
      this.showBorder = false,
      this.showArrow = true,
      this.onOpenOrClose,
      this.stream,
      this.actions})
      : super(key: key);

  @override
  _NewExpandablePanelState createState() => _NewExpandablePanelState();
}

class _NewExpandablePanelState extends State<NewExpandablePanel> {
  ExpandableController _controller;
  VoidCallback listener;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (listener != null) _controller.removeListener(listener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _controller = widget.controller ?? ExpandableController.of(context);
    assert(_controller != null);

    if (widget.onOpenOrClose != null) {
      if (listener != null) _controller.removeListener(listener);
      listener = () => widget.onOpenOrClose(_controller.expanded);
      _controller.addListener(listener);
    }

    return AnimatedBuilder(
      animation: _controller,
      child: Container(
        decoration: widget.showBorder
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                border: Border.all(
                  color: globalElevations.expandBorder.backgroundColor,
                  width: 0.6,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0, 2),
                    spreadRadius: -2,
                    blurRadius: 2,
                  ),
                ],
              )
            : BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black,
                    offset: Offset(0, 2),
                    spreadRadius: -2,
                    blurRadius: 2,
                  ),
                ],
              ),
        child: ClipRRect(
          borderRadius: const BorderRadius.all(
            Radius.circular(6),
          ),
          child: Container(
            color: globalElevations.e01dp.backgroundColor,
            child: ExpandablePanel(
              controller: _controller,
              header: widget.header,
              collapsed: SizedBox(width: double.infinity),
              expanded: Column(
                children: <Widget>[
                  if (widget.details != null) widget.details,
                  if (widget.actions != null)
                    Container(
                      color: globalElevations
                          .expandPanelDetailsColor.backgroundColor,
                      child: Row(
                        children: <Widget>[
                          ...widget.actions.map(
                            (action) {
                              if (action is Widget)
                                return action;
                              else
                                return FlatButton(
                                  child: Text(
                                    action.name.toUpperCase(),
                                    style: TextStyle(
                                        color:
                                            Theme.of(context).indicatorColor),
                                  ),
                                  onPressed: action.onTap,
                                );
                            },
                          ).toList(),
                        ],
                      ),
                    )
                ],
              ),
              tapHeaderToExpand: true,
              hasIcon: widget.showArrow,
            ),
          ),
        ),
      ),
      builder: (BuildContext context, Widget child) {
        return AnimatedContainer(
          decoration: _controller.expanded
              ? BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    color: Theme.of(context).indicatorColor.withOpacity(0.6),
                    width: 1.5,
                  ),
                  color: globalElevations.e08dp.backgroundColor,
                )
              : BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    color: globalElevations.e24dp.backgroundColor,
                    width: 0.8,
                  ),
                  color: globalElevations.e08dp.backgroundColor,
                ),
          duration: Duration(milliseconds: 300),
          child: child,
        );
      },
    );
  }
}

class ExpandablePanelAction {
  final String name;
  final VoidCallback onTap;

  const ExpandablePanelAction(this.name, this.onTap);
}
