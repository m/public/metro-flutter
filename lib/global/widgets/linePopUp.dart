import 'package:flutter/material.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/utils.dart';

/// This widget will display any `ReadyLine` into a small badge.
class LineDisplayPopUp extends StatelessWidget {
  final Line line;
  final VoidCallback onTap;
  final double size;

  const LineDisplayPopUp({Key key, this.line, this.onTap, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isRound = line.type == 'TRAM' || line.type == 'CHRONO';
    bool isRect = line.type == 'C38' || line.type == 'GRESIVAUDAN'|| line.type == 'TRANSISERE'|| line.type == 'FLEXO' || line.type == 'VOIRONAIS' || line.type == 'SCOL' || line.type == 'NAVETTE';
    bool isTER = line.type.contains('SNC');

    double height = size == null ? 32 : size;
    double width = height;
    double cornerRadius = 6;
    height += height * (20/100);
    width = height;

    if (isRound) cornerRadius = height / 2;
    if (isRect) width = height * 2;
    if (isTER) width = height * 2;

    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(cornerRadius),
            color: line.color == '' ? Theme.of(context).colorScheme.primary : Utils.colorFromHex(line.color),
            boxShadow: <BoxShadow>[
              BoxShadow(color: Colors.black45, offset: Offset(0, 1), blurRadius: 3)
            ]),
        child: Center(
          child: Text(
            line.shortName, textAlign: TextAlign.center,
            style: TextStyle(
              color: line.color == '' ? Colors.black : Utils.colorFromHex(line.textColor),
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
        ),
      ),
    );
  }
}
