import 'package:flutter/material.dart';

/// Show a rounded button with an icon as [iconData] and an optional [title] on the right.
class RoundedButton extends StatelessWidget {
  final Function callback;
  final IconData iconData;
  final Color color;
  final String title;
  final double height;

  const RoundedButton({Key key, this.title, @required this.callback, @required this.iconData, this.color, this.height = 32}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    Color buttonColor = color ?? theme?.colorScheme?.primary ??  Colors.blue;
    Color higlightButtonColor = theme?.colorScheme?.surface?.withOpacity(0.3) ??  Colors.white;
    Color higlightBorderButtonColor = theme?.colorScheme?.secondary ??  Colors.white;
    if(title == null)
      // Without text
      return SizedBox(
        height: height,
        width: height,
        child: OutlineButton(
          highlightElevation: 8,
          highlightColor: higlightButtonColor,
          highlightedBorderColor: higlightBorderButtonColor,
          borderSide: BorderSide(color: buttonColor),
          shape: CircleBorder(),
          padding: const EdgeInsets.all(0),
          child: Icon(iconData, color: buttonColor,),
          onPressed: () async => await callback(context),
        ),
      );

    // With text
    return SizedBox(
      height: height,
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
        onPressed: () async => await callback(context),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(iconData),
            const SizedBox(width: 8,),
            Text(title),
          ],
        ),
      ),
    );
  }
}