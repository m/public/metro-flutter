import 'package:flutter/material.dart';
import 'dart:ui';
import 'dart:math';

class CustomSwitchDrawer extends StatefulWidget {
  @required
  final bool value;
  @required
  final Function(bool) onChanged;
  final String textLight;
  final String textDark;
  final Color colorOn;
  final Color colorOff;
  final double textSize;
  final Duration animationDuration;
  final IconData iconOn;
  final IconData iconOff;
  final Function onTap;
  final Function onDoubleTap;
  final Function onSwipe;
  final bool change;

  CustomSwitchDrawer(
      {this.value = false,
      this.textDark,
      this.textLight,
      this.textSize = 14.0,
      this.colorOn = Colors.green,
      this.colorOff = Colors.red,
      this.iconOff = Icons.flag,
      this.iconOn = Icons.wb_sunny,
      this.animationDuration = const Duration(milliseconds: 600),
      this.onTap,
      this.onDoubleTap,
      this.onSwipe,
      this.onChanged,
      this.change});

  @override
  _CustomSwitchDrawerState createState() => _CustomSwitchDrawerState();
}

class _CustomSwitchDrawerState extends State<CustomSwitchDrawer> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  double value = 0.0;

  bool turnState;

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(vsync: this, lowerBound: 0.0, upperBound: 1.0, duration: Duration(milliseconds: 0));
    animation = CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    animationController.addListener(() {
      setState(() {
        value = animation.value;
      });
    });
    turnState = widget.value;
    _determine();
  }

  @override
  Widget build(BuildContext context) {
    Color transitionColor = Color.lerp(widget.colorOff, widget.colorOn, value);
    return GestureDetector(
      onDoubleTap: () {
        _action();
        if (widget.onDoubleTap != null) widget.onDoubleTap();
      },
      onTap: () {
        _action();
        if (widget.onTap != null) widget.onTap();
      },
      onPanEnd: (details) {
        _action();
        if (widget.onSwipe != null) widget.onSwipe();
        //widget.onSwipe();
      },
      child: Container(
        padding: EdgeInsets.all(5),
        width: 200,
        height: 40,
        decoration: BoxDecoration(color: transitionColor, borderRadius: BorderRadius.circular(30)),
        child: Stack(
          children: <Widget>[
            // Dark
            Transform.translate(
              offset: Offset(10 * (value - 2), 0), //original
              child: Opacity(
                opacity: (1 - value).clamp(0.0, 1.0),
                child: Container(
                  padding: EdgeInsets.only(right: 25),
                  alignment: Alignment.centerRight,
                  height: 40,
                  child: Text(
                    widget.textLight,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ),
            // Light
            Transform.translate(
              offset: Offset(10 * (3 - value), 0), //original
              child: Opacity(
                opacity: value.clamp(0.0, 1.0),
                child: Container(
                  padding: EdgeInsets.only(/*top: 10,*/ left: 20),
                  alignment: Alignment.centerLeft,
                  height: 40,
                  child: Text(
                    widget.textDark,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(80 * (value * 1.93) - 2, 0),
              child: Container(
                height: 40,
                width: 40,
                alignment: Alignment.bottomRight,
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Opacity(
                        opacity: (1 - value).clamp(0.0, 1.0),
                        child: Icon(
                          widget.iconOff,
                          size: 21,
                          color: transitionColor,
                        ),
                      ),
                    ),
                    Center(
                        child: Opacity(
                            opacity: value.clamp(0.0, 1.0),
                            child: Icon(
                              widget.iconOn,
                              size: 21,
                              color: transitionColor,
                            ))),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _action() {
    _determine(changeState: true);
  }

  _determine({bool changeState = false}) {
    setState(() {
      if (changeState) turnState = !turnState;
      (turnState) ? animationController.forward() : animationController.reverse();

      widget.onChanged(turnState);
    });
  }
}
