import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';

class ExpandablePanel extends StatefulWidget {
  final Widget body;
  final Widget detail;
  final bool isExpanded;
  final bool showBorder;
  final bool showArrow;
  final Elevation elevation;
  final Function(bool) onOpenOrClose;
  final ExpandablePanelController controller;
  final List<ExpandablePanelAction> actions;
  final bool showShadow;

  const ExpandablePanel({
    Key key,
    this.detail = const SizedBox(height: 0),
    this.isExpanded = false,
    this.showBorder = true,
    this.showArrow = true,
    this.body,
    this.elevation,
    this.onOpenOrClose,
    this.controller,
    this.actions,
    this.showShadow = true,
  }) : super(key: key);

  @override
  _ExpandablePanelState createState() => _ExpandablePanelState();
}

// TODO with Material widget (eg Card, Material, Ink,...)
class _ExpandablePanelState extends State<ExpandablePanel> with SingleTickerProviderStateMixin {
  // Global values used by the widget
  static const Color _collapsedBorderColor = Colors.transparent;
  static const int _animationSpeedInMilliseconds = 175;

  bool _isExpanded = false;

  final GlobalKey _bodyKey = GlobalKey();
  final GlobalKey _detailKey = GlobalKey();
  double _bodyHeight;
  double _detailHeight;

  AnimationController _arrowRotationController;

  @override
  void initState() {
    super.initState();
    // Create rotation controller to apply animation to the arrow when the panel expend/collapse
    _arrowRotationController = AnimationController(duration: const Duration(milliseconds: _animationSpeedInMilliseconds), vsync: this);

    // Get defined expanded parameter
    _isExpanded = widget.isExpanded;

    // Used to found out size of body and detail widgets
    _evaluateHeight();

    // Listen to controller callback
    if (widget.controller != null) {
      widget.controller.onExpandedChange = (bool isExpanded) {
        setState(() {
          _isExpanded = isExpanded;
        });
      };
      widget.controller.onEvaluateSizeRequest = _evaluateHeight;
    }
  }

  @override
  Widget build(BuildContext context) {
    // Evaluate the height of the widget
    double totalHeight;
    if (_bodyHeight == null || _detailHeight == null)
      totalHeight = 0;
    else
      totalHeight = _isExpanded ? _bodyHeight + _detailHeight + 2 : _bodyHeight;

    // Animate the arrow
    _isExpanded ? _arrowRotationController.forward() : _arrowRotationController.reverse();

    // Build the widget
    return AnimatedContainer(
      height: totalHeight,
      decoration: BoxDecoration(
        color: widget.elevation.backgroundColor,
        borderRadius: BorderRadius.circular(8),
        border: _getBorder(),
        boxShadow: widget.showShadow ? [
          BoxShadow(
              color: Colors.black.withOpacity(0.4),
              offset: Offset(0, 3),
              spreadRadius: -1,
              blurRadius: 2,
            ),
        ] : []
      ),
      duration: Duration(milliseconds: _animationSpeedInMilliseconds),
      curve: Curves.easeInOut,
      child: ClipRRect(
        borderRadius: const BorderRadius.all(
          Radius.circular(6),
        ),
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Column(
            children: <Widget>[
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  setState(() {
                    _isExpanded = !_isExpanded;

                    if (widget.onOpenOrClose != null) widget.onOpenOrClose(_isExpanded);
                  });
                },
                child: Container(
                  key: _bodyKey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: widget.body,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: _getArrowWidget(),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                key: _detailKey,
                child: Column(
                  children: <Widget>[
                    widget.detail,
                    if (widget.actions != null)
                      Row(
                        children: widget.actions
                            .map(
                              (action) => FlatButton(
                                child: Text(action.name.toUpperCase(), style: TextStyle(color: Theme.of(context).indicatorColor),),
                                onPressed: action.onTap,
                              ),
                            )
                            .toList(),
                      )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    if(widget.controller != null) widget.controller.dispose();
    super.dispose();
  }

  void _evaluateHeight() {
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      setState(() {
        _bodyHeight = (_bodyKey.currentContext.findRenderObject() as RenderBox).size.height;
        _detailHeight = (_detailKey.currentContext.findRenderObject() as RenderBox).size.height;
      });
    });
  }

  BoxBorder _getBorder() {
    if (widget.showBorder) {
      return Border.all(
        color: _isExpanded ? Theme.of(context).indicatorColor : globalElevations.e24dp.backgroundColor,
        width: _isExpanded ? 2 : 0.8,
      );
    } else {
      return Border.all(
        color: _collapsedBorderColor,
        width: 0,
      );
    }
  }

  Widget _getArrowWidget() {
    if (widget.showArrow) {
      return Padding(
        padding: const EdgeInsets.all(16),
        child: RotationTransition(
          turns: Tween(begin: 0.0, end: 0.5).animate(_arrowRotationController),
          child: Icon(Icons.keyboard_arrow_down),
        ),
      );
    } else {
      return null;
    }
  }
}

class ExpandablePanelController {
  Function(bool) onExpandedChange;
  VoidCallback onEvaluateSizeRequest;

  void expand() {
    if (onExpandedChange != null) onExpandedChange(true);
  }

  void collapse() {
    if (onExpandedChange != null) onExpandedChange(false);
  }

  void evaluateSize() {
    if(onEvaluateSizeRequest != null) onEvaluateSizeRequest();
  }

  void dispose() {
    onExpandedChange = null;
    onEvaluateSizeRequest = null;
  }
}

class ExpandablePanelAction {
  final String name;
  final VoidCallback onTap;

  const ExpandablePanelAction(this.name, this.onTap);
}
