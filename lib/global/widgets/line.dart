import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/global/elevation.dart';

/// This widget will display any `ReadyLine` into a small badge.
class LineDisplay extends StatelessWidget {
  final Line line;
  final VoidCallback onTap;
  final double size;
  final bool isFavoriteAction;
  final bool isRouteDetails;
  final bool isBottomSheet;
  final bool isSchedule;

  const LineDisplay(
      {Key key,
      this.line,
      this.onTap,
      this.size,
      this.isFavoriteAction = false,
      this.isRouteDetails = false,
      this.isBottomSheet = false,
      this.isSchedule = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isRound = line.type == 'TRAM' || line.type == 'NAVETTE' || line.type == 'CHRONO';
    bool isRect = line.type == 'C38' ||
        line.type == 'GRESIVAUDAN' ||
        line.type == 'TRANSISERE' ||
        line.type == 'VOIRONAIS' ||
        line.type == 'MCO' ||
        line.type == 'SCOL';
    bool isTER = line.type.contains('SNC');

    double height = size == null ? 32 : size;
    double width = height;
    double cornerRadius = 6;
    height += height * (20 / 100);
    width = height;

    int nsv = BlocProvider.master<NsvBloc>().getNsvId(line.id.replaceAll(':', '_'));

    if (isRound) cornerRadius = height / 2;
    if (isRect) width = height * 2;
    if (isTER) width = height * 2;

    return Container(
      width: width + 10,
      height: height + 12,
      child: StreamBuilder<List<NsvData>>(
        stream: BlocProvider.master<NsvBloc>().onDownloadNsvInfoCompleted,
        builder: (context, snapshot) {
          if (snapshot.data != null && snapshot.data.isNotEmpty) {
            String asset = nsv == 4
                ? globalElevations.perturbation_1
                : (nsv == 2 || nsv == 3)
                    ? globalElevations.perturbation_2
                    : '';
            Color lineColor = line.color == '' ? Theme.of(context).colorScheme.primary : Utils.colorFromHex(line.color);
            Color lineTextColor = line.textColor == '' ? Colors.black : Utils.colorFromHex(line.textColor);
            double iconLeftPosition = isFavoriteAction
                ? isRect
                    ? 70
                    : 30
                : (isRouteDetails || isBottomSheet)
                    ? 32
                    : isRect
                        ? 86
                        : 38;
            return Container(
              width: width,
              height: height,
              child: Stack(
                children: [
                  GestureDetector(
                    onTap: onTap,
                    child: Container(
                      padding: EdgeInsets.only(left: 4, bottom: 4, top: 4),
                      child: Container(
                        height: height,
                        width: width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(cornerRadius),
                          color: nsv != 4 ? lineColor : lineColor.withOpacity(0.4),
                          boxShadow: <BoxShadow>[
                            BoxShadow(color: Colors.black45, offset: Offset(0, 3), blurRadius: 4),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            line.shortName,
                            style: TextStyle(
                              color: nsv != 4 ? lineTextColor : lineTextColor.withOpacity(0.4),
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: (nsv == 2 || nsv == 3 || nsv == 4),
                    child: Positioned(
                      bottom: isSchedule ? 48 :isBottomSheet
                          ? 33
                          : isRouteDetails
                              ? 33
                              : !isFavoriteAction
                                  ? 38
                                  : 35,
                      left: isSchedule ? 48 : iconLeftPosition,
                      child: SizedBox(
                        height: (isRouteDetails || isBottomSheet)
                            ? 16
                            : !isFavoriteAction
                                ? 22
                                : 18,
                        width: (isRouteDetails || isBottomSheet)
                            ? 16
                            : !isFavoriteAction
                                ? 22
                                : 18,
                        child: SvgPicture.asset(
                          asset,
                          height: (isRouteDetails || isBottomSheet)
                              ? 16
                              : !isFavoriteAction
                                  ? 22
                                  : 18,
                          semanticsLabel: 'icon',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else
            return Container();
        },
      ),
    );
  }
}
