import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/localization/translations.dart';
import 'package:store_redirect/store_redirect.dart';

import '../blocs/bloc_provider.dart';
import '../blocs/navigation_bloc.dart';

class UpdateDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UpdateDialogState();
}

class _UpdateDialogState extends State<UpdateDialog> {

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Mise à jour disponible"),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 16),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                style: TextStyle(
                  color: globalElevations.fstTextColor.backgroundColor,
                  fontSize: 16,
                ),
                children: [
                  TextSpan(
                      text: 'Une nouvelle version d Info Voyageur est disponible !', style: TextStyle(fontSize: 18)),
                ]),
          ),
          SizedBox(height: 16),
          Text('Mettre à jour maintenant ? ', style: TextStyle(fontSize: 18),),
          SizedBox(height: 20),

        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(
            'METTRE À JOUR',
            style: TextStyle(color: Theme.of(context).colorScheme.primary),
          ),
          onPressed: () {
            StoreRedirect.redirect(
                androidAppId: "org.lametro.metromobilite",
                iOSAppId: "966169282");
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text(
            translations.text('light_theme_modal.close'),
            style: TextStyle(color: Theme.of(context).colorScheme.primary),
          ),
          onPressed: () {
            BlocProvider.master<NavigationBloc>().switchInfoPopup();
            Navigator.pop(context, true);
          },
        )
      ],
    );
  }
}
