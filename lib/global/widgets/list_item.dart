import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:flutter_svg/flutter_svg.dart';


class ListItem extends StatelessWidget {
  final dynamic iconData;
  final String text1;
  final String text2;
  final String text3;
  final double iconPadding;
  final bool isItinerary;

  const ListItem({Key key, this.iconData, this.text1, this.text2, this.text3, this.iconPadding = 8.0, this.isItinerary = false}) : super(key: key);

  ListItem.normal({Key key, this.iconData, this.text1, this.text2, this.text3}) : this.isItinerary = false, this.iconPadding = 8.0, super(key: key);
  ListItem.itinerary({Key key, this.iconData, this.text1, this.text2, this.text3}) : this.isItinerary = true, this.iconPadding = 8.0, super(key: key);
  ListItem.fullIcon({Key key, String text1, String text2, String text3}) : this(key: key, text1: text1, text2: text2, text3: text3, iconPadding: 0);

  @override
  Widget build(BuildContext context) {
    // TODO : use TextThem for text size and format
    var primaryStyle = const TextStyle(fontSize: 14,);
    var secondaryStyle = TextStyle(fontSize: 12, color: globalElevations.sndTextColor.backgroundColor);
    return Row(
      children: <Widget>[
        // Icon
        if(iconData != null) Center(
          child: Padding(
            padding: EdgeInsets.only(left: 8, top: iconPadding, bottom: iconPadding),
              child: (iconData is IconData)
                  ? Icon(
                      iconData,
                      size: 32,
//                      color: Theme.of(context).unselectedWidgetColor,
                    )
                  : SvgPicture.asset(
                      iconData,
                      height: 32,
                      semanticsLabel: 'Realtime logo',
//                      color: Theme.of(context).unselectedWidgetColor,
                    ),
            ),
        ),
        const SizedBox(width: 8,),
        // Text
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Wrap(
                children: [
                  Text(
                    text1,
                    style: primaryStyle,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                  ),
                ],
              ),
              if (text2 != null)
                Text(
                  text2,
                  style: isItinerary ? primaryStyle : secondaryStyle,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
              if (text3 != null)
                Text(
                  text3,
                  style: isItinerary ? primaryStyle : secondaryStyle,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
            ],
          ),
        ),
        const SizedBox(width: 8,),
        //Trailling
      ],
    );
  }

}