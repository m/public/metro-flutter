import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/mergeable.dart';

const double _kPanelHeaderCollapsedHeight = kMinInteractiveDimension;
const EdgeInsets _kPanelHeaderExpandedDefaultPadding = EdgeInsets.symmetric(
    vertical: 64.0 - _kPanelHeaderCollapsedHeight
);

class _SaltedKey<S, V> extends LocalKey {
  const _SaltedKey(this.salt, this.value);

  final S salt;
  final V value;

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType)
      return false;
    return other is _SaltedKey<S, V>
        && other.salt == salt
        && other.value == value;
  }

  @override
  int get hashCode => hashValues(runtimeType, salt, value);

  @override
  String toString() {
    final String saltString = S == String ? "<'$salt'>" : '<$salt>';
    final String valueString = V == String ? "<'$value'>" : '<$value>';
    return '[$saltString $valueString]';
  }
}

typedef ExpansionPanelCallback = void Function(int panelIndex, bool isExpanded);

typedef ExpansionPanelHeaderBuilder = Widget Function(BuildContext context, bool isExpanded);

class ExpansionPanelTest {

  ExpansionPanelTest({
    @required this.headerBuilder,
    @required this.body,
    this.isExpanded = false,
    this.canTapOnHeader = false,
  }) : assert(headerBuilder != null),
        assert(body != null),
        assert(isExpanded != null),
        assert(canTapOnHeader != null);

  final ExpansionPanelHeaderBuilder headerBuilder;

  final Widget body;

  final bool isExpanded;

  final bool canTapOnHeader;

}

class ExpansionPanelRadio extends ExpansionPanelTest {

  ExpansionPanelRadio({
    @required this.value,
    @required ExpansionPanelHeaderBuilder headerBuilder,
    @required Widget body,
    bool canTapOnHeader = false,
  }) : assert(value != null),
        super(
        body: body,
        headerBuilder: headerBuilder,
        canTapOnHeader: canTapOnHeader,
      );

  final Object value;
}

class ExpansionPanelListTest extends StatefulWidget {

  const ExpansionPanelListTest({
    Key key,
    this.children = const <ExpansionPanelTest>[],
    this.expansionCallback,
    this.animationDuration = kThemeAnimationDuration,
    this.expandedHeaderPadding = _kPanelHeaderExpandedDefaultPadding,
  }) : assert(children != null),
        assert(animationDuration != null),
        _allowOnlyOnePanelOpen = false,
        initialOpenPanelValue = null,
        super(key: key);

  const ExpansionPanelListTest.radio({
    Key key,
    this.children = const <ExpansionPanelRadio>[],
    this.expansionCallback,
    this.animationDuration = kThemeAnimationDuration,
    this.initialOpenPanelValue,
    this.expandedHeaderPadding = _kPanelHeaderExpandedDefaultPadding,
  }) : assert(children != null),
        assert(animationDuration != null),
        _allowOnlyOnePanelOpen = true,
        super(key: key);


  final List<ExpansionPanelTest> children;


  final ExpansionPanelCallback expansionCallback;

  final Duration animationDuration;

  final bool _allowOnlyOnePanelOpen;

  final Object initialOpenPanelValue;

  final EdgeInsets expandedHeaderPadding;

  @override
  State<StatefulWidget> createState() => _ExpansionPanelListTestState();
}

class _ExpansionPanelListTestState extends State<ExpansionPanelListTest> {
  ExpansionPanelRadio _currentOpenPanel;

  @override
  void initState() {
    super.initState();
    if (widget._allowOnlyOnePanelOpen) {
      assert(_allIdentifiersUnique(), 'All ExpansionPanelRadio identifier values must be unique.');
      if (widget.initialOpenPanelValue != null) {
        _currentOpenPanel =
            searchPanelByValue(widget.children.cast<ExpansionPanelRadio>(), widget.initialOpenPanelValue);
      }
    }
  }

  @override
  void didUpdateWidget(ExpansionPanelListTest oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget._allowOnlyOnePanelOpen) {
      assert(_allIdentifiersUnique(), 'All ExpansionPanelRadio identifier values must be unique.');

      if (!oldWidget._allowOnlyOnePanelOpen) {
        _currentOpenPanel =
            searchPanelByValue(widget.children.cast<ExpansionPanelRadio>(), widget.initialOpenPanelValue);
      }
    } else {
      _currentOpenPanel = null;
    }
  }

  bool _allIdentifiersUnique() {
    final Map<Object, bool> identifierMap = <Object, bool>{};
    for (final ExpansionPanelRadio child in widget.children.cast<ExpansionPanelRadio>()) {
      identifierMap[child.value] = true;
    }
    return identifierMap.length == widget.children.length;
  }

  bool _isChildExpanded(int index) {
    if (widget._allowOnlyOnePanelOpen) {
      final ExpansionPanelRadio radioWidget = widget.children[index] as ExpansionPanelRadio;
      return _currentOpenPanel?.value == radioWidget.value;
    }
    return widget.children[index].isExpanded;
  }

  void _handlePressed(bool isExpanded, int index) {
    if (widget.expansionCallback != null)
      widget.expansionCallback(index, isExpanded);

    if (widget._allowOnlyOnePanelOpen) {
      final ExpansionPanelRadio pressedChild = widget.children[index] as ExpansionPanelRadio;

      for (int childIndex = 0; childIndex < widget.children.length; childIndex += 1) {
        final ExpansionPanelRadio child = widget.children[childIndex] as ExpansionPanelRadio;
        if (widget.expansionCallback != null &&
            childIndex != index &&
            child.value == _currentOpenPanel?.value)
          widget.expansionCallback(childIndex, false);
      }

      setState(() {
        _currentOpenPanel = isExpanded ? null : pressedChild;
      });
    }
  }

  ExpansionPanelRadio searchPanelByValue(List<ExpansionPanelRadio> panels, Object value)  {
    for (final ExpansionPanelRadio panel in panels) {
      if (panel.value == value)
        return panel;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final List<MergeableMaterialItemCustom> items = <MergeableMaterialItemCustom>[];

    for (int index = 0; index < widget.children.length; index += 1) {
      if (_isChildExpanded(index) && index != 0 && !_isChildExpanded(index - 1))
        items.add(MaterialGapCustom(size: 0,key: _SaltedKey<BuildContext, int>(context, index * 2 - 1)));

      final ExpansionPanelTest child = widget.children[index];
      final Widget headerWidget = child.headerBuilder(
        context,
        _isChildExpanded(index),
      );

      Widget expandIconContainer = Container(
        child: ExpandIcon(
          isExpanded: _isChildExpanded(index),
          padding: const EdgeInsets.all(16.0),
          disabledColor: globalElevations.fstTextColor.backgroundColor,
          onPressed: !child.canTapOnHeader
              ? (bool isExpanded) => _handlePressed(isExpanded, index)
              : null,
        ),
      );
      if (!child.canTapOnHeader) {
        final MaterialLocalizations localizations = MaterialLocalizations.of(context);
        expandIconContainer = Semantics(
          label: _isChildExpanded(index)? localizations.expandedIconTapHint : localizations.collapsedIconTapHint,
          container: true,
          child: expandIconContainer,
        );
      }
      Widget header = Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: _isChildExpanded(index)
                  ? BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8),
                    )
                  : BorderRadius.all(Radius.circular(8)),
              color: _isChildExpanded(index) ? globalElevations.e24dp.backgroundColor : Theme.of(context).bottomSheetTheme.backgroundColor,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: AnimatedContainer(
                    duration: widget.animationDuration,
                    curve: Curves.fastOutSlowIn,
                    margin: _isChildExpanded(index) ? widget.expandedHeaderPadding : EdgeInsets.zero,
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(minHeight: _kPanelHeaderCollapsedHeight),
                      child: headerWidget,
                    ),
                  ),
                ),
                expandIconContainer,
              ],
            ),
          ),
        ],
      );
      if (child.canTapOnHeader) {
        header = Container(
          color: Colors.transparent,
          child: MergeSemantics(
            child: InkWell(
              onTap: () => _handlePressed(_isChildExpanded(index), index),
              child: header,
            ),
          ),
        );
      }

      items.add(
        MaterialSliceCustom(
          key: _SaltedKey<BuildContext, int>(context, index * 2),
          child: Container(
            color: Theme.of(context).bottomSheetTheme.backgroundColor,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).bottomSheetTheme.backgroundColor,
                      border: Border.all(
                        width: 1,
                        color: globalElevations.e24dp.backgroundColor,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))
                  ),
                  child: Column(
                    children: <Widget>[
                      header,
                      AnimatedCrossFade(
                        firstChild: Container(height: 0.0),
                        secondChild: child.body,
                        firstCurve: const Interval(0.0, 0.6, curve: Curves.fastOutSlowIn),
                        secondCurve: const Interval(0.4, 1.0, curve: Curves.fastOutSlowIn),
                        sizeCurve: Curves.fastOutSlowIn,
                        crossFadeState: _isChildExpanded(index) ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                        duration: widget.animationDuration,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8.0,)
              ],
            ),
          ),
        ),
      );

      if (_isChildExpanded(index) && index != widget.children.length - 1)
        items.add(MaterialGapCustom(size: 0.0,key: _SaltedKey<BuildContext, int>(context, index * 2 + 1)));
    }

    return MergeableMaterialCustom(
      elevation: null,
      hasDividers: false,
      children: items,
    );
  }
}
