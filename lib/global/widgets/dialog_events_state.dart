import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/disturbances/pages/disturbances.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:preferences/preference_service.dart';
import 'package:metro/localization/translations.dart';

class MyEventsDialog extends StatefulWidget {
  final List<EventModel> myEvent;

  const MyEventsDialog({Key key, this.myEvent}) : super(key: key);

  @override
  _MyEventsDialogState createState() => _MyEventsDialogState();
}

class _MyEventsDialogState extends State<MyEventsDialog> {
  bool showEventsInfoModal = true;

  @override
  Widget build(BuildContext context) {
    return MyAlertDialog(
      title: Row(
        children: <Widget>[
          Text("Important ", style: TextStyle(fontSize: 20, color: Color(0xFFf96c6c)),),
          Text(" !", style: TextStyle(fontSize: 26, color: Color(0xFFf96c6c), fontWeight: FontWeight.bold),),
        ],
      ),
      content:
      ListView.builder(
        itemCount: widget.myEvent.length,
        itemBuilder: (BuildContext context, int index) {
          EventModel event = widget.myEvent[index];
          return Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RaisedButton(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        (event.splitTextHeader.length < 2
                                ? event.splitTextHeader[0]
                                : event.splitTextHeader[1].substring(1)[0].toUpperCase() + event.splitTextHeader[1].substring(2)) ??
                            "",
                        style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 14), textAlign: TextAlign.left
                      ),
                      SizedBox(height: 6,),
                      Text(
                        "A partir du " +
                            (event.dateDebut ?? ''),
                        style: TextStyle(color: Colors.grey, fontSize: 12),
                      )
                    ],
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              color:
              globalElevations.e24dp.backgroundColor,
              elevation: 0,
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => DisturbancesPage(0, importantEvent: true, eventKey: event.key,)));
              },
            ),
          );
        },
        shrinkWrap: true,
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 2.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Checkbox(
                checkColor: Colors.black,
                visualDensity: VisualDensity.comfortable,
                activeColor: Theme.of(context).indicatorColor,
                value: !showEventsInfoModal,
                onChanged: (bool value) => setState(() {
                  showEventsInfoModal = !showEventsInfoModal;
                }),
              ),
              Text(translations.text("favorites.modal_not_display"),
                style: TextStyle(color: globalElevations.sndTextColor.backgroundColor),)
            ],
          ),
        ),
        Container(
          alignment: Alignment.centerRight,
          child: FlatButton(
            child: Text(translations.text("favorites.modal_close"),style: TextStyle(color: Theme.of(context).indicatorColor)),
            onPressed: () {
              if (showEventsInfoModal == false) {
                for (EventModel e in widget.myEvent)
                  PrefService.setBool(Consts.preferencesEventsInfoModal + "_" + e.key, showEventsInfoModal);
              }
              BlocProvider.master<NavigationBloc>().switchEventPopup();
              Navigator.pop(context);
            },
          ),
        ),
      ],
    );
  }

}

class MyAlertDialog extends StatelessWidget {
  /// Creates an alert dialog.
  ///
  /// Typically used in conjunction with [showDialog].
  ///
  /// The [contentPadding] must not be null. The [titlePadding] defaults to
  /// null, which implies a default that depends on the values of the other
  /// properties. See the documentation of [titlePadding] for details.
  const MyAlertDialog({
    Key key,
    this.title,
    this.titlePadding,
    this.content,
    this.contentPadding = const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 24.0),
    this.actions,
    this.semanticLabel,
  })
      : assert(contentPadding != null),
        super(key: key);

  final Widget title;

  final EdgeInsetsGeometry titlePadding;

  final Widget content;

  final EdgeInsetsGeometry contentPadding;

  final List<Widget> actions;

  final String semanticLabel;

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[];
    String label = semanticLabel;

    if (title != null) {
      children.add(new Padding(
        padding: titlePadding ?? new EdgeInsets.fromLTRB(24.0, 24.0, 24.0, content == null ? 20.0 : 0.0),
        child: new DefaultTextStyle(
          style: Theme
              .of(context)
              .textTheme
              .title,
          child: new Semantics(child: title, namesRoute: true),
        ),
      ));
    } else {
      switch (defaultTargetPlatform) {
        case TargetPlatform.iOS:
          label = semanticLabel;
          break;
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          label = semanticLabel ?? MaterialLocalizations
              .of(context)
              ?.alertDialogLabel;
      }
    }

    if (content != null) {
      children.add(new Flexible(
        child: new Padding(
          padding: contentPadding,
          child: new DefaultTextStyle(
            style: Theme
                .of(context)
                .textTheme
                .subhead,
            child: content,
          ),
        ),
      ));
    }
//
//    if (actions != null) {
//      children.add(new ButtonTheme.bar(
//        child: new ButtonBar(
//          children: actions,
//        ),
//      ));
//    }

    if (actions != null)
      children.add(Padding(
        padding: const EdgeInsets.only(bottom: 6.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: actions,
        ),
      ));

    Widget dialogChild = new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );

    if (label != null)
      dialogChild = new Semantics(
          namesRoute: true,
          label: label,
          child: dialogChild
      );

    return new Dialog(child: dialogChild);
  }
}