import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  const CustomSwitch({Key key, this.value = false, this.onChanged}) : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> {
  bool _isSwitched;

  @override
  void initState() {
    super.initState();
    _isSwitched = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _changeSwitchValue(!_isSwitched);
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Switch(
          activeColor: Theme.of(context).indicatorColor,
          value: _isSwitched,
          onChanged: (value) {
            _changeSwitchValue(value);
          },
        ),
      ),
    );
  }

  void _changeSwitchValue(bool value) {
    setState(() {
      _isSwitched = value;
    });

    if(widget.onChanged != null)
      widget.onChanged(value);
  }
}
