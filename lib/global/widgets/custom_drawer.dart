import 'package:flutter/material.dart';
import 'package:metro/atmo/blocs/atmo_bloc.dart';
import 'package:metro/atmo/models/atmo_info_new.dart';
import 'package:metro/atmo/pages/atmo.dart';
import 'package:metro/atmo/widgets/atmo_widget.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/disturbances/pages/disturbances.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/blocs/traficolor_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/pages/info.dart';
import 'package:metro/global/widgets/switch_drawer.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/preferences/bloc/preferences.dart';
import 'package:metro/preferences/pages/preferences.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/global/blocs/bike_bloc.dart';


class CustomDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State {
  var _darkTheme = true;

  Widget _buildDrawer() {
    _darkTheme = Theme.of(context).brightness == Brightness.dark;
    return Drawer(
      // column holds all the widgets in the drawer
      child: Column(
        children: <Widget>[
          Expanded(
            // ListView contains a group of widgets that scroll inside the drawer
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                // appBar
                Row(
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.arrow_back), onPressed: () => Navigator.pop(context)),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[Text(translations.text("drawer.menu_title")), Image.asset("assets/launcher/logo_m.png", height: 24)],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                // Atmo
                GestureDetector(
                  onTap: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AtmoPage()),
                    );
                  },
                  child: StreamBuilder<IndiceAtmoInfo>(
                    stream: BlocProvider.of<IndiceAtmoBloc>(context).onDownloadAtmoInfoCompleted,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data != null) {
                          return Hero(
                            tag: 'atmo',
                            child: AtmoWidget(
                              atmoInfo: snapshot.data,
                              header: true,
                            ),
                          );
                        }
                      }
                      return Text(translations.text('atmo.no_data'));
                    },
                  ),
                ),
                const SizedBox(height: 16),
                // Menu items
                StreamBuilder<NavigationCategory>(
                  stream: BlocProvider.of<NavigationBloc>(context).onNavigationCategoryChanged,
                  builder: (context, snapshot) {
                    return Container(
                      color: snapshot.data == NavigationCategory.map ? Theme.of(context).colorScheme.primary.withOpacity(0.25) : Colors.transparent,
                      child: ListTile(
                        leading: Icon(
                          Icons.location_on,
                          color: snapshot.data == NavigationCategory.map
                              ? Theme.of(context).colorScheme.primary
                              : globalElevations.fstTextColor.backgroundColor,
                        ),
                        title: Text(
                          translations.text('drawer.accueil'),
                          style: TextStyle(
                            color: snapshot.data == NavigationCategory.map
                                ? Theme.of(context).colorScheme.primary
                                : globalElevations.fstTextColor.backgroundColor,
                          ),
                        ),
                        onTap: () {
                          BlocProvider.of<NavigationBloc>(context).changeTab(NavigationCategory.map);
                          // BlocProvider.of<NavigationBloc>(context).changeTabByIndex(0);
                          Navigator.pop(context);
                        },
                      ),
                    );
                  },
                ),
                // Menu items
                StreamBuilder<NavigationCategory>(
                  stream: BlocProvider.of<NavigationBloc>(context).onNavigationCategoryChanged,
                  builder: (context, snapshot) {
                    return Container(
                      child: ListTile(
                        leading: Icon(
                          Icons.settings,
                        ),
                        title: Text(
                          translations.text('drawer.preferences'),
                        ),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => PreferencesPage()));
                          // BlocProvider.of<NavigationBloc>(context).changeTabByIndex(0);
                        },
                      ),
                    );
                  },
                ),
                StreamBuilder<int>(
                    stream: BlocProvider.master<EventsBloc>().onReadExceptionnalEventsChanged,
                    builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                      bool showBadge = false;
                      if (snapshot != null) {
                        switch (snapshot.data) {
                          case 0:
                            showBadge = false;
                            break;
                          case 1:
                            showBadge = false;
                            break;
                          case 2:
                            showBadge = true;
                            break;
                          default:
                            showBadge = false;
                        }
                      } else {
                        showBadge = false;
                      }

                      return ListTile(
                        leading: Stack(children: [
                          Icon(Icons.warning),
                          Visibility(
                            visible: showBadge,
                            child: new Positioned(
                              left: 17,
                              bottom: 8,
                              child: new Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "!",
                                    style: TextStyle(fontSize: 14,
                                        fontWeight: FontWeight.w900,
                                        color: Color(0xFFf96c6c)),
                                    textAlign: TextAlign.center,
                                  )),
                            ),
                          ),
                        ]),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => DisturbancesPage(0)));
                        },
                        title: Text(translations.text('drawer.perturbations')),
                      );
                    }),

                StreamBuilder<NavigationCategory>(
                  stream: BlocProvider.of<NavigationBloc>(context).onNavigationCategoryChanged,
                  builder: (context, snapshot) {
                    return Container(
                      child: ListTile(
                        leading: Icon(MetroIcons.voiture),
                        title: Text(
                          translations.text('drawer.state'),
                        ),
                        onTap: () {
                          BlocProvider.master<BikeBloc>().downloadBike(isBottomFilter: true);
                          // BlocProvider.of<MapBloc>(context).updateCamMarkersLayout(Elevations.darkModeEnabled(false));
                          BlocProvider.of<TraficolorBloc>(context).downloadTraficolor();
                          BlocProvider.of<MapBloc>(context).saveCurrentPositionAndZoom();
                          BlocProvider.of<MapBloc>(context).moveTo(MapPositionAndZoom.withLatLng(
                              LatLng((Consts.grenobleMaxLat - Consts.grenobleMinLat) / 2 + Consts.grenobleMinLat,
                                  (Consts.grenobleMaxLng - Consts.grenobleMinLng) / 1.89 + Consts.grenobleMinLng),
                              13));
                          BlocProvider.of<NavigationBloc>(context).changeTab(NavigationCategory.traficolor);
                          Navigator.pop(context);
                        },
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.info),
                  title: Text(translations.text('drawer.info')),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => InfoPage()));
                  },
                ),
              ],
            ),
          ),
          // This container holds the align
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                // This align moves the children to the bottom
                child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    // This container holds all the children that will be aligned
                    // on the bottom and should not scroll with the above ListView
                    child: Container(

                        // This align moves the children to the bottom
                        child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            // This container holds all the children that will be aligned
                            // on the bottom and should not scroll with the above ListView
                            child: Center(
                              child: Container(
                                  child: Column(
                                children: <Widget>[
                                  Divider(color: getDividerColor()),
                                  StreamBuilder<bool>(
                                      stream: BlocProvider.of<PreferencesBloc>(context).onMyThemeChanged,
                                      initialData: BlocProvider.of<PreferencesBloc>(context).themeChanged,
                                      builder: (context, snapshot) {
                                        if (snapshot.data != null) {
                                          return Padding(
                                              padding: const EdgeInsets.all(14.0),
                                              child: new CustomSwitchDrawer(
                                                  //initial value
                                                  value: snapshot.data,
                                                  textDark: translations.text("preferences.dark_theme"),
                                                  textLight: translations.text("preferences.light_theme"),
                                                  colorOn: Colors.cyan[200],
                                                  colorOff: Colors.cyan[200],
                                                  iconOn: Icons.brightness_3,
                                                  iconOff: Icons.wb_sunny,
                                                  textSize: 16.0,
                                                  onChanged: (bool state) {
                                                    WidgetsBinding.instance.addPostFrameCallback(
                                                        (_) => BlocProvider.of<PreferencesBloc>(context).onThemeChanged(state, context));
                                                  }));
                                        } else {
                                          return EmptyCell();
                                        }
                                      })
                                ],
                              )),
                            ))))),
          )
        ],
      ),
    );
  }

  Color getDividerColor() {
    if (Theme.of(context).brightness == Brightness.dark) {
      return Colors.white;
    } else {
      return Colors.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    _darkTheme = Theme.of(context).brightness == Brightness.dark;
    return _buildDrawer();
  }
}
