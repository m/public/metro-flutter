import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/localization/translations.dart';
import 'package:preferences/preferences.dart';

class LightThemeDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LightThemeDialogState();
}

class _LightThemeDialogState extends State<LightThemeDialog> {
  bool _showThemeModal = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(translations.text("light_theme_modal.title")),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image.asset('assets/light_theme_alert.png', height: 125),
          SizedBox(height: 16),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                style: TextStyle(
                  color: globalElevations.fstTextColor.backgroundColor,
                  fontSize: 16,
                ),
                children: [
                  TextSpan(
                      text: translations.text('light_theme_modal.content')),
                  TextSpan(
                      text: translations.text('light_theme_modal.italic'),
                      style: TextStyle(fontStyle: FontStyle.italic))
                ]),
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Checkbox(
                value: !_showThemeModal,
                onChanged: (bool value) => setState(() {
                  _showThemeModal = !_showThemeModal;
                }),
              ),
              Text(translations.text('light_theme_modal.do_not_display')),
            ],
          ),
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(
            translations.text('light_theme_modal.close'),
            style: TextStyle(color: Theme.of(context).colorScheme.primary),
          ),
          onPressed: () {
            if (_showThemeModal == false)
              PrefService.setBool(
                  Consts.preferencesLightThemeModal, _showThemeModal);
            BlocProvider.of<NavigationBloc>(context).switchLightThemePopup();
            Navigator.pop(context);
          },
        )
      ],
    );
  }
}
