import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/main.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:rxdart/rxdart.dart';

part 'database.g.dart';

/// To stock properties for the database.
class DatabaseInfos extends Table {
  static String get initialiseId => "initialise";

  TextColumn get id => text()();
  TextColumn get value => text()();

  @override
  Set<Column> get primaryKey => {id};
}

// ------------------------------------------------------------------------------
// --------------------------------Favorites-------------------------------------
// ------------------------------------------------------------------------------
/// Table to save objects representing a favorite item.
class FavoriteDetails extends Table {
  IntColumn get id => integer().autoIncrement().named('favorite')();
  IntColumn get type => integer().map(const FavoriteTypeConverter())();
  IntColumn get position => integer()(); // UNIQUE
  IntColumn get point1 => integer()();
  IntColumn get point2 => integer().nullable()();
}

// TODO : indexes !!
/// Table to save objects representing a list of LineId for a favorite cluster.
@DataClassName('LineEntry')
class LineEntries extends Table {
  IntColumn get favorite => integer()();
  TextColumn get line => text()();
}

/// Table to save objects representing transports modes for a favorite itinerary.
@DataClassName('ModeOfTransport')
class ModesOfTransport extends Table {
  IntColumn get favorite => integer()();
  BoolColumn get bike => boolean()();
  BoolColumn get car => boolean()();
  BoolColumn get carPooling => boolean()();
  BoolColumn get carSharing => boolean()();
  BoolColumn get publicTransports => boolean()();
  BoolColumn get disabledPerson => boolean()();

  @override
  List<String> get customConstraints => [
    "UNIQUE (favorite, bike, car, public_transports, disabled_person)"
  ];
}

// ------------------------------------------------------------------------------
// --------------------------------Properties-------------------------------------
// ------------------------------------------------------------------------------
/// Table to save objects representing the base properties for the properties object of a point.
@DataClassName('DBProperty')
class DBProperties extends Table {
  @override
  String get tableName => 'properties';
  IntColumn get id => integer().named('property').autoIncrement()();
  TextColumn get metroId => text()();
  TextColumn get name => text()();
  TextColumn get city => text().nullable()();
  IntColumn get type => integer().map(const PropertyTypeConverter()).named('property_type')();

  @override
  List<String> get customConstraints => [
    "UNIQUE (metro_id, property_type)"
  ];
}

@DataClassName('DBPlaceProperty')
class DBPlaceProperties extends Table {
  @override
  String get tableName => 'place_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  IntColumn get dist => integer()();
}

@DataClassName('DBClusterProperty')
class DBClusterProperties extends Table {
  @override
  String get tableName => 'cluster_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  IntColumn get dist => integer()();
  BoolColumn get visible => boolean()();
  TextColumn get code => text()();
}

@DataClassName('DBAgencyProperty')
class DBAgencyProperties extends Table {
  @override
  String get tableName => 'agency_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text()();
  TextColumn get address => text()();
}

@DataClassName('DBParkingProperty')
class DBParkingProperties extends Table {
  @override
  String get tableName => 'parking_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text()();
  TextColumn get address => text()();
  IntColumn get total => integer()();
}

@DataClassName('DBCodeProperty')
class DBCodeProperties extends Table {
  @override
  String get tableName => 'code_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text().nullable()();
}

@DataClassName('DBIrveProperty')
class DBIrveProperties extends Table {
  @override
  String get tableName => 'irve_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text().nullable()();
  TextColumn get amenageur => text().nullable()();
  TextColumn get pdcTypes => text().nullable()();
  TextColumn get pdcNb => text().nullable()();
}

@DataClassName('DBStreetProperty')
class DBStreetProperties extends Table {
  @override
  String get tableName => 'street_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  IntColumn get dist => integer()();
}

@DataClassName('DBCitizProperty')
class DBCitizProperties extends Table {
  @override
  String get tableName => 'citiz_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text()();
}

@DataClassName('DBCitizyeaProperty')
class DBCitizyeaProperties extends Table {
  @override
  String get tableName => 'citizyea_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get licencePlate => text()();
  IntColumn get fuelLevel => integer()();
}

@DataClassName('DBCamProperty')
class DBCamProperties extends Table {
  @override
  String get tableName => 'cam_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text()();
  IntColumn get cap => integer()();
}

@DataClassName('DBStopProperty')
class DBStopProperties extends Table {
  @override
  String get tableName => 'stop_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get gtfsId => text()();
  TextColumn get gtfsClusterId => text()();
}

@DataClassName('DBCarpoolingProperty')
class DBCarpoolingProperties extends Table {
  @override
  String get tableName => 'carpooling_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get placeId => text()();
  BoolColumn get isOpen => boolean()();
  IntColumn get placeLeft => integer()();
  IntColumn get nearbyLines => integer().nullable()();
  TextColumn get typeOfParking => text()();
}

@DataClassName('DBAutostopProperty')
class DBAutostopProperties extends Table {
  @override
  String get tableName => 'autostop_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get code => text()();
  TextColumn get address => text()();
  TextColumn get direction1 => text()();
  TextColumn get direction2 => text().nullable()();
  TextColumn get direction3 => text().nullable()();
}

@DataClassName('DBEnergyProperty')
class DBEnergyProperties extends Table {
  @override
  String get tableName => 'energy_properties';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  TextColumn get address => text()();
}

@DataClassName('DBGeometry')
class DBGeometries extends Table {
  @override
  String get tableName => 'geometries';
  @override
  Set<Column> get primaryKey => {property};

  IntColumn get property => integer()();
  RealColumn get latitude => real()();
  RealColumn get longitude => real()();
}

@DataClassName('LastSearch')
class LastSearches extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get property => integer()();
  IntColumn get position => integer()();
}

@UseMoor(include: {'tables.moor'},
    tables: [
      DatabaseInfos,
      FavoriteDetails,
      LineEntries,
      ModesOfTransport,
      DBProperties,
      DBClusterProperties,
      DBAgencyProperties,
      DBParkingProperties,
      DBCodeProperties,
      DBIrveProperties,
      DBPlaceProperties,
      DBStreetProperties,
      DBCitizProperties,
      DBCitizyeaProperties,
      DBCamProperties,
      DBStopProperties,
      DBCarpoolingProperties,
      DBAutostopProperties,
      DBEnergyProperties,
      DBGeometries,
      LastSearches
    ],
    daos: [DatabaseInfosDao, FavoriteDao, PointDao, LastSearchDao])
class MyDatabase extends _$MyDatabase {
  MyDatabase() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite', logStatements: false)) {
    init();
    migration;
  }

  void init() async {
    if (!await databaseInfosDao.isInitialised) {
      databaseInfosDao.setIsInitialised();
    }
  }

  @override
  int get schemaVersion => 2;

  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (Migrator m) {
        return m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        if (from == 1) {
          // we added the dueDate property in the change from version 1
          await m.createTable(dBIrveProperties);
        }
      }
  );

}

// ------------------------------------------------------------------------------
// --------------------------------DAOs-------------------------------------
// ------------------------------------------------------------------------------

@UseDao(tables: [DBProperties, DBGeometries, LastSearches])
class LastSearchDao extends DatabaseAccessor<MyDatabase> with _$LastSearchDaoMixin {
  LastSearchDao(MyDatabase db) : super(db);

  Future<int> addResearch(Point argument) async {
    assert(argument != null, 'Argument cannot be null');

    int propertyId;
    DBProperty property = await database.pointDao.getPropertyFromPoint(argument);
    if(property == null) {
      // Point is not already in the database : add it
      propertyId = await database.pointDao.addProperty(argument);
    } else
      propertyId = property.id;

    int count = await _getCount();

    if(count >= Consts.maxLastSearches)
      // We already have max last searches : delete the first
      await (delete(lastSearches)..where((ls) => ls.position.equals(Consts.maxLastSearches))).go();

    await database.updateLastSearchPosition();
    return _insert(propertyId);
  }

  Future<int> _insert(int propertyId) => into(lastSearches).insert(LastSearchesCompanion.insert(
    property: propertyId,
    position: 1,
  ));

  Future<List<LastSearchPoint>> get getLastResearches async{
    List<LastSearch> listLastSearches = await (select(lastSearches)..orderBy([(ls) => OrderingTerm(expression: ls.position)])).get();
    Set<int> pointIds = Set();
    for(var ls in listLastSearches)
      pointIds.add(ls.property);

    Map<int, Point> mapPoints = await database.pointDao.getPointsStreamWithIds(pointIds.toList()).first;

    return listLastSearches.map((ls) => LastSearchPoint(lastSearch: ls, point: mapPoints[ls.property])).toList();
  }

  Future<int> _getCount() => database.getCountLastSearches().getSingle();
}

@UseDao(tables: [FavoriteDetails, LineEntries, ModesOfTransport])
class FavoriteDao extends DatabaseAccessor<MyDatabase> with _$FavoriteDaoMixin {
  FavoriteDao(MyDatabase db) : super(db);

  /// Obtain a stream that observes all the favorites et and gets updated when an entry is updated or modified.
  Stream<List<Favorite>> get allFavoritesStream {
    final favoritesStream = select(favoriteDetails).watch();

    return favoritesStream.switchMap((favoritesDetails) {
      final Map<int, FavoriteDetail> idToFav = {};
      final Map<int, List<String>> idToLines = {};
      final Map<int, List<String>> idToModes = {};
      final List<int> pointsIds = [];
      for (var fav in favoritesDetails) {
        idToFav[fav.id] = fav;
        idToLines[fav.id] = [];
        idToModes[fav.id] = null;
        pointsIds.add(fav.point1);
        if (fav.point2 != null) pointsIds.add(fav.point2);
      }
      final List<int> ids = idToFav.keys.toList();

      final Stream<Map<int, Point>> pointsStream = database.pointDao.getPointsStreamWithIds(pointsIds);

      final Stream<Map<int, List<String>>> linesEntriesStream = (select(lineEntries)..where((lines) => isIn(lines.favorite, ids))).watch().map((lineEntries) {
        for(var lineEntry in lineEntries) {
          idToLines.putIfAbsent(lineEntry.favorite, () => []).add(lineEntry.line.split(':')[1]);
        }
        return idToLines;
      });

      final Stream<Map<int, ModeOfTransport>> modesOfTransportStream = (select(modesOfTransport)..where((modes) => isIn(modes.favorite, ids))).watch().map((modes) {
        return {
          for(var mode in modes)
            mode.favorite: mode
        };
      });

      return Rx.combineLatest3(pointsStream, linesEntriesStream, modesOfTransportStream,
        (Map<int, Point> points, Map<int, List<String>> lineEntries, Map<int, ModeOfTransport> modes) {
          return [
            for (var id in ids)
              Favorite(
                details: idToFav[id],
                point1: points[idToFav[id].point1],
                point2: points[idToFav[id].point2],
                lines: idToLines[id],
                modes: modes[id],
              ),
          ];
      });
    });
  }

  Future<List<FavoriteDetail>> get allFavorites => select(favoriteDetails).get();

  Future<List<Point>> searchFavorites (bool limited) async {
    List<int> ids = limited ? await database.getLimitedFavoritesSearchIds(getFavoriteTypeIdFromEnum(FavoriteType.cluster)).get()
        : await database.getFavoritesSearchIds(getFavoriteTypeIdFromEnum(FavoriteType.cluster)).get();

    return database.pointDao.getPointsWithIds(ids);
  }

  /// Returns true if the provided [data] is an object that is favorite else it returns false.
  Future<bool> isFavorite(dynamic data) async {
    assert(data != null);
    return await getFavorite(data) != null;
  }

  /// Returns the id of the associated favorite from [data].
  Future<FavoriteDetail> getFavorite(dynamic data) async {
    assert(data != null);

    if (data is Favorite)
      return data.details;
    else if (data is Point)
      return database.isAFavoriteOtherTypes(getPropertyTypeIdFromEnum(data.properties.type), data.properties.id, getFavoriteTypeIdFromEnum(FavoriteType.itinerary)).getSingle();
    else if (data is FavoriteItinerary) {
      return database.isAFavoriteItinerary(
          data.departure.properties.id,
          getPropertyTypeIdFromEnum(data.departure.properties.type),
          data.destination.properties.id,
          getPropertyTypeIdFromEnum(data.destination.properties.type),
          data.modes.bike,
          data.modes.car,
          data.modes.publicTransports,
          data.modes.disabledPerson).getSingle();
    } else if (data is FavoriteCluster)
      return database.isAFavoriteOtherTypes(getPropertyTypeIdFromEnum(data.stop.properties.type), data.stop.properties.id, getFavoriteTypeIdFromEnum(FavoriteType.itinerary)).getSingle();
    else
      throw Future.error(UnsupportedError('Not handled case : the type of data (${data.runtimeType}) is not a supported type.'));
  }

  /// Add a favorite to the database.
  /// It takes a dynamic object [argument]. Depending on the type of [argument], it will create the appropriate favorite item and add the
  /// property and specific property item associated with it if it is not already in the database.
  /// Does not try to look if an associated oject is already present.
  Future<int> addFavorite(dynamic argument) async {
    assert(argument != null, 'Argument cannot be null');

    if (argument is Point) {
      // special cases
      if (argument.properties.type == PropertiesType.clusters) {
        // Favorite clusters with all lines
        return transaction(() async {
          // Check if the associated property is already here
          // Else add the property and specific property
          DBProperty property = await database.pointDao.getPropertyFromPoint(argument);
          int propertyId = property?.id ?? await database.pointDao.addProperty(argument);

          // Add favorite with last order
          return await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
            type: FavoriteType.cluster,
            position: await getLastIndexOrder(),
            point1: propertyId,
          ));
        });
      } else if (argument.properties.type == PropertiesType.par || argument.properties.type == PropertiesType.pkg) {
        // Favorite parking (parking or 'parking relais')
        return transaction(() async {
          DBProperty property = await database.pointDao.getPropertyFromPoint(argument);
          int propertyId = property?.id ?? await database.pointDao.addProperty(argument);

          return await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
            type: FavoriteType.parking,
            position: await getLastIndexOrder(),
            point1: propertyId,
          ));
        });
      } else if (argument.properties.type == PropertiesType.undefined /*CAMERA*/) {
        // Favorite Camera
        return transaction(() async {
          DBProperty property = await database.pointDao.getPropertyFromPoint(argument);
          int propertyId = property?.id ?? await database.pointDao.addProperty(argument);

          return await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
              type: FavoriteType.camera,
              position: await getLastIndexOrder(),
              point1: propertyId
          ));
        });
      } else {
        // All the other points are treated the same, it's a favorite point which redirect to the map
        return transaction(() async {
          DBProperty property = await database.pointDao.getPropertyFromPoint(argument);
          int propertyId = property?.id ?? await database.pointDao.addProperty(argument);

          // Add favorite with last position
          return await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
            type: FavoriteType.poi,
            position: await getLastIndexOrder(),
            point1: propertyId,
          ));
        });
      }
    } else if (argument is FavoriteItinerary) {
      // Favorite itinerary
      return transaction(() async {
        DBProperty departure = await database.pointDao.getPropertyFromPoint(argument.departure);
        int departureId = departure?.id ?? await database.pointDao.addProperty(argument.departure);
        DBProperty destination = await database.pointDao.getPropertyFromPoint(argument.destination);
        int destinationId = destination?.id ?? await database.pointDao.addProperty(argument.destination);

        // Add favorite with last order
        int id = await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
          type: FavoriteType.itinerary,
          position: await getLastIndexOrder(),
          point1: departureId,
          point2: Value(destinationId),
        ));

        assert(id != null);

        return await into(modesOfTransport).insert(
            ModesOfTransportCompanion.insert(
                favorite: id,
                bike: argument.modes.bike,
                car: argument.modes.car,
                carPooling: argument.modes.carPooling,
                carSharing: argument.modes.carSharing,
                publicTransports: argument.modes.publicTransports,
                disabledPerson: argument.modes.disabledPerson
            ));
      });
    } else if (argument is FavoriteCluster) {
      // Favorite cluster with the lines associated
      return transaction(() async {
        DBProperty property = await database.pointDao.getPropertyFromPoint(argument.stop);
        int propertyId = property?.id ?? await database.pointDao.addProperty(argument.stop);

        // Add favorite with last order
        int id = await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
          type: FavoriteType.cluster,
          position: await getLastIndexOrder(),
          point1: propertyId,
        ));

        assert(id != null);
        if (argument.lines == null || argument.lines.length == 0)
          return id;
        return await into(lineEntries).insertAll([for(var line in argument.lines) LineEntry(favorite: id, line: line.id)]).then((_) => id);
      });
    }
    else if (argument is Favorite){
      // Favorite itinerary
      return transaction(() async {
        DBProperty departure = await database.pointDao.getPropertyFromPoint(argument.point1);
        int departureId = departure?.id ?? await database.pointDao.addProperty(argument.point1);
        DBProperty destination = await database.pointDao.getPropertyFromPoint(argument.point2);
        int destinationId = destination?.id ?? await database.pointDao.addProperty(argument.point2);

        // Add favorite with last order
        int id = await into(favoriteDetails).insert(FavoriteDetailsCompanion.insert(
          type: FavoriteType.itinerary,
          position: await getLastIndexOrder(),
          point1: departureId,
          point2: Value(destinationId),
        ));

        assert(id != null);

        return await into(modesOfTransport).insert(
            ModesOfTransportCompanion.insert(
                favorite: id,
                bike: argument.modes.bike,
                car: argument.modes.car,
                carPooling: argument.modes.carPooling,
                carSharing: argument.modes.carSharing,
                publicTransports: argument.modes.publicTransports,
                disabledPerson: argument.modes.disabledPerson
            ));
      });
  }else
      return Future.error(UnsupportedError('Not handled case'));
  }

  Future updateFavoritePosition(dynamic data, int newIndex) async {
    assert(data != null);
    FavoriteDetail fav = await getFavorite(data);
    if (fav != null) return updateFavoriteFromId(fav, newIndex);
    return Future.error(ArgumentError('There is no favorite associated with the passed data.'));
  }

  /// Delete a favorite from [data].
  Future<void> deleteFavorite(dynamic data) async {
    assert(data != null);

    FavoriteDetail fav = await getFavorite(data);
    if (fav != null) {
      return deleteFavoriteFromId(fav);
    }

    return Future.error(ArgumentError('There is no favorite associated with the passed data.'));
  }

  /// Delete a favorite cluster from [data].
  void deleteFavoriteCluster(Point data) async {
    assert(data != null);

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;

    if (listFavs != null) {
      for (Favorite favs in listFavs){
        if (data.properties.id == favs.point1.properties.id){
          await deleteFavoriteFromId(favs.details);
        }
      }
    }
  }

  Future updateFavoriteFromId(FavoriteDetail fav, int newIndex) async {
    assert(fav != null);
    return transaction(() async {
      await (update(favoriteDetails)..where((f) => f.id.equals(fav.id))).write(FavoriteDetail(position: newIndex, id: null, point1: null, type: null));
    });
  }

  /// Delete a favorite from FavoriteDetail object [fav].
  /// [PointDao.deleteIfUnusedProperty] If the property is not still used by other tables after the deletion of the favorite, it deletes it too.
  Future<void> deleteFavoriteFromId(FavoriteDetail fav, {bool deleteAll = false}) {
    assert(fav != null);

    return transaction(() async {
      // Delete the favoriteDetail associated and the first point if it is not used by other entries
      await (delete(favoriteDetails)..where((f) => f.id.equals(fav.id))).go();
      await database.pointDao.deleteIfUnusedProperty(fav.point1);

      switch (fav.type) {
        case FavoriteType.cluster:
          // Delete the lines entries
          await (delete(lineEntries)..where((l) => l.favorite.equals(fav.id))).go();
          break;

        case FavoriteType.itinerary:
          // Delete the second point if it is not used and the modeOfTransport
          await database.pointDao.deleteIfUnusedProperty(fav.point2);
          await (delete(modesOfTransport)..where((m) => m.favorite.equals(fav.id))).go();
          break;

        case FavoriteType.parking:
        case FavoriteType.camera:
        case FavoriteType.poi:
        case FavoriteType.tier:
          //TODO: delete these type of favorite
          break;
      }

      if(!deleteAll)
        await updatePositionAfterDelete(fav.position);
    });
  }

  /// Delete all favorites.
  Future<void> deleteAllFavorites() {
    return transaction(() async {
      List<FavoriteDetail> favoritesDetails = await allFavorites;
      await Future.sync(() async {
        for(var fav in favoritesDetails)
          await deleteFavoriteFromId(fav, deleteAll: true);
      });
    });
  }

  /// déplacer un favoris (modification de la position)
  //TODO

  /// Update the position of the favorites after a deletion of an item.
  /// [positionDeleted] is the position of the item deleted.
  Future<void> updatePositionAfterDelete(int positionDeleted) {
    return database.updatePositionAfterDelete(positionDeleted);
  }

  /// Get the last position from the favorites returns the the greater number.
  Future<int> getLastIndexOrder() async {
    FavoriteDetail favoriteDetail = await (select(favoriteDetails)..orderBy([(f) => OrderingTerm(expression: f.position, mode: OrderingMode.desc)])..limit(1)).getSingle();
    return favoriteDetail?.position != null ? favoriteDetail.position + 1 : 0 ;
  }
}

// Todo : DAO pour les points
// mettre à jour un point à partir d'un id
@UseDao(tables: [
  DBProperties,
  DBClusterProperties,
  DBAgencyProperties,
  DBParkingProperties,
  DBCodeProperties,
  DBIrveProperties,
  DBPlaceProperties,
  DBStreetProperties,
  DBCitizProperties,
  DBCitizyeaProperties,
  DBCamProperties,
  DBStopProperties,
  DBCarpoolingProperties,
  DBAutostopProperties,
  DBEnergyProperties,
  DBGeometries
])
class PointDao extends DatabaseAccessor<MyDatabase> with _$PointDaoMixin {
  PointDao(MyDatabase db) : super(db);

  Future<List<DBProperty>> get allProperties => select(dBProperties).get();

  /// Adds a property to the database from a Point [point].
  /// Returns the id of the new property if it is added otherwise an error.
  Future<int> addProperty(Point point) async {
    DBPropertiesCompanion companion = _getPropertiesCompanionFromPoint(point);
    int propertyId;

    return transaction(() async {
      propertyId = await into(dBProperties).insert(companion);
      await _insertSpecificPropertiesFromPoint(point, propertyId);
      await _addGeometry(propertyId, point.geometry);
      return propertyId;
    });
  }

  /// Deletes a property from Point [point].
  /// It is only deleted if the property is not used by others tables (Favorites, Search, etc...) anymore.
  Future<int> deletePropertyFromPoint(Point point) async {
    if (point == null)
      return 0;
    final DBProperty property = await getPropertyFromPoint(point);
    if (property != null) {
      return deleteIfUnusedProperty(property.id);
    }
    return 0;
  }

  // TODO : add other cases where DBProperty is used (tables FavoriteDetails, LastSearches, ...)
  /// Deletes a property from its [id] only if it is not used by others tables (Favorites, Search, etc...) anymore.
  /// Always called in a transaction.
  Future<int> deleteIfUnusedProperty(int id) async {
    assert(id != null);

    int count = await database.propertyUsed(id).getSingle();
    if (count == 0) {
      return _deleteProperty(id);
    }
    return Future.value(0);
  }

  /// Deletes a property by its [id].
  /// Always called in a transaction.
  Future<int> _deleteProperty(int id) async {
    assert(id != null);

    final DBProperty property = await getProperty(id);
    await _deleteGeometry(id);
    await _deleteSpecificProperty(id, property.type);
    return await (delete(dBProperties)..where((p) => p.id.equals(id))).go();
  }

  /// Returns true if the [point] is already in the database, false otherwise.
  Future<bool> containsPropertyFromPoint(Point point) async {
    DBProperty property = await getPropertyFromPoint(point);
    return property != null;
  }

  /// Returns true if a property associated with this [id] is already in the database, false otherwise.
  Future<bool> containsProperty(int id) async {
    DBProperty property = await getProperty(id);
    return property != null;
  }

  /// Returns the property entry in the database corresponding to this [point] or null if it not present.
  Future<DBProperty> getPropertyFromPoint(Point point) async {
    assert(point != null);
    return (select(dBProperties)..where((p) => p.metroId.equals(point.properties.id))..where((p) => p.type.equals(getPropertyTypeIdFromEnum(point.properties.type)))).getSingle();
  }

  /// Returns the property associated with this [id] or null if it is not present.
  Future<DBProperty> getProperty(int id) {
    return (select(dBProperties)..where((p) => p.id.equals(id))).getSingle();
  }

  /// Get the PropertiesCompanion associated with the Point [point].
  DBPropertiesCompanion _getPropertiesCompanionFromPoint(Point point) {
    assert(point != null);

    return DBPropertiesCompanion.insert(
      metroId: point.properties.id,
      name: point.properties.name,
      city: Value(point.properties.city),
      type: point.properties.type,
    );
  }

  /// Insert in the database the specific property entry of a [point].
  /// [propertyId] is the id of the property corresponding to this point that was just inserted.
  /// For example, a ClusterProperty point has 2 entries in the database : in the Properties table and in the ClusterProperties table which is the specific property table.
  Future<int> _insertSpecificPropertiesFromPoint(Point point, int propertyId) {
    assert(point != null);

    Table table;
    UpdateCompanion companion;

    switch (point.properties.type) {
      case PropertiesType.undefined:
        return Future.error(UnsupportedError('Not handled properties type : ${point.properties.type.toString()}'));
      case PropertiesType.clusters:
        table = database.dBClusterProperties;
        ClustersProperties clusterProperty = point.properties as ClustersProperties;
        companion = DBClusterPropertiesCompanion.insert(
            property: propertyId,
            dist: clusterProperty.dist,
            code: clusterProperty.code,
            visible: clusterProperty.visible
        );
        break;
      case PropertiesType.agenceM:
      case PropertiesType.depositaire:
      case PropertiesType.pointService:
        table = database.dBAgencyProperties;
        AgencyProperties agencyProperty = point.properties as AgencyProperties;
        companion = DBAgencyPropertiesCompanion.insert(
            property: propertyId,
            code: agencyProperty.code,
            address: agencyProperty.address
        );
        break;
      case PropertiesType.par:
      case PropertiesType.pkg:
      case PropertiesType.mvc:
      case PropertiesType.mva:
        table = database.dBParkingProperties;
        ParkingProperties parkingProperty = point.properties as ParkingProperties;
        companion = DBParkingPropertiesCompanion.insert(
            property: propertyId,
            code: parkingProperty.code,
            address: parkingProperty.address,
            total: parkingProperty.total
        );
        break;
      case PropertiesType.recharge:
        table = database.dBCodeProperties;
        CodeProperties codeProperty = point.properties as CodeProperties;
        companion = DBCodePropertiesCompanion.insert(
            property: propertyId,
            code: Value(codeProperty.code),
        );
        break;
      case PropertiesType.irve:
        table = database.dBIrveProperties;
        IrveProperties codeProperty = point.properties as IrveProperties;
        companion = DBIrvePropertiesCompanion.insert(
          property: propertyId,
          code: Value(codeProperty.code),
            amenageur: Value(codeProperty.amenageur),
            pdcTypes: Value(codeProperty.pdcTypes),
            pdcNb: Value(codeProperty.pdcNb)
        );
        break;
      case PropertiesType.lieux:
        table = database.dBPlaceProperties;
        PlaceProperties placeProperty = point.properties as PlaceProperties;
        companion = DBPlacePropertiesCompanion.insert(
          property: propertyId,
          dist: placeProperty.dist,
        );
        break;
      case PropertiesType.rue:
        table = database.dBStreetProperties;
        StreetProperties streetProperty = point.properties as StreetProperties;
        companion = DBStreetPropertiesCompanion.insert(
          property: propertyId,
          dist: streetProperty.dist,
        );
        break;
      case PropertiesType.citiz:
        table = database.dBCitizProperties;
        CitizProperties citizProperty = point.properties as CitizProperties;
        companion = DBCitizPropertiesCompanion.insert(
          property: propertyId,
          code: citizProperty.code,
        );
        break;
      case PropertiesType.citizyea:
        table = database.dBCitizyeaProperties;
        CitizyeaProperties citizyeaProperty = point.properties as CitizyeaProperties;
        companion = DBCitizyeaPropertiesCompanion.insert(
          property: propertyId,
          fuelLevel: citizyeaProperty.fuelLevel,
          licencePlate: citizyeaProperty.licencePlate,
        );
        break;
      case PropertiesType.cam:
        table = database.dBCamProperties;
        CamProperties citizyeaProperty = point.properties as CamProperties;
        companion = DBCamPropertiesCompanion.insert(
          property: propertyId,
          code: citizyeaProperty.code,
          cap: citizyeaProperty.cap,
        );
        break;
      case PropertiesType.stops:
        table = database.dBStopProperties;
        StopsProperties stopProperty = point.properties as StopsProperties;
        companion = DBStopPropertiesCompanion.insert(
          property: propertyId,
          gtfsClusterId: stopProperty.gtfsClusterId,
          gtfsId: stopProperty.gtfsId,
        );
        break;
      case PropertiesType.parkingCov:
      case PropertiesType.pointCov:
        table = database.dBCarpoolingProperties;
        CarpoolingProperties carpoolingProperty = point.properties as CarpoolingProperties;
        companion = DBCarpoolingPropertiesCompanion.insert(
            property: propertyId,
            placeId: carpoolingProperty.placeId,
            isOpen: carpoolingProperty.isOpen,
            placeLeft: carpoolingProperty.placeLeft,
            // TODO handle nearbyLines
            typeOfParking: carpoolingProperty.typeOfParking
        );
        break;
      case PropertiesType.autostop:
        table = database.dBAutostopProperties;
        AutostopProperties autostopProperty = point.properties as AutostopProperties;
        companion = DBAutostopPropertiesCompanion.insert(
            property: propertyId,
            address: autostopProperty.address,
            code: autostopProperty.code,
            direction1: autostopProperty.direction1,
            direction2: Value(autostopProperty.direction2),
            direction3: Value(autostopProperty.direction3)
        );
        break;
      case PropertiesType.hydrogene:
      case PropertiesType.gpl:
      case PropertiesType.gnv:
        table = database.dBEnergyProperties;
        EnergyProperties energyProperty = point.properties as EnergyProperties;
        companion = DBEnergyPropertiesCompanion.insert(
          property: propertyId,
          address: energyProperty.address,
        );
        break;
      case PropertiesType.tier:
        // TODO: Handle this case.
        break;
    }

    return into(table).insert(companion);
  }

  JoinedSelectStatement _getQueryForPointsWithIds(List<int> ids) => (select(dBProperties).join([
    leftOuterJoin(dBPlaceProperties, dBPlaceProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBClusterProperties, dBClusterProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBAgencyProperties, dBAgencyProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBParkingProperties, dBParkingProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBCodeProperties, dBCodeProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBIrveProperties, dBIrveProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBStreetProperties, dBStreetProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBCitizProperties, dBCitizProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBCitizyeaProperties, dBCitizyeaProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBCamProperties, dBCamProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBStopProperties, dBStopProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBCarpoolingProperties, dBCarpoolingProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBAutostopProperties, dBAutostopProperties.property.equalsExp(dBProperties.id)),
    leftOuterJoin(dBEnergyProperties, dBEnergyProperties.property.equalsExp(dBProperties.id)),
    innerJoin(dBGeometries, dBGeometries.property.equalsExp(dBProperties.id))
  ])..where((isIn(dBProperties.id, ids))));

  Future<List<Point>> getPointsWithIds(List<int> ids) async =>
      await getPointsStreamWithIds(ids).first.then((mapPoints) => mapPoints.values.toList(), onError: (e) => null);

  /// Get a stream observing a list of [Point] which are associated with the list of [ids] of Properties table.
  /// Returns the points in the Map with the property id as the key and the point as the value.
  Stream<Map<int, Point>> getPointsStreamWithIds(List<int> ids) {
    return _getQueryForPointsWithIds(ids).watch().map((rows) {
      final Map<int, Point> points = {};
      for (var row in rows) {
        final DBProperty property = row.readTable(dBProperties);
        final DBGeometry geometry = row.readTable(dBGeometries);
        switch (property.type) {
          case PropertiesType.undefined:
            throw UnimplementedError('Not handled properties type');
          case PropertiesType.clusters:
            var specificProperty = row.readTable(dBClusterProperties);
            points[property.id] = Point(
                properties: ClustersProperties(
                    PropertiesType.clusters,
                    property.metroId,
                    property.name,
                    property.city,
                    specificProperty.visible,
                    specificProperty.dist,
                    specificProperty.code),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.agenceM:
          case PropertiesType.depositaire:
          case PropertiesType.pointService:
            var specificProperty = row.readTable(dBAgencyProperties);
            points[property.id] = Point(
                properties: AgencyProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, specificProperty.address),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.par:
          case PropertiesType.pkg:
          case PropertiesType.mvc:
          case PropertiesType.mva:
            var specificProperty = row.readTable(dBParkingProperties);
            points[property.id] = Point(
              properties: ParkingProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, specificProperty.address, specificProperty.total, -1),
              geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.recharge:
            var specificProperty = row.readTable(dBCodeProperties);
            points[property.id] = Point(
                // properties: IrveProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, specificProperty.amenageur, null, specificProperty.pdcTypes, specificProperty.pdcNb),
                properties: CodeProperties(property.type, property.metroId, property.name, property.city, specificProperty.code),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.irve:
            var specificProperty = row.readTable(dBIrveProperties);
            points[property.id] = Point(
                properties: IrveProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, specificProperty.amenageur, null, specificProperty.pdcTypes, specificProperty.pdcNb),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude]));
            break;
          case PropertiesType.lieux:
            var specificProperty = row.readTable(dBPlaceProperties);
            points[property.id] = Point(
                properties: PlaceProperties(property.type, property.metroId, property.name, property.city, specificProperty.dist),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.rue:
            var specificProperty = row.readTable(dBStreetProperties);
            points[property.id] = Point(
                properties: StreetProperties(property.type, property.metroId, property.name, property.city, specificProperty.dist),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.citiz:
            var specificProperty = row.readTable(dBCitizProperties);
            points[property.id] = Point(
                properties: CitizProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, null),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.citizyea:
            var specificProperty = row.readTable(dBCitizyeaProperties);
            points[property.id] = Point(
              properties: CitizyeaProperties(property.type, property.metroId, property.name, property.city, specificProperty.licencePlate, specificProperty.fuelLevel),
              geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.cam:
            var specificProperty = row.readTable(dBCamProperties);
            points[property.id] = Point(
                properties: CamProperties(property.type, property.metroId, property.name, property.city, specificProperty.code, specificProperty.cap, -1),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.stops:
            var specificProperty = row.readTable(dBStopProperties);
            points[property.id] = Point(
              properties: StopsProperties(property.type, property.metroId, property.name, property.city, specificProperty.gtfsId, specificProperty.gtfsClusterId),
              geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.parkingCov:
          case PropertiesType.pointCov:
            var specificProperty = row.readTable(dBCarpoolingProperties);
            points[property.id] = Point(
                properties: CarpoolingProperties(
                    property.type,
                    property.metroId,
                    property.name,
                    property.city,
                    specificProperty.placeId,
                    specificProperty.isOpen,
                    specificProperty.placeLeft,
                    null,
                    specificProperty.typeOfParking),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.autostop:
            var specificProperty = row.readTable(dBAutostopProperties);
            points[property.id] = Point(
                properties: AutostopProperties(
                    property.type,
                    property.metroId,
                    property.name,
                    property.city,
                    specificProperty.code,
                    specificProperty.address,
                    specificProperty.direction1,
                    specificProperty.direction2,
                    specificProperty.direction3),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.hydrogene:
          case PropertiesType.gpl:
          case PropertiesType.gnv:
            var specificProperty = row.readTable(dBEnergyProperties);
            points[property.id] = Point(
                properties: EnergyProperties(property.type, property.metroId, property.name, property.city, specificProperty.address,),
                geometry: Geometry(coordinates: [geometry.longitude, geometry.latitude])
            );
            break;
          case PropertiesType.tier:
            // TODO: Handle this case.
            break;
        }
      }
      return points;
    });
  }

  /// Deletes in the database the specific property entry of a [Point] object.
  /// [id] is the [DBProperty] id corresponding to the point that went to delete.
  /// [type] is the [PropertiesType] needed to find the specific property table.
  /// For example, a [ClusterProperty] point has 2 entries in the database : in the [DBProperties] table and in the [DBClusterProperties] table which is
  /// the specific property table. Here, we want to delete the entry in the [DBClusterProperties] table.
  Future<int> _deleteSpecificProperty(int id, PropertiesType type) {
    switch (type) {
      case PropertiesType.undefined:
        return Future.error(throw UnsupportedError('Not handled properties type'));
      case PropertiesType.clusters:
        return (delete(database.dBClusterProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.agenceM:
      case PropertiesType.depositaire:
      case PropertiesType.pointService:
        return (delete(database.dBAgencyProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.par:
      case PropertiesType.pkg:
      case PropertiesType.mvc:
      case PropertiesType.mva:
        return (delete(database.dBParkingProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.recharge:
        return (delete(database.dBCodeProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.irve:
        return (delete(database.dBIrveProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.lieux:
        return (delete(database.dBPlaceProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.rue:
        return (delete(database.dBStreetProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.citiz:
        return (delete(database.dBCitizProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.citizyea:
        return (delete(database.dBCitizyeaProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.cam:
        return (delete(database.dBCamProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.stops:
        return (delete(database.dBStopProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.parkingCov:
      case PropertiesType.pointCov:
        return (delete(database.dBCarpoolingProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.autostop:
        return (delete(database.dBAutostopProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.hydrogene:
      case PropertiesType.gpl:
      case PropertiesType.gnv:
        return (delete(database.dBEnergyProperties)..where((p) => p.property.equals(id))).go();
      case PropertiesType.tier:
        // TODO: Handle this case.
        break;
    }

    return Future.error(throw UnsupportedError('Not handled properties type'));
  }

  /// Add the associated [Geometry] for a [Point].
  /// [id] is the [DBProperty.id] of the point.
  /// [geometry] is the corresponding [Geometry] of the [Point].
  Future<int> _addGeometry(int id, Geometry geometry) {
    return into(dBGeometries).insert(DBGeometriesCompanion.insert(property: id, latitude: geometry.coordinates[1], longitude: geometry.coordinates[0]));
  }

  Future<int> _deleteGeometry(int id) {
    return (delete(dBGeometries)..where((g) => g.property.equals(id))).go();
  }

}

@UseDao(tables: [DatabaseInfos])
class DatabaseInfosDao extends DatabaseAccessor<MyDatabase> with _$DatabaseInfosDaoMixin {
  DatabaseInfosDao(MyDatabase db) : super(db);

  Future<bool> get isInitialised async{
    DatabaseInfo info = await (select(databaseInfos)..where((di) => di.id.equals(DatabaseInfos.initialiseId))).getSingle();
    return info == null ? false : int.parse(info.value) == 1;
  }

  Future setIsInitialised() {
    return transaction(() async {
      await into(databaseInfos).insert(DatabaseInfosCompanion(id: Value(DatabaseInfos.initialiseId), value: Value('1')), orReplace: true);
      //await initPropertyTypesTable();
    });
  }
}

class PropertyTypeConverter extends TypeConverter<PropertiesType, int> {
  const PropertyTypeConverter();

  @override
  PropertiesType mapToDart(int fromDb) {
    if (fromDb == null) {
      return null;
    }
    return getPropertyTypeFromId(fromDb);
  }

  @override
  int mapToSql(PropertiesType value) {
    if (value == null) {
      return null;
    }

    return getPropertyTypeIdFromEnum(value);
  }
}

class FavoriteTypeConverter extends TypeConverter<FavoriteType, int> {
  const FavoriteTypeConverter();

  @override
  FavoriteType mapToDart(int fromDb) {
    if (fromDb == null) {
      return null;
    }
    return getFavoriteTypeFromId(fromDb);
  }

  @override
  int mapToSql(FavoriteType value) {
    if (value == null) {
      return null;
    }

    return getFavoriteTypeIdFromEnum(value);
  }
}