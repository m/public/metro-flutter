import 'package:flutter/material.dart';

class Elevation {
  final Color backgroundColor;

  const Elevation(this.backgroundColor);
}

class Elevations {
  final Elevation e00dp;
  final Elevation e01dp;
  final Elevation e02dp;
  final Elevation e03dp;
  final Elevation e04dp;
  final Elevation e06dp;
  final Elevation e08dp;
  final Elevation e12dp;
  final Elevation e16dp;
  final Elevation e24dp;

  final Elevation fstTextColor;
  final Elevation sndTextColor;

  final Elevation inverseColor;
  final Elevation unSelectedIconColor;

  final Elevation expandPanelColor;
  final Elevation expandPanelDetailsColor;

  final Elevation backgroundWhite;
  final Elevation backgroundExpand;

  final Elevation infoBackground;

  final Elevation bottomSheetBar;

  final String test;

  final String footerAsset;
  final String iconVoironnais;
  final Elevation popupMenu;

  final String assetsPath;

  final Elevation blueColor;
  final Elevation separator;
  final Elevation expandBorder;
  final String perturbation_1;
  final String perturbation_2;

  final Elevation nsvBackgroundColor_2;
  final Elevation nsvBackgroundColor_3;
  final Elevation nsvBackgroundColor_4;
  final Elevation nsvBackgroundColorNormal;

  final Elevation perturbationRedColor;

  Elevations({
    this.e00dp,
    this.e01dp,
    this.e02dp,
    this.e03dp,
    this.e04dp,
    this.e06dp,
    this.e08dp,
    this.e12dp,
    this.e16dp,
    this.e24dp,
    this.fstTextColor,
    this.sndTextColor,
    this.inverseColor,
    this.unSelectedIconColor,
    this.expandPanelColor,
    this.expandPanelDetailsColor,
    this.backgroundWhite,
    this.backgroundExpand,
    this.test,
    this.footerAsset,
    this.iconVoironnais,
    this.infoBackground,
    this.bottomSheetBar,
    this.separator,
    this.expandBorder,
    this.popupMenu,
    this.assetsPath,
    this.blueColor,
    this.perturbation_1,
    this.perturbation_2,
    this.nsvBackgroundColor_2,
    this.nsvBackgroundColor_3,
    this.nsvBackgroundColor_4,
    this.nsvBackgroundColorNormal,
    this.perturbationRedColor
  });

  factory Elevations.darkModeEnabled(bool isEnabled) => !isEnabled ? Elevations._buildLightTheme() : Elevations._buildDarkTheme();

  factory Elevations._buildDarkTheme() => Elevations(
    e00dp: const Elevation(const Color(0xFF16111A)),
    e01dp: const Elevation(const Color(0xFF221D26)),
    e02dp: const Elevation(const Color(0xFF16111A)),
    e03dp: const Elevation(const Color(0xFF28242C)),
    e04dp: const Elevation(const Color(0xFF2B262F)),
    e06dp: const Elevation(const Color(0xFF302B33)),
    e08dp: const Elevation(const Color(0xFF26222A)),
    e12dp: const Elevation(const Color(0xFF322E36)),
    e16dp: const Elevation(const Color(0xFF39343C)),
    e24dp: const Elevation(const Color(0xFF3B373F)),
    fstTextColor: const Elevation(const Color(0xFFFFFFFF)),
    sndTextColor: const Elevation(const Color(0xFFC5C5C5)),
    inverseColor: const Elevation(const Color(0xFF000000)),
    unSelectedIconColor: const Elevation(const Color(0xFFc3c2c4)),
    expandPanelColor: const Elevation(const Color(0xFF221D26)),
    expandPanelDetailsColor: const Elevation(const Color(0xFF39343C)),
    backgroundWhite: const Elevation(const Color(0xFF16111A)),
    backgroundExpand: const Elevation(const Color(0xFF26222A)),
    test: 'https://data.mobilites-m.fr/carte-dark/{z}/{x}/{y}.png',
    footerAsset: 'assets/footer_dark.png',
    iconVoironnais: 'assets/logos/TPV.png',
    infoBackground: const Elevation(const Color(0xFF26222A)),
    bottomSheetBar: const Elevation(const Color(0x1FFFFFFF)),
    separator: const Elevation(const Color(0x1FFFFFFF)),
    popupMenu: const Elevation(const Color(0xFF3B373F)),
    assetsPath: 'assets/markers/dark/',
    blueColor: const Elevation(const Color(0xFF96dbeb)),
    expandBorder: const Elevation(const Color(0x00FFFFFF)),
    perturbation_1: 'assets/icons/ic_hors_service_dark.svg',
    perturbation_2: 'assets/icons/ic_perturb_dark.svg',
    nsvBackgroundColor_2: const Elevation(const Color(0xFF714A4A)),
    nsvBackgroundColor_3: const Elevation(const Color(0xFF992E2E)),
    nsvBackgroundColor_4: const Elevation(const Color(0xFF5C595F)),
    nsvBackgroundColorNormal: const Elevation(const Color(0xFF221D26)),
    perturbationRedColor: const Elevation(const Color(0xFFEC6C6C)),
  );

  factory Elevations._buildLightTheme() => Elevations(
    e00dp: const Elevation(const Color(0xFFEEEEEE)),
    e01dp: const Elevation(const Color(0xFFFFFFFF)),
    e02dp: const Elevation(const Color(0xFFFFFFFF)),
    e03dp: const Elevation(const Color(0xFFFFFFFF)),
    e04dp: const Elevation(const Color(0xFFFFFFFF)),
    e06dp: const Elevation(const Color(0xFFFFFFFF)),
    e08dp: const Elevation(const Color(0xFFFFFFFF)),
    e12dp: const Elevation(const Color(0xFFFFFFFF)),
    e16dp: const Elevation(const Color(0xFFFFFFFF)),
    e24dp: const Elevation(const Color(0xFFEEEEEE)),
    fstTextColor: const Elevation(Colors.black87),
    sndTextColor: const Elevation(const Color(0xFF7f7c82)),
    inverseColor: const Elevation(const Color(0xFFFFFFFF)),
    unSelectedIconColor: const Elevation(const Color(0xFF757575)),
    expandPanelColor: const Elevation(const Color(0xFFEEEEEE)),
    expandPanelDetailsColor: const Elevation(const Color(0xFFEEEEEE)),
    backgroundWhite: const Elevation(const Color(0xFFFFFFFF)),
    backgroundExpand: const Elevation(const Color(0xFFEEEEEE)),
    test: 'https://data.mobilites-m.fr/carte/{z}/{x}/{y}.png',
    footerAsset: 'assets/footer_light.png',
    iconVoironnais: 'assets/logos/TPV_Light.png',
    infoBackground: const Elevation(const Color(0xFFFFFFFF)),
    bottomSheetBar: const Elevation(const Color(0xFF7f7c82)),
    separator: const Elevation(const Color(0xFFEEEEEE)),
    popupMenu: const Elevation(const Color(0xFFF3F3F3)),
    assetsPath: 'assets/markers/light/',
    blueColor: const Elevation(const Color(0xFF3da4c2)),
    expandBorder: const Elevation(const Color(0xFFB3B3B3)),
    perturbation_1: 'assets/icons/ic_hors_service_light.svg',
    perturbation_2: 'assets/icons/ic_perturb_light.svg',
    nsvBackgroundColor_2: const Elevation(const Color(0xFFF7E5E0)),
    nsvBackgroundColor_3: const Elevation(const Color(0xFFB92D00)),
    nsvBackgroundColor_4: const Elevation(const Color(0xFF212121)),
    nsvBackgroundColorNormal: const Elevation(const Color(0xFFEEEEEE)),
    perturbationRedColor: const Elevation(const Color(0xFFC4004C)),
  );
}

Elevations globalElevations;
