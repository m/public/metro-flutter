import 'package:flutter/material.dart';

class Language{
  Language(this.name, this.code);

  String name;
  String code;

  String getName() {
    return name;
  }

  void setCode(String code) {
    this.code = code;
  }

  String getCode() {
    return code;
  }

  void setName(String name) {
    this.name = name;
  }

}