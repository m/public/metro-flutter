import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/custom_drawer.dart';
import 'package:metro/global/widgets/dialog_events_state.dart';
import 'package:metro/global/widgets/light_theme_dialog.dart';
import 'package:metro/global/widgets/update_store_dialog.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:metro/disturbances/pages/disturbances.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/main.dart';
import 'package:package_info/package_info.dart';
import 'package:preferences/preference_service.dart';

import 'package:flutter_apns/apns.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as ss;

class NavigationPage extends StatefulWidget {
  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  String favoritesDeleteAll = translations.text('favorite.reorder');
  bool showEventsInfoModal = false;
  bool showLightThemeModal = false;
  List<EventModel> list;
  bool isLoading = true;
  bool isOpenConnectivityPanel = true;
  bool _canShowEventsInfoModal = true;

  @override
  initState() {
    getList();
    getRead();
    if (Platform.isAndroid) {
      _getFCMTokenAndroid();
    }
    if (Platform.isIOS) {
      _getFCMTokenIOS();
    }
  }

  getRead() async {
    await BlocProvider.master<EventsBloc>().isReaded();
  }

  Future _showNotification(String title, String message) async {

    var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: (id, title, body, payload) {
      print('onDidReceiveLocalNotification');
      return null;
    });

    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    FlutterLocalNotificationsPlugin notification = FlutterLocalNotificationsPlugin();

    await notification.initialize(initializationSettings, onSelectNotification: (payload) {
      print('onDidReceiveLocalNotification');
      return null;
    });

    var androidPlatformChannelSpecifics = AndroidNotificationDetails('id', 'name', 'description');

    var platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics, iOS: IOSNotificationDetails());
    notification.show(
      0,
      '$title',
      '$message',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

  _getFCMTokenAndroid() async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

    String pushToken = await _firebaseMessaging.getToken();
    final storage = new ss.FlutterSecureStorage();
    await storage.write(key: 'pushtoken', value: pushToken);

    _firebaseMessaging.configure(
      onBackgroundMessage: Platform.isIOS ? null : Fcm.myBackgroundMessageHandler,
      onMessage: (Map<String, dynamic> message) async {
        _showNotification(message['notification']['title'], message['notification']['body']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DisturbancesPage(1)),
        );
        _showNotification(message['notification']['title'], message['notification']['body']);
      },
      onResume: (Map<String, dynamic> message) async {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => DisturbancesPage(1)),
        );
        _showNotification(message['notification']['title'], message['notification']['body']);
      },
    );
  }

  _getFCMTokenIOS() async {
    final connector = createPushConnector();

    connector.configure(
      onLaunch: (Map<String, dynamic> message) async {
        showNotification(message['aps']);
      },
      onResume: (Map<String, dynamic> message) async {
        showNotification(message['aps']);
      },
      onMessage: (Map<String, dynamic> message) async {
        showNotification(message['aps']);
      },
      // onBackgroundMessage: onBackgroundPush,
    );

    connector.token.addListener(() async {
      final storage = new ss.FlutterSecureStorage();
      await storage.write(key: 'pushtoken', value: connector.token.value);
    });

    connector.requestNotificationPermissions();
  }

  showNotification(notification) {
    Fluttertoast.showToast(
      msg: Platform.isAndroid
          ? notification['title'] + ': ' + notification['body']
          : notification['title'] + ': ' + notification['alert'],
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Color(0xffb49bda),
      textColor: Colors.black,
      fontSize: 16.0,
    );
  }

  getList() async {
    list = await BlocProvider.master<EventsBloc>().getExceptionalEvents();
    setState(() {
      isLoading = false;
    });
  }

  void _showEventsInfoModal(BuildContext context, List<EventModel> event) {
    List<EventModel> eventList = [];

    for (EventModel e in event) {
      if (PrefService.getBool(
              Consts.preferencesEventsInfoModal + "_" + e.key) ==
          null) {
        eventList.add(e);
      }
    }

    if (eventList.isNotEmpty && _canShowEventsInfoModal)
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) => MyEventsDialog(myEvent: event),
      ).then((_) => _canShowEventsInfoModal = false);
  }

  void _showLightThemeModal(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) => LightThemeDialog(),
    );
  }

  //TODO : HALIMA UPDATE MODALE
  Future<void> _showUpdateModal(BuildContext context) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String latestVersionNumber = '5.0.3';

    if (packageInfo.version != latestVersionNumber) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => UpdateDialog(),
      );
    }
  }

  int calculateDifference(DateTime date) {
    DateTime now = DateTime.now();
    return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
  }

  @override
  Widget build(BuildContext context) {
    DateTime dt = DateTime.parse('2021-09-01 00:01:00');
    print(calculateDifference(dt));

    if (calculateDifference(dt) >= 0){
      if (PrefService.getBool('modify_Old_Fav') == null) PrefService.setBool('modify_Old_Fav', true);
      if (PrefService.getBool('modify_Old_Fav')) BlocProvider.master<FavoritesBloc>().modifyOldFavorites(context);
    }

    if (isLoading == false) {
      showEventsInfoModal =
          (PrefService.getBool(Consts.preferencesEventsInfoModal) ?? true) &&
              BlocProvider.master<NavigationBloc>().showEventsPopup;
      showLightThemeModal =
          (PrefService.getBool(Consts.preferencesLightThemeModal) ?? true) &&
              BlocProvider.master<NavigationBloc>().showLightThemePopup;
    }

    if (showEventsInfoModal) {
      Future.delayed(Duration.zero, () => _showEventsInfoModal(context, list));
    }
    // if (showLightThemeModal)
    //   Future.delayed(Duration.zero, () => _showLightThemeModal(context));

    return WillPopScope(
      onWillPop: () async =>
          BlocProvider.master<NavigationBloc>().backNavigation(),
      child: SafeArea(
        child: StreamBuilder<Widget>(
          stream: BlocProvider.of<NavigationBloc>(context).onPageChanged,
          builder: (context, snapshot) {
            Widget body = snapshot.data;
            if (body == null) body = _buildEmptyWidget();

            return Scaffold(
              appBar: buildAppBar(),
              body: Stack(
                key: BlocProvider.of<NavigationBloc>(context).getSafeAreaKey(),
                children: <Widget>[
                  body,
                  _buildConnectivityPanel(),
                ],
              ),
              bottomNavigationBar: _buildBottomNavigationBar(),
              drawer: CustomDrawer(),
              endDrawerEnableOpenDragGesture: false,
              // endDrawer: FiltersDrawerWidget(),
            );
          },
        ),
      ),
    );
  }

  Widget _buildEmptyWidget() {
    return Center(
      child: const AnimatedLoadingLogo(),
    );
  }

  // TODO : refactor to have all AppBars here and see if we can only have one
  Widget buildAppBar() {
    switch (BlocProvider.of<NavigationBloc>(context).navigationCategory) {
      case NavigationCategory.favorites:
        FavoritesBloc favoritesBloc = BlocProvider.of<FavoritesBloc>(context);

        return AppBar(
          title: Text(
            translations.text('favorites.title'),
            style:
                TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          ),
          backgroundColor: globalElevations.e04dp.backgroundColor,
          iconTheme: IconThemeData(
              color: globalElevations.fstTextColor.backgroundColor),
          actions: <Widget>[
            // Cancel reorder action
            StreamBuilder<bool>(
              stream: favoritesBloc.onReorderChanged,
              builder: (context, snapshot) {
                if (!snapshot.hasData || snapshot.data == null)
                  return Container();

                return Visibility(
                  visible: snapshot.data,
                  child: IconButton(
                    icon: Icon(Icons.cancel),
                    onPressed: () => favoritesBloc.cancelReorderFavorites(),
                  ),
                );
              },
            ),
            // Validate reorder action
            StreamBuilder<bool>(
              stream: favoritesBloc.onReorderChanged,
              builder: (context, snapshot) {
                if (!snapshot.hasData || snapshot.data == null)
                  return Container();

                return Visibility(
                  visible: snapshot.data,
                  child: IconButton(
                    icon: Icon(Icons.check),
                    onPressed: () => favoritesBloc.validateReorderFavorites(),
                  ),
                );
              },
            ),
            PopupMenuButton<String>(
              onSelected: choiceAction,
              color: globalElevations.popupMenu.backgroundColor,
              icon: Icon(
                Icons.more_vert,
                color: globalElevations.fstTextColor.backgroundColor,
              ),
              itemBuilder: (BuildContext context) {
                return getChoices().map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
        );
      default:
        return null;
    }
  }

  List<String> getChoices() {
    List<String> choicesList = List();
    choicesList.add(translations.text('favorites.reorder'));
    choicesList.add(translations.text('favorites.delete_all.title'));

    return choicesList;
  }

  void choiceAction(String choice) {
    if (choice == translations.text('favorites.reorder')) {
      BlocProvider.of<FavoritesBloc>(context).requestReorderFavorites();
    } else if (choice == translations.text('favorites.delete_all.title')) {
      _showFavoritesConfirmDialog();
    }
  }

  Widget _buildBottomNavigationBar() {
    return StreamBuilder<Object>(
      stream:
          BlocProvider.of<NavigationBloc>(context).onNavigationCategoryChanged,
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null)
          return _buildEmptyWidget();

        return BottomNavigationBar(
          elevation: 16,
          backgroundColor: globalElevations.e16dp.backgroundColor,
          selectedItemColor: Theme.of(context).accentColor,
          unselectedItemColor:
              globalElevations.unSelectedIconColor.backgroundColor,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.place),
              title: Text(
                translations.text('around_me.carte'),
                style: TextStyle(fontWeight: FontWeight.w300),
              ),
            ),
            BottomNavigationBarItem(
              icon: Stack(
                children: <Widget>[
                  Icon(Icons.favorite),
                ],
              ),
              title: Text(translations.text('favorites.title'),
                  style: TextStyle(fontWeight: FontWeight.w300)),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.near_me),
              title: Text(translations.text('route.title'),
                  style: TextStyle(fontWeight: FontWeight.w300)),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.departure_board),
              title: Text(translations.text('schedule.LINES'),
                  style: TextStyle(fontWeight: FontWeight.w300)),
            ),
          ],
          currentIndex: NavigationCategory.values.indexOf(
              BlocProvider.of<NavigationBloc>(context)
                  .getMajorCategory(snapshot.data)),
          onTap: (int index) {
            BlocProvider.of<NavigationBloc>(context).changeTabByIndex(index);
          },
        );
      },
    );
  }

  Widget _buildConnectivityPanel() {
    return FutureBuilder(
      future: Connectivity().checkConnectivity(),
      builder: (context, connectiviy) {
        return StreamBuilder<ConnectivityResult>(
          stream: Connectivity().onConnectivityChanged,
          initialData: connectiviy.data,
          builder: (context, snapshot) {
            if (!snapshot.hasData || snapshot.data == null) return Container();

            if (snapshot.data == ConnectivityResult.none) {
              return Visibility(
                visible: isOpenConnectivityPanel,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.redAccent,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 8.0, top: 16.0, bottom: 16.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.85,
                                child: Column(
                                  children: <Widget>[
                                    Text(translations
                                        .text('generic.no_connectivity')),
                                  ],
                                ),
                              ),
                              Container(
                                alignment: Alignment.topRight,
                                width: MediaQuery.of(context).size.width * 0.07,
                                child: Column(
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: new IconButton(
                                        icon: new Icon(Icons.close),
                                        onPressed: () {
                                          setState(() {
                                            isOpenConnectivityPanel = false;
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                    ),
                  ],
                ),
              );

              // If connectivity come back
            } else {
              isOpenConnectivityPanel = true;
            }
            return Container();
          },
        );
      },
    );
  }

  Future<void> _showFavoritesConfirmDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translations.text('favorites.delete_all.title')),
          content: SingleChildScrollView(
            child: Text(translations.text('favorites.delete_all.dialog_text')),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(translations.text('generic.cancel').toUpperCase()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(translations.text('generic.ok').toUpperCase()),
              onPressed: () {
                BlocProvider.master<FavoritesBloc>().deleteAllFavorites();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class Fcm {

  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    print('AppPushs myBackgroundMessageHandler : $message');
    showNotification('background title', 'background body');
    return Future<void>.value();
  }

  static Future showNotification(String title, String message) async {
    var initializationSettingsAndroid = AndroidInitializationSettings('launcher');
    var initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: (id, title, body, payload) {
      print('onDidReceiveLocalNotification');
      return null;
    });

    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    FlutterLocalNotificationsPlugin notification = FlutterLocalNotificationsPlugin();

    await notification.initialize(initializationSettings, onSelectNotification: (payload) {
      return null;
    });

    var androidPlatformChannelSpecifics = AndroidNotificationDetails('id', 'name', 'description');

    var platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics, iOS: IOSNotificationDetails());
    notification.show(
      0,
      '$title',
      '$message',
      platformChannelSpecifics,
      payload: 'Default_Sound',
    );
  }

}