import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/global/pages/data_credits.dart';
import 'package:metro/global/pages/technical_credits.dart';
import 'package:metro/localization/translations.dart';
import 'package:package_info/package_info.dart';

import '../utils.dart';

class InfoPage extends TraceableStatefulWidget {
  InfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  int _versionTapCount = 0;
  Timer _versionTapTimer;

  Widget _buildVersionNumber(AsyncSnapshot<PackageInfo> snapshot) {
    WebServices ws = BlocProvider.of<WebServices>(context);
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasError) {
        return Center(
            child: Text(translations.text('information.version_error'),
                style:
                    TextStyle(color: Theme.of(context).colorScheme.primary)));
      } else {
        return GestureDetector(
          onTap: () {
            if (_versionTapTimer != null) _versionTapTimer.cancel();

            _versionTapCount++;

            if (_versionTapCount >= 5) {
              _versionTapCount = 0;

              setState(() {
                if (ws.host == WebServiceHost.prod)
                  ws.changeHost(WebServiceHost.preprod);
                else
                  ws.changeHost(WebServiceHost.prod);
              });

              return;
            }

            _versionTapTimer = Timer(Duration(milliseconds: 750), () {
              _versionTapCount = 0;
            });
          },
          child: Text(
            'Version ' + snapshot.data.version,
            style: TextStyle(color: Theme.of(context).colorScheme.primary),
          ),
        );
      }
    } else
      return CircularProgressIndicator();
  }

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).colorScheme.primary;
    final TextStyle type1 = TextStyle(
        color: primaryColor, fontWeight: FontWeight.bold, fontSize: 16);

    WebServices ws = BlocProvider.of<WebServices>(context);

    return Scaffold(
      backgroundColor: globalElevations.infoBackground.backgroundColor,
      appBar: AppBar(
        backgroundColor: globalElevations.e16dp.backgroundColor,
        iconTheme:
            IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
        elevation: 16,
        title: Text(
          translations.text('drawer.info'),
          style:
              TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
      ),
      body: FutureBuilder<PackageInfo>(
        future: PackageInfo.fromPlatform(),
        builder: (context, snapshot) {
          return LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: constraints.copyWith(
                    minHeight: constraints.maxHeight,
                    maxHeight: double.infinity,
                  ),
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 16),
                        Icon(
                          MetroIcons.agence_m,
                          size: 100,
                          color: Theme.of(context)?.colorScheme?.primary ??
                              Colors.white,
                        ),
                        SizedBox(height: 20),
                        Visibility(
                          visible: ws.host == WebServiceHost.preprod,
                          child: Center(
                            child: Text(
                              translations.text('information.debug_mode') + " - Build : " + snapshot.data.buildNumber,
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                        ),
                        _buildVersionNumber(snapshot),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.all(6),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: primaryColor)),
                                  onPressed: () {
                                    Utils.launchUrl(
                                        'https://www.mobilites-m.fr/pages/FAQ.html');
                                  },
                                  color: Colors.transparent,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right: 8.0),
                                        child: Icon(Icons.public, color: Theme.of(context).colorScheme.primary,),
                                      ),
                                      Center(
                                        child: Text(
                                            translations
                                                .text('information.faq')
                                                .toUpperCase(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold, fontSize: 16)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 1),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: primaryColor)),
                                  onPressed: () {
                                    Utils.launchUrl(
                                        'https://www.mobilites-m.fr/');
                                  },
                                  color: Colors.transparent,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right: 8.0),
                                        child: Icon(Icons.public, color: Theme.of(context).colorScheme.primary,),
                                      ),
                                      Center(
                                        child: Text(
                                            translations
                                                .text('information.website')
                                                .toUpperCase(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Theme.of(context).colorScheme.primary, fontWeight: FontWeight.bold, fontSize: 16)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 1),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: primaryColor)),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DataCreditsPage())),
                                  color: Colors.transparent,
                                  child: Text(
                                      translations
                                          .text('information.data_credits')
                                          .toUpperCase(),
                                      style: type1),
                                ),
                              ),
                              SizedBox(height: 1),
                              SizedBox(
                                width: double.infinity,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      side: BorderSide(color: primaryColor)),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TechnicalCreditsPage())),
                                  color: Colors.transparent,
                                  child: Text(
                                      translations
                                          .text('information.technical_credits')
                                          .toUpperCase(),
                                      style: type1),
                                ),
                              ),
                              SizedBox(height: 1),
                              SizedBox(
                                width: double.infinity,
                                child: RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: primaryColor)),
                                  onPressed: () {
                                    Utils.launchUrl(
                                        'mailto:contact@mobilites-m.fr');
                                  },
                                  color: primaryColor,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(right: 8.0),
                                        child: Icon(Icons.public, color: globalElevations.inverseColor.backgroundColor,),
                                      ),
                                      Center(
                                        child: Text(
                                            translations
                                                .text('information.contact')
                                                .toUpperCase(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: globalElevations.inverseColor.backgroundColor, fontWeight: FontWeight.bold, fontSize: 16)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                alignment: Alignment.bottomLeft,
                                image: AssetImage(
                                  globalElevations.footerAsset,
                                ),
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
