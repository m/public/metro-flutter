import 'package:flutter/material.dart';

class Page{
  Page(this.position, this.name);

  int position;
  String name;

  int getPosition(){
    return this.position;
  }

  String getName(){
    return this.name;
  }

  setPosition(int position){
    this.position = position;
  }

  setName(String name){
    this.name = name;
  }


}
