import 'package:flutter/material.dart';
import 'package:metro/localization/translations.dart';

class TechnicalCreditsPage extends StatelessWidget {
  Widget _buildTitle(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(
        title,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildText(String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(translations.text('information.technical_credits'))),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle("1. Crédits techniques"),
              _buildText(
                  "Cette application est conçue, développée et éditée par Grenoble-Alpes Métropole et le Syndicat Mixte des Mobilités de l'aire Grenobloise (SMMAG)\nLe Forum\n3, rue Malakoff\n38031 Grenoble Cedex 01"),
              _buildText(
                  "Conception et ergonomie : SMMAG et Viséo Technologies\nCharte graphique : SMMAG, Viséo Technologies et Atelier Frumy\nCartographie et fond de plan: Open Street Map\nLibrairies open source : leaflet, angular.js, flutter, material, url_launcher, dio, path_provider_ex, package_info, rxdart, flutter_svg, html_unescape, moor_flutter, dynamic_theme, image_picker, image_cropper, flutter_email_sender, camera, animations, flutter_staggered_animations, cupertino_icons, flutter_map, http, scoped_model, preferences, location, shared_preferences, flutter_full_pdf_viewer, esys_flutter_share, flutter_secure_storage, expandable, permission_handler, fluttertoast, flutter_apns, connectivity, flutter_matomo, video_player, chewie, provider ...\nCalculateur d'itinéraire : OpenTripPlanner\nDéveloppement back-end, front-end, right&left hand, hotfixes 24/24 et tacos : Sully-Group\nCaféine, demandes impossibles, gifs animés et playlist : SMMAG"),
              SizedBox(height: 8),
              _buildTitle("2. Direction de la publication"),
              _buildText(
                  "Agnès Delarue, Directrice du département mobilités et transports"),
              SizedBox(height: 8),
              _buildTitle("3. Contenu éditorial"),
              _buildText(
                  "Nous mettons tout en œuvre pour offrir aux utilisateurs de cette application des informations fiables et vérifiées.\nCependant, malgré tous les soins apportés, lil peut y avoir des inexactitudes, des défauts de mise à jour ou des erreurs.\nLe SMMAG ne serait être tenu responsable des cas mentionnés ci-dessus ou en cas d'inaccessibilité technique ou de falsification des contenus."),
              _buildText(
                  "Nous remercions les utilisateurs de cette application de nous faire part d'éventuelles omissions, erreurs ou corrections par mail, twitter ou facebook.\nL’équipe éditoriale se réserve le droit de modifier ou de corriger le contenu de cette mention légale à tout moment."),
              _buildText(
                  "Tous les textes, images et résultats présents sur cette application sont sous la responsabilité et sont la propriété des maîtres d'ouvrage de M : SMMAG & Grenoble-Alpes Métropole\nDe fait, toute reproduction ou modification (totale ou partielle) de ces éléments sans autorisation préalable et écrite est interdite."),
              _buildText(
                  "Pour tous renseignements, vous pouvez nous contacter via le formulaire de contact ci-dessous ou par courrier postal à l'adresse suivante :\nMobilités M\nSMMAG\nLe Forum\n3, rue Malakoff\n38031 Grenoble Cedex 01"),
            ],
          ),
        ),
      ),
    );
  }
}
