import 'package:flutter/material.dart';
import 'package:metro/localization/translations.dart';

class DataCreditsPage extends StatelessWidget {
  Widget _buildTitle(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(
        title,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildText(String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(title: Text(translations.text('information.data_credits'))),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle("Origine des données et utilisation"),
              _buildText(
                  "- Les données (arrêts, horaires théoriques et dynamiques, parkings relais) du réseau TAG sont fournies par le SMMAG.\nIssues de l'exploitation TAG, elles sont consolidées, maintenues et diffusées par M.\nCes données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données (arrêts, horaires théoriques et dynamiques) du réseau TransIsère sont fournies par le Département de l'Isère.\nIssues de l'exploitation TransIsère, elles sont maintenues et diffusées par Itinisère.\nCes données sont publiées au titre de l'opendata sous licence ODbL sur www.itinisère.fr\nSur le périmètre géographique du SMMAG, ces données sont consolidées et publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données (arrêts, horaires théoriques et dynamiques) du réseau TouGo sont fournies par le SMMAG.\nIssues de l'exploitation TouGo, elles sont maintenues par GR4 et diffusées par M.\nCes données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données (arrêts, horaires théoriques et dynamiques) du réseau Pays Voironnais sont fournies par le SMMAG.\nIssues de l'exploitation des Transports du Pays Voironnais, elles sont maintenues par XXX et diffusées par M.\nCes données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les arrêts et horaires des trains sont fournies par la SNCF\nCes données sont publiées par la SNCF au titre de l'opendata sous licence ouverte."),
              _buildText(
                  "- Les données de stationnement sont fournies par Grenoble-Alpes Métropole\nIssues de l'exploitation Park-Grenoble-Alpes Métropole, elles sont consolidées, maintenues et diffusées par M.\nLes données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données Citiz et Yea! sont fournies par Citiz\nCertaines de ces données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données de covoiturage (localisation des aires et parkings, points d'autostop organisé) sont fournies par Grenoble-Alpes Métropole\nLes données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText(
                  "- Les données dynamiques de trafic routier sont fournies par Grenoble-Alpes Métropole, la DIR-CE, le Département de l'Isère et AREA\nIssues de l'exploitation des différents tronçons routiers par Grenoble-Alpes Métropole, la DIR-CE, le Département de l'Isère et AREA, elles sont consolidées et diffusées par M sur le territoire métropolitain\nCertaines de ces données sont publiées au titre de l'opendata sous licence ODbL sur www.mobilites-m.fr"),
              _buildText("- Les webcams trafic sont fournies par la DIR-CE"),
              _buildText(
                  "- Les informations de qualité de l'air sont fournies par ATMO Auvergne Rhône Alpes"),
              _buildText(
                  "Pour toute question sur l'utilisation et la réutilisation des données, pour les conditions d'application de la licence, merci de nous contacter via contact@mobilites-m.fr"),
            ],
          ),
        ),
      ),
    );
  }
}
