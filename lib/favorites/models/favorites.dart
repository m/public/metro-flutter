import 'package:metro/global/database/database.dart';
import 'package:metro/global/helpers/hash.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';

class Favorite {
  final FavoriteDetail details;
  final Point point1;
  final Point point2;
  final List<String> lines;
  final ModeOfTransport modes;

  Favorite({this.details, this.point1, this.point2, this.lines, this.modes});

  factory Favorite.copyWithPos(Favorite oldFav, int newPos) {
    return Favorite(
      details: FavoriteDetail(
        id: oldFav.details.id,
        point1: oldFav.details.point1,
        point2: oldFav.details.point2,
        position: newPos,
        type: oldFav.details.type,
      ),
      point1: oldFav.point1,
      point2: oldFav.point2,
      lines: oldFav.lines,
      modes: oldFav.modes,
    );
  }

  @override
  int get hashCode => details.id;

  @override
  bool operator ==(other) {
    if (other is! Favorite) return false;

    return details.id == other.details.id;
  }
}

class FavoriteCluster {
  final Point stop;
  final List<Line> lines;

  FavoriteCluster({this.stop, this.lines});
}

class FavoriteItinerary {
  final Point departure;
  final Point destination;
  final ModeOfTransport modes;

  FavoriteItinerary({this.departure, this.destination, this.modes});

  @override
  int get hashCode => hash3(departure, destination, modes);

  @override
  bool operator ==(other) {
    if (other is! FavoriteItinerary) return false;

    return departure == other.departure &&
        destination == other.destination &&
        modes == other.modes;
  }
}

enum FavoriteType { cluster, parking, camera, itinerary, poi, tier }

int getFavoriteTypeIdFromEnum(FavoriteType type) {
  assert(type != null);

  return type.index;
}

FavoriteType getFavoriteTypeFromId(int id) {
  assert(id != null);

  return FavoriteType.values[id];
}

int getPropertyTypeIdFromEnum(PropertiesType type) {
  assert(type != null);

  return type.index;
}

PropertiesType getPropertyTypeFromId(int id) {
  assert(id != null);

  return PropertiesType.values[id];
}
