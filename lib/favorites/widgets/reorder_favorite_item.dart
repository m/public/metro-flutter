import 'package:flutter/material.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/list_item.dart';
import 'package:metro/global/widgets/reorderable_list_item.dart';

class ReorderFavoriteItem extends StatelessWidget {
  final Widget header;

  const ReorderFavoriteItem({Key key, this.header}) : super(key: key);

  ReorderFavoriteItem.camera(Favorite favorite)
      : this(
          key: ValueKey(favorite.hashCode),
          header: ListItem.normal(
            iconData: favorite?.point1?.properties?.iconData(),
            text1: favorite.point1.properties.name,
          ),
        );

  ReorderFavoriteItem.itinerary(Favorite favorite)
      : this(
          key: ValueKey(favorite.hashCode),
          header: ListItem.itinerary(
            iconData: Icons.directions,
            text1: favorite?.point1?.properties?.name,
            text2: favorite?.point2?.properties?.name,
          ),
        );

  ReorderFavoriteItem.parking(Favorite favorite)
      : this(
          key: ValueKey(favorite.hashCode),
          header: ListItem.normal(
            iconData: favorite?.point1?.properties?.iconData(),
            text1: favorite.point1.properties.name,
          ),
        );

  ReorderFavoriteItem.poi(Favorite favorite)
      : this(
          key: ValueKey(favorite.hashCode),
          header: ListItem.normal(
            iconData: favorite?.point1?.properties?.iconData(),
            text1: favorite.point1.properties.name,
          ),
        );

  ReorderFavoriteItem.stop(Favorite favorite)
      : this(
          key: ValueKey(favorite.hashCode),
          header: ReorderableListItem(
            iconData: favorite?.point1?.properties?.iconData(),
            text: favorite.point1.properties.name,
            code: favorite.lines.isEmpty ? "" : favorite.lines.first,
          ),
        );

  @override
  Widget build(BuildContext context) {
    return Column(
      key: ValueKey(header.hashCode),
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(
              Radius.circular(6),
            ),
            child: Container(
              color: globalElevations.e08dp.backgroundColor,
              child: Row(
                children: <Widget>[
                  Expanded(child: header),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(Icons.drag_handle),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
