import 'dart:async';

import 'package:flutter/material.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';

class FavoriteButton extends StatefulWidget {
  final dynamic data;
  final bool border;

  const FavoriteButton({Key key, @required this.data, this.border = false})
      : assert(data is Point || data is FavoriteItinerary || data == null, 'data type is not supported, should be Point or FavoriteItinerary'),
        super(key: key);

  @override
  _FavoriteButtonState createState() => _FavoriteButtonState();
}

class _FavoriteButtonState extends State<FavoriteButton> {
  Future<bool> isFavorite;
  bool hasBeenTapped;
  dynamic data;

  @override
  void initState() {
    super.initState();
    hasBeenTapped = true;
  }

  _changeValue(bool val) {
    isFavorite = Future<bool>.delayed(Duration(milliseconds: 0), () => val);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _setData();
    return FutureBuilder<bool>(
      future: isFavorite,
      builder: (context, snapshot) {
        IconData data = Icons.favorite_border;
        bool value;
        if (snapshot.hasData && !snapshot.hasError) {
          hasBeenTapped = false;
          value = snapshot.data;
          if (value) data = Icons.favorite;
        } else
          value = false;

        return InkWell(
          radius: 80,
          borderRadius: BorderRadius.circular(50),
          child: Container(
            decoration: widget.border
                ? ShapeDecoration(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                        side: BorderSide(
                          color: Theme.of(context).colorScheme.primary,
                        )))
                : null,
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 250),
              child: Icon(data, key: ValueKey(value), color: Theme.of(context).indicatorColor),
              transitionBuilder: (child, animation) {
                if (((child as Icon).key as ValueKey).value)
                  return ScaleTransition(
                      child: child,
                      alignment:
                          value ? Alignment.bottomCenter : Alignment.topCenter,
                      scale:
                          Tween<double>(begin: 0, end: 1).animate(animation));
                else
                  return ScaleTransition(
                      child: child,
                      alignment:
                          value ? Alignment.topCenter : Alignment.bottomCenter,
                      scale:
                          Tween<double>(begin: 0, end: 1).animate(animation));
              },
            ),
          ),
          onTap: hasBeenTapped ? null : value ? _removeFavorite : _addFavorite,
        );
      },
    );
  }

  _removeFavorite() async {
    if (hasBeenTapped || !mounted) return;

    setState(() => hasBeenTapped = true);

    if (data is FavoriteItinerary) {

      await BlocProvider.of<FavoritesBloc>(context).removeFavoriteItinerary(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteItinerary(data);

    } else if ((data as Point).properties is ClustersProperties) {

      String longitude = data.geometry.coordinates[0].toString();
      String latitude = data.geometry.coordinates[1].toString();

      var cluster = await BlocProvider.of<FavoritesBloc>(context).checkFavoriteCluster(data);

      await BlocProvider.of<FavoritesBloc>(context).addFavoriteCluster(context, longitude, latitude, cluster, _changeValue);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);

    } else {

      await BlocProvider.of<FavoritesBloc>(context).removeFavorite(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);

    }
    if (mounted) setState(() {});
  }

  _addFavorite() async {
    if (hasBeenTapped || !mounted) return;

    setState(() => hasBeenTapped = true);

    if (data is FavoriteItinerary) {

      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteItinerary(data);

    } else if ((data as Point).properties is ClustersProperties) {

      String longitude = data.geometry.coordinates[0].toString();
      String latitude = data.geometry.coordinates[1].toString();

      var cluster = await BlocProvider.of<FavoritesBloc>(context).checkFavoriteCluster(data);

      await BlocProvider.of<FavoritesBloc>(context).addFavoriteCluster(context, longitude, latitude, cluster, _changeValue);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);

    } else {

      await BlocProvider.of<FavoritesBloc>(context).addFavorite(data);
      isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavoriteCluster(data);

    }
    if (mounted) setState(() {});
  }

  void _setData() {
    if (data == null || data != widget.data) {
      data = widget.data;
      if (widget.data is FavoriteItinerary)
        isFavorite =
            BlocProvider.of<FavoritesBloc>(context).isFavoriteItinerary(data);
      else
        isFavorite = BlocProvider.of<FavoritesBloc>(context).isFavorite(data);
    }
  }
}
