import 'dart:async';
import 'dart:collection';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import 'package:metro/schedule/blocs/schedule.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/favorites/widgets/custom_reorderable_list.dart';
import 'package:metro/favorites/widgets/reorder_favorite_item.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/models/formatted_schedule.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/dialog_state.dart';
import 'package:metro/global/widgets/list_item.dart';
import 'package:metro/global/widgets/new_expandable_panel.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/main.dart';
import 'package:metro/schedule/widgets/favorites_bottom_module.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/map/widgets/line_selection_dialog.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/schedule/widgets/schedule_details.dart';
import 'package:metro/schedule/widgets/schedule_details_favorites.dart';
import 'package:metro/schedule/widgets/favorites_details.dart';
import 'package:metro/video/video_component.dart';

import 'package:metro/extensions/extensions.dart' show ContainsFav;


class PopUpChoice {
  final String text;
  final IconData icon;

  const PopUpChoice(this.text, this.icon);
}

class FavoritesPage extends TraceableStatefulWidget {
  FavoritesPage({Key key}) : super(key: key, name: MatomoBloc.screen_favorites);

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage>
    with WidgetsBindingObserver {
  /// Queue of Favorite to delete.
  Queue<Favorite> queue;

  List<ExpandableController> _controllers = List();
  StreamSubscription _onSelectedFavoriteItemChanged;

  FavoritesBloc favoritesBloc;
  bool showInfoModal;
  final GlobalKey _loadingDialogKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    favoritesBloc = BlocProvider.of<FavoritesBloc>(context);
    queue = favoritesBloc.queueRemovedFavorites;

    favoritesBloc.resetSelectedLineStop();

    _onSelectedFavoriteItemChanged =
        favoritesBloc.onSelectedFavoriteItemListener(_controllers);

    // showInfoModal =
    //     (PrefService.getBool(Consts.preferencesInfoModal) ?? true) &&
    //         BlocProvider.master<NavigationBloc>().showPopup;
    // if (showInfoModal)
    //   Future.delayed(Duration.zero, () => _showInfoModal(context));

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;

    if(listFavs != null)
      for (Favorite fav in listFavs){
        if (fav.point1.properties.type == PropertiesType.recharge){
          database.favoriteDao.deleteFavorite(fav);
        }
      }

  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _onSelectedFavoriteItemChanged.cancel();
    super.dispose();
  }

  void _showInfoModal(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return MyDialog();
        });
  }

  List<Widget> _buildFavoritesCluster(List<Favorite> data) {
    data.sort((a, b) => a.details.position.compareTo(b.details.position));
    List<Widget> _children = [];
    for (int index = 0; index < data.length; index++) {
      ExpandableController controller = ExpandableController();
      _controllers.add(controller);
      Favorite fav = data[index];

      switch (fav.details.type) {
        case FavoriteType.cluster:
          _children.add(_FavoriteItem.stop(fav, controller, index, context));
          break;
        case FavoriteType.parking:
          _children.add(_FavoriteItem.parking(fav, controller, index));
          break;
        case FavoriteType.camera:
          _children.add(_FavoriteItem.camera(fav, controller, index));
          break;
        case FavoriteType.itinerary:
          _children.add(_FavoriteItem.itinerary(fav, controller, index));
          break;
        case FavoriteType.poi:
          _children.add(_FavoriteItem.poi(fav, controller, index));
          break;
        case FavoriteType.tier:
          // TODO: Handle this case.
          break;
      }
    }
    return _children;
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedFavoritePage(
      data: this,
      child: StreamBuilder<List<Favorite>>(
        stream: favoritesBloc.onFavoritesChanged,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const AnimatedLoadingLogo();
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Center(
              child: Text(
                translations.text('favorites.empty_list'),
                textAlign: TextAlign.center,
              ),
            );
          }

          return StreamBuilder<bool>(
            stream: favoritesBloc.onReorderChanged,
            builder: (context, snapshotReorder) {
              if (!snapshotReorder.hasData || snapshotReorder.data == null)
                return Container();

              if (snapshotReorder.data) {
                // If we are currently reordering favorites
                return Theme(
                  data: Theme.of(context),
                  child: CustomReorderableListView(
                    onReorder: _onReorder,
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      ...snapshot.data.map((Favorite fav) {
                        switch (fav.details.type) {
                          case FavoriteType.cluster:
                            return ReorderFavoriteItem.stop(fav);
                          case FavoriteType.parking:
                            return ReorderFavoriteItem.parking(fav);
                          case FavoriteType.camera:
                            return ReorderFavoriteItem.camera(fav);
                          case FavoriteType.itinerary:
                            return ReorderFavoriteItem.itinerary(fav);
                          case FavoriteType.poi:
                            return ReorderFavoriteItem.poi(fav);
                          default:
                            return null;
                        }
                      }).toList(),
                    ],
                  ),
                );
              } else {
                // We are not reordering favorites
                _controllers.clear();
                favoritesBloc.resetSelectedLineStop();
                return Theme(
                  data: Theme.of(context),
                  child: SingleChildScrollView(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                    child: AnimationLimiter(
                      child: Column(
                        children: AnimationConfiguration.toStaggeredList(
                          duration: const Duration(milliseconds: 375),
                          childAnimationBuilder: (widget) => SlideAnimation(
                            child: FadeInAnimation(child: widget),
                            horizontalOffset: 50.0,
                          ),
                          children: _buildFavoritesCluster(snapshot.data),
                        ),
                      ),
                    ),
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }

  void _onReorder(int oldIndex, int newIndex) =>
      favoritesBloc.reorderFavorites(oldIndex, newIndex);

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: better handle the deletion when quitting app or changing page
  }

  /// Get the [ExpandablePanelAction] to delete a favorite.
  ExpandablePanelAction deleteAction(Favorite favorite) =>
      ExpandablePanelAction(
          translations.text('generic.delete'), () => deleteFavorite(favorite));

  /// Get the [ExpandablePanelAction] to see the point on the map.
  ExpandablePanelAction seeMapAction(Favorite favorite) =>
      ExpandablePanelAction(translations.text('favorites.see_map'),
          () => _seeFavoriteOnMap(favorite));

  ExpandablePanelAction seeBottomSheetAction(Favorite favorite) =>
      ExpandablePanelAction("Autres actions", () => openBottomSheet(context, favorite));

  /// Get the [ExpandablePanelAction] to open the itinerary.
  ExpandablePanelAction openRouteAction(Favorite favorite) =>
      ExpandablePanelAction(translations.text('favorites.open_route'),
          () => _openFavoriteItineraryInRoutePage(favorite));

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> showSnackBar(
      Favorite favorite) {
    return Scaffold.of(context)
        .showSnackBar(_buildDeleteFavoriteSnackBar(favorite));
  }

  Future<List<FormattedScheduleModel>> formattedList (Favorite fav) {
    Future<List<FormattedScheduleModel>> listFormatted = BlocProvider.of<ScheduleBloc>(context)
        .loadClusters((fav.point1.properties as ClustersProperties).code, null, BlocProvider.master<LinesBloc>().lines);
    return listFormatted;
  }

  Future<bool> hasLine(Favorite fav) async {
    List<FormattedScheduleModel> formattedTempList = await formattedList(fav);
    List<FormattedScheduleModel> newList = [];
    ReadyLine readyLine;

    for (var x in formattedTempList) {
      if (x.code == fav.lines[0]) {
        newList.add(x);
      }
    }

    if (newList.length != 0) {
      readyLine = BlocProvider.master<LinesBloc>()
          .lines
          .where((line) => line.line.shortName == newList[0].code)
          .first;
    }

    if (readyLine == null)
      return false;
    else
      return true;
  }

  openBottomSheet(BuildContext context, Favorite fav) async {
    bool b = await hasLine(fav);
    b ? showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            backgroundColor: Colors.transparent,
            builder: (BuildContext context) {
              return FavoritesBottomModule(
                favorite: fav,
                onTapRemoveFav: () {
                  deleteFavorite(fav);
                },
                onTapAddFav: () {
                  _addLineToStop(fav, context);
                },
              );
            })
        : Fluttertoast.showToast(
            msg: 'Aucune action disponible pour cette ligne',
            toastLength: Toast.LENGTH_SHORT);
  }

  Future _addLineToStop(Favorite fav, BuildContext context) async {
    List<Line> lines = await BlocProvider.of<MapBloc>(context).getLinesFromStop(
        fav.point1.geometry.coordinates[0], fav.point1.geometry.coordinates[1], (fav.point1.properties as ClustersProperties).code);
    if (fav.details.type == FavoriteType.cluster && lines.length > 1) {
      List<Line> previousSelection = [];
      lines.forEach((line) {
        if (BlocProvider.of<FavoritesBloc>(context)
            .favorites
            .hasFavorite(line.shortName, fav.point1.properties.name))
          previousSelection.add(line);
      });
      showDialog<SelectionData>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => LineSelectionDialog(lines, fav,
            previousSelection: previousSelection),
      ).then((value) {
        if (value.value) {
          if (value.selectAll) {
            value.selectedLines.clear();
            value.selectedLines.addAll(lines);
          }
          List<Line> unselected = [];
          lines.forEach((line) {
            if (!value.selectedLines.contains(line)) unselected.add(line);
          });
          value.selectedLines.forEach((line) {
            List<Line> tmp = [];
            tmp.add(line);
            FavoriteCluster favoriteCluster =
            FavoriteCluster(stop: fav.point1, lines: tmp);
            if (!BlocProvider.of<FavoritesBloc>(context)
                .favorites
                .hasFavorite(line.shortName, fav.point1.properties.name))
              database.favoriteDao.addFavorite(favoriteCluster);
          });
          unselected.forEach((line) {
            if (BlocProvider.of<FavoritesBloc>(context)
                .favorites
                .hasFavorite(line.shortName, fav.point1.properties.name))
              database.favoriteDao.deleteFavorite(
                  BlocProvider.of<FavoritesBloc>(context)
                      .favorites
                      .getFavorite(line.shortName, fav.point1.properties.name));
          });
        }
      });
    } else {
      Fluttertoast.showToast(
          msg: translations.text("favorites.msg"),
          toastLength: Toast.LENGTH_SHORT);
    }
  }

  SnackBar _buildDeleteFavoriteSnackBar(Favorite fav) => SnackBar(
        duration: const Duration(seconds: 3),
        content: Text(
            '${fav.point1.properties.name} ${translations.text('favorites.deleted')}'),
        action: SnackBarAction(
          label: translations.text('generic.cancel').toUpperCase(),
          onPressed: () {
            // Do not delete the favorite
            BlocProvider.master<FavoritesBloc>().removeFavoriteFromQueue();
          },
        ),
      );

  void deleteFavorite(Favorite fav) async {
    assert(fav != null);

    if (fav.details.type == FavoriteType.itinerary)
      return favoritesBloc.removeFavoriteItinerary(fav);

    if (!queue.any((item) => item.details.id == fav.details.id)) {
      favoritesBloc.addToQueue(fav);

      showSnackBar(fav).closed.then((SnackBarClosedReason reason) async {
        // If the user did not use the action, delete the first favorite in the queue.
        if (reason != SnackBarClosedReason.action)
          await favoritesBloc.removeFavoriteFromQueue(delete: true);
      });
    }
  }

  /// Display the favorite on map : the [Favorite.point1]'s details page.
  void _seeFavoriteOnMap(Favorite favorite) {
    NavigationBloc navigationBloc = BlocProvider.of<NavigationBloc>(context);
    MapBloc mapBloc = BlocProvider.of<MapBloc>(context);
    navigationBloc.changeTab(NavigationCategory.map);

    switch (favorite.details.type) {
      case FavoriteType.cluster:
        mapBloc.setBottomSheetContent(BottomSheetCategory.schedule,
            arguments: favorite.point1);
        break;
      case FavoriteType.poi:
      case FavoriteType.camera:
      case FavoriteType.parking:
        mapBloc.setBottomSheetContent(BottomSheetCategory.POIDetails,
            arguments: favorite.point1);
        break;
      case FavoriteType.itinerary:
        break;
      case FavoriteType.tier:
        // TODO: Handle this case.
        break;
    }
    mapBloc.moveTo(MapPositionAndZoom.withLatLng(Utils.coordinatesFromArray(favorite.point1.geometry.coordinates), 17));
    mapBloc.selectPOI(favorite.point1.properties.id);
  }

  /// See the route to the favorite, that is to the [Favorite.point1].
  void goToFavorite(Favorite favorite) {
    RoutesBloc routesBloc = BlocProvider.master<RoutesBloc>();
    routesBloc.setDeparture(null);
    routesBloc.setDestination(favorite.point1);
    BlocProvider.master<NavigationBloc>().changeTab(NavigationCategory.route);
  }

  /// Open the associated route on [RoutePage].
  void _openFavoriteItineraryInRoutePage(Favorite favorite) async {
    RoutesBloc routesBloc = BlocProvider.master<RoutesBloc>();
    routesBloc.setItineraryFromFavorites(favorite, context);
    // Close the loading dialog
    if (favorite.point2.properties.name == "Ma Position"){
      showLoadingDialog(context, _loadingDialogKey);
      bool gotLocation = await BlocProvider.master<LocationBloc>().getLocation();
      Navigator.of(_loadingDialogKey.currentContext, rootNavigator: true).pop();
      if (gotLocation) {
        if (Utils.isPositionInGrenoble(
            _currentLatitude(context), _currentLongitude(context))) {
          favorite.point2.geometry.coordinates[0] = _currentLongitude(context);
          favorite.point2.geometry.coordinates[1] = _currentLatitude(context);
          BlocProvider.master<NavigationBloc>()
              .changeTab(NavigationCategory.route);
        } else {
          BlocProvider.master<NavigationBloc>()
              .changeTab(NavigationCategory.route);
        }
      }
    }else
      BlocProvider.master<NavigationBloc>()
          .changeTab(NavigationCategory.route);


  }
}

Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
  return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SimpleDialog(key: key, children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16),
            child: Center(
              child: Row(children: [
                AnimatedLoadingLogo(
                  height: 60,
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                    child: Text(
                  translations.text('search.location_loading_message'),
                  softWrap: true,
                ))
              ]),
            ),
          )
        ]);
      });
}

double _currentLongitude(BuildContext context) {
  LocationData location = BlocProvider.of<LocationBloc>(context).location;
  return location?.longitude;
}

double _currentLatitude(BuildContext context) {
  LocationData location = BlocProvider.of<LocationBloc>(context).location;
  return location?.latitude;
}

class _FavoriteItem extends StatefulWidget {

  final Favorite favorite;
  final ExpandableController controller;
  final int index;
  final Widget header;
  final Widget details;
  final VoidCallback onTap;

  _FavoriteItem(
      {@required this.favorite,
        @required this.controller,
        @required this.index,
        this.header,
        this.details,
        this.onTap});


  _FavoriteItem.camera(
      Favorite favorite, ExpandableController controller, int index)
      : this(
      favorite: favorite,
      controller: controller,
      index: index,
      header: ListItem.normal(
        iconData: favorite?.point1?.properties?.iconData(),
        text1: favorite.point1.properties.name,
      ));

  _FavoriteItem.itinerary(
      Favorite favorite, ExpandableController controller, int index)
      : this(
      favorite: favorite,
      controller: controller,
      index: index,
      header: ListItem.itinerary(
        iconData: Icons.directions,
        text1: favorite?.point1?.properties?.name,
        text2: favorite?.point2?.properties?.name,
      ));

  _FavoriteItem.parking(
      Favorite favorite, ExpandableController controller, int index)
      : this(
      favorite: favorite,
      controller: controller,
      index: index,
      header: FavoriteDynamicData(
        favorite: favorite,
      ));

  //TODO : Halima modifier avec les favoris caméra
  _FavoriteItem.poi(
      Favorite favorite, ExpandableController controller, int index)
      : this(
      favorite: favorite,
      controller: controller,
      index: index,
      header: (favorite?.point1?.properties?.type == PropertiesType.agenceM
          || favorite?.point1?.properties?.type == PropertiesType.mva
          || favorite?.point1?.properties?.type == PropertiesType.irve)
          ? FavoriteDynamicData(favorite: favorite)
          : ListItem.normal(
        iconData: favorite?.point1?.properties?.iconData(),
        text1: favorite.point1.properties.name,
      ),
      details: favorite.point1.properties.type == PropertiesType.cam
          ? VideoComponent(
          'https://datatest.metromobilite.fr/api/cam/video?name=' +
              (favorite.point1.properties as CamProperties).code +
              '.mp4')
          : EmptyCell());

  _FavoriteItem.stop(
      Favorite favorite, ExpandableController controller, int index, BuildContext ctx)
      : this(
      favorite: favorite,
      controller: controller,
      index: index,
      header: ListItem.normal(
        iconData: favorite?.point1?.properties?.iconData(),
        text1: favorite.point1.properties.name,
      ),
      details: ScheduleDetailsOfPassage(
          code: (favorite.point1.properties as ClustersProperties).code,
          isBottomSheet: false),
      onTap: () {
        if (favorite.details.type == FavoriteType.cluster &&
            favorite.lines.isEmpty) {
          String longitude =
          favorite.point1.geometry.coordinates[0].toString();
          String latitude =
          favorite.point1.geometry.coordinates[1].toString();
          BlocProvider.master<MapBloc>()
              .addLinesDeleteOldFavs(ctx, longitude, latitude, favorite, favorite.point1);
        }
      });

  @override
  State<StatefulWidget> createState() => _FavoriteItemState();

}

/// Widget representing a favorite item in the list of favorites.
class _FavoriteItemState extends State<_FavoriteItem> {

  @override
  Widget build(BuildContext context) {
    assert(widget.favorite != null);
    final _FavoritesPageState pageState = _InheritedFavoritePage.of(context);
    if (widget.favorite.details.type == FavoriteType.cluster &&
        widget.favorite.lines.isNotEmpty) {
      return FutureBuilder<List<dynamic>>(
          future: _buildActions(pageState, context),
          builder: (context, snapshot) {
            if (snapshot.data == null || !snapshot.hasData)
              return Container();
            return ExpandableNotifier(
              controller: widget.controller,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: NewExpandablePanel(
                    showArrow: false,
                    showBorder: true,
                    header: _buildHeader(widget.controller),
                    actions: snapshot.data,
                    onOpenOrClose: (expanded) {
                      setState(() {
                        _onOpenOrClose(expanded, context);
                      });
                    }),
              ),
            );
          });
    } else {
      return FutureBuilder<List<dynamic>>(
          future: _buildActions(pageState, context),
          builder: (context, snapshot) {
            if (snapshot.data == null || !snapshot.hasData) return Container();
            return ExpandableNotifier(
              controller: widget.controller,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: NewExpandablePanel(
                  header: widget.header,
                  details: widget.details,
                  actions: snapshot.data,
                  onOpenOrClose: (expanded) =>
                      _onOpenOrClose(expanded, context),
                ),
              ),
            );
          });
    }
  }

  Future _addLineToStop(Favorite fav, BuildContext context) async {
    List<Line> lines = await BlocProvider.of<MapBloc>(context).getLinesFromStop(
        fav.point1.geometry.coordinates[0], fav.point1.geometry.coordinates[1], (fav.point1.properties as ClustersProperties).code);
    if (fav.details.type == FavoriteType.cluster && lines.length > 1) {
      List<Line> previousSelection = [];
      lines.forEach((line) {
        if (BlocProvider.of<FavoritesBloc>(context)
            .favorites
            .hasFavorite(line.shortName, fav.point1.properties.name))
          previousSelection.add(line);
      });
      showDialog<SelectionData>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => LineSelectionDialog(lines, fav,
            previousSelection: previousSelection),
      ).then((value) {
        if (value.value) {
          if (value.selectAll) {
            value.selectedLines.clear();
            value.selectedLines.addAll(lines);
          }
          List<Line> unselected = [];
          lines.forEach((line) {
            if (!value.selectedLines.contains(line)) unselected.add(line);
          });
          value.selectedLines.forEach((line) {
            List<Line> tmp = [];
            tmp.add(line);
            FavoriteCluster favoriteCluster =
                FavoriteCluster(stop: fav.point1, lines: tmp);
            if (!BlocProvider.of<FavoritesBloc>(context)
                .favorites
                .hasFavorite(line.shortName, fav.point1.properties.name))
              database.favoriteDao.addFavorite(favoriteCluster);
          });
          unselected.forEach((line) {
            if (BlocProvider.of<FavoritesBloc>(context)
                .favorites
                .hasFavorite(line.shortName, fav.point1.properties.name))
              database.favoriteDao.deleteFavorite(
                  BlocProvider.of<FavoritesBloc>(context)
                      .favorites
                      .getFavorite(line.shortName, fav.point1.properties.name));
          });
        }
      });
    } else {
      Fluttertoast.showToast(
          msg: translations.text("favorites.msg"),
          toastLength: Toast.LENGTH_SHORT);
    }
  }

  List<PopUpChoice> _getChoices(Favorite favorite) {
    List<PopUpChoice> choices = [
      PopUpChoice(translations.text('favorites.remove'), Icons.delete)
    ];
    if (favorite.details.type == FavoriteType.cluster) {
      choices
          .add(PopUpChoice(translations.text('favorites.add_line'), Icons.add));
    }
    if (favorite.point1.properties.type != PropertiesType.cam) choices.add(PopUpChoice(translations.text('favorites.go_to'), Icons.near_me));
    return choices;
  }

  void _choiceAction(String choice, Favorite favorite,
      _FavoritesPageState pageState, BuildContext context) {
    if (choice == translations.text('favorites.remove'))
      pageState.deleteFavorite(favorite);
    else if (choice == translations.text('favorites.add_line'))
      _addLineToStop(favorite, context);
    else if (choice == translations.text('favorites.go_to'))
      pageState.goToFavorite(favorite);
  }

  Future<List<dynamic>> _buildActions (
      _FavoritesPageState pageState, BuildContext context) async {
    switch (widget.favorite.details.type) {
      case FavoriteType.itinerary:
        return [
          pageState.deleteAction(widget.favorite),
          pageState.openRouteAction(widget.favorite),
        ];
      case FavoriteType.cluster:
        bool b = await hasLine(widget.favorite);
        return [
          Expanded(child: Container()),
          b
              ? pageState.seeBottomSheetAction(widget.favorite)
              : pageState.deleteAction(widget.favorite),
        ];
      default:
        return [
          pageState.seeMapAction(widget.favorite),
          Expanded(child: Container()),
          PopupMenuButton<String>(
            onSelected: (choice) =>
                _choiceAction(choice, widget.favorite, pageState, context),
            color: globalElevations.popupMenu.backgroundColor,
            icon:
                Icon(Icons.more_horiz, color: Theme.of(context).indicatorColor),
            itemBuilder: (BuildContext context) =>
                _getChoices(widget.favorite).map((PopUpChoice choice) {
              return PopupMenuItem<String>(
                value: choice.text,
                child: Row(
                  children: <Widget>[
                    Icon(choice.icon),
                    SizedBox(width: 8),
                    Text(choice.text)
                  ],
                ),
              );
            }).toList(),
          )
        ];
    }
  }

  Future<List<FormattedScheduleModel>> formattedList (Favorite fav) {
    Future<List<FormattedScheduleModel>> listFormatted = BlocProvider.of<ScheduleBloc>(context)
        .loadClusters((fav.point1.properties as ClustersProperties).code, null, BlocProvider.master<LinesBloc>().lines);
    return listFormatted;
  }

  Future<bool> hasLine(Favorite fav) async {
    List<FormattedScheduleModel> formattedTempList = await formattedList(fav);
    List<FormattedScheduleModel> newList = [];
    ReadyLine readyLine;

    for (var x in formattedTempList) {
      if (x.code == fav.lines[0]) {
        newList.add(x);
      }
    }

    if (newList.length != 0) {
      readyLine = BlocProvider.master<LinesBloc>()
          .lines
          .where((line) => line.line.shortName == newList[0].code)
          .first;
    }

    if (readyLine == null)
      return false;
    else
      return true;
  }

  _onOpenOrClose(bool expanded, BuildContext context) {
    if (expanded) {
      BlocProvider.of<FavoritesBloc>(context).onTappedFavoriteItem(widget.index);
      if (widget.onTap != null) widget.onTap();
    }
  }

  Widget _buildHeader(ExpandableController controller) {
    return ScheduleDetailsOfPassageFavorites(
        code: (widget.favorite.point1.properties as ClustersProperties).code,
        isBottomSheet: false,
        favorite: widget.favorite,
        linesFavorites: widget.favorite.lines,
        controller: controller);
  }


}

class _InheritedFavoritePage extends InheritedWidget {
  const _InheritedFavoritePage({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);

  final _FavoritesPageState data;

  @override
  bool updateShouldNotify(_InheritedFavoritePage oldWidget) => true;

  static _FavoritesPageState of(BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<_InheritedFavoritePage>())
          .data;
}

class ListViewCard extends StatefulWidget {
  final int index;
  final Key key;
  final List<String> listItems;

  ListViewCard(this.listItems, this.index, this.key);

  @override
  _ListViewCard createState() => _ListViewCard();
}

class _ListViewCard extends State<ListViewCard> {
  Widget _buildTitle() {
    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.topLeft,
      child: Text(
        'Title ${widget.listItems[widget.index]}',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        textAlign: TextAlign.left,
        maxLines: 5,
      ),
    );
  }

  Widget _buildDescription() {
    return Container(
      padding: const EdgeInsets.all(8),
      alignment: Alignment.topLeft,
      child: Text(
        'Description ${widget.listItems[widget.index]}',
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
        textAlign: TextAlign.left,
        maxLines: 5,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(4),
      color: Colors.white,
      child: InkWell(
        splashColor: Colors.blue,
        onTap: () => Fluttertoast.showToast(
            msg: "Item ${widget.listItems[widget.index]} selected.",
            toastLength: Toast.LENGTH_SHORT),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _buildTitle(),
                  _buildDescription(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
              child: Icon(
                Icons.reorder,
                color: Colors.grey,
                size: 24,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
