import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/database/database.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/route_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_ex/path_provider_ex.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';

class FavoritesBloc extends BlocBase {
  final MyDatabase database;

  FavoritesBloc(this.database) : assert(database != null);

  BehaviorSubject<List<Favorite>> _favoritesController =
      BehaviorSubject<List<Favorite>>.seeded(null);
  Stream<List<Favorite>> get onFavoritesChanged =>
      _favoritesController.stream;
  List<Favorite> get favorites => _favoritesController.value;
  int lastFavoritesLength;
  StreamSubscription<List<Favorite>> onDbFavoritesChanged;

  BehaviorSubject<Queue<Favorite>> _queueRemovedFavoritesController =
      BehaviorSubject<Queue<Favorite>>.seeded(Queue());
  Stream<Queue<Favorite>> get onQueueRemovedFavoritesChanged =>
      _queueRemovedFavoritesController.stream;
  Queue<Favorite> get queueRemovedFavorites =>
      _queueRemovedFavoritesController.value;

  BehaviorSubject<int> _selectedFavoriteItemController =
      BehaviorSubject<int>.seeded(null);
  Stream<int> get onSelectedFavoriteItemChanged =>
      _selectedFavoriteItemController.stream;
  int get selectedFavoriteItem => _selectedFavoriteItemController.value;
  int lastSelectedFavoriteItem;

  BehaviorSubject<List<Point>> _favoritesInSearchController =
      BehaviorSubject<List<Point>>.seeded(null);
  Stream<List<Point>> get onFavoritesInSearchChanged =>
      _favoritesInSearchController.stream;
  List<Point> get favoritesInSearch => _favoritesInSearchController.value;

  BehaviorSubject<bool> _reorderController =
      BehaviorSubject<bool>.seeded(false);
  Stream<bool> get onReorderChanged => _reorderController.stream;
  bool get isReordering => _reorderController.value;

  List<Favorite> _favoritesBeforeReorder;
  bool newNameFav = true;

  @override
  void initState() async {
    onDbFavoritesChanged = Rx.combineLatest2<List<Favorite>,
            Queue<Favorite>, List<Favorite>>(
        database.favoriteDao.allFavoritesStream, onQueueRemovedFavoritesChanged,
        (dbFavorites, queuedFavorites) {
      if (queueRemovedFavorites.isNotEmpty &&
          lastFavoritesLength != null &&
          lastFavoritesLength > dbFavorites.length) {
        // The favorite has correctly been removed
        _queueRemovedFavoritesController.sink
            .add(queueRemovedFavorites..removeFirst());
      }

      lastFavoritesLength = dbFavorites.length;

      if (queuedFavorites.isNotEmpty) {
        List<Favorite> temp = List.from(dbFavorites);
        temp.removeWhere((f) => queuedFavorites.contains(f));
        return temp;
      }
      return dbFavorites;
    }).listen((data) {
      bool shouldUpdateStream(List<Favorite> current) {
        if (favorites == null && current == null) return false;
        if (favorites == null ||
            current == null ||
            favorites.length != current.length) return true;
        for (int i = 0; i < current.length; i++) {
          if (current[i]?.details?.id != favorites[i]?.details?.id) return true;
        }
        return false;
      }

      if (shouldUpdateStream(data)) _favoritesController.sink.add(data);
    });

    bool hasImportedFavorites =
        PrefService.getBool(shared_preferences_key_has_imported_favorites);
    if (hasImportedFavorites == null || !hasImportedFavorites) {
      PrefService.setBool(shared_preferences_key_has_imported_favorites, true);
      await BlocProvider.master<FavoritesBloc>().importFavorites();
    }
  }

  @override
  void dispose() {
    _favoritesController.close();
    _queueRemovedFavoritesController.close();
    _selectedFavoriteItemController.close();
    _favoritesInSearchController.close();
    _reorderController.close();
    onDbFavoritesChanged.cancel();
  }

  StreamSubscription<dynamic> onSelectedFavoriteItemListener(
          List<ExpandableController> controllers) =>
      onSelectedFavoriteItemChanged.listen((data) {
        if (data != null && controllers != null && controllers.isNotEmpty) {
          ExpandableController selectedLineStopController = controllers[data];
          int indexLastSelected = lastSelectedFavoriteItem;
          ExpandableController lastSelectedLineStopController =
              indexLastSelected == null ? null : controllers[indexLastSelected];
          if (selectedLineStopController != null) {
            if (lastSelectedLineStopController != null) {
              if (selectedLineStopController == lastSelectedLineStopController)
                selectedLineStopController.expanded = true;
              else
                lastSelectedLineStopController.expanded = false;
            }
          }
        }
      });

  Future addFavoriteCluster(BuildContext ctx, String longitude, String latitude,
      dynamic data, Function(bool) update) async {
    await BlocProvider.master<MapBloc>()
        .addLinesToFavorites(ctx, longitude, latitude, data, update);
  }

  Future<void> removeFavorite(dynamic data) async {
    if (data == null)
      return Future.error(Exception('data should not be null.'));

    return database.favoriteDao.deleteFavorite(data);
  }

  Future<void> removeFavoriteItinerary(dynamic data) async {
    if (data == null)
      return Future.error(Exception('data should not be null.'));

    return database.favoriteDao.deleteFavorite(data);
  }

  Future<void> removeFavoriteCluster(Point data) async {
    if (data == null)
      return Future.error(Exception('data should not be null.'));

    if (await isFavorite(data))
      return database.favoriteDao.deleteFavoriteCluster(data);
    else
      return;
  }

  Future<void> addIfNotFavorite(dynamic data) async {
    if (data != null) {
      if (!await isFavorite(data)) await database.favoriteDao.addFavorite(data);
    }
  }

  Future<void> deleteAndRecreate(dynamic data) async {
    if (data != null) {
      await database.favoriteDao.deleteFavorite(data);
      await database.favoriteDao.addFavorite(data);
    }
  }

  /// Check if the passed [data] is within the favorites.
  Future<bool> isFavorite(dynamic data) async {
    if (data == null) return false;

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;
    List<Point> listPoints = [];
    if (listFavs != null) {
      for (Favorite favs in listFavs) {
        listPoints.add(favs.point1);
      }
    }

    if (listPoints.contains(data)) {
      return true;
    } else {
      return false;
    }

  }

  /// Check if the passed [data] is within the favorites.
  Future<bool> isFavoriteCluster(Point point) async {
    if (point == null) return false;

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;
    List<Point> listPoints = [];
    if (listFavs != null) {
      for (Favorite favs in listFavs) {
        listPoints.add(favs.point1);
      }
    }

    if (listPoints.contains(point)) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> isFavoriteClusterLines(Point point, String line) async {
    if (point == null) return false;

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;
    List<String> listPoints = [];
    if (listFavs != null) {
      for (Favorite favs in listFavs) {
        if (favs.point1 == point){
          listPoints.add(favs.lines[0]);
        }
      }
    }

    if (listPoints.contains(line)){
      return true;
    } else{
      return false;
    }
  }

  Future<void> deleteFavoriteClusterLines(Point point, String line) async {
    if (point == null) return false;

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;

    if (listFavs != null) {
      for (Favorite favs in listFavs) {
        if (favs.lines[0] == line){
          return database.favoriteDao.deleteFavorite(favs);
        }
      }
    }
  }

  Future<dynamic> checkFavoriteCluster(Point point) async {
    if (point == null) return false;

    List<Favorite> listFavs = BlocProvider.master<FavoritesBloc>().favorites;
    List<Favorite> oldfavs = [];
    if (listFavs != null) {
      for (Favorite favs in listFavs) {
        if (favs.point1 == point){
          oldfavs.add(favs);
        }
      }

      if (oldfavs.isNotEmpty){
        return oldfavs;
      }else{
        return point;
      }

    }
  }

  /// Check if the passed [data] is within the favorites.
  Future<bool> isFavoriteItinerary(dynamic data) async {
    bool isFavorite = await database.favoriteDao.isFavorite(data);
    // If no favorite is found, return false;
    return isFavorite ?? true;
  }

  /// Remove all favorites.
  Future<void> deleteAllFavorites() {
    return database.favoriteDao.deleteAllFavorites();
  }

  Future<void> addFavorite(dynamic data) async {
    if (data == null)
      return Future.error(Exception('data should not be null.'));
    return database.favoriteDao.addFavorite(data);
  }

  void requestReorderFavorites() {
    if (isReordering) return;

    _favoritesBeforeReorder = List.from(favorites);
    _reorderController.sink.add(true);
  }

  void reorderFavorites(int oldPosition, int newPosition) {
    List<Favorite> newFavs = favorites;
    Favorite selectedFav = newFavs[oldPosition];
    newFavs.removeAt(oldPosition);
    newFavs.insert(
        oldPosition < newPosition ? newPosition - 1 : newPosition, selectedFav);
    _favoritesController.sink.add(newFavs);
  }

  Future validateReorderFavorites() async {
    _reorderController.sink.add(false);

    List<Favorite> favs = List.from(favorites);

    List<Favorite> newFavs = [];

    // update each favorite in db
    int newIndex = 0;
    for (Favorite fav in favs) {
      if (fav.details.position != newIndex) {
        await database.favoriteDao.updateFavoritePosition(fav, newIndex);
        newFavs.add(Favorite.copyWithPos(fav, newIndex));
      } else
        newFavs.add(fav);
      newIndex++;
    }
    _favoritesController.sink.add(newFavs);
  }

  void cancelReorderFavorites() {
    _reorderController.sink.add(false);
    _favoritesController.sink.add(_favoritesBeforeReorder);
  }

  Future<void> importFavorites() async {
    PointsBloc pointsBloc = BlocProvider.master<PointsBloc>();
    List<dynamic> importedFavorites = List();
    List<Point> cameras;

    void addToImportedFavoritesIfNotAlreadyIn(dynamic data) {
      if (!importedFavorites.contains(data)) importedFavorites.add(data);
    }

    Future<void> importParking(Map<String, dynamic> item,
        {bool fromPoi = false, PropertiesType type}) async {
      Point point;
      // Splits P+R from the name. If it is there, length = 2 else length = 1 and set the boolean accordingly to request the good type.
      String key = fromPoi ? 'text' : 'name';
      List<String> splits = (item[key] as String)?.split('P+R');
      if (splits == null) return;
      String name = splits[splits.length - 1];
      if (type == null) {
        bool par = splits.length == 2;
        type = par ? PropertiesType.par : PropertiesType.pkg;
      }

      List<Point> results = await pointsBloc.requestPointsWithQuery(type, name);
      if (results == null || results.length == 0) {
        // Cant get the Point : cancel the import of this favorite
        return;
      } else if (results.length == 1) {
        // Take the only result
        point = results.first;
      } else if (results.length > 1) {
        // Take the first result that correspond to the name or the first result
        point = results.firstWhere(
            (p) => p.properties.name == item[key] as String,
            orElse: () => null);
      }

      if (point != null) addToImportedFavoritesIfNotAlreadyIn(point);
    }

    Future<void> importItinerary(Map<String, dynamic> item) async {
      Point departure, destination, temp;
      double longitude, latitude;
      String name;
      String whichPoint;
      for (int i = 0; i < 2; i++) {
        temp = null;
        if (i == 0)
          whichPoint = 'departure';
        else {
          whichPoint = 'arrival';
          if (departure == null)
            // No need to continue, we couldnt get the departure
            return;
        }

        name = item[whichPoint]['address'] as String;
        longitude = item[whichPoint]['longitude'].toDouble();
        latitude = item[whichPoint]['latitude'].toDouble();

        if (name == null || longitude == null || latitude == null) return;

        List<Point> results = await pointsBloc.requestPointsInZoneForImport(
            longitude, longitude, latitude, latitude, bboxPointTypes);
        if (results == null || results.length == 0) {
          // Cant get the Point : cancel the import of this favorite
          return;
        } else if (results.length == 1) {
          // Take the only result
          temp = results.first;
        } else if (results.length > 1) {
          // Take the first result that correspond to the name or the first result
          temp = results.firstWhere((p) => p.properties.name == name,
              orElse: () => null);
        }
        if (i == 0)
          departure = temp;
        else
          destination = temp;
      }

      if (departure != null && destination != null) {
        // we create the favorite
        addToImportedFavoritesIfNotAlreadyIn(FavoriteItinerary(
            departure: departure,
            destination: destination,
            modes: ModeOfTransport(
                favorite: null,
                bike: false,
                car: false,
                carPooling: false,
                carSharing: false,
                publicTransports: true,
                disabledPerson: false)));
      }
    }

    Future<void> importStop(Map<String, dynamic> item,
        {bool fromPoi = false}) async {
      Point point;
      String key = fromPoi ? 'text' : 'name';
      String name = item[key] as String;
      String code = item['idStop'] as String;

      if (name == null) return;

      name = removeCityFromName(name);

      List<Point> results = await pointsBloc.requestClustersWithQuery(name);
      if (results == null || results.length == 0) {
        // Cant get the Point : cancel the import of this favorite
        return;
      } else if (results.length == 1) {
        // Take the only result
        point = results.first;
      } else if (results.length > 1) {
        // Take the first result that correspond to the name or the first result
        if (code != null)
          point = results.firstWhere(
              (p) => (p.properties as ClustersProperties).code == code,
              orElse: () => null);
      }

      if (point != null) {
        List<Line> lines = await BlocProvider.master<MapBloc>()
            .getLinesFromStop(point.geometry.coordinates[0].toString(),
                point.geometry.coordinates[1].toString(), (point.properties as ClustersProperties).code);
        lines.forEach((line) {
          List<Line> test = [];
          test.add(line);
          FavoriteCluster favoriteCluster =
              FavoriteCluster(stop: point, lines: test);
          addToImportedFavoritesIfNotAlreadyIn(favoriteCluster);
        });
      }
    }

    Future<void> importCam(Map<String, dynamic> item) async {
      if (cameras != null) {
        String name = item['LIBELLE'];
        Point camera = cameras.firstWhere((c) => c.properties.name == name,
            orElse: () => null);
        if (camera != null) addToImportedFavoritesIfNotAlreadyIn(camera);
      }
    }

    Future<void> importPoi(Map<String, dynamic> item) async {
      Point point;
      List<String> splits = (item['imagesource'] as String)?.split('.');
      String name = item['text'] as String;
      if (splits == null) return;

      // Take the second to last with contains the name of the image source to interpolate the type.
      String imageType = splits[splits.length - 2];
      if (imageType == null) return;

      PropertiesType type;
      if (imageType.startsWith('autostop'))
        type = PropertiesType.autostop;
      else if (imageType.startsWith('ic_agence_metro'))
        type = PropertiesType.agenceM;
      else if (imageType.startsWith('cam'))
        type = PropertiesType.cam;
      else if (imageType.startsWith('p_citelib'))
        type = PropertiesType.citiz;
      else if (imageType.startsWith('p_MV_Agence'))
        type = PropertiesType.mva;
      else if (imageType.startsWith('p_MV_Consigne'))
        type = PropertiesType.mvc;
      else if (imageType.startsWith('p_PKG'))
        type = PropertiesType.pkg;
      else if (imageType.startsWith('p_pointservice'))
        type = PropertiesType.pointService;
      else if (imageType.startsWith('p_PR'))
        type = PropertiesType.par;
      else if (imageType.startsWith('p_relaisTAG'))
        type = PropertiesType.depositaire;
      else if (imageType.startsWith('pictorecharge'))
        type = PropertiesType.recharge;
      else if (imageType.startsWith('pointArret') ||
          imageType.startsWith('zone_arret')) type = PropertiesType.clusters;

      if (type == null) {
        debugPrint('Type $imageType not handled for import.');
        return;
      }

      if (type == PropertiesType.par ||
          type == PropertiesType.pkg ||
          type == PropertiesType.mvc) {
        await importParking(item, fromPoi: true, type: type);
        return;
      }

      if (type == PropertiesType.clusters) {
        await importStop(
          item,
          fromPoi: true,
        );
        return;
      }

      List<Point> results = await pointsBloc.requestPointsWithQuery(type, name);
      if (results == null || results.length == 0) {
        // Cant get the Point : cancel the import of this favorite
        return;
      } else if (results.length == 1) {
        // Take the only result
        point = results.first;
      } else if (results.length > 1) {
        // Take the first result that correspond to the name or the first result
        point =
            results.firstWhere((p) => p.properties.name == name, orElse: null);
      }
      if (point != null) addToImportedFavoritesIfNotAlreadyIn(point);
    }

    Future<bool> importFavorites(String path) async {
      try {
        File file = File(path);
        String favorites = await file.readAsString();
        debugPrint(favorites, wrapWidth: 200);
        var parsed = json.decode(favorites);
        // Download all cameras
        cameras = await pointsBloc.requestCam();

        for (final item in parsed['favorites'] as List) {
          int type = item['type'] as int;
          switch (type) {
            case 0:
              // Favorite of itinerary
              await importItinerary(item);
              break;
            case 1:
              // Favorite of stop
              await importStop(item);
              break;
            case 2:
              // Favorite of webcam
              await importCam(item);
              break;
            case 3:
              // Favorite of parking
              await importParking(item);
              break;
            case 4:
              // Favorite of poi
              await importPoi(item);
              break;
          }
        }

        // Add to favorite
        for (final fav in importedFavorites) addIfNotFavorite(fav);
        return true;
      } catch (_) {
        debugPrint('Issues while importing favorites');
        return false;
      }
    }

    Future<void> importFavoritesAndroid() async {
      List<StorageInfo> storagesInfo = await PathProviderEx.getStorageInfo();
      for (StorageInfo storage in storagesInfo) {
        String path = '${storage.rootDir}/$importAndroidDirectyFavoritesName';
        bool done = await importFavorites(path);
        if (done) break;
      }
    }

    Future<void> importFavoritesIos() async {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      String appDocPath = appDocDir.path;
      String path = '$appDocPath/$importFavoritesName';

      await importFavorites(path);
    }

    try {
      if (Platform.isAndroid) {
        Map<PermissionGroup, PermissionStatus> permissions =
            await PermissionHandler()
                .requestPermissions([PermissionGroup.storage]);
        if (permissions != null &&
            permissions.containsKey(PermissionGroup.storage)) {
          if (permissions[PermissionGroup.storage] ==
              PermissionStatus.granted) {
            await importFavoritesAndroid();
          } else
            // TODO: show messsage maybe
            debugPrint('Permission denied while importing favorites');
          return;
        } else
          debugPrint('Issues while importing favorites');
      } else if (Platform.isIOS) {
        await importFavoritesIos();
      }
    } catch (_) {
      debugPrint("Can't import favorites");
    }
  }

  void onTappedFavoriteItem(int index) {
    if (index == selectedFavoriteItem) return;
    lastSelectedFavoriteItem = selectedFavoriteItem;
    _selectedFavoriteItemController.sink.add(index);
  }

  void resetSelectedLineStop() {
    _selectedFavoriteItemController.sink.add(null);
    lastSelectedFavoriteItem = null;
  }

  void requestSearchFavorites({bool limited = false}) async {
    _favoritesInSearchController.sink.add(null);
    List<Point> points = await database.favoriteDao.searchFavorites(limited);
    _favoritesInSearchController.sink.add(points);
  }

  void addToQueue(Favorite fav) {
    queueRemovedFavorites.add(fav);
    _queueRemovedFavoritesController.sink.add(queueRemovedFavorites);
  }

  Future<void> removeFavoriteFromQueue({bool delete = false}) async {
    if (delete) {
      Favorite favoriteToDelete = queueRemovedFavorites.first;
      if (favoriteToDelete == null) throw Exception('error favorite null');

      await BlocProvider.master<FavoritesBloc>()
          .removeFavorite(favoriteToDelete);
    } else {
      _queueRemovedFavoritesController.sink
          .add(queueRemovedFavorites..removeFirst());
    }
  }

  Future<void> modifyOldFavorites(BuildContext context) async {
    List<String> oldName = ["1000", "1010", "1020", "1230", "1040", "1050", "1060", "1410", "1450", "1980", "1981", "1982", "1150",
      "1130", "1140", "2070", "2080", "2090", "2900", "2960", "2990", "7000", "7010", "7110", "7500", "1380", "7300", "7330",
      "7350", "7360", "2180", "2610", "2700", "5000", "5020", "5200", "5250", "5100", "5110", "5120", "5130", "3000", "3010",
      "3020", "3030", "3040", "3011", "6020", "6070", "6030", "6060", "6080", "6550", "6200", "6010", "6051", "6052", "4100",
      "4101", "4110", "4310", "4600", "4500", "EXP1", "EXP2", "EXP3", "EXP4", "EXP5", "EXP6", "EXP7", "7320" ];

    List<String> newName = ["T10", "T11", "T12", "T13", "T14", "T15", "T16", "T17", "T18", "T20", "T21", "T22", "T23", "T30", "T31",
    "T32", "T33", "T34", "T35", "T36", "T37", "T40", "T41", "T42", "T44", "T43", "T50", "T51", "T55", "T56", "T52", "T53", "T54",
    "T60", "T61", "T62", "T63", "T64", "T65", "T66", "T67", "T75", "T71", "T76", "T73", "T77", "T70", "T80", "T81", "T82", "T83", "T84",
      "T85", "T86", "T87", "T88", "T89", "T90", "T91", "T92", "T93", "T94", "T95", "X01", "X02", "X03", "X04", "X05", "X06", "X07", "X08"];

    List<Favorite> favoritesList = favorites;

    if (favoritesList != null && favoritesList.isNotEmpty)
      for (Favorite fav in favoritesList) {
        for (String old in oldName) {
          if (old.contains(fav.lines[0])) {
            int i = oldName.indexOf(old);
            List<Point> tempPoints = await BlocProvider.master<PointsBloc>()
                .getPointsListWithIdLine('C38:' + newName[i]);
            for (Point p in tempPoints) {
              if (fav.point1.properties.name == p.properties.name) {
                List<Line> lines = await BlocProvider.master<MapBloc>()
                    .getLinesFromStop(
                        p.geometry.coordinates[0].toString(),
                        p.geometry.coordinates[1].toString(),
                        (p.properties as ClustersProperties).code);
                for (Line l in lines) {
                  if (l.shortName == newName[i]) {
                    List<Line> tempL = [];
                    tempL.add(l);
                    FavoriteCluster favoriteCluster =
                        FavoriteCluster(stop: p, lines: tempL);
                    await removeFavorite(fav);
                    await addFavorite(favoriteCluster);
                  }
                }
              }
            }
          }
        }
        PrefService.setBool('modify_Old_Fav', false);
      }
  }

  static const String shared_preferences_key_has_imported_favorites =
      'has_imported_favorites';
  static const String importFavoritesName = 'Favoris_Metromobilité.txt';
  static const String importAndroidDirectyFavoritesName =
      'Documents/$importFavoritesName';
  static const String bboxPointTypes =
      'clusters,lieux,rue,PKG,PAR,MVC,MVA,agenceM,pointService';
}
