extension CompareTime on DateTime {
  bool isSameDay(DateTime other) {
    return (this.year == other.year &&
        this.month == other.month &&
        this.day == other.day);
  }

  bool isBeforeSameDay(DateTime other) {
    if (!this.isSameDay(other)) return false;
    return this.isBefore(other);
  }
}
