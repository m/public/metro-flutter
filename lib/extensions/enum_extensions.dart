import 'package:metro/filters/models/filter.dart';

extension TravelModeModifier on TravelMode {
  String get string => this.toString().split('.').last;
}
