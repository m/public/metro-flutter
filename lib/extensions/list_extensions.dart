import 'package:metro/favorites/models/favorites.dart';

extension ContainsFav on List<Favorite> {
  bool hasFavorite(String favName, String stopName) {
    for (Favorite fav in this) {
      if (fav.lines.isNotEmpty &&
          favName == fav.lines.first &&
          fav.point1.properties.name == stopName) return true;
    }
    return false;
  }

  Favorite getFavorite(String favName, String stopName) {
    for (Favorite fav in this) {
      if (fav.lines.isNotEmpty &&
          favName == fav.lines.first &&
          fav.point1.properties.name == stopName) return fav;
    }
    return null;
  }
}
