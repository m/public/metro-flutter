class IconDataModel {
  String id;
  String gtfsId;
  String shortName;
  String longName;
  String color;
  String textColor;
  String mode;
  String type;

  IconDataModel({this.id, this.gtfsId, this.shortName, this.longName, this.color,
      this.textColor, this.mode, this.type});

  IconDataModel.fromStandard(this.id, this.gtfsId, this.shortName, this.longName, this.color,
      this.textColor, this.mode, this.type);

  factory IconDataModel.fromJson(Map<dynamic, dynamic> json) {
    return new IconDataModel(
      id: json['id'] as String,
      gtfsId: json['gtfsId'] as String,
      shortName: json['shortName'] as String,
      longName: json['longName'] as String,
      color: json['color'] as String,
      textColor: json['textColor'] as String,
      mode: json['mode'] as String,
      type: json['type'] as String,
    );
  }
}
