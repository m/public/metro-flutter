class LoginDataModel {
  String token;
  UserModel user;

  LoginDataModel.fromJson(Map<String, dynamic> parsedJson) {
    token = parsedJson['token'];
    user = UserModel(parsedJson['user']);
  }
}

class UserModel {
  String identifiant;
  int ident;
  String datecreation;
  String debutvalidite;
  bool success;
  String role;

  UserModel(userModel) {
    identifiant = userModel['identifiant'];
    ident = userModel['ident'];
    datecreation = userModel['datecreation'];
    debutvalidite = userModel['debutvalidite'];
    success = userModel['success'];
    role = userModel['role'];
  }
}