import 'package:metro/global/elevation.dart';
import 'package:intl/intl.dart' show DateFormat;

class EventModel {
  String key;
  String type;
  int id;
  String dateDebut;
  String dateFin;
  String heureDebut;
  String heureFin;
  double latitude;
  double longitude;
  String weekEnd;
  String listeLigneArret;
  String texte;
  String plan;
  bool visibleTC;
  bool visibleVoiture;
  bool visibleVelo;
  bool visibleVenteTitres;
  bool visibleBandeauSite;
  bool visibleBandeauAppli;

  // Front data
  List<String> splitText;
  List<String> splitTextHeader;

  bool isSEM;
  bool hasIcon;
  String iconColor;
  String iconTextColor;
  String iconText;
  String gtfsId;
  int nsv_id;

  EventModel({this.key, this.type, this.id, this.dateDebut, this.dateFin, this.heureDebut,
      this.heureFin, this.latitude, this.longitude, this.weekEnd,
      this.listeLigneArret, this.texte, this.plan, this.visibleTC,
      this.visibleVoiture, this.visibleVelo, this.visibleVenteTitres,
      this.visibleBandeauSite, this.visibleBandeauAppli, this.nsv_id});

  factory EventModel.fromJson(Map<dynamic, dynamic> json, String key) {

    int nsv = 0;
    if (json['nsv_id'] is int){
      nsv = json['nsv_id'];
    }

    return new EventModel(
      key: key,
      type: json['type'] as String,
      id: json['id'] as int,
      dateDebut: json['dateDebut'] as String,
      dateFin: json['dateFin'] as String,
      heureDebut: json['heureDebut'] as String,
      heureFin: json['heureFin'] as String,
      latitude: double.tryParse(json['latitude'].toString()) ?? -1,
      longitude: double.tryParse(json['longitude'].toString()) ?? -1,
      weekEnd: json['heureFin'] as String,
      listeLigneArret: json['listeLigneArret'] as String ?? '',
      texte: json['texte'] as String,
      plan: json['plan'] as String,
      visibleTC: json['visibleTC'],
      visibleVoiture: json['visibleVoiture'],
      visibleVelo: json['visibleVelo'],
      visibleVenteTitres: json['visibleVenteTitres'],
      visibleBandeauSite: json['visibleBandeauSite'],
      visibleBandeauAppli: json['visibleBandeauAppli'],
      nsv_id: nsv,
    );

    // else return null;
  }

  String markerPath() {
    // TODO : gerer les types manquant
    switch(type) {
      case 'accident':
        return globalElevations.assetsPath + 'p_accident.png';
      case 'bouchon':
        return globalElevations.assetsPath + 'p_bouchon.png';
      case 'chantier':
        return globalElevations.assetsPath + 'p_chantier.png';
      case 'greve':
        return globalElevations.assetsPath + 'p_greve.png';
      case 'manifestation':
        return globalElevations.assetsPath + 'p_manifestation.png';
      case 'meteo':
        return globalElevations.assetsPath + 'p_meteo.png';
      case 'obstacle':
        return globalElevations.assetsPath + 'p_obstacle.png';
      case 'restriction':
      //case 'restriction_ltc':
        return globalElevations.assetsPath + 'p_restriction.png';
      case 'info':
        return globalElevations.assetsPath + 'p_info.png';
      case 'panne':
        return globalElevations.assetsPath + 'p_panne.png';
      case 'plans_d_urgence':
        return globalElevations.assetsPath + 'p_plans_d_urgence.png';
      default:
        return globalElevations.assetsPath + 'p_accident.png';
    }
  }
}