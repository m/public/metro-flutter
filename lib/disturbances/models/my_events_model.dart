class MyEventsModel {
  List<TrajetModel> trajets = [];

  MyEventsModel.fromTargets() {
    List<TrajetModel> temp = [];
    trajets = temp;
  }

  MyEventsModel.fromJson(Map<String, dynamic> parsedJson) {

    List<TrajetModel> temp = [];
    List tajetList = parsedJson['data']['trajets'];

    if (tajetList != null)
    for(var obj in tajetList) {
      temp.add(TrajetModel(obj));
    }
    trajets = temp;
  }
}

class TrajetModel {
  String intitule;
  List<TronconModel> troncons;
  List<PeriodModel> periods;

  TrajetModel(trajet) {
    intitule = trajet['intitule'];
    List<TronconModel> tempTroncons = [];
    List<PeriodModel> tempPeriods = [];

    for(var obj in (trajet['troncons'] as List)) {
      tempTroncons.add(TronconModel(obj));
    }
    troncons = tempTroncons;

    for(var obj in (trajet['periods'] as List)) {
      tempPeriods.add(PeriodModel(obj));
    }
    periods = tempPeriods;
  }
}

class TronconModel {
  String arretDepart;
  String arretDestination;
  String ligne;

  TronconModel(troncon) {
    arretDepart = troncon['arretDepart'];
    arretDestination = troncon['arretDestination'];
    ligne = troncon['ligne'];
  }
}

class PeriodModel {
  int heureDepartPeriode;
  int heureFinPeriode;

  PeriodModel(period) {
    heureDepartPeriode = period['heureDepartPeriode'];
    heureFinPeriode = period['heureFinPeriode'];
  }
}