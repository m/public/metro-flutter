import 'dart:io';
import 'package:expandable/expandable.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/disturbances/pages/details_disturbances.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/widgets/new_expandable_panel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/pdf_screen.dart';
import 'package:metro/localization/translations.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DisturbancesTile extends StatefulWidget {
  final EventModel event;
  final String eventKey;
  final ExpandableController controller;

  DisturbancesTile(this.event, this.eventKey, this.controller) : assert(controller != null);

  @override
  State<StatefulWidget> createState() => _DisturbancesTileState();
}

class _DisturbancesTileState extends State<DisturbancesTile> with AutomaticKeepAliveClientMixin {
  EventsBloc _eventsBloc;

  Future<void> _pdfLoader(fileUrl, String title) async {
    _eventsBloc.managePDFLoading(true);
    final filename = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(fileUrl));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File('$dir/$filename');
    await file.writeAsBytes(bytes);
    _eventsBloc.managePDFLoading(false);
    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFScreen(file.path, title, bytes)));
  }

  @override
  void initState() {
    super.initState();
    _eventsBloc = BlocProvider.of<EventsBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    double height = 46;
    double width = height;
    double cornerRadius = 6;
    if (widget.event.listeLigneArret != null) {
      if (widget.event.listeLigneArret.contains('SEM')) cornerRadius = height / 2;
      if (!widget.event.listeLigneArret.contains('SEM')) width = height * 2;
    }

    int nsv = widget.event.nsv_id;
    String asset = nsv == 4
        ? globalElevations.perturbation_1
        : (nsv == 2 || nsv == 3)
            ? globalElevations.perturbation_2
            : '';

    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailsDisturbancesPage(widget.event),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 6.0),
        child: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: globalElevations.e01dp.backgroundColor,
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black26,
                blurRadius: 2.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          child: Row(
            children: [
              (widget.event.hasIcon != null && widget.event.hasIcon == true)
                  ? Stack(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: 16, left: 4),
                          child: Container(
                            height: height,
                            width: width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(cornerRadius),
                                color: (widget.event.iconColor == ''
                                        ? Theme.of(context).colorScheme.primary
                                        : Utils.colorFromHex(widget.event.iconColor)) ??
                                    Colors.transparent,
                                boxShadow: <BoxShadow>[BoxShadow(color: Colors.black45, offset: Offset(0, 3), blurRadius: 6)]),
                            child: Center(
                              child: Text(
                                widget.event.iconText ?? '',
                                style: TextStyle(
                                  color: (widget.event.iconColor == '' ? Colors.black : Utils.colorFromHex(widget.event.iconTextColor)) ??
                                      Colors.transparent,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: (nsv == 2 || nsv == 3 || nsv == 4),
                          child: Positioned(
                            bottom: 30,
                            left: 38,
                            child: SizedBox(
                              height: 16,
                              width: 16,
                              child: SvgPicture.asset(
                                asset,
                                height: 16,
                                semanticsLabel: 'icon',
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  : (!widget.event.visibleBandeauAppli)
                      ? Padding(
                          padding: EdgeInsets.only(right: 10, left: 8),
                          child: Container(
                            alignment: Alignment.center,
                            width: 36,
                            height: 36,
                            child: Image.asset('assets/sacado-icon.png'),
                          ),
                        )
                      : Padding(
                          padding: EdgeInsets.only(left: 16, right: 28),
                          child: Container(
                            alignment: Alignment.center,
                            child: Text(
                              "!",
                              style: TextStyle(fontSize: 34, fontWeight: FontWeight.bold, color: Color(0xFFf96c6c)),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      (widget.event.splitTextHeader.length < 2
                              ? widget.event.splitTextHeader[0]
                              : widget.event.splitTextHeader[1].substring(1)[0].toUpperCase() + widget.event.splitTextHeader[1].substring(2)) ??
                          "",
                      style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
                    ),
                    !widget.event.dateFin.contains("2050")
                        ? Text(
                            "${translations.text('disturbances.all.until')} ${(widget.event.dateFin ?? '')}",
                            style: TextStyle(color: Colors.grey, fontSize: 14),
                          )
                        : Container(),
                  ],
                ),
              ),
              Icon(
                Icons.navigate_next,
                color: globalElevations.unSelectedIconColor.backgroundColor,
                size: 30,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
