import 'dart:async';

import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/disturbances/pages/disturbances_all.dart';
import 'package:metro/disturbances/pages/disturbances_important.dart';
import 'package:metro/disturbances/pages/disturbances_myalerts.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/disturbances/pages/reporting.dart';

class DisturbancesPage extends StatefulWidget {
  @override
  DisturbancesPageState createState() => DisturbancesPageState();

  final int chosenIndex;
  final bool importantEvent;
  final String eventKey;

  DisturbancesPage(this.chosenIndex, {this.importantEvent = false, this.eventKey = "00"});
}

class DisturbancesPageState extends State<DisturbancesPage> with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(
      icon: Icon(Icons.warning_amber_rounded),
      text: "IMPORTANT",
    ),
    Tab(
      icon: Icon(Icons.directions_bus),
      text: "BUS/TRAM",
    ),
    Tab(
      icon: Icon(Icons.alarm),
      text: translations.text('disturbances.ALERTS'),
    ),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: myTabs.length,
      vsync: this,
    );
    BlocProvider.of<EventsBloc>(context).fetchAllEvents();
    setState(() {
      _tabController.animateTo(widget.chosenIndex);
    });
    BlocProvider.of<MatomoBloc>(context).trackScreen(MatomoBloc.screen_disturbances_my_alerts, 'test');
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget buttonAdd() {
    return GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ReportingPage())),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 10.0, 10.0, 10.0),
        child: Container(
          decoration: BoxDecoration(
            color: globalElevations.blueColor.backgroundColor,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Text(
                "SIGNALER",
                style: TextStyle(color: globalElevations.inverseColor.backgroundColor, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [buttonAdd()],
        backgroundColor: globalElevations.e16dp.backgroundColor,
        iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
        title: Text(
          translations.text('drawer.perturbations_titre'),
          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
        bottom: TabBar(
          controller: _tabController,
          labelColor: globalElevations.fstTextColor.backgroundColor,
          tabs: myTabs,
          onTap: (index) async {
            String screenName = index == 0 ? MatomoBloc.screen_disturbances_my_alerts : MatomoBloc.screen_disturbances_all;
            BlocProvider.of<MatomoBloc>(context).trackScreen(screenName, 'test');
          },
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          ImportantPage(
            importantEvent: widget.importantEvent,
            eventKey: widget.eventKey,
          ),
          AllPage(
            importantEvent: widget.importantEvent,
            eventKey: widget.eventKey,
          ),
          MyAlertsPage()
        ],
      ),
    );
  }
}
