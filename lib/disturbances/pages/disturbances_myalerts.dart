import 'package:flutter/material.dart';
import 'package:metro/disturbances/models/my_events_model.dart';
import 'package:metro/disturbances/pages/disturbances.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/disturbances/models/login_data_model.dart';

import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as ss;
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MyAlertsPage extends StatefulWidget {
  @override
  _MyAlertsPageState createState() => _MyAlertsPageState();
}

class _MyAlertsPageState extends State<MyAlertsPage> {
  bool _connected = false;
  String _username = '';
  String _token = '';

  String _userLogin = '';
  String _userPassword = '';
  final storage = new ss.FlutterSecureStorage();
  int callNumber = 0;

  @override
  void initState() {
    super.initState();

    // Initialisation of variable during page build

    //BlocProvider.master<EventsBloc>().loadMyEvents();

    List<Future<String>> list;

    _loginStateCheck();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globalElevations.e02dp.backgroundColor,
      body: _connected == null || false ? _buildView() : _buildView(),
    );
  }

  _loginStateCheck() async {
    // Si le token ou le nom d'utilisateur ne sont pas dans le sécure storage,
    // on affiche la vue de connexion, sinon on affiche les notifications
    final storage = new ss.FlutterSecureStorage();
    _token = await storage.read(key: 'token');
    _username = await storage.read(key: 'userLogin');
    if (mounted) {
      // We want to call only once the method fetchMyEvents
      if (callNumber < 1) {
        setState(() {
          if (_token != null && _username != null) {
            BlocProvider.of<EventsBloc>(context).updateSmartphoneId();
            _connected = true;
          } else {
            _token = '';
            _username = '';
            _connected = false;
          }
        });
        callNumber++;
      } else {
        _token = '';
        _username = '';
        _connected = false;
      }
    }
    callNumber = 0;
  }

  Widget _buildLoadingView() {
    return Center(
      child: AnimatedLoadingLogo(),
    );
  }

  Widget _buildView() {
    return Column(
      children: <Widget>[
        Visibility(
          visible: _connected,
          child: Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      _username,
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    RaisedButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(24),
                      ),
                      onPressed: () => _logout(),
                      color: Colors.white,
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                Divider(color: Colors.white),
                StreamBuilder(
                  stream: BlocProvider.of<EventsBloc>(context).onMyEventsChanged,
                  builder: (context, AsyncSnapshot<MyEventsModel> snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data.trajets == [] ||
                          snapshot.data.trajets == null) {
                        return Expanded(
                          child: Center(
                              child: Text(
                                  translations
                                      .text('disturbances.alerts.no_alerts'),
                                  style: TextStyle(color: Colors.white))),
                        );
                      } else {
                        return Flexible(
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 5),
                              Padding(
                                padding: const EdgeInsets.only(left : 8.0),
                                child: Text(translations.text(
                                    'disturbances.alerts.notifications_activated')),
                              ),
                              SizedBox(height: 5),
                              Divider(color: Colors.white),
                              SizedBox(height: 5),
                              snapshot.data.trajets.isNotEmpty ? _buildList(snapshot.data) :
                              Text("Aucun trajet n'a été enregistré."),
                            ],
                          ),
                        );
                      }
                    } else if (snapshot.hasError) {
                      return Center(child: Text(snapshot.error.toString()));
                    }
                    return Center(child: CircularProgressIndicator());
                  },
                ),
              ],
            ),
          ),
        ),
        Visibility(
          visible: !_connected,
          child: Flexible(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 16, 20, 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 8, bottom: 26),
                      child: Text(
                        translations.text('disturbances.alerts.header'),
                        style: TextStyle(height: 1.5),
                      ),
                    ),
                    Text(
                      translations.text('disturbances.alerts.login'),
                      style: TextStyle(color: Colors.grey),
                    ),
                    TextFormField(
                      onChanged: (text) {
                        _userLogin = text;
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 8),
                        hintText: translations
                            .text('disturbances.alerts.login_placeholder'),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      translations.text('disturbances.alerts.password'),
                      style: TextStyle(color: Colors.grey),
                    ),
                    TextFormField(
                      obscureText: true,
                      onChanged: (text) {
                        _userPassword = text;
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 8),
                        hintText: translations
                            .text('disturbances.alerts.password_placeholder'),
                      ),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        onPressed: _log,
                        color: Theme.of(context).colorScheme.primary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Text(
                          translations
                              .text('disturbances.alerts.connect')
                              .toUpperCase(),
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        onPressed: () =>
                            launch('https://www.metromobilite.fr/siv.html'),
                        color: globalElevations.e02dp.backgroundColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                          side: BorderSide(
                              color: Theme.of(context).colorScheme.primary,
                              width: 2),
                        ),
                        child: Text(
                          translations
                              .text('disturbances.alerts.create_account')
                              .toUpperCase(),
                          style: TextStyle(
                              fontSize: 20,
                              color: Theme.of(context).colorScheme.primary),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildList(MyEventsModel dataList) {
    return Flexible(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: dataList.trajets.length,
        itemBuilder: (BuildContext context, int index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Text(dataList.trajets[index].intitule),
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 15,
                  ),
                  Text(dataList.trajets[index].troncons[0].ligne),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(dataList.trajets[index].troncons[0].arretDepart),
                      Text(dataList.trajets[index].troncons[0].arretDestination),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Text(translations.text('disturbances.alerts.from') +
                  ' ' +
                  dataList.trajets[index].periods[0].heureDepartPeriode
                      .toString() +
                  'h ' +
                  translations.text('disturbances.alerts.to') +
                  ' ' +
                  dataList.trajets[index].periods[0].heureFinPeriode
                      .toString() +
                  'h'),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: Divider(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<LoginDataModel> _login(username, password) async {
    Dio dio = new Dio();
    Response response;
    try {
      response = await dio.post(
          BlocProvider.master<WebServices>().getSivHost() + 'login',
          data: {"login": _userLogin, "password": _userPassword},
          options: Options(contentType: 'application/json', headers: {
            Headers.acceptHeader: 'application/json',
            Headers.contentTypeHeader: 'application/json',
          }));
    } on DioError catch (error) {
      _loginErr(!error.message.contains('403'));
    }

    if (response.statusCode == 200) {
      if (response.data['status'] == 400) {
        _loginErr(false);
        return null;
      } else {
        return LoginDataModel.fromJson(response.data);
      }
    } else {
      _loginErr(true);
      throw Exception('Failed to load user data');
    }
  }

  void _log() async {
    if (_userLogin == '' || _userPassword == '') {
      _loginErr(false);
    } else {
      _login(_userLogin, _userPassword).then((LoginDataModel userData) async {
        if (userData != null) {
          await storage.write(key: 'token', value: userData.token);
          await storage.write(key: 'userLogin', value: userData.user.identifiant);
          _connected = true;
          BlocProvider.of<EventsBloc>(context).insertSmartphoneId();
          _loginStateCheck();
        }
      });
    }
  }

  void _loginErr(bool server) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title:
              Text(translations.text('disturbances.alerts.login_error_header')),
          content: server
              ? Text(
                  translations.text('disturbances.alerts.login_error500_text'))
              : Text(
                  translations.text('disturbances.alerts.login_error400_text')),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _logout() async {
    await storage.delete(key: 'token');
    await storage.delete(key: 'userLogin');
    setState(() {
      _connected = false;
    });
  }
}
