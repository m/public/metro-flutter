import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/nsv_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:metro/global/widgets/pdf_screen.dart';

class DetailsDisturbancesPage extends StatefulWidget {
  @override
  DetailsDisturbancesPageState createState() => DetailsDisturbancesPageState();

  final EventModel event;

  DetailsDisturbancesPage(this.event);
}

class DetailsDisturbancesPageState extends State<DetailsDisturbancesPage> with SingleTickerProviderStateMixin {
  EventsBloc _eventsBloc;
  String service = 'Service normal';

  @override
  void initState() {
    super.initState();
    _eventsBloc = BlocProvider.of<EventsBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int nsv = BlocProvider.master<NsvBloc>().getNsvId(widget.event.listeLigneArret);
    String asset = nsv == 4
        ? globalElevations.perturbation_1
        : (nsv == 2 || nsv == 3)
            ? globalElevations.perturbation_2
            : '';
    if (nsv == 2) {
      service = 'Service légèrement perturbé';
    } else if (nsv == 3) {
      service = 'Service très perturbé';
    } else if (nsv == 4) {
      service = 'Hors service';
    }
    double height = 100;
    double width = height;
    double cornerRadius = 12;
    if (widget.event.listeLigneArret != null) {
      if (widget.event.listeLigneArret.contains('SEM')) cornerRadius = height / 2;
      if (!widget.event.listeLigneArret.contains('SEM')) width = height * 2;
    }
    return Container(
      child: Scaffold(
        backgroundColor: globalElevations.e02dp.backgroundColor,
        body: Padding(
          padding: const EdgeInsets.only(top: 24),
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    height: 120,
                    decoration: BoxDecoration(
                      color: globalElevations.e16dp.backgroundColor,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 2.0,
                          offset: Offset(0, 2),
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: globalElevations.e01dp.backgroundColor,
                                borderRadius: BorderRadius.circular(8.0),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color: Colors.black26,
                                    blurRadius: 2.0,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              child: Icon(
                                Icons.arrow_back,
                                size: 36,
                              ),
                            ),
                          ),
                          (widget.event.hasIcon != null && widget.event.hasIcon == true)
                              ? Stack(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(right: 8, left: 4),
                                      child: Container(
                                        height: height,
                                        width: width,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(cornerRadius),
                                          color: (widget.event.iconColor == ''
                                                  ? Theme.of(context).colorScheme.primary
                                                  : Utils.colorFromHex(widget.event.iconColor)) ??
                                              Colors.transparent,
                                          boxShadow: <BoxShadow>[BoxShadow(color: Colors.black45, offset: Offset(0, 3), blurRadius: 6)],
                                        ),
                                        child: Center(
                                          child: Text(
                                            widget.event.iconText ?? '',
                                            style: TextStyle(
                                              color: (widget.event.iconColor == '' ? Colors.black : Utils.colorFromHex(widget.event.iconTextColor)) ??
                                                  Colors.transparent,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 40,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: (nsv == 2 || nsv == 3 || nsv == 4),
                                      child: Positioned(
                                        bottom: 72,
                                        left: 80,
                                        child: SizedBox(
                                          height: 28,
                                          width: 28,
                                          child: SvgPicture.asset(
                                            asset,
                                            height: 28,
                                            semanticsLabel: 'icon',
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : (!widget.event.visibleBandeauAppli)
                                  ? Padding(
                                      padding: EdgeInsets.only(right: 10, left: 8),
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: 100,
                                        height: 100,
                                        child: Image.asset('assets/sacado-icon.png'),
                                      ),
                                    )
                                  : Padding(
                                      padding: EdgeInsets.only(left: 16, right: 28),
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "!",
                                          style: TextStyle(fontSize: 34, fontWeight: FontWeight.bold, color: Color(0xFFf96c6c)),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8, 12, 8, 8),
                          child: Column(
                            children: [
                              Text(
                                (widget.event.splitTextHeader.length < 2
                                        ? widget.event.splitTextHeader[0]
                                        : widget.event.splitTextHeader[1].substring(1)[0].toUpperCase() +
                                            widget.event.splitTextHeader[1].substring(2)) ??
                                    "",
                                style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 18),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, bottom: 8),
                          child: Text(
                            service,
                            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                          ),
                        ),
                        widget.event.dateDebut != null
                            ? Padding(
                                padding: const EdgeInsets.only(left: 8, bottom: 8),
                                child: Text(
                                  'Début le ' + widget.event.dateDebut,
                                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                                ),
                              )
                            : Container(),
                        (widget.event.dateFin != null && !widget.event.dateFin.contains("2050"))
                            ? Padding(
                                padding: const EdgeInsets.only(left: 8, bottom: 8),
                                child: Text(
                                  'Fin le ' + widget.event.dateFin,
                                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                                ),
                              )
                            : Container(),
                        SizedBox(
                          height: 14,
                        ),
                        Container(
                          height: 0.2,
                          color: globalElevations.fstTextColor.backgroundColor,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: Text(
                              (widget.event.splitText.length > 3
                                      ? widget.event.splitText[3].trim()
                                      : (widget.event.splitText.length > 2 ? widget.event.splitText[2].trim() : widget.event.splitText[1].trim())) ??
                                  '',
                              style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Visibility(
                          visible: (widget.event.plan != null && widget.event.plan != '') ? true : false,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: FlatButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16), side: BorderSide(color: Color(0xFFB49BDA))),
                                onPressed: () {
                                  widget.event.plan.contains("pdf") ? _pdfLoader(widget.event.plan ?? '', 'Plan') : Utils.launchUrl(widget.event.plan);
                                },
                                child: Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                                    child: Wrap(
                                      crossAxisAlignment: WrapCrossAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.public,
                                          color: Theme.of(context).colorScheme.primary,
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          "Plus d'infos".toUpperCase(),
                                          style: TextStyle(color: Theme.of(context).indicatorColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _pdfLoader(fileUrl, String title) async {
    _eventsBloc.managePDFLoading(true);
    final filename = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(fileUrl));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File('$dir/$filename');
    await file.writeAsBytes(bytes);
    _eventsBloc.managePDFLoading(false);
    Navigator.push(context, MaterialPageRoute(builder: (context) => PDFScreen(file.path, title, bytes)));
  }
}
