import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';

class ReportingPage extends TraceableStatefulWidget {
  ReportingPage({Key key})
      : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _ReportingPageState createState() => _ReportingPageState();
}

class _ReportingPageState extends State<ReportingPage> {
  List<String> events = [
    translations.text('reporting.accident'),
    translations.text('reporting.traffic'),
    translations.text('reporting.obstacle'),
    translations.text('reporting.other')
  ];
  String dropdownValue = translations.text('reporting.accident');
  bool isGpsActive = false;
  final GlobalKey _loadingDialogKey = GlobalKey();
  List<PickedFile> _listFile = [];
  final _controller = TextEditingController();

  Widget _buildTakePicsBtn() {
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: BorderSide(color: Theme.of(context).colorScheme.primary)),
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Text(translations.text("reporting.take_picture"),
              style: TextStyle(color: Theme.of(context).colorScheme.primary))),
      onPressed: () => showImageDialog(),
    );
  }

  Widget _buildImgList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        ..._listFile
            .map((elem) => ListTile(
                  leading: Icon(Icons.check),
                  title: Text(elem.path.split('/').last),
                  trailing: IconButton(
                    icon: Icon(Icons.delete_forever),
                    onPressed: () => setState(() => _listFile.remove(elem)),
                  ),
                ))
            .toList()
      ],
    );
  }

  Widget _buildDescriptionInput() {
    return Container(
      margin: EdgeInsets.only(top: 8),
      decoration: BoxDecoration(
          border:
              Border.all(color: globalElevations.sndTextColor.backgroundColor),
          borderRadius: BorderRadius.circular(12)),
      padding: EdgeInsets.all(16),
      child: Theme(
          data: Theme.of(context).copyWith(
              inputDecorationTheme: Theme.of(context)
                  .inputDecorationTheme
                  .copyWith(enabledBorder: InputBorder.none)),
          child: TextField(
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            controller: _controller,
            maxLines: 12,
            decoration: InputDecoration(
              hintText: translations.text("reporting.textfield"),
              border: InputBorder.none,
            ),
          )),
    );
  }

  Widget _buildDescriptionTitle() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Text(translations.text("reporting.event_description")),
      color: globalElevations.e00dp.backgroundColor,
    );
  }

  Widget _buildBottomButtons() {
    return Row(
      children: <Widget>[
        Expanded(
          child: FlatButton(
            child: Text(translations.text("reporting.cancel"),
                style: TextStyle(color: Theme.of(context).colorScheme.primary)),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        Expanded(
            child: RaisedButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: Theme.of(context).colorScheme.primary,
          child: Text(translations.text("reporting.send")),
          onPressed: () =>
              sendEmail(_listFile.map((elem) => elem.path).toList()),
        ))
      ],
    );
  }

  Widget _buildGeolocButton() {
    return Row(
      children: <Widget>[
        Icon(Icons.gps_fixed),
        Padding(
          padding: const EdgeInsets.all(8),
          child: Text(translations.text("reporting.gps_active")),
        ),
        Switch(
          value: isGpsActive,
          activeTrackColor: Colors.lightGreenAccent,
          activeColor: Colors.grey,
          onChanged: (value) {
            setState(() {
              isGpsActive = value;
              if (value) {
                BlocProvider.of<LocationBloc>(context).getLocation();
                myPosition(context);
              }
            });
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          translations.text('reporting.title'),
          style:
              TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            padding: EdgeInsets.all(16),
            child: ConstrainedBox(
              constraints: constraints.copyWith(
                  minHeight: constraints.maxHeight - 56,
                  maxHeight: double.infinity),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(translations
                                      .text('reporting.event_type')),
                                  DropdownButton<String>(
                                    value: dropdownValue,
                                    onChanged: (String newValue) => setState(
                                        () => dropdownValue = newValue),
                                    items: events.map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  )
                                ],
                              ),
                            ),
                            Expanded(flex: 2, child: _buildTakePicsBtn())
                          ],
                        ),
                      ),
                    ),
                    _buildImgList(),
                    Stack(
                      children: <Widget>[
                        _buildDescriptionInput(),
                        Positioned(left: 20, child: _buildDescriptionTitle())
                      ],
                    ),
                    _buildGeolocButton(),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: _buildBottomButtons(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<void> myPosition(BuildContext context) async {
    showLoadingDialog(context, _loadingDialogKey);
    await BlocProvider.master<LocationBloc>().getLocation();
    // Close the loading dialog
    Navigator.of(_loadingDialogKey.currentContext, rootNavigator: true).pop();
  }

  double _currentLongitude(BuildContext context) {
    LocationData location = BlocProvider.of<LocationBloc>(context).location;
    return location?.longitude;
  }

  double _currentLatitude(BuildContext context) {
    LocationData location = BlocProvider.of<LocationBloc>(context).location;
    return location?.latitude;
  }

  Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SimpleDialog(key: key, children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16),
              child: Center(
                child: Row(children: [
                  AnimatedLoadingLogo(height: 60),
                  const SizedBox(width: 16),
                  Expanded(
                      child: Text(
                    translations.text('search.location_loading_message'),
                    softWrap: true,
                  ))
                ]),
              ),
            )
          ]);
        });
  }

  Future<void> alerteMessagePosition(BuildContext context) {
    // flutter defined function
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text(translations.text('alert_message.title')),
          content: Text(translations.text('alert_message.content')),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text(translations.text('alert_message.close')),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }

  String _generateBody() {
    String body = "";
    if (isGpsActive)
      body +=
          'GPS: latitude: ${_currentLatitude(context)} - longitude: ${_currentLongitude(context)}\n';
    body += "${_controller.text}";
    return body;
  }

  /// Test mailer by sending email to yourself
  sendEmail(List<String> rawArgs) async {
    final Email email = Email(
      body: _generateBody(),
      subject: "Nouvelle perturbations $dropdownValue",
      recipients: ["contact@mobilites-m.fr"],
      attachmentPaths: rawArgs,
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }

  showImageDialog() async {
    ImageSource source = await showDialog<ImageSource>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(translations.text("reporting.modal_title")),
            content: Text(translations.text("reporting.picture_source")),
            actions: <Widget>[
              FlatButton(
                child: Text(translations.text("reporting.camera")),
                onPressed: () => Navigator.pop(context, ImageSource.camera),
              ),
              FlatButton(
                child: Text(translations.text("reporting.galerie")),
                onPressed: () => Navigator.pop(context, ImageSource.gallery),
              )
            ],
          );
        });
    if (source != null) {
      PickedFile image = await ImagePicker.platform.pickImage(source: source);
      if (image != null) setState(() => _listFile.add(image));
    }
  }
}
