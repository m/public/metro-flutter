import 'dart:async';
import 'package:animations/animations.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:metro/disturbances/widgets/disturbances_tile.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:preferences/preference_service.dart';

const double _fabDimension = 56.0;

class AllPage extends StatefulWidget {
  @override
  AllPageState createState() => AllPageState();

  final bool importantEvent;
  final String eventKey;

  AllPage({this.importantEvent = false, this.eventKey = "00"});
}

class AllPageState extends State<AllPage> {
  TextStyle activatedChip =
  TextStyle(color: globalElevations.inverseColor.backgroundColor);
  TextStyle disabledChip =
  TextStyle(color: globalElevations.fstTextColor.backgroundColor);
  bool _tag;
  bool _transisere;
  bool _important;
  String _falseEndDate = '31/12/2050 23:59';
  List<ExpandableController> _controllers = List();
  StreamSubscription<dynamic> _onSelectedEventItemChanged;
  EventsBloc _eventsBloc;

  void _tagToggle() {
    setState(() {
      _tag = !_tag;
      PrefService.setBool('tag_disturbances', _tag);
    });
  }

  void _transisereToggle() {
    setState(() {
      _transisere = !_transisere;
      PrefService.setBool('transisere_disturbances', _transisere);
    });
  }

  void _importantToggle() {
    setState(() {
      _important = !_important;
      PrefService.setBool('important_disturbances', _important);
    });
  }

  @override
  void initState() {
    super.initState();

    _eventsBloc = BlocProvider.of<EventsBloc>(context);

    _tag = PrefService.getBool('tag_disturbances') ?? true;
    _transisere = PrefService.getBool('transisere_disturbances') ?? false;
    _important = PrefService.getBool('important_disturbances') ?? false;

    if (widget.importantEvent && _important == false) _important = true;

    _eventsBloc.resetSelectedItem();

    _onSelectedEventItemChanged = BlocProvider.of<EventsBloc>(context)
        .onSelectedEventItemListener(_controllers);

    if (_important) _onOpenOrClose(context);
  }

  @override
  void dispose() {
    _onSelectedEventItemChanged.cancel();
    _controllers.forEach((e) => e.dispose());
    super.dispose();
  }

  _onOpenOrClose(BuildContext context) {
    List<EventModel> list =
        BlocProvider.of<EventsBloc>(context).allExceptionnalEvents;

    for (int index = 0; index < list.length; index++) {
      for (EventModel e in list) {
        BlocProvider.of<EventsBloc>(context).isReading(e.key);
        BlocProvider.of<EventsBloc>(context).onTappedEventItem(index);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globalElevations.e00dp.backgroundColor,
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                // RaisedButton(
                //   shape: new RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(18),
                //   ),
                //   onPressed: () {
                //     _onOpenOrClose(context);
                //     BlocProvider.of<EventsBloc>(context).resetSelectedItem();
                //     _importantToggle();
                //   },
                //   color: _important
                //       ? Theme.of(context).indicatorColor
                //       : globalElevations.e24dp.backgroundColor,
                //   child: Text(
                //     "Important",
                //     style: _important ? activatedChip : disabledChip,
                //   ),
                // ),
                SizedBox(width: 5),
                RaisedButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18),
                  ),
                  onPressed: () {
                    BlocProvider.of<EventsBloc>(context).resetSelectedItem();
                    _tagToggle();
                  },
                  color: _tag
                      ? Theme.of(context).indicatorColor
                      : globalElevations.e24dp.backgroundColor,
                  child: Text(
                    translations.text('disturbances.alerts.tag'),
                    style: _tag ? activatedChip : disabledChip,
                  ),
                ),
                SizedBox(width: 5),
                RaisedButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18),
                  ),
                  onPressed: () {
                    BlocProvider.of<EventsBloc>(context).resetSelectedItem();
                    _transisereToggle();
                  },
                  color: _transisere
                      ? Theme.of(context).indicatorColor
                      : globalElevations.e24dp.backgroundColor,
                  child: Text(
                    translations.text('disturbances.alerts.transisere'),
                    style: _transisere ? activatedChip : disabledChip,
                  ),
                ),
              ],
            ),
          ),
          StreamBuilder<bool>(
            stream: _eventsBloc.onLoadingPDFChanged,
            builder: (_, snapshot) => Visibility(
                visible: snapshot.data ?? false, child: AnimatedLoadingLogo()),
          ),
          StreamBuilder<List<EventModel>>(
            stream: _eventsBloc.onEventsChanged,
            builder: (_, snapshot) {
              if (snapshot.hasData) {
                return Flexible(
                    child: Column(
                      children: <Widget>[
                        filterList(orderFilter(snapshot.data), false)
                      ],
                    ));
              } else if (snapshot.hasError)
                return Text(snapshot.error.toString());
              return Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
      // floatingActionButton: OpenContainer(
      //   transitionType: ContainerTransitionType.fade,
      //   openBuilder: (context, _) => ReportingPage(),
      //   closedElevation: 6.0,
      //   closedShape: const RoundedRectangleBorder(
      //     borderRadius: BorderRadius.all(Radius.circular(_fabDimension / 2)),
      //   ),
      //   closedColor: Theme.of(context).colorScheme.secondary,
      //   closedBuilder: (context, openContainer) {
      //     return SizedBox(
      //       height: _fabDimension,
      //       width: _fabDimension,
      //       child: Center(
      //           child: Icon(
      //         Icons.add,
      //         color: Theme.of(context).colorScheme.onSecondary,
      //       )),
      //     );
      //   },
      // ),
    );
  }

  List<EventModel> orderFilter(List<EventModel> data) {
    List<EventModel> finalTempList = [];
    List<EventModel> listTram = [];
    List<EventModel> listProximoFlexo = [];
    List<EventModel> listChrono = [];
    List<EventModel> listTransisere1 = [];
    List<EventModel> listTransisere2 = [];

    List<String> tempListTram = ['A', 'B', 'C', 'D', 'E'];
    List<String> tempListProximoFlexo = [
      '12',
      '13',
      '14',
      '15',
      '16',
      '19',
      '20',
      '21',
      '22',
      '23',
      '25',
      '26',
      '40',
      '41',
      '42',
      '43',
      '44',
      '45',
      '46',
      '47',
      '48',
      '49',
      '50',
      '51',
      '54',
      '55',
      '56',
      '60',
      '61',
      '62',
      '63',
      '64',
      '65',
      '66',
      '67',
      '68',
      '70',
      '71'
    ];
    List<String> tempListChrono = ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'];
    List<String> tempListTransisere1 = ['EXP1', 'EXP2', 'EXP3'];
    List<String> tempListTransisere2 = [
      '1380',
      '3000',
      '3030',
      '4100',
      '4101',
      '4110',
      '4310',
      '4500',
      '5000',
      '5100',
      '5110',
      '5120',
      '5130',
      '5200',
      '6010',
      '6020',
      '6030',
      '6051',
      '6052',
      '6060',
      '6070',
      '6080',
      '6200',
      '6550',
      '7000',
      '7010',
      '7110',
      '7300',
      '7320',
      '7330',
      '7350',
      '7360',
      '7500'
    ];

    //IMPORTANT EVENT
    for (EventModel e in data) {
      if (e.visibleBandeauAppli == true) {
        finalTempList.add(e);
      }
    }

    //TRAM
    for (EventModel e in data) {
      if (tempListTram.contains(e.iconText)) {
        listTram.add(e);
      }
    }
    Comparator<EventModel> comparatorTram =
        (a, b) => a.iconText.compareTo(b.iconText);
    listTram.sort(comparatorTram);
    for (EventModel e in listTram) finalTempList.add(e);

    //CHRONO
    for (EventModel e in data) {
      if (tempListChrono.contains(e.iconText)) {
        listChrono.add(e);
      }
    }
    Comparator<EventModel> comparatorChrono = (a, b) =>
        int.parse(a.iconText.substring(1)[0])
            .compareTo(int.parse(b.iconText.substring(1)[0]));
    listChrono.sort(comparatorChrono);
    for (EventModel e in listChrono) finalTempList.add(e);

    //PROXIMO FLEXO
    for (EventModel e in data) {
      if (tempListProximoFlexo.contains(e.iconText)) {
        listProximoFlexo.add(e);
      }
    }
    Comparator<EventModel> comparatorProximoFlexo =
        (a, b) => int.parse(a.iconText).compareTo(int.parse(b.iconText));
    listProximoFlexo.sort(comparatorProximoFlexo);
    for (EventModel e in listProximoFlexo) finalTempList.add(e);

    //SACADO
    for (EventModel e in data) {
      if (e.iconText == null && e.visibleBandeauAppli == false) {
        finalTempList.add(e);
      }
    }

    //TRANSISERE
    for (EventModel e in data) {
      if (tempListTransisere1.contains(e.iconText)) {
        listTransisere1.add(e);
      }
    }

    Comparator<EventModel> comparatorTransisere = (a, b) =>
        int.parse(a.iconText.substring(3)[0])
            .compareTo(int.parse(b.iconText.substring(3)[0]));
    listTransisere1.sort(comparatorTransisere);
    for (EventModel e in listTransisere1) finalTempList.add(e);

    for (EventModel e in data) {
      if (tempListTransisere2.contains(e.iconText)) {
        listTransisere2.add(e);
      }
    }

    Comparator<EventModel> comparatorTransisere2 =
        (a, b) => int.parse(a.iconText).compareTo(int.parse(b.iconText));
    listTransisere2.sort(comparatorTransisere2);
    for (EventModel e in listTransisere2) finalTempList.add(e);

    return finalTempList;
  }

  Widget filterList(List<EventModel> data, bool isExceptionalEvent) {
    List<EventModel> result = [];

    if (_tag && _transisere) {
      result = data
          .where((x) => ((x.key.contains('C38') || x.listeLigneArret.contains('C38') || x.key.contains('SEM'))))
          .toList();
      return buildList(result);
    } else if (!_tag && _transisere) {
      result = data
          .where(
              (x) => (x.key.contains('C38') && x.visibleBandeauAppli == false) || (x.listeLigneArret.contains('C38') && x.visibleBandeauAppli == false))
          .toList();
      return buildList(result);
    } else if (_tag && !_transisere) {
      result = data
          .where(
              (x) => (x.key.contains('SEM') && x.visibleBandeauAppli == false))
          .toList();
      return buildList(result);
    } else
      return Text('');
  }

  Widget buildList(List<EventModel> eventList) {
    _controllers.clear();
    return Flexible(
        child: ListView(children: [
          ...eventList.map<Widget>((e) {
            ExpandableController controller = ExpandableController();
            if (e.key == widget.eventKey) controller.toggle();
            _controllers.add(controller);
            return DisturbancesTile(e, widget.eventKey, controller);
          }).toList()
        ]));
  }
}
