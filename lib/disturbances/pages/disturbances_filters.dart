import 'dart:async';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:metro/disturbances/widgets/disturbances_tile.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';

class DisturbancesFilters extends StatefulWidget {
  @override
  DisturbancesFiltersState createState() => DisturbancesFiltersState();

  final bool importantEvent;
  final String eventKey;
  final String lineText;

  DisturbancesFilters({this.importantEvent = false, this.eventKey = "00", this.lineText = ""});
}

class DisturbancesFiltersState extends State<DisturbancesFilters> {
  List<ExpandableController> _controllers = List();
  StreamSubscription<dynamic> _onSelectedEventItemChanged;
  EventsBloc _eventsBloc;

  @override
  void initState() {
    super.initState();
    _eventsBloc = BlocProvider.of<EventsBloc>(context);
    _eventsBloc.resetSelectedItem();
    _onSelectedEventItemChanged = BlocProvider.of<EventsBloc>(context).onSelectedEventItemListener(_controllers);
  }

  @override
  void dispose() {
    _onSelectedEventItemChanged.cancel();
    _controllers.forEach((e) => e.dispose());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globalElevations.e00dp.backgroundColor,
      appBar: AppBar(
        backgroundColor: globalElevations.e16dp.backgroundColor,
        iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
        title: Text(
          "Perturbations de la ligne " + widget.lineText,
          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
      ),
      body: Column(
        children: <Widget>[
          StreamBuilder<List<EventModel>>(
            stream: _eventsBloc.onEventsChanged,
            builder: (_, snapshot) {
              if (snapshot.hasData) {
                return Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 14.0),
                    child: Column(
                      children: <Widget>[
                        (orderFilter(snapshot.data) == null || orderFilter(snapshot.data).isEmpty)
                            ? Center(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(8.0, 48.0, 8.0, 8.0),
                                  child: Text(
                                    'Aucunes perturbations signalées sur la ligne ' + widget.lineText,
                                    style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16.0),
                                  ),
                                ),
                              )
                            : filterList(orderFilter(snapshot.data))
                      ],
                    ),
                  ),
                );
              } else if (snapshot.hasError) return Text(snapshot.error.toString());
              return Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }

  List<EventModel> orderFilter(List<EventModel> data) {
    List<EventModel> finalTempList = [];

    for (EventModel e in data) {
      if (e.iconText == widget.lineText) {
        finalTempList.add(e);
      }
    }

    return finalTempList;
  }

  Widget filterList(List<EventModel> data) {
    List<EventModel> result = [];
    result = data.where((x) => ((x.key.contains('C38') || x.key.contains('SEM') || x.visibleBandeauAppli == true))).toList();
    return buildList(result);
  }

  Widget buildList(List<EventModel> eventList) {
    _controllers.clear();
    return Flexible(
        child: ListView(children: [
      ...eventList.map<Widget>((e) {
        ExpandableController controller = ExpandableController();
        if (e.key == widget.eventKey) controller.toggle();
        _controllers.add(controller);
        return DisturbancesTile(e, widget.eventKey, controller);
      }).toList()
    ]));
  }
}
