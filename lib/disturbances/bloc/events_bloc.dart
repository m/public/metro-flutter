import 'dart:async';
import 'dart:io';
import 'package:expandable/expandable.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/disturbances/models/my_events_model.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart' as http;
import 'package:metro/disturbances/models/icon_data_model.dart';
import 'dart:convert';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as ss;

class EventsBloc extends BlocBase {
  BehaviorSubject<List<EventModel>> _eventsController = BehaviorSubject<List<EventModel>>.seeded(null);
  Stream<List<EventModel>> get onEventsChanged => _eventsController.stream;
  List<EventModel> get allEvents => _eventsController.value;

  BehaviorSubject<MyEventsModel> _myEventsController = BehaviorSubject<MyEventsModel>.seeded(null);
  Stream<MyEventsModel> get onMyEventsChanged => _myEventsController.stream;
  MyEventsModel get myEvents => _myEventsController.value;

  BehaviorSubject<int> _selectedEventItemController = BehaviorSubject<int>.seeded(null);
  Stream<int> get onSelectedEventChanged => _selectedEventItemController.stream;
  int get selectedEventItem => _selectedEventItemController.value;
  int lastSelectedEventItem;

  BehaviorSubject<List<EventModel>> _exceptionnalEventsController = BehaviorSubject<List<EventModel>>.seeded(null);
  Stream<List<EventModel>> get onExceptionnalEventsChanged => _exceptionnalEventsController.stream;
  List<EventModel> get allExceptionnalEvents => _exceptionnalEventsController.value;

  BehaviorSubject<int> readExceptionnalEventsController = BehaviorSubject<int>.seeded(0);
  Stream<int> get onReadExceptionnalEventsChanged => readExceptionnalEventsController.stream;
  int get eventsRead => readExceptionnalEventsController.value;

  BehaviorSubject<bool> _loadingPDFController = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get onLoadingPDFChanged => _loadingPDFController.stream;
  bool get loadingPDF => _loadingPDFController.value;

  int size = 0;
  bool isRead = false;
  bool isNotRead = false;
  List<EventModel> myEventsVisibleList = List<EventModel>();

  dispose() {
    _eventsController.close();
    _myEventsController.close();
    _selectedEventItemController.close();
    _loadingPDFController.close();
    readExceptionnalEventsController.close();
    _exceptionnalEventsController.close();
  }

  @override
  void initState() {
    BlocProvider.master<WebServices>().onHostChanged.listen((host) {
      fetchAllEvents();
    });
  }

  fetchAllEvents() async {
    List<EventModel> eventList = await _loadAllEvents();
    _eventsController.sink.add(eventList);
  }

  String getCountEvent(String id) {
    var count = allEvents.where((e) => e.iconText == id).toList().length;
    if (count == null || count == 0) return '';
    return count == 1 ? count.toString() + ' évènement' : count.toString() + ' évènements';
  }

  // For the first connection only - send new smartphone ID
  insertSmartphoneId() async {
    int _platformId = Platform.isAndroid ? 2 : 1;
    final storage = new ss.FlutterSecureStorage();
    String _pushToken = await storage.read(key: 'pushtoken');
    String _token = await storage.read(key: 'token');
    PrefService.setString('smartphoneid', _pushToken);
    Dio dioPost = new Dio();
    dioPost.post(
      BlocProvider.master<WebServices>().getSivHost() + 'insertsmartphoneid',
      data: {'os': _platformId, 'smartphoneId': _pushToken},
      options: Options(
        contentType: 'application/json',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $_token'},
      ),
    ).then((response) {
      if (response.statusCode == 200) {
        BlocProvider.master<EventsBloc>().updateSmartphoneId();
      } else {
        print('Failed to fetch updateSmartphoneId. Error code : ' + response.statusCode.toString());
      }
    });
  }

  // Update smartphoneId only if it was modified
  Future updateSmartphoneId() async {
    final storage = new ss.FlutterSecureStorage();
    String _pushToken = await storage.read(key: 'pushtoken');

    String savedSmartphoneId = PrefService.getString('smartphoneid');
    if (savedSmartphoneId != _pushToken) {
      PrefService.setString('smartphoneid', _pushToken);
      Dio dio = new Dio();
      Response responseUpdateID = await dio.post(BlocProvider.master<WebServices>().getSivHost() + 'api/updatephoneid',
          data: {"smartphoneId": _pushToken, "oldSmartphoneId": _pushToken},
          options: Options(contentType: 'application/json', headers: {
            Headers.acceptHeader: 'application/json',
            Headers.contentTypeHeader: 'application/json',
          }));

      if (responseUpdateID.statusCode != 200) {
        throw Exception('Failed to fetch updateSmartphoneId features');
      }
    }
    await getSubscription();
  }

  Future getSubscription() async {
    final storage = new ss.FlutterSecureStorage();
    String _pushToken = await storage.read(key: 'pushtoken');
    MyEventsModel myEventsData;

    var data = await http.get(BlocProvider.master<WebServices>().getSivHost() + 'api/getabonnement?smartphoneId=' + _pushToken);
    var jsonResult = jsonDecode(data.body);
    myEventsData = MyEventsModel.fromJson(jsonResult);
    _myEventsController.sink.add(myEventsData);
  }

  void onTappedEventItem(int index) {
    if (index == selectedEventItem) return;
    lastSelectedEventItem = selectedEventItem;
    _selectedEventItemController.sink.add(index);
  }

  void resetSelectedItem() {
    _selectedEventItemController.sink.add(null);
    lastSelectedEventItem = null;
  }

  StreamSubscription<dynamic> onSelectedEventItemListener(List<ExpandableController> controllers) => onSelectedEventChanged.listen((data) {
        if (data != null && controllers != null && controllers.isNotEmpty) {
          final selectedLineStopController = controllers[data];
          int indexLastSelected = lastSelectedEventItem;
          ExpandableController lastSelectedLineStopController = indexLastSelected == null ? null : controllers[indexLastSelected];
          if (selectedLineStopController != null) {
            if (lastSelectedLineStopController != null) {
              if (selectedLineStopController == lastSelectedLineStopController)
                selectedLineStopController.expanded = true;
              else
                lastSelectedLineStopController.expanded = false;
            }
          }
        }
      });

  Future<List<EventModel>> getExceptionalEvents() async {
    List<EventModel> allEvents = [];
    List<String> dataKeys = [];
    List<IconDataModel> iconDataList = [];

    // Chargement du détail des icones afin de mapper la config avec les icones évenement
    //var data1 = await http.get("http://datatest.metromobilite.fr/api/routers/default/index/routes");
    var data1 = await BlocProvider.master<WebServices>().get('routers/default/index/routes');
    iconDataList = jsonDecode(data1.body).map<IconDataModel>((data) => new IconDataModel.fromJson(data)).toList();

    // On enlève les évenements qui ne rentrent pas dans le périmetre de ce qu'on doit afficher
    //iconDataList.removeWhere((item) => item.id.substring(0, 3) != 'C38' && item.id.substring(0, 3) != 'SEM');
    // TODO : Yassine -> verifier si cette modif n'impacte pas la page SIV

    // Chargement des évenements
    var data = await BlocProvider.master<WebServices>().get('dyn/evt/json');
    var jsonResult = jsonDecode(data.body);

    // On extrait les clés d'objects (Oui le backend est horrible)
    for (var key in jsonResult.keys) {
      dataKeys.add(key.toString());
    }

    // On enlève les clés qui ne rentrent pas dans le périmetre des évenements qu'on doit afficher
    //dataKeys.removeWhere((item) => !item.contains('SEM') && !item.contains('C38'));
    // TODO : Yassine -> verifier si cette modif n'impacte pas la page SIV

    // On genere une liste d'objets à partir des objets contenu dans l'objet de la réponse http
    for (int i = 0; i < dataKeys.length; i++) {
      EventModel eventModel = EventModel.fromJson(jsonResult[dataKeys[i]], dataKeys[i]);
      if (eventModel != null) allEvents.add(eventModel);
    }

    // Transformations et regreoupement des données des 2 WS :
    // On sépare les données dans des listes (coté backend, texte par exemple contient plusieurs données séparés par des '|')
    for (int i = 0; i < allEvents.length; i++) {
      allEvents[i].splitText = allEvents[i].texte.split("|");
      allEvents[i].splitTextHeader = allEvents[i].splitText[0].split(":");
      dataKeys[i].contains('SEM') ? allEvents[i].isSEM = true : allEvents[i].isSEM = false;

      // Mapper les codes d'icones du ws 2 avec les données d'affichage de ces dernieres récupérés depuis le ws 1
      if (allEvents[i].listeLigneArret != null) {
        int tempIndex = -1;

        if (allEvents[i].listeLigneArret.length >= 4)
          tempIndex = iconDataList.indexWhere((el) => el.id.contains(allEvents[i].listeLigneArret.replaceAll('_', ':')));

        if (tempIndex == -1) {
          allEvents[i].hasIcon = false;
        } else {
          var tempIconData = iconDataList[tempIndex];
          allEvents[i].hasIcon = true;
          allEvents[i].iconText = tempIconData.shortName;
          allEvents[i].iconTextColor = tempIconData.textColor;
          allEvents[i].iconColor = tempIconData.color;
          allEvents[i].gtfsId = tempIconData.gtfsId;
        }
      }
    }

    List<EventModel> eventList = allEvents;
    List<EventModel> eventsVisibleList = List<EventModel>();
    for (var event in eventList) {
      if (event.visibleBandeauAppli) {
        eventsVisibleList.add(event);
        myEventsVisibleList.add(event);
      }
    }

    if (eventsVisibleList.length == 0) readExceptionnalEventsController.sink.add(0);

    _exceptionnalEventsController.sink.add(eventsVisibleList);

    return eventsVisibleList;
  }

  Future<List<EventModel>> _loadAllEvents() async {
    List<EventModel> allEvents = [];
    List<String> dataKeys = [];
    List<IconDataModel> iconDataList = [];

    // Chargement du détail des icones afin de mapper la config avec les icones évenement
    //var data1 = await http.get("http://datatest.metromobilite.fr/api/routers/default/index/routes");
    var data1 = await BlocProvider.master<WebServices>().get('routers/default/index/routes');
    iconDataList = jsonDecode(data1.body).map<IconDataModel>((data) => new IconDataModel.fromJson(data)).toList();

    // On enlève les évenements qui ne rentrent pas dans le périmetre de ce qu'on doit afficher
    //iconDataList.removeWhere((item) => item.id.substring(0, 3) != 'C38' && item.id.substring(0, 3) != 'SEM');
    // TODO : Yassine -> verifier si cette modif n'impacte pas la page SIV

    // Chargement des évenements
    var data = await BlocProvider.master<WebServices>().get('dyn/evt/json');
    var jsonResult = jsonDecode(data.body);

    // On extrait les clés d'objects (Oui le backend est horrible)
    for (var key in jsonResult.keys) {
      dataKeys.add(key.toString());
    }

    // On enlève les clés qui ne rentrent pas dans le périmetre des évenements qu'on doit afficher
    //dataKeys.removeWhere((item) => !item.contains('SEM') && !item.contains('C38'));
    // TODO : Yassine -> verifier si cette modif n'impacte pas la page SIV

    // On genere une liste d'objets à partir des objets contenu dans l'objet de la réponse http
    for (int i = 0; i < dataKeys.length; i++) {
      EventModel eventModel = EventModel.fromJson(jsonResult[dataKeys[i]], dataKeys[i]);
      if (eventModel != null) allEvents.add(eventModel);
    }

    // Transformations et regreoupement des données des 2 WS :
    // On sépare les données dans des listes (coté backend, texte par exemple contient plusieurs données séparés par des '|')
    for (int i = 0; i < allEvents.length; i++) {
      allEvents[i].splitText = allEvents[i].texte.split("|");
      allEvents[i].splitTextHeader = allEvents[i].splitText[0].split(":");
      dataKeys[i].contains('SEM') ? allEvents[i].isSEM = true : allEvents[i].isSEM = false;

      // Mapper les codes d'icones du ws 2 avec les données d'affichage de ces dernieres récupérés depuis le ws 1
      if (allEvents[i].listeLigneArret != null) {
        int tempIndex = -1;

        if (allEvents[i].listeLigneArret.length >= 4)
          tempIndex = iconDataList.indexWhere((el) => el.id.contains(allEvents[i].listeLigneArret.replaceAll('_', ':')));

        if (tempIndex == -1) {
          allEvents[i].hasIcon = false;
        } else {
          var tempIconData = iconDataList[tempIndex];
          allEvents[i].hasIcon = true;
          allEvents[i].iconText = tempIconData.shortName;
          allEvents[i].iconTextColor = tempIconData.textColor;
          allEvents[i].iconColor = tempIconData.color;
          allEvents[i].gtfsId = tempIconData.gtfsId;
        }
      }
    }

    return allEvents;
  }

  Future<int> getNumberExceptionalEvents() async {
    return (await getExceptionalEvents()).length;
  }

  hasBeenRead(String key) {
    bool result = PrefService.getBool('preferences_events_info_read_$key');

    // If result == null so event has been read
    return result == null ? false : true;
  }

  isReading(String key) async {
    PrefService.setBool('preferences_events_info_read_$key', true);
    if (await isReaded() == 1) {
      readExceptionnalEventsController.sink.add(1);
    }
  }

  // Check if all exceptional events are readed
  Future<bool> isReaded() async {
    await getExceptionalEvents();

    isRead = false;
    isNotRead = true;
    bool result;
    if (myEventsVisibleList.length != 0) {
      for (int i = 0; i < myEventsVisibleList.length; i++) {
        EventModel event = myEventsVisibleList[i];
        isRead = (hasBeenRead(event.key)) ?? true;
        isNotRead = (hasBeenRead(event.key)) ?? false;
        if (!isNotRead) {
          result = false;
        } else {
          result = true;
        }
      }
    } else
      result = true;

    result ? readExceptionnalEventsController.sink.add(1) : readExceptionnalEventsController.sink.add(2);

    return result;
  }

  void managePDFLoading(bool val) => _loadingPDFController.sink.add(val);
}
