import 'package:metro/filters/filters_type.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/localization/translations.dart';
import 'package:preferences/preferences.dart';
import 'package:rxdart/rxdart.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/filters/models/CheckboxTileController.dart';

class FiltersBloc implements BlocBase {
  Map<FiltersType, CheckboxTileController> _controllers = Map();
  FiltersBloc _bloc;
  final List<Filter> filters = [
    Filter(
        id: 1,
        travelMode: TravelMode.public_transport,
        title: translations.text('filter.values.stops'),
        type: FiltersType.stops,
        requestId: 'clusters'),
    Filter(
        id: 2, travelMode: TravelMode.public_transport, title: translations.text('filter.values.trams'), type: FiltersType.trams, requestId: 'TRAM'),
    Filter(
      id: 3,
      travelMode: TravelMode.public_transport,
      title: translations.text('filter.values.buses'),
      type: FiltersType.buses,
      subFilters: [
        Filter(
          id: 4,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.buses_chrono'),
          type: FiltersType.buses_chrono,
          requestId: 'CHRONO',
        ),
        Filter(
          id: 5,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.buses_proximo'),
          type: FiltersType.buses_proximo,
          requestId: 'PROXIMO',
        ),
        Filter(
          id: 6,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.buses_flexo'),
          type: FiltersType.buses_flexo,
          requestId: 'FLEXO',
        ),
      ],
    ),
    Filter(
      id: 7,
      travelMode: TravelMode.public_transport,
      title: translations.text('filter.values.c38'),
      type: FiltersType.c38,
      requestId: 'C38',
    ),
    Filter(
      id: 11,
      travelMode: TravelMode.public_transport,
      title: translations.text('filter.values.gresivaudan'),
      type: FiltersType.gresivaudan,
      subFilters: [
        Filter(
          id: 12,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.gresivaudan_structurante'),
          type: FiltersType.gresivaudan_structurante,
          requestId: 'Structurantes',
        ),
        Filter(
          id: 13,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.gresivaudan_secondaires'),
          type: FiltersType.gresivaudan_secondaires,
          requestId: 'Secondaires',
        ),
      ],
    ),
    Filter(
      id: 8,
      travelMode: TravelMode.public_transport,
      title: translations.text('filter.values.voironnais'),
      type: FiltersType.voironnais,
      subFilters: [
        Filter(
          id: 9,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.voironnais_urbaines'),
          type: FiltersType.voironnais_urbaines,
          requestId: 'Urbaines',
        ),
        Filter(
          id: 10,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.voironnais_interurbaines'),
          type: FiltersType.voironnais_interurbaines,
          requestId: 'Interurbaines',
        ),
      ],
    ),
    Filter(
      id: 14,
      travelMode: TravelMode.car,
      title: translations.text('filter.values.parkings'),
      type: FiltersType.parkings,
      subFilters: [
        Filter(
          id: 15,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.parkings_PKG'),
          type: FiltersType.parkings_PKG,
          requestId: 'PKG',
          iconPath: MetroIcons.pkg,
        ),
        Filter(
          id: 16,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.parkings_PAR'),
          type: FiltersType.parkings_PAR,
          requestId: 'PAR',
          iconPath: MetroIcons.par,
        ),
      ],
    ),
    Filter(
      id: 18,
      travelMode: TravelMode.car,
      title: translations.text('filter.values.selfserve'),
      type: FiltersType.selfserve,
      subFilters: [
        Filter(
          id: 19,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.selfserve_citiz'),
          type: FiltersType.selfserve_citiz,
          requestId: 'citiz',
          iconPath: MetroIcons.citiz,
        ),
        Filter(
          id: 20,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.selfserve_yea'),
          type: FiltersType.selfserve_yea,
          requestId: 'citizyea',
          iconPath: MetroIcons.citiz_yea,
        ),
      ],
    ),
    Filter(
      id: 21,
      travelMode: TravelMode.car,
      title: translations.text('filter.values.carpooling'),
      type: FiltersType.carpooling,
      subFilters: [
        Filter(
          id: 23,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.carpooling_parking'),
          type: FiltersType.carpooling_parking,
          requestId: 'parkingCov',
          iconPath: MetroIcons.parking_cov,
        ),
        Filter(
          id: 24,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.carpooling_picking'),
          type: FiltersType.carpooling_picking,
          requestId: 'pointCov',
          iconPath: MetroIcons.point_cov,
        ),
        Filter(
          id: 25,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.carpooling_stops'),
          type: FiltersType.carpooling_stops,
          requestId: 'autostop',
          iconPath: MetroIcons.autostop,
        ),
      ],
    ),
    Filter(
      id: 26,
      travelMode: TravelMode.car,
      title: translations.text('filter.values.energy'),
      type: FiltersType.energy,
      subFilters: [
        Filter(
          id: 27,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.energy_electric'),
          type: FiltersType.energy_electric,
          requestId: 'irve',
        ),
        Filter(
          id: 28,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.energy_hydrogen'),
          type: FiltersType.energy_hydrogen,
          requestId: 'hydrogene',
          iconPath: MetroIcons.hydrogene,
        ),
        Filter(
          id: 29,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.energy_GNV'),
          type: FiltersType.energy_GNV,
          requestId: 'gnv',
          iconPath: MetroIcons.gnv,
        ),
        Filter(
          id: 30,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.energy_GPL'),
          type: FiltersType.energy_GPL,
          requestId: 'gpl',
          iconPath: MetroIcons.gpl,
        ),
      ],
    ),
    Filter(
      id: 31,
      travelMode: TravelMode.public_transport,
      title: translations.text('filter.values.stations'),
      type: FiltersType.stations,
      subFilters: [
        Filter(
          id: 32,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.stations_agenceM'),
          type: FiltersType.stations_agenceM,
          requestId: 'agenceM',
          iconPath: MetroIcons.agence_m,
        ),
        Filter(
          id: 33,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.stations_TAG'),
          type: FiltersType.stations_TAG,
          requestId: 'depositaire',
          iconPath: MetroIcons.relais_tag,
        ),
        Filter(
          id: 34,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.stations_transisere'),
          type: FiltersType.stations_transisere,
          requestId: 'pointService',
        ),
      ],
    ),
    Filter(
      id: 36,
      travelMode: TravelMode.car,
      title: translations.text('filter.values.trafic'),
      type: FiltersType.trafic,
      subFilters: [
        Filter(
          id: 37,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.trafic_state'),
          type: FiltersType.trafic_state,
          requestId: 'road_section',
        ),
        Filter(
          id: 38,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.trafic_event'),
          type: FiltersType.trafic_event,
        ),
        Filter(
          id: 39,
          travelMode: TravelMode.car,
          title: translations.text('filter.values.trafic_cam'),
          type: FiltersType.trafic_cam,
          requestId: 'CAM',
        ),
      ],
    ),
    Filter(
      id: 40,
      travelMode: TravelMode.bike,
      title: translations.text('filter.values.bikes'),
      type: FiltersType.bikes,
      subFilters: [
        Filter(
          id: 41,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_tempo'),
          type: FiltersType.bikes_tempo,
          requestId: 'tempovelo',
          legendeIconPath: "assets/bikes/legende-tempo-velo.png",
        ),
        Filter(
          id: 42,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_chrono'),
          type: FiltersType.bikes_chrono,
          requestId: 'chronovelo',
          legendeIconPath: "assets/bikes/legende-crhono-velo.png",
        ),
        Filter(
          id: 43,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_verte'),
          type: FiltersType.bikes_verte,
          requestId: 'voieverte',
          legendeIconPath: "assets/bikes/legende-voies-vertes-velo.png",
        ),
        Filter(
          id: 44,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_amenage'),
          type: FiltersType.bikes_amenage,
          requestId: 'veloamenage',
          legendeIconPath: "assets/bikes/legende-itineraires-amenages-velo.png",
        ),
        Filter(
          id: 45,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_nonamenage'),
          type: FiltersType.bikes_nonamenage,
          requestId: 'velononamenage',
          legendeIconPath: "assets/bikes/legende-itineraire-non-amenages-velo.png",
        ),
        Filter(
          id: 46,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.bikes_difficile'),
          type: FiltersType.bikes_difficile,
          requestId: 'velodifficile',
          legendeIconPath: "assets/bikes/legende-itineraires-difficiles-velo.png",
        ),
      ],
      hasInfo: true,
    ),
    Filter(
      id: 54,
      travelMode: TravelMode.bike,
      title: "Aires de services",
      type: FiltersType.veloservice,
      requestId: 'veloservice',
      hasInfo: true
    ),
    Filter(
      id: 47,
      travelMode: TravelMode.bike,
      title: translations.text('filter.values.parking'),
      type: FiltersType.bikes_parking,
      subFilters: [
        Filter(
          id: 48,
          travelMode: TravelMode.bike,
          title: translations.text('filter.values.parkings_bike'),
          type: FiltersType.parkings_bike,
          requestId: 'MVC',
        ),
      ],
    ),
    Filter(
      id: 53,
      travelMode: TravelMode.bike,
      title: "Vélos en libre service",
      type: FiltersType.pony,
      requestId: 'ponyVehicle',
    ),
    Filter(
      id: 49,
      travelMode: TravelMode.bike,
      title: "Location",
      type: FiltersType.bikes_location,
      subFilters: [
        Filter(
          id: 50,
          travelMode: TravelMode.public_transport,
          title: translations.text('filter.values.stations_bike'),
          type: FiltersType.stations_bike,
          requestId: 'MVA',
          iconPath: MetroIcons.mva,
        ),
      ],
    ),
    Filter(
      id: 52,
      travelMode: TravelMode.tier,
      title: translations.text('filter.values.tier'),
      type: FiltersType.tier,
      requestId: 'tier',
      hasInfo: true,
    ),
  ];

  BehaviorSubject<Map<Filter, bool>> _filtersValueController;

  /// Event called whenever the value of a filter changed.
  Stream<Map<Filter, bool>> get onFiltersValueChanged => _filtersValueController.stream;

  /// The current filter->value map.
  Map<Filter, bool> get filtersValue => _filtersValueController.value;

  BehaviorSubject<int> subfiltersStateController = BehaviorSubject<int>.seeded(3);

  /// Event called whenever the value of a filter changed.
  Stream<int> get onSubfiltersStateChanged => subfiltersStateController.stream;

  /// The current filter->value map.
  int get subfiltersState => subfiltersStateController.value;

  BehaviorSubject<List<Filter>> customFiltersValueController;

  Stream<List<Filter>> get onCustomFiltersValueChanged => customFiltersValueController.stream;

  List<Filter> get customFiltersValues => customFiltersValueController.value;

  // Display filter button
  BehaviorSubject<bool> buttonFilterValueController = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get onButtonFilterValueChanged => buttonFilterValueController.stream;
  bool get buttonFilterValue => buttonFilterValueController.value;

  BehaviorSubject<bool> filterPageController = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get onfilterPageChanged => filterPageController.stream;
  bool get filterPageValue => filterPageController.value;

  FiltersBloc() {
    Map<Filter, bool> filterValueMap = Map();

    // Fetching data from prefs
    for (Filter filter in filters) {
      _setFilterToPrefs(filter.type, false);
      filterValueMap[filter] = false;

      if (filter.hasSubFilters) {
        for (Filter subFilter in filter.subFilters) {
          _setFilterToPrefs(filter.type, false);
          filterValueMap[subFilter] = false;
        }
      }
    }

    _filtersValueController = BehaviorSubject<Map<Filter, bool>>.seeded(filterValueMap);

    for (MapEntry<Filter, bool> filtersValue in filtersValue.entries) {
      _controllers[filtersValue.key.type] ??= createController(filtersValue.key, test: "filter_bloc");
      setFilterValue(filtersValue.key, false);
    }
  }

  Map<Filter, bool> busMapFilters = {};
  Map<Filter, bool> bikeMapFilters = {};
  Map<Filter, bool> carMapFilters = {};
  Map<Filter, bool> customMapFilters = {};
  Map<Filter, bool> oldMapFilters = {};

  List<int> bus = [1, 2, 3, 4, 7, 10, 11, 12, 13, 8];
  List<int> bike = [41, 42, 43, 44, 47, 48, 49, 50];
  List<int> car = [14, 15, 16, 21, 23, 24, 25, 26, 27, 28, 29, 30, 36, 37, 38, 39];
  List<int> custom = [];

  @override
  void initState() {
    _bloc = BlocProvider.master<FiltersBloc>();
    List<String> savedStrList = PrefService.getStringList('customFiltersTest');
    List<Filter> filtersList = [];
    _bloc.customFiltersValueController = BehaviorSubject<List<Filter>>.seeded(filtersList);
    if (savedStrList != null) {
      List<int> intCustomList = savedStrList.map((i) => int.parse(i)).toList();
      for (Filter f in filters) {
        if (intCustomList.contains(f.id)) {
          filtersList.add(f);
        }
      }
      _bloc.customFiltersValueController.sink.add(filtersList.toSet().toList());
    }

  }

  void setCustomFilterValue(int filters, bool valueFilters) async {
    Map<Filter, bool> filtersMap = {};

    if (filters == 1) {
      filtersMap = busMapFilters;
    } else if (filters == 2) {
      filtersMap = bikeMapFilters;
    } else if (filters == 3) {
      filtersMap = carMapFilters;
    } else if (filters == 4) {
      filtersMap = customMapFilters;
    }

    if (oldMapFilters != null) {
      oldMapFilters.forEach((key, value) {
        onValueChange(key, false, "calledFrom");
      });
      filtersMap.forEach((key, value) {
        onValueChange(key, valueFilters, "calledFrom");
      });
    } else {
      filtersMap.forEach((key, value) {
        onValueChange(key, valueFilters, "calledFrom");
      });
    }

    oldMapFilters.addAll(filtersMap);
  }

  void fuckingTest() {}

  bool getFiltersValueFromPrefs(Filter filter) {
    int result = 0;
    int result2 = 0;
    if (filter.hasSubFilters) {
      for (int i = 0; i < filter.subFilters.length; i++) {
        if (_getFilterFromPrefsTest(filter.subFilters[i].type, false)) {
          result++;
        } else {
          result2++;
        }
      }
      // All items are enabled
      if (result == filter.subFilters.length) {
        return true;
      }
      // All items are disabled
      else if (result2 == filter.subFilters.length) {
        return false;
       }
      // Part of items are enabled
      else {
        return null;
     }
    }
    return null;
  }

  bool isFilterSelected(Filter filter) => filtersValue[filter];

  changeButtonFilterValue(bool b) {
    _bloc.buttonFilterValueController.sink.add(b);
  }

  changeFilterPageValue(bool b) {
    _bloc.filterPageController.sink.add(b);
  }

  CheckboxTileController createController(Filter filter, {String test = ""}) {
    if (_controllers[filter.type] != null) return _controllers[filter.type];
    CheckboxTileController controller = CheckboxTileController(filtersValue[filter]);
    controller.onValueChanged = (value) {
      _bloc.onValueChange(filter, value, "createController");
    };
    _controllers[filter.type] = controller;
    return controller;
  }

  @override
  void dispose() {
    _filtersValueController.close();
    subfiltersStateController.close();
    customFiltersValueController.close();
    buttonFilterValueController.close();
    filterPageController.close();
  }

  void setFilterValue(Filter filter, bool value) {
    // Process and send data using the stream
    Map<Filter, bool> map = filtersValue;
    map[filter] = value;
    _filtersValueController.sink.add(map);
  }

  bool getValueFromController(Filter filter) {
    return _controllers[filter.type].value;
  }

  bool getFilterValue(FiltersType type) {
    return filtersValue.entries.singleWhere((i) {
      return i.key.type == type;
    }).value;
  }

  Filter getFilterFromPropType(PropertiesType propertiesType) {
    switch (propertiesType) {
      case PropertiesType.par:
      case PropertiesType.pkg:
      case PropertiesType.mvc:
        return filters[FiltersPosition.parkings.index];
      case PropertiesType.clusters:
      case PropertiesType.stops:
        return filters[FiltersPosition.stops.index];
      case PropertiesType.citiz:
      case PropertiesType.citizyea:
        return filters[FiltersPosition.selfserve.index];
      case PropertiesType.tier:
        return filters[FiltersPosition.tier.index];
      case PropertiesType.veloservice:
        return filters[FiltersPosition.veloservice.index];
      case PropertiesType.pony:
        return filters[FiltersPosition.pony.index];
      case PropertiesType.recharge:
      case PropertiesType.irve:
      case PropertiesType.hydrogene:
      case PropertiesType.gpl:
      case PropertiesType.gnv:
        return filters[FiltersPosition.energy.index];
      case PropertiesType.pointCov:
      case PropertiesType.parkingCov:
      case PropertiesType.autostop:
        return filters[FiltersPosition.carpooling.index];
      case PropertiesType.depositaire:
      case PropertiesType.agenceM:
      case PropertiesType.pointService:
      case PropertiesType.mva:
        return filters[FiltersPosition.stations.index];
      case PropertiesType.cam:
      case PropertiesType.rue:
      case PropertiesType.lieux:
      case PropertiesType.undefined:
        return null;
    }
    return null;
  }

  void customFilters(List<Filter> filter) {
    _bloc.customFiltersValueController.sink.add(filter);
  }

  void onValueChange(Filter filter, bool value, String calledFrom) {
    _bloc.setFilterValue(filter, value);

    switch (filter.type) {
      case FiltersType.stops:
        _controllers[FiltersType.stops].changeValue(value);
        break;
      case FiltersType.trams:
        _controllers[FiltersType.trams].changeValue(value);
        break;
      case FiltersType.c38:
        _controllers[FiltersType.c38].changeValue(value);
        break;
      case FiltersType.buses:
        if (value == null) break;
        _controllers[FiltersType.buses_chrono].changeValue(value);
        _controllers[FiltersType.buses_proximo].changeValue(value);
        _controllers[FiltersType.buses_flexo].changeValue(value);
        break;
      case FiltersType.tier:
        if (value == null) break;
        _controllers[FiltersType.tier].changeValue(value);
        break;
      case FiltersType.pony:
        if (value == null) break;
        _controllers[FiltersType.pony].changeValue(value);
        break;
      case FiltersType.buses_chrono:
      case FiltersType.buses_proximo:
      case FiltersType.buses_flexo:
        final filter_buses_chrono = filtersValue.keys.firstWhere((e) => e.type == FiltersType.buses_chrono);
        final filter_buses_proximo = filtersValue.keys.firstWhere((e) => e.type == FiltersType.buses_proximo);
        final filter_buses_flexo = filtersValue.keys.firstWhere((e) => e.type == FiltersType.buses_flexo);

        if ((filtersValue[filter_buses_chrono] == filtersValue[filter_buses_proximo]) &&
            (filtersValue[filter_buses_chrono] == filtersValue[filter_buses_flexo])) {
          _controllers[FiltersType.buses].changeValue(value);
        } else {
          _controllers[FiltersType.buses].changeValue(null);
        }
        break;
      case FiltersType.voironnais:
        if (value == null) break;
        _controllers[FiltersType.voironnais_urbaines].changeValue(value);
        _controllers[FiltersType.voironnais_interurbaines].changeValue(value);
        break;
      case FiltersType.voironnais_urbaines:
      case FiltersType.voironnais_interurbaines:
        final filter_voironnais_urbaines = filtersValue.keys.firstWhere((e) => e.type == FiltersType.voironnais_urbaines);
        final filter_voironnais_interurbaines = filtersValue.keys.firstWhere((e) => e.type == FiltersType.voironnais_interurbaines);

        if ((filtersValue[filter_voironnais_urbaines] == filtersValue[filter_voironnais_interurbaines])) {
          _controllers[FiltersType.voironnais].changeValue(value);
        } else {
          _controllers[FiltersType.voironnais].changeValue(null);
        }
        break;
      case FiltersType.gresivaudan:
        if (value == null) break;
        _controllers[FiltersType.gresivaudan_structurante].changeValue(value);
        _controllers[FiltersType.gresivaudan_secondaires].changeValue(value);
        break;
      case FiltersType.gresivaudan_structurante:
      case FiltersType.gresivaudan_secondaires:
        final filter_gresivaudan_structurante = filtersValue.keys.firstWhere((e) => e.type == FiltersType.gresivaudan_structurante);
        final filter_gresivaudan_secondaires = filtersValue.keys.firstWhere((e) => e.type == FiltersType.gresivaudan_secondaires);

        if ((filtersValue[filter_gresivaudan_structurante] == filtersValue[filter_gresivaudan_secondaires])) {
          _controllers[FiltersType.gresivaudan].changeValue(value);
        } else {
          _controllers[FiltersType.gresivaudan].changeValue(null);
        }
        break;
      case FiltersType.parkings:
        if (value == null) break;

        _controllers[FiltersType.parkings_PKG].changeValue(value);
        _controllers[FiltersType.parkings_PAR].changeValue(value);
        break;
      case FiltersType.parkings_PKG:
      case FiltersType.parkings_PAR:
        final filter_parkings_PKG = filtersValue.keys.firstWhere((e) => e.type == FiltersType.parkings_PKG);
        final filter_parkings_PAR = filtersValue.keys.firstWhere((e) => e.type == FiltersType.parkings_PAR);

        if ((filtersValue[filter_parkings_PKG] == filtersValue[filter_parkings_PAR])) {
          _controllers[FiltersType.parkings].changeValue(value);
        } else {
          _controllers[FiltersType.parkings].changeValue(null);
        }
        break;
      case FiltersType.bikes_parking:
        _controllers[FiltersType.parkings_bike].changeValue(value);
        break;
      case FiltersType.parkings_bike:
        _controllers[FiltersType.bikes_parking].changeValue(value);
        break;
      case FiltersType.selfserve:
        if (value == null) break;

        _controllers[FiltersType.selfserve_citiz].changeValue(value);
        _controllers[FiltersType.selfserve_yea].changeValue(value);
        break;
      case FiltersType.selfserve_citiz:
      case FiltersType.selfserve_yea:
        final filter_selfserve_citiz = filtersValue.keys.firstWhere((e) => e.type == FiltersType.selfserve_citiz);
        final filter_selfserve_yea = filtersValue.keys.firstWhere((e) => e.type == FiltersType.selfserve_yea);

        if ((filtersValue[filter_selfserve_citiz] == filtersValue[filter_selfserve_yea])) {
          _controllers[FiltersType.selfserve].changeValue(value);
        } else {
          _controllers[FiltersType.selfserve].changeValue(null);
        }
        break;
      case FiltersType.carpooling:
        if (value == null) break;
        // _controllers[FiltersType.carpooling_charging].changeValue(value);
        _controllers[FiltersType.carpooling_parking].changeValue(value);
        _controllers[FiltersType.carpooling_picking].changeValue(value);
        _controllers[FiltersType.carpooling_stops].changeValue(value);
        break;
      // case FiltersType.carpooling_charging:
      case FiltersType.carpooling_parking:
      case FiltersType.carpooling_picking:
      case FiltersType.carpooling_stops:
        final filter_carpooling_parking = filtersValue.keys.firstWhere((e) => e.type == FiltersType.carpooling_parking);
        final filter_carpooling_picking = filtersValue.keys.firstWhere((e) => e.type == FiltersType.carpooling_picking);
        final filter_carpooling_stops = filtersValue.keys.firstWhere((e) => e.type == FiltersType.carpooling_stops);

        if ((filtersValue[filter_carpooling_parking] == filtersValue[filter_carpooling_picking]) &&
            (filtersValue[filter_carpooling_parking] == filtersValue[filter_carpooling_stops])) {
          _controllers[FiltersType.carpooling].changeValue(value);
        } else {
          _controllers[FiltersType.carpooling].changeValue(null);
        }
        break;
      case FiltersType.energy:
        if (value == null) break;

        _controllers[FiltersType.energy_electric].changeValue(value);
        _controllers[FiltersType.energy_hydrogen].changeValue(value);
        _controllers[FiltersType.energy_GNV].changeValue(value);
        _controllers[FiltersType.energy_GPL].changeValue(value);
        break;
      case FiltersType.energy_electric:
      case FiltersType.energy_hydrogen:
      case FiltersType.energy_GNV:
      case FiltersType.energy_GPL:
        final filterElec = filtersValue.keys.firstWhere((e) => e.type == FiltersType.energy_electric);
        final filterHydro = filtersValue.keys.firstWhere((e) => e.type == FiltersType.energy_hydrogen);
        final filterGNV = filtersValue.keys.firstWhere((e) => e.type == FiltersType.energy_GNV);
        final filterGPL = filtersValue.keys.firstWhere((e) => e.type == FiltersType.energy_GPL);

        if ((filtersValue[filterElec] == filtersValue[filterHydro]) &&
            (filtersValue[filterElec] == filtersValue[filterGNV]) &&
            (filtersValue[filterElec] == filtersValue[filterGPL])) {
          _controllers[FiltersType.energy].changeValue(value);
          final filterEnergy = filtersValue.keys.firstWhere((e) => e.type == FiltersType.energy);
        } else {
          _controllers[FiltersType.energy].changeValue(null);
        }
        break;
      case FiltersType.stations:
        if (value == null) break;
        _controllers[FiltersType.stations_agenceM].changeValue(value);
        _controllers[FiltersType.stations_TAG].changeValue(value);
        _controllers[FiltersType.stations_transisere].changeValue(value);
        break;
      case FiltersType.stations_agenceM:
      case FiltersType.stations_TAG:
      case FiltersType.stations_transisere:
        final filter_stations_agenceM = filtersValue.keys.firstWhere((e) => e.type == FiltersType.stations_agenceM);
        final filter_stations_TAG = filtersValue.keys.firstWhere((e) => e.type == FiltersType.stations_TAG);
        final filter_stations_transisere = filtersValue.keys.firstWhere((e) => e.type == FiltersType.stations_transisere);

        if ((filtersValue[filter_stations_agenceM] == filtersValue[filter_stations_TAG]) &&
            (filtersValue[filter_stations_agenceM] == filtersValue[filter_stations_transisere])) {
          _controllers[FiltersType.stations].changeValue(value);
        } else {
          _controllers[FiltersType.stations].changeValue(null);
        }
        break;
      case FiltersType.trafic:
        if (value == null) break;
        _controllers[FiltersType.trafic_state].changeValue(value);
        _controllers[FiltersType.trafic_event].changeValue(value);
        _controllers[FiltersType.trafic_cam].changeValue(value);
        break;
      case FiltersType.bikes_location:
        if (value == null) break;
        _controllers[FiltersType.stations_bike].changeValue(value);
        break;
      case FiltersType.stations_bike:
        if (value == null) break;
        _controllers[FiltersType.bikes_location].changeValue(value);
        break;
      case FiltersType.veloservice:
        if (value == null) break;
        _controllers[FiltersType.veloservice].changeValue(value);
        break;
      case FiltersType.trafic_state:
      case FiltersType.trafic_event:
      case FiltersType.trafic_cam:
        final filter_trafic_state = filtersValue.keys.firstWhere((e) => e.type == FiltersType.trafic_state);
        final filter_trafic_event = filtersValue.keys.firstWhere((e) => e.type == FiltersType.trafic_event);
        final filter_trafic_cam = filtersValue.keys.firstWhere((e) => e.type == FiltersType.trafic_cam);

        if ((filtersValue[filter_trafic_event] == filtersValue[filter_trafic_state]) &&
            (filtersValue[filter_trafic_event] == filtersValue[filter_trafic_cam])) {
          _controllers[FiltersType.trafic].changeValue(value);
        } else {
          _controllers[FiltersType.trafic].changeValue(null);
        }
        break;
      case FiltersType.bikes:
        if (value == null) break;
        _controllers[FiltersType.bikes_chrono].changeValue(value);
        _controllers[FiltersType.bikes_difficile].changeValue(value);
        _controllers[FiltersType.bikes_amenage].changeValue(value);
        _controllers[FiltersType.bikes_nonamenage].changeValue(value);
        _controllers[FiltersType.bikes_tempo].changeValue(value);
        _controllers[FiltersType.bikes_verte].changeValue(value);
        break;
      case FiltersType.bikes_amenage:
      case FiltersType.bikes_chrono:
      case FiltersType.bikes_difficile:
      case FiltersType.bikes_nonamenage:
      case FiltersType.bikes_tempo:
      case FiltersType.bikes_verte:
        final filter_bikes_amenage = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_amenage);
        final filter_bikes_chrono = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_chrono);
        final filter_bikes_difficile = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_difficile);
        final filter_bikes_nonamenage = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_nonamenage);
        final filter_bikes_tempo = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_tempo);
        final filter_bikes_verte = filtersValue.keys.firstWhere((e) => e.type == FiltersType.bikes_verte);

        if ((filtersValue[filter_bikes_amenage] == filtersValue[filter_bikes_chrono]) &&
            (filtersValue[filter_bikes_amenage] == filtersValue[filter_bikes_difficile]) &&
            (filtersValue[filter_bikes_amenage] == filtersValue[filter_bikes_nonamenage]) &&
            (filtersValue[filter_bikes_amenage] == filtersValue[filter_bikes_tempo]) &&
            (filtersValue[filter_bikes_amenage] == filtersValue[filter_bikes_verte])) {
          _controllers[FiltersType.bikes].changeValue(value);
        } else {
          _controllers[FiltersType.bikes].changeValue(null);
        }
        break;
      default:
        break;
    }
  }

  void addCustomFilters(List<int> customList) {
    List<int> intProductListOriginal = customList;
    List<String> strList = intProductListOriginal.map((i) => i.toString()).toList();
    PrefService.setStringList("customFiltersTest", strList);
  }

  // Since Checkbox uses tristate, I was no longer able to store and use bool, and had to use int instead.
  void _setFilterToPrefs(FiltersType type, bool value) {
    int transformedValue;
    switch (value) {
      case true:
        transformedValue = 1;
        break;
      case false:
        transformedValue = -1;
        break;
      default:
        transformedValue = 0;
        break;
    }
    PrefService.setInt('filter_' + type.toString(), transformedValue);
  }

  // Since Checkbox uses tristate, I was no longer able to store and use bool, and had to use int instead.
  bool _getFilterFromPrefs(FiltersType type, bool defaultValue) {
    // 1) If there is no stored data (fresh install or no prefs), I define default values (from json config)
    if (PrefService.getInt('filter_' + type.toString()) == null) _setFilterToPrefs(type, defaultValue);
    // 2) Then we set the default switch value to the stored data
    switch (PrefService.getInt('filter_' + type.toString())) {
      case 1:
        return true;
      case -1:
        return false;
      default:
        return null;
    }
  }

  bool _getFilterFromPrefsTest(FiltersType type, bool defaultValue) {
    // 1) If there is no stored data (fresh install or no prefs), I define default values (from json config)
    if (PrefService.getInt('filter_' + type.toString()) == null) _setFilterToPrefs(type, false);
    // 2) Then we set the default switch value to the stored data
    switch (PrefService.getInt('filter_' + type.toString())) {
      case 1:
        return true;
      case -1:
        return false;
      default:
        return null;
    }
  }
}

enum FiltersPosition {
  stops,
  trams,
  buses,
  c38,
  voironnais,
  gresivaudan,
  parkings,
  selfserve,
  carpooling,
  energy,
  stations,
  trafic,
  bikes,
  tier,
  pony,
  veloservice
}
