import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';

import 'package:metro/global/metro_icons.dart';

class TierInfoPage extends TraceableStatefulWidget {
  TierInfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _TierInfoPage createState() => _TierInfoPage();
}

class _TierInfoPage extends State<TierInfoPage> {

  @override
  Widget build(BuildContext context) {

    final List<Map<String, dynamic>> infosBike = [
      {
        'title': 'Tier',
        'imagePath': 'assets/img_trottinette_TIER.png',
        'description': "500 trottinettes ont été déployées. Cette offre sera progressivement renforcée pour atteindre 1 000 véhicules par la suite. \n\nBridée à 20 km/h pour un poids total de 29 kg, la trottinette Tier dispose d’une autonomie de 50 km. Dotée d’un frein avant moteur, d’un frein arrière à tambour et de doubles suspensions, elle assure un freinage en douceur.Une sonnette est intégrée au guidon, aucun câble n’est apparent pour éviter les manipulations et le vandalisme. Une double béquille renforcée permet de s’assurer de la stabilité du stationnement.Le fin de location ne peut s'effectuer qu'aux emplacements de stationnement dédiés. \n\nTarifs : 1,00 € par déblocage + 0,15 € / min de trajet.",
      }
    ];

    return Scaffold(
        backgroundColor: globalElevations.inverseColor.backgroundColor,
        appBar: AppBar(
          backgroundColor: globalElevations.e16dp.backgroundColor,
          iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
          elevation: 4,
          title: Text(
            "Location",
            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ListView.builder(
                  itemCount: infosBike.length,
                  itemBuilder: (context, index){
                    var info = infosBike[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right:8.0, left:8.0),
                          child: ExpandablePanel(
                            controller: ExpandableController(initialExpanded: true),
                            headerAlignment: ExpandablePanelHeaderAlignment.center,
                            tapBodyToCollapse: true,
                            header: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'assets/icons/kick-scooter-icon.svg',
                                    height: 24,
                                    semanticsLabel: 'Tier logo',
                                    color: Colors.white,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                    child: Text(info['title'], style: TextStyle(fontSize: 16,),),
                                  )
                                ],
                              ),
                            ),
                            expanded: Container(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left : 8.0, right: 8.0),
                                    child: Image.asset(info['imagePath'],
                                      scale: 0.8,),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, right : 8.0, top: 16.0, bottom: 16.0),
                                    child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(info['description'],
                                            style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, height: 1.5, fontSize: 15))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(height: 0.8, color: Colors.grey)
                      ],
                    );
                  }
              ),
            ),
          ],
        ));
  }
}
