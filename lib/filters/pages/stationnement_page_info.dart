import 'dart:async';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';

import 'package:metro/global/metro_icons.dart';

class StationnementInfoPage extends TraceableStatefulWidget {
  StationnementInfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _StationnementInfoPage createState() => _StationnementInfoPage();
}

class _StationnementInfoPage extends State<StationnementInfoPage> {

  @override
  Widget build(BuildContext context) {

    final List<Map<String, dynamic>> infosBike = [
      {
        'title': 'Consignes collectives',
        'imagePath_1': 'assets/stationnement/collective_01.png',
        'imagePath_2': 'assets/stationnement/collective_02.png',
        'imagePath_3': 'assets/stationnement/collective_03.png',
        'imagePath_4': 'assets/stationnement/collective_04.png',
        'description': 'Des consignes collectives sont répartis dans toute l’agglomération, la vallée du Grésivaudan et le pays Voironnais. Le plus souvent au niveau des P+R et des gares. Ces consignes nécessite un abonnement.',
      },
      {
        'title': 'Tempo Velo',
        'imagePath_1': 'assets/stationnement/individuelle_01.png',
        'imagePath_2': 'assets/stationnement/individuelle_02.png',
        'imagePath_3': 'assets/stationnement/individuelle_03.png',
        'imagePath_4': 'assets/stationnement/individuelle_04.png',
        'description': 'Ces consignes proposent un nombre limité de places individuelles de stationnement fermées (en moyenne entre 5 et 10 places). Pour la plupart d’entre elles, il est nécessaire de posséder un abonnement. Seuls les consignes aux gares de Lancey et Goncelin sont gratuites.',
      },
    ];

    return Scaffold(
        backgroundColor: globalElevations.inverseColor.backgroundColor,
        appBar: AppBar(
          backgroundColor: globalElevations.e16dp.backgroundColor,
          iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
          elevation: 4,
          title: Text(
            "STATIONNEMENTS",
            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ListView.builder(
                  itemCount: infosBike.length,
                  itemBuilder: (context, index){
                    var info = infosBike[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right:8.0, left:8.0),
                          child: ExpandablePanel(
                            headerAlignment: ExpandablePanelHeaderAlignment.center,
                            tapBodyToCollapse: true,
                            header: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(MetroIcons.mvc),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                    child: Text(info['title'], style: TextStyle(fontSize: 16,),),
                                  )
                                ],
                              ),
                            ),
                            expanded: Container(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.5 - 16.00,
                                        child: Image.asset(info['imagePath_1'], fit: BoxFit.cover),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.5 - 16.00,
                                        child: Image.asset(
                                          info['imagePath_2'],
                                          fit: BoxFit.cover,
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.5 - 16.00,
                                        child: Image.asset(info['imagePath_3'], fit: BoxFit.cover),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.5 - 16.00,
                                        child: Image.asset(
                                          info['imagePath_4'],
                                          fit: BoxFit.cover,
                                        ),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, right : 8.0, top: 16.0, bottom: 16.0),
                                    child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(info['description'],
                                          style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, height: 1.5, fontSize: 15))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(height: 0.8, color: Colors.grey)
                      ],
                    );
                  }
              ),
            ),
          ],
        ));
  }
}
