import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';

class BikesServiceInfoPage extends TraceableStatefulWidget {
  BikesServiceInfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _BikesInfoPage createState() => _BikesInfoPage();
}

class _BikesInfoPage extends State<BikesServiceInfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globalElevations.inverseColor.backgroundColor,
      appBar: AppBar(
        backgroundColor: globalElevations.e16dp.backgroundColor,
        iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
        elevation: 4,
        title: Text(
          "Aire de services",
          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, left: 8.0),
                  child: ExpandablePanel(
                    controller: ExpandableController(initialExpanded: true),
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    tapBodyToCollapse: true,
                    header: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Aire de services",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      )
                    ),
                    expanded: Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Image.asset(
                              'assets/bikes/aire_de_service.png',
                              scale: 0.8,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 16.0, bottom: 24.0),
                            child: Container(
                                alignment: Alignment.topLeft,
                                child: Text("Des aires de services sont mis à disposition des cyclistes. Elles sont dotées de plan du quartier et du réseau, d'une pompe à vélo  et d'un espace de repos disposant d'un banc.",
                                    style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, height: 1.5, fontSize: 15))),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(height: 0.8, color: Colors.grey)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
