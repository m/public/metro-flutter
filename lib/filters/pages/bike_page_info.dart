import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';

class BikesInfoPage extends TraceableStatefulWidget {
  BikesInfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _BikesInfoPage createState() => _BikesInfoPage();
}

class _BikesInfoPage extends State<BikesInfoPage> {

  @override
  Widget build(BuildContext context) {

    final List<Map<String, dynamic>> infosBike = [
      {
        'color': Colors.yellow,
        'size': 6.0,
        'dashed': false,
        'title': 'Tempo Velo',
        'imagePath': 'assets/bikes/tempo_velo.png',
        'description': 'Pistes temporaires afin d’éviter d’encombrer les transports en commun et permettre le respect des distanciations sociales',
      },
      {
        'color': Colors.purple,
        'size': 6.0,
        'dashed': false,
        'title': 'Chrono Velo',
        'imagePath': 'assets/bikes/chrono-velo.png',
        'description': 'Axes de liaisons majeurs offrant des itinéraires directs, confortables et sécurisés',
      },
      {
        'color': Colors.green,
        'size': 6.0,
        'dashed': false,
        'title': 'Voies Vertes',
        'imagePath': 'assets/bikes/voie_verte.png',
        'description': 'À la différence d’une piste cyclable qui est réservée uniquement aux cyclistes, une voie verte est un aménagement réservée aux déplacements non motorisés : vélos, piétons, rollers et parfois cavaliers',
      },
      {
        'color': Colors.green,
        'size': 4.0,
        'dashed': false,
        'title': 'Voies Aménagées',
        'imagePath': 'assets/bikes/voie_amenage.png',
        'description': 'Proximité des voitures et sécurisé',
      },
      {
        'color': Colors.green,
        'size': 6.0,
        'dashed': true,
        'title': 'Voies non aménagées',
        'imagePath': 'assets/bikes/voie_non_amenage.png',
        'description': 'Proximité des voitures et sécurisé',
      },
      {
        'color': Colors.orange,
        'size': 6.0,
        'dashed': true,
        'title': 'Voies difficiles',
        'imagePath': 'assets/bikes/voie_difficile.png',
        'description': 'Circulation à proximité des voitures, absence de marquage, ou travaux qui peuvent rendre plus difficiles la progression des cyclistes',
      },
    ];

    return Scaffold(
        backgroundColor: globalElevations.inverseColor.backgroundColor,
        appBar: AppBar(
          backgroundColor: globalElevations.e16dp.backgroundColor,
          iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
          elevation: 4,
          title: Text(
            "Pistes cyclables",
            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ListView.builder(
                  itemCount: infosBike.length,
                  itemBuilder: (context, index){
                    var info = infosBike[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right:8.0, left:8.0),
                          child: ExpandablePanel(
                            controller: ExpandableController(initialExpanded: true),
                            headerAlignment: ExpandablePanelHeaderAlignment.center,
                            tapBodyToCollapse: true,
                            header: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  !info['dashed']
                                      ? Container(height: info['size'], width: 30, color: info['color'])
                                      : Text("••••", style: TextStyle(color: info['color'], fontSize: 20)),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                    child: Text(info['title'], style: TextStyle(fontSize: 16,),),
                                  )
                                ],
                              ),
                            ),
                            expanded: Container(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left : 8.0, right: 8.0),
                                    child: Image.asset(info['imagePath'],
                                      scale: 0.8,),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, right : 8.0, top: 16.0, bottom: 24.0),
                                    child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(info['description'],
                                          style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, height: 1.5, fontSize: 15))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(height: 0.8, color: Colors.grey)
                      ],
                    );
                  }
              ),
            ),
          ],
        ));
  }
}
