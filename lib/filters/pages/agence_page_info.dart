import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';

import 'package:metro/global/metro_icons.dart';

class AgenceInfoPage extends TraceableStatefulWidget {
  AgenceInfoPage({Key key}) : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _AgenceInfoPage createState() => _AgenceInfoPage();
}

class _AgenceInfoPage extends State<AgenceInfoPage> {

  @override
  Widget build(BuildContext context) {

    final List<Map<String, dynamic>> infosBike = [
      {
        'title': 'Agence Metrovélo',
        'imagePath': 'assets/metrovelo/agence.png',
        'description': 'Sur l’agglomération Grenobloise vous pouvez louer un Métrovélo pour une courte ou longue durée dans les agences Métrovélo',
      }
    ];

    return Scaffold(
        backgroundColor: globalElevations.inverseColor.backgroundColor,
        appBar: AppBar(
          backgroundColor: globalElevations.e16dp.backgroundColor,
          iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
          elevation: 4,
          title: Text(
            "Location",
            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: ListView.builder(
                  itemCount: infosBike.length,
                  itemBuilder: (context, index){
                    var info = infosBike[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right:8.0, left:8.0),
                          child: ExpandablePanel(
                            headerAlignment: ExpandablePanelHeaderAlignment.center,
                            tapBodyToCollapse: true,
                            header: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(MetroIcons.mva),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                    child: Text(info['title'], style: TextStyle(fontSize: 16,),),
                                  )
                                ],
                              ),
                            ),
                            expanded: Container(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left : 8.0, right: 8.0),
                                    child: Image.asset(info['imagePath'],
                                      scale: 0.8,),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, right : 8.0, top: 16.0, bottom: 16.0),
                                    child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(info['description'],
                                          style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, height: 1.5, fontSize: 15))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(height: 0.8, color: Colors.grey)
                      ],
                    );
                  }
              ),
            ),
          ],
        ));
  }
}
