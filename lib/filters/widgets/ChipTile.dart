import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottom_filters.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:preferences/preference_service.dart';
import 'package:metro/filters/models/CheckboxTileController.dart';

class ChipTileParameters {
  final String title;
  final bool value;
  final bool haveIcon;
  final IconData iconPath;
  final Color chipColor;
  Color chipTextColor;
  final Filter filter;
  final Filter filterParent;

  ChipTileParameters(this.title, this.chipColor,
      {this.value = true,
      this.haveIcon = false,
      this.iconPath,
      this.filter,
      this.filterParent});
}

class ChipTile extends StatefulWidget {
  final ChipTileParameters parameters;
  final CheckboxTileController controller;

  ChipTile({@required this.parameters, @required this.controller});

  @override
  State<StatefulWidget> createState() => _ChipTileState();
}

class _ChipTileState extends State<ChipTile> {
  bool _isSelected;
  Color chipTextColor;
  FiltersBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<FiltersBloc>(context);
    _isSelected = widget.parameters.value;

    try {
      if (widget.controller != null) {
        // Notify the controller to get the default value
        widget.controller.changeValue(_isSelected, notify: false);
        // Listen to controller callbacks
        //widget.controller.onValueChanged = (value) => _changeValue(value);
      }
    } catch (e) {
      print(e);
    }
  }

  void _changeValue(bool value) {
    try {
      if (widget.controller != null) widget.controller.changeValue(value);
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        if (mounted) setState(() => _isSelected = value);
        if (widget.controller?.onValueChanged != null)
          widget.controller.onValueChanged(_isSelected);
      });
    } catch (e) {
      print("********************* CHANGE VALUE ERROR " + e);
    }
  }

  @override
  Widget build(BuildContext context) {
    chipTextColor = globalElevations.fstTextColor.backgroundColor;

    Widget icons;

    return StreamBuilder<Object>(
        stream: BlocProvider.of<FiltersBloc>(context).onFiltersValueChanged,
        initialData: BlocProvider.of<FiltersBloc>(context)
            .filtersValue[widget.parameters.filter],
        builder: (context, snapshot) {
          if (widget.parameters.haveIcon) {
            icons = Icon(widget.parameters.iconPath,
                color: BlocProvider.of<FiltersBloc>(context)
                        .filtersValue[widget.parameters.filter]
                    ? Colors.black87
                    : chipTextColor);
          } else if (widget.parameters.filter.legendeIconPath != null) {
            icons = Image.asset(widget.parameters.filter.legendeIconPath);
          } else {
            icons = null;
          }
          return ChoiceChip(
            padding: const EdgeInsets.only(left: 8, right: 8),
            label: Text(widget.parameters.title),
            avatar: icons,
            labelStyle: TextStyle(
                color: BlocProvider.of<FiltersBloc>(context)
                        .filtersValue[widget.parameters.filter]
                    ? Colors.black87
                    : chipTextColor),
            selected: snapshot.data != null
                ? BlocProvider.of<FiltersBloc>(context)
                    .filtersValue[widget.parameters.filter]
                : _isSelected,
            onSelected: (value) {
              if (mounted)
                setState(() {
                  BlocProvider.of<FiltersBloc>(context).onValueChange(
                      widget.parameters.filter, value, "ChipTile");
                  chipTextColor = Colors.redAccent;
                  if (value == null) value = !_isSelected;
                  _changeValue(value);
                  BottomSheetFiltersBody.customFilters
                      .add(widget.parameters.filter.id);
                  checkIfAllSubfiltersHaveSameState(
                      widget.parameters.filterParent);
                });
            },
            selectedColor: widget.parameters.chipColor,
          );
        });
  }

  checkIfAllSubfiltersHaveSameState(Filter filter) {
    int result = 0;
    int result2 = 0;
    if (filter.hasSubFilters) {
      for (int i = 0; i < filter.subFilters.length; i++) {
        if (_bloc.filtersValue[filter.subFilters[i]]) {
          result++;
        } else {
          result2++;
        }
      }
      // All items are enabled
      if (result == filter.subFilters.length) {
        BlocProvider.of<FiltersBloc>(context)
            .subfiltersStateController
            .sink
            .add(0);
        BlocProvider.of<FiltersBloc>(context).onValueChange(
            widget.parameters.filterParent,
            true,
            "checkIfAllSubfiltersHaveSameState1");
        PrefService.setString(
            Consts.preferencesFilterIdSelected, filter.id.toString());
      }
      // All items are disabled
      else if (result2 == filter.subFilters.length) {
        BlocProvider.of<FiltersBloc>(context)
            .subfiltersStateController
            .sink
            .add(1);
        BlocProvider.of<FiltersBloc>(context).onValueChange(
            widget.parameters.filterParent,
            false,
            "checkIfAllSubfiltersHaveSameState2");
        PrefService.setString(
            Consts.preferencesFilterIdSelected, filter.id.toString());
      }
      // Part of items are enabled
      else {
        BlocProvider.of<FiltersBloc>(context)
            .subfiltersStateController
            .sink
            .add(2);
        BlocProvider.of<FiltersBloc>(context).onValueChange(
            widget.parameters.filterParent,
            null,
            "checkIfAllSubfiltersHaveSameState3");
        PrefService.setString(
            Consts.preferencesFilterIdSelected, filter.id.toString());
      }
    }
  }
}
