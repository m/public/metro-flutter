import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/consts.dart';
import 'package:flutter/material.dart';
import 'package:metro/filters/models/CheckboxTileParameters.dart';
import 'package:metro/filters/models/CheckboxTileController.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/bottomsheet/contents/bottom_filters.dart';
import 'package:metro/filters/pages/bike_page_info.dart';
import 'package:metro/filters/pages/bike_service_page_info.dart';
import 'package:metro/filters/pages/tier_page_info.dart';
import 'package:preferences/preference_service.dart';
import 'package:metro/map/pages/map_page.dart';

class CheckboxTile extends StatefulWidget {
  final CheckboxTileParameters parameters;
  final CheckboxTileController controller;

  const CheckboxTile({Key key, @required this.parameters, this.controller})
      : super(key: key);

  @override
  _CheckboxTileState createState() => _CheckboxTileState();
}

class _CheckboxTileState extends State<CheckboxTile> {
  bool _isSelected;
  FiltersBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<FiltersBloc>(context);

    // Get the initial value
    _isSelected = widget.parameters.value;

    try {
      if (widget.controller != null) {
        // Notify the controller to get the default value
        widget.controller.changeValue(_isSelected, notify: false);
        // Listen to controller callbacks
        widget.controller.onValueChanged = (value) => _changeValue(value);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      child: Container(
        color: globalElevations.e02dp.backgroundColor,
        child: Row(
          children: <Widget>[
            StreamBuilder<int>(
                stream: BlocProvider.of<FiltersBloc>(context)
                    .onSubfiltersStateChanged,
                initialData: 3,
                builder: (context, snapshot) {
                  return Checkbox(
                    tristate: true,
                    value: getValue(snapshot.data, widget.parameters),
                    checkColor: globalElevations.inverseColor.backgroundColor,
                    activeColor: globalElevations.blueColor.backgroundColor,
                    onChanged: (bool value) {
                      if (value == null) value = !_isSelected;
                      _changeValue(value);
                      value
                          ? FiltersHeader.customFilterList
                              .add(widget.parameters.filter)
                          : FiltersHeader.customFilterList
                              .remove(widget.parameters.filter);
                      BlocProvider.of<FiltersBloc>(context).onValueChange(
                          widget.parameters.filter,
                          value,
                          "filters_page:Checkbox:onChanged");
                    },
                  );
                }),
            Expanded(
              child: Row(
                children: <Widget>[
                  Text(
                    widget.parameters.text,
                    style: TextStyle(
                        fontSize: 16,
                        color: globalElevations.sndTextColor.backgroundColor),
                  ),
                  SizedBox(width: 8),
                  widget.parameters.filter.hasInfo
                      ? GestureDetector(
                          onTap: () {
                            switch (widget.parameters.filter.id) {
                              case 40:
                                return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BikesInfoPage()));
                              case 54:
                                return Navigator.push(context, MaterialPageRoute(builder: (context) => BikesServiceInfoPage()));
//                              case 47:
//                                return Navigator.push(context, MaterialPageRoute(builder: (context) => StationnementInfoPage()));
//                              case 49:
//                                return Navigator.push(context, MaterialPageRoute(builder: (context) => AgenceInfoPage()));
                              case 52:
                                return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => TierInfoPage()));
                              default:
                                return Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MapPage()));
                            }
                          },
                          child: Icon(
                            Icons.info_outline,
                            color:
                                globalElevations.sndTextColor.backgroundColor,
                          ))
                      : Container()
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () => _changeValue(!_isSelected),
    );
  }

  void _changeValue(bool value) {
    if (widget.controller != null) widget.controller.changeValue(value);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (mounted) {
        setState(() {
          _isSelected = value;
        });
      }
    });

    if (widget.parameters.onChanged != null)
      widget.parameters.onChanged(_isSelected);
  }

  bool getValue(int snapshotData, CheckboxTileParameters parameters) {
    if (parameters.filter.hasSubFilters) {
      if (PrefService.getString(Consts.preferencesFilterIdSelected) != null) {
        if (widget.parameters.filter.id.toString() == PrefService.getString(Consts.preferencesFilterIdSelected)) {
          print("FILTER SNAPSHOT : " + snapshotData.toString());
          if (snapshotData == 0) {
            print("GET VALUE CHECKBOX - 0");
            _isSelected = _bloc.filtersValue[parameters.filter.subFilters[0]];
          } else if (snapshotData == 1) {
            print("GET VALUE CHECKBOX - 1");
            _isSelected = _bloc.filtersValue[parameters.filter.subFilters[0]];
          } else if (snapshotData == 2) {
            print("GET VALUE CHECKBOX - 2");
            _isSelected = null;
          }
          PrefService.setString(Consts.preferencesFilterIdSelected, null);
        }
      }
    }

    return _isSelected;
  }
}
