import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/expandable_panel_filters.dart';
import 'package:metro/filters/widgets/CheckboxTile.dart';

class CheckboxGroupTile extends StatelessWidget {
  final CheckboxTile choice;
  final List<Widget> subChoices;

  CheckboxGroupTile({@required this.choice, @required this.subChoices});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: globalElevations.e02dp.backgroundColor,
      child: ExpandablePanelFilters(
        body: choice,
        showBorder: false,
        detail: Container(
          padding: const EdgeInsets.only(left: 22),
          alignment: Alignment.centerLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: subChoices,
          ),
        ),
      ),
    );
  }
}
