class CheckboxTileController {
  Function(bool) onValueChanged;

  bool get value => _value;

  bool _value;

  CheckboxTileController(this._value);

  void changeValue(bool newValue, {bool notify = true}) {
    if (_value == newValue) return;

    _value = newValue;

    if (onValueChanged != null && notify) onValueChanged(newValue);
  }
}