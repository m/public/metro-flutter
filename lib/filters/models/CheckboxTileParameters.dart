import 'package:metro/filters/models/filter.dart';

class CheckboxTileParameters {
  final String text;
  final bool value;
  final Function(bool) onChanged;
  final Filter filter;

  CheckboxTileParameters(this.text,
      {this.value = true, this.onChanged, this.filter});
}
