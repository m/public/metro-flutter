import 'package:flutter/material.dart';
import 'package:metro/filters/filters_type.dart';

enum TravelMode { public_transport, car, bike, tier }

class Filter {
  final int id;
  final TravelMode travelMode;
  final String title;
  final FiltersType type;
  final bool defaultValue;
  final List<Filter> subFilters;
  final String requestId;
  final IconData iconPath;
  final bool hasInfo;
  final String legendeIconPath;

  Filter({
    @required this.id,
    @required this.travelMode,
    @required this.title,
    @required this.type,
    this.defaultValue = false,
    // this.hasSubFilters
    this.subFilters = const [],
    this.requestId = '',
    // this.hasIcon
    this.iconPath,
    this.hasInfo = false,
    this.legendeIconPath,
  });

  bool get hasSubFilters => subFilters != null && subFilters.isNotEmpty;
  bool get haveIcon => iconPath != null;
}
