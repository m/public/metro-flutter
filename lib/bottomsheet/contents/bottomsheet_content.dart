import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/global/widgets/rounded_button.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/widgets/route_details.dart';

class BottomSheetContent {
  Widget header;
  Key headerKey;
  Widget body;
}

class DefaultBottomSheetHeader extends StatelessWidget {
  final Point point;

  const DefaultBottomSheetHeader({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 16, left: 8.0),
      child: Row(
        children: <Widget>[
          Container(
            width: 56.0,
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: (point.properties.iconData() is IconData)
                  ? Icon(
                      point.properties.iconData(),
                      size: 34,
                      color: Theme.of(context).unselectedWidgetColor,
                    )
                  : SvgPicture.asset(
                      point.properties.iconData(),
                      height: 28,
                      semanticsLabel: 'Realtime logo',
                      color: Theme.of(context).unselectedWidgetColor,
                    ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  point.properties.name,
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
                ),
                Text(
                  (point.properties is TierProperties)
                      ? "Trottinettes électriques en libre-service"
                      : PropertiesTypeExtension.getTypeName(point.properties.type).toUpperCase(),
                  style: TextStyle(color: globalElevations.sndTextColor.backgroundColor, fontSize: 14),
                ),
                PropertiesTypeExtension.getPoiAddress(point) != ''
                    ? Text(PropertiesTypeExtension.getPoiAddress(point), style: TextStyle(color: globalElevations.sndTextColor.backgroundColor))
                    : EmptyCell(),
              ],
            ),
          ),
          SizedBox(width: 16),
          (point.properties.type != PropertiesType.tier &&
                  point.properties.type != PropertiesType.pony &&
                  point.properties.type != PropertiesType.veloservice &&
                  point.properties.type != PropertiesType.citizyea &&
                  point.properties.type != PropertiesType.citiz)
              ? FavoriteButton(data: point)
              : EmptyCell()
        ],
      ),
    );
  }
}

class BottomSheetBodyNearbyLines extends StatefulWidget {
  final LatLng position;

  const BottomSheetBodyNearbyLines({Key key, @required this.position}) : super(key: key);

  @override
  _BottomSheetBodyNearbyLinesState createState() => _BottomSheetBodyNearbyLinesState();
}

class _BottomSheetBodyNearbyLinesState extends State<BottomSheetBodyNearbyLines> {
  List<ReadyLine> lines;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ReadyLine>>(
      future: BlocProvider.master<LinesBloc>().nearbyLines(widget.position),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return SizedBox();

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              translations.text('around_me.nearby_lines'),
              style: TextStyle(fontSize: 14.0, color: globalElevations.sndTextColor.backgroundColor),
            ),
            SizedBox(height: 20),
            Wrap(
                children: snapshot.data
                    .map((rLine) => Padding(
                          padding: const EdgeInsets.only(right: 8.0, bottom: 8.0),
                          child: LineDisplay(
                            line: rLine.line,
                            isBottomSheet: true,
                          ),
                        ))
                    .toList()),
            SizedBox(height: 16),
          ],
        );
      },
    );
  }
}

class BottomSheetBodyItineraryButton extends StatelessWidget {
  final Point point;

  const BottomSheetBodyItineraryButton({Key key, this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RoundedButton(
            iconData: Icons.near_me,
            title: translations.text('modal_bottom_sheet.itinerary').toUpperCase(),
            callback: (c) {
              BlocProvider.of<RoutesBloc>(context).setDestination(point);
              BlocProvider.of<NavigationBloc>(context).changeTab(NavigationCategory.route);
            },
          ),
        ],
      ),
    );
  }
}
