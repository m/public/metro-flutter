import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/disturbances/models/event_model.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';

class BottomSheetEventContent extends BottomSheetContent {
  BottomSheetEventContent(EventModel event) {
    headerKey = GlobalKey();
    header = BottomSheetEventHeader(key: headerKey, event: event);
    body = BottomSheetEventBody(event: event);
  }
}

class BottomSheetEventHeader extends StatelessWidget {
  final EventModel event;

  const BottomSheetEventHeader({Key key, @required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: () => BlocProvider.of<MapBloc>(context).setBottomSheetContent(BottomSheetCategory.traficolor),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Row(
          children: <Widget>[
            // Icon(Icons.close),
            SizedBox(width: 16),
            Expanded(
              child: Text(
                translations.text('events.' + event.type),
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class BottomSheetEventBody extends StatelessWidget {
  final EventModel event;

  const BottomSheetEventBody({Key key, @required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Text(event.texte.replaceAll('|', '\n')),
    );
  }
}
