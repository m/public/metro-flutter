import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/video/video_component.dart';

class BottomSheetCamContent extends BottomSheetContent {
  BottomSheetCamContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetCamBody(key: ValueKey(point.hashCode),point: point);
  }
}

class BottomSheetCamBody extends StatelessWidget {
  final Point point;

  const BottomSheetCamBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    CamProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          VideoComponent('https://datatest.metromobilite.fr/api/cam/video?name=' + properties.code + '.mp4')
        ],
      ),
    );
  }
}
