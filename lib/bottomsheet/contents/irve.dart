import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/irve_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomSheetIrveContent extends BottomSheetContent {
  BottomSheetIrveContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetIrveBody(point: point);
  }
}

class BottomSheetIrveBody extends StatelessWidget {
  final Point point;

  const BottomSheetIrveBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IrveProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 16),
          Column(
            children: BlocProvider.master<IrveBloc>().pdcIrve(properties.code).map((pdc) {
              return _createView(pdc, context);
            }).toList(),
            // children: properties.pdc.map((pdc) {
            //   return _createView(pdc, context);
            // }).toList(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createView(IrvePdcData pdc, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top : 8.0),
      child: IntrinsicHeight(
        child: ExpandablePanel(
          elevation: globalElevations.expandPanelColor,
          showArrow: false,
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Container(
                  height: 54,
                  width: 76,
                  child: SvgPicture.asset(
                    irveIcon(pdc.type_prise),
                    height: 28,
                    color: Theme.of(context).unselectedWidgetColor,
                  ),
                ),
                SizedBox(width: 16),
                Text(pdc.type_prise, style: TextStyle(fontSize: 16),),
              ],
            ),
          ),
          detail: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: globalElevations.e24dp.backgroundColor,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          _openUrl((point.properties as IrveProperties).amenageur == "SEDI"
                              ? 'https://www.eborn.fr/'
                              : 'https://play.google.com/store/apps/details?id=com.byes.cd.alize&hl=fr&gl=US');
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(12, 16, 16, 16),
                          child: Text(
                            'SE CONNECTER',
                            textAlign: TextAlign.start,
                            maxLines: 1,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Theme.of(context).indicatorColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}

String irveIcon(String pdc){

  if (pdc == "CHAdeMO"){
    return 'assets/irve/IRVE_Chademo.svg';
  }else if(pdc == "T2"){
    return 'assets/irve/IRVE_Type_2_2.svg';
  }else if(pdc == "combo"){
    return 'assets/irve/IRVE_Combo.svg';
  }else if(pdc == "E/F + T2"){
    return 'assets/irve/IRVE_EF.svg';
  }else if(pdc == "E/F - T2" || pdc == "EF - T2"){
    return 'assets/irve/IRVE_Type_2.svg';
  }else if(pdc == "CHADEMO - COMBO"){
    return 'assets/irve/IRVE_Chademo.svg';
  }else{
    print("ASSETS IRVE NON RENSEIGNEE");
    return 'assets/irve/IRVE_Type_3.svg';
  }

}

_openUrl(String url) {
  launch(url);
}
