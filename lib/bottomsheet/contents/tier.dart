import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomSheetTierContent extends BottomSheetContent {
  BottomSheetTierContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetTierBody(point: point);
  }
}

class BottomSheetTierBody extends StatelessWidget {
  final Point point;

  const BottomSheetTierBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TierProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 16),
          Column(
            children: <Widget>[
              ExpandablePanel(
                elevation: globalElevations.e02dp,
                showArrow: false,
                body: Row(
                  children: <Widget>[
                    ClipRect(
                      child: Container(
                        height: 88,
                        width: 144,
                        child: Image.asset('assets/img_trottinette_TIER.png', fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left :8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text("Batterie " + (point.properties as TierProperties).batteryLevel.toString() + "%",
                                    maxLines: 1, overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16)),
                              ],
                            ),
                            SizedBox(height: 4),
                            Text(
                              'Vitesse max 20km/h',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(color: Colors.white70),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                  ],
                ),
                detail: Container(
                  color: globalElevations.e24dp.backgroundColor,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            _openUrl((point.properties as TierProperties).deepLink);
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(12, 16, 16, 16),
                            child: Text(
                              "DÉBLOQUER VIA L'APPLI TIER",
                              textAlign: TextAlign.start,
                              maxLines: 1,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color(0xffb49bda),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

_openUrl(String url) {
  launch(url);
}
