import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomSheetYeaContent extends BottomSheetContent {
  BottomSheetYeaContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetYeaBody(point: point);
  }
}

class BottomSheetYeaBody extends StatelessWidget {
  final Point point;

  const BottomSheetYeaBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CitizyeaProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 16),
          Column(
            children: <Widget>[
              ExpandablePanel(
                elevation: globalElevations.e02dp,
                showArrow: false,
                body: Row(
                  children: <Widget>[
                    ClipRect(
                      child: Container(
                        height: 88,
                        width: 144,
                        child: Image.network(
                          BlocProvider.master<WebServices>().getHost() + 'citiz/YEA_3404/photo',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(properties.name.split('/')[0].toString()),
                          SizedBox(height: 4),
                          Row(
                            children: <Widget>[
                              Text('Niveau d\'essence  ${properties.fuelLevel} %',
                                  maxLines: 1, overflow: TextOverflow.ellipsis,
                                  style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                          SizedBox(height: 4),
                          Text(
                            'Plaque : ${properties.licencePlate}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.white70),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: 8),
                  ],
                ),
                detail: Container(
                  color: globalElevations.e24dp.backgroundColor,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            _openUrl('https://citiz.coop/');
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(12, 16, 16, 16),
                            child: Text(
                              'RÉSERVER',
                              textAlign: TextAlign.start,
                              maxLines: 1,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 16,
                                color: Color(0xffb49bda),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

_openUrl(String url) {
  launch(url);
}
