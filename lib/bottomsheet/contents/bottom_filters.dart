import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/filters/widgets/ChipTile.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/widgets/expansion_panel.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/filters/widgets/CheckboxGroupTile.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/filters/models/CheckboxTileParameters.dart';
import 'package:metro/filters/widgets/CheckboxTile.dart';
import 'package:metro/extensions/enum_extensions.dart' show TravelModeModifier;

class FiltersContent extends BottomSheetContent {
  FiltersContent(MapController mapController) {
    headerKey = GlobalKey();
    header = FiltersHeader(key: headerKey);
    body = BottomSheetFiltersBody(mapController: mapController);
  }
}

class BottomSheetFiltersBody extends StatefulWidget {
  final MapController mapController;
  static List<int> customFilters = [];

  const BottomSheetFiltersBody({Key key, @required this.mapController})
      : super(key: key);

  @override
  _BottomSheetFiltersBodyState createState() => _BottomSheetFiltersBodyState();
}

class _BottomSheetFiltersBodyState extends State<BottomSheetFiltersBody> {
  List<Widget> list = List();
  FiltersBloc _bloc;
  List<TravelMode> _data = List<TravelMode>();
  Color chipColor = globalElevations.blueColor.backgroundColor;
  Color chipTextColor;
  bool chipSelected;
  List<Filter> filtersList = new List<Filter>();
  bool checkBoxValue;

  CheckboxTile _createCheckboxTile(Filter filter) {
    return CheckboxTile(
      controller: _bloc.createController(filter, test: "CHECKBOX TILE"),
      parameters: CheckboxTileParameters(
        filter.title,
        // value: _bloc.getFiltersValueFromPrefs(filter),
        // value: _bloc.filtersValue[filter],
        // value: BlocProvider.master<FiltersBloc>().getFiltersValueFromPrefs(filter),
        value: _bloc.getValueFromController(filter),
        filter: filter,
        onChanged: (value) {
          /*_bloc.onValueChange(filter, value, "_createCheckboxTile");
          if (filter != null) {
            _bloc.subfiltersValueController.sink.add(true);
          }*/
        },
      ),
    );
  }

  Widget _createChipTile(Filter filter, Filter filterParent) {
    return ChipTile(
      controller: _bloc.createController(filter, test: "CHIPTILE"),
      parameters: ChipTileParameters(filter.title, chipColor,
          value: _bloc.filtersValue[filter],
          haveIcon: filter.haveIcon,
          iconPath: filter.iconPath,
          filter: filter,
          filterParent: filterParent),
    );
  }

  travelModesCreation() {
    TravelMode publicTransport = new TravelMode(
        id: 1,
        code: "public_transport",
        value: translations.text("transport_type.public"));
    TravelMode bike = new TravelMode(
        id: 2, code: "bike", value: translations.text("transport_type.bike"));
    TravelMode tier = new TravelMode(
        id: 3, code: "tier", value: translations.text("transport_type.tier"));
    TravelMode car = new TravelMode(
        id: 4, code: "car", value: translations.text("transport_type.car"));
    _data.add(publicTransport);
    _data.add(bike);
    _data.add(tier);
    _data.add(car);
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
    travelModesCreation();
    _bloc = BlocProvider.of<FiltersBloc>(context);

    filtersList = _bloc.filters;

  }

  IconData iconPanel(String value) {
    switch (value) {
      case 'Transport en commun':
        return MetroIcons.bus;
      case 'Vélo':
        return MetroIcons.velo;
      case 'Trottinettes':
        return Icons.close;
      case 'Voiture':
        return MetroIcons.voiture;
      default:
        return Icons.directions_bus;
    }
  }

  Widget _buildPanel() {
    return ExpansionPanelListTest(
      expandedHeaderPadding: null,
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanelTest>((TravelMode tm) {
        return ExpansionPanelTest(
          canTapOnHeader: true,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 8,
                      ),
                      (tm.value.toString() != "Trottinettes")
                          ? Icon(
                              iconPanel(tm.value.toString()),
                              color:
                                  globalElevations.fstTextColor.backgroundColor,
                            )
                          : SvgPicture.asset(
                              'assets/icons/kick-scooter-icon.svg',
                              height: 18,
                              semanticsLabel: 'Tier logo',
                              color:
                                  globalElevations.fstTextColor.backgroundColor,
                            ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(tm.value.toString(),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500)),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Wrap(
                      alignment: WrapAlignment.start,
                      children: getFilterListInFunctionOfTravelMode(tm.code)
                          .map((filter) {
                        checkBoxValue = _bloc.filtersValue[filter];
                        /*bool isSubFilterTrue = false;
                        if (filter.hasSubFilters) {
                          for (Filter subFilter in f.subFilters) {
                            if (subFilter.getValue() == true) filterList.add(subFilter.id);
                          }
                        }*/
                        return (_bloc.getValueFromController(filter) == true || _bloc.getValueFromController(filter) == null)
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    right: 8.0, bottom: 16),
                                child: showFilters(filter.title),
                              )
                            : EmptyCell();
                      }).toList(),
                    ),
                  ),
                )
              ],
            );
          },
          body: Column(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:
                    getFilterListInFunctionOfTravelMode(tm.code).map((filter) {
                  if (_bloc.filtersValue[filter] == true)
                    BottomSheetFiltersBody.customFilters.add(filter.id);
                  checkBoxValue = _bloc.filtersValue[filter];
                  if (filter.hasSubFilters) {
                    return CheckboxGroupTile(
                      choice: _createCheckboxTile(filter),
                      subChoices: filter.subFilters.map((subFilter) {
                        if (_bloc.filtersValue[filter] == true)
                          BottomSheetFiltersBody.customFilters
                              .add(subFilter.id);
                        return _createChipTile(subFilter, filter);
                      }).toList(),
                    );
                  } else {
                    return _createCheckboxTile(filter);
                  }
                }).toList(),
              ),
            ],
          ),
          isExpanded: tm.isExpanded,
        );
      }).toList(),
    );
  }

  Widget showFilters(String filter) {
    return Padding(
      padding: const EdgeInsets.only(right: 4.0),
      child: Container(
        decoration: BoxDecoration(
            color: globalElevations.e24dp.backgroundColor,
            borderRadius: BorderRadius.circular(2)),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text(
            filter,
            style: TextStyle(
                fontSize: 12.0,
                color: globalElevations.fstTextColor.backgroundColor),
          ),
        ),
      ),
    );
  }

  List<Filter> getFilterListInFunctionOfTravelMode(String travelMode) {
    List<Filter> list = _bloc.filters;
    List<Filter> listResults = new List<Filter>();
    for (Filter filter in list) {
      if (filter.travelMode.string == travelMode) {
        listResults.add(filter);
      }
    }
    return listResults;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
            child: Column(
              children: <Widget>[
                Container(
                  color: globalElevations.e02dp.backgroundColor,
                  child: _buildPanel(),
                ),
              ],
            )),
      ],
    );
  }
}

class FiltersHeader extends StatefulWidget {
  const FiltersHeader({Key key}) : super(key: key);
  static List<Filter> customFilterList = [];

  @override
  _FiltersHeaderState createState() => _FiltersHeaderState();
}

class _FiltersHeaderState extends State<FiltersHeader> {
  FiltersBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<FiltersBloc>(context);
    _bloc.changeFilterPageValue(true);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Filtres",
                    style: TextStyle(
                        fontSize: 18,
                        color: globalElevations.fstTextColor.backgroundColor),
                  ),
                  Text(
                    "Mode personnalisé",
                    style: TextStyle(
                        fontSize: 14,
                        color: globalElevations.sndTextColor.backgroundColor),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {
                  List<int> filterList = [];

                  for (Filter f in BlocProvider.master<FiltersBloc>().filters) {
                    if (BlocProvider.master<FiltersBloc>().filtersValue[f] == true) {
                      filterList.add(f.id);
                    }
                    if (f.hasSubFilters) {
                      for (Filter subFilter in f.subFilters) {
                        if (_bloc.filtersValue[subFilter] == true) {
                          filterList.add(subFilter.id);
                        }
                      }
                    }
                  }

                  BlocProvider.master<FiltersBloc>()
                      .addCustomFilters(filterList);
                  BlocProvider.master<FiltersBloc>()
                      .customFilters(FiltersHeader.customFilterList);
                  BlocProvider.master<MapBloc>()
                      .setBottomSheetContent(BottomSheetCategory.POIAroundMe);
                },
                child: Icon(
                  Icons.close,
                  color: globalElevations.fstTextColor.backgroundColor,
                  size: 28,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }
}

class TravelMode {
  TravelMode({
    @required this.id,
    @required this.value,
    @required this.code,
    this.isExpanded = false,
  });

  int id;
  String value;
  String code;
  bool isExpanded;
}
