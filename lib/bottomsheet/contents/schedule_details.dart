import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/schedule/widgets/schedule_details_bottomsheet.dart';

class BottomSheetScheduleDetailsContent extends BottomSheetContent {
  BottomSheetScheduleDetailsContent(Point point) {
    headerKey = GlobalKey();
    header = BottomSheetScheduleDetailsHeader(key: headerKey, point: point);
    body = BottomSheetScheduleDetailsBottomBody(point: point);
  }
}

class BottomSheetScheduleDetailsHeader extends StatelessWidget {
  final Point point;

  const BottomSheetScheduleDetailsHeader({Key key, @required this.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              // Title
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(point.properties.iconData(), size: 40),
                    SizedBox(width: 8),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.all(2)),
                          Text(
                            point.properties.name,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(height: 2),
                          Text(
                            translations.text('schedule_of_passage.stop_zone'),
                            style: TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // Legends and controls
              FavoriteButton(data: point),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: RaisedButton(
              highlightColor: Colors.white,
              color: Theme.of(context).indicatorColor,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
              onPressed: () {
                BlocProvider.of<RoutesBloc>(context).setDestination(point);
                BlocProvider.of<NavigationBloc>(context)
                    .changeTab(NavigationCategory.route);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Icon(Icons.near_me, color: globalElevations.inverseColor.backgroundColor),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      translations
                          .text('modal_bottom_sheet.itinerary')
                          .toUpperCase(),
                      style: TextStyle(fontSize: 14, color: globalElevations.inverseColor.backgroundColor),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BottomSheetScheduleDetailsBottomBody extends StatelessWidget {
  final Point point;

  const BottomSheetScheduleDetailsBottomBody({Key key, @required this.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ClustersProperties clusterProperties =
        point.properties as ClustersProperties;
    return ScheduleDetailsBottomSheet(
        code: clusterProperties.code, routeName: '', point: point);
  }
}
