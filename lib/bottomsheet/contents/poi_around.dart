import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/widgets/route_details.dart';

class BottomSheetPOIAroundContent extends BottomSheetContent {
  BottomSheetPOIAroundContent(MapController mapController) {
    headerKey = GlobalKey();
    header = BottomSheetPOIAroundHeader(key: headerKey);
    body = BottomSheetPOIAroundBody(mapController: mapController);
  }
}

class BottomSheetPOIAroundHeader extends StatelessWidget {
  const BottomSheetPOIAroundHeader({Key key}) : super(key: key);

  Color _brightnessToColor(Brightness brightness) =>
      brightness == Brightness.light ? Colors.white : Colors.black;

  Widget _buildFilterButton(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      elevation: 2.0,
      color: Theme.of(context).buttonTheme.colorScheme.primary,
      onPressed: () => Scaffold.of(context).openEndDrawer(),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.layers,
              color: _brightnessToColor(Theme.of(context).brightness),
            ),
            SizedBox(width: 8),
            Text(
              translations.text('around_me.filter'),
              style: TextStyle(
                  color: _brightnessToColor(Theme.of(context).brightness)),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              translations.text('around_me.title'),
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          _buildFilterButton(context),
        ],
      ),
    );
  }
}

class BottomSheetPOIAroundBody extends StatelessWidget {
  final MapController mapController;

  const BottomSheetPOIAroundBody({Key key, @required this.mapController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Point>>(
      stream: BlocProvider.of<PointsBloc>(context).onRequestedPointsDownloaded,
      initialData: BlocProvider.of<PointsBloc>(context).pointsList,
      builder: (context, snapshot) {
        // Required blocs
        MapBloc mapBloc = BlocProvider.of<MapBloc>(context);
        FavoritesBloc favoritesBloc = BlocProvider.of<FavoritesBloc>(context);

        Widget widget;
        //I only display visible points (=> if the map is zoomed enough to see those points)
        List<Point> data = snapshot.data?.where((point) {
          return point.properties.isVisible(mapController.zoom);
        })?.toList();

        // If the data is currently downloading, I show a progress indicator
        if (data == null) {
          widget = Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(12),
              ),
              const AnimatedLoadingLogo(),
            ],
          );
        }
        // If there is no data to display at all, I show an empty statement
        else if (snapshot.data != null && data.isEmpty) {
          widget = Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(12),
              ),
              Center(
                child: Text(translations.text('around_me.nothing_to_display')),
              ),
            ],
          );
        }
        // Otherwise, I display all the points
        else {
          widget = Padding(
            padding: const EdgeInsets.only(top: 1.0),
            child: AnimationLimiter(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: AnimationConfiguration.toStaggeredList(
                  duration: const Duration(milliseconds: 375),
                  childAnimationBuilder: (widget) => SlideAnimation(
                    child: FadeInAnimation(child: widget),
                    horizontalOffset: 50.0,
                  ),
                  children: data.map((point) {
                    print("_______________________");
                    print(point.properties.type);
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8, left: 8, right: 6),
                      child: RaisedButton(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Row(
                            children: <Widget>[
                              (point.properties.iconData() is IconData)
                                  ? Icon(
                                point.properties.iconData(),
                                size: 36,
                                color: Theme.of(context).unselectedWidgetColor,
                              )
                                  : Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: SvgPicture.asset(
                                point.properties.iconData(),
                                height: 28,
                                semanticsLabel: 'icon',
                                color: Theme.of(context).unselectedWidgetColor,
                              ),
                                  ),
                              SizedBox(width: 16),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 4.0, bottom: 4.0, left: 6.0),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        point.properties.name,
                                        style: TextStyle(
                                          color: globalElevations
                                              .fstTextColor.backgroundColor,
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ),
                                      Text(
                                        PropertiesTypeExtension
                                            .getDetailsPoiType(point),
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color: globalElevations
                                                .sndTextColor.backgroundColor),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ),
                                      PropertiesTypeExtension.getThirdDetailsPoiType(point) != '' ?
                                      Text(
                                        PropertiesTypeExtension
                                            .getThirdDetailsPoiType(point),
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color: globalElevations
                                                .sndTextColor.backgroundColor),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ) : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              (point.properties.type != PropertiesType.tier && point.properties.type != PropertiesType.pony
                                  && point.properties.type != PropertiesType.citiz && point.properties.type != PropertiesType.citizyea)
                                  ? FavoriteButton(
                                      data: point,
                                    )
                                  : EmptyCell()
                            ],
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        color:
                        globalElevations.expandPanelColor.backgroundColor,
                        elevation: 0,
                        onPressed: () {
                          mapBloc.selectPOI(point.properties.id);
                          // Move view
                          mapBloc.moveTo(MapPositionAndZoom.withLatLng(
                              Utils.coordinatesFromArray(
                                  point.geometry.coordinates),
                              17));
                          // Change bottomsheet content
                          mapBloc.setBottomSheetContent(
                              BottomSheetCategory.POIDetails,
                              arguments: point);
                        },
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
          );
        }

        return widget;
      },
    );
  }
}
