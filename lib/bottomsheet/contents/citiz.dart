import 'dart:core' as prefix0;
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/citiz.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/localization/translations.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomSheetCitizContent extends BottomSheetContent {
  BottomSheetCitizContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetCitizBody(point: point);
  }
}

class BottomSheetCitizBody extends StatelessWidget {
  final Point point;

  const BottomSheetCitizBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CitizProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 16),
          StreamBuilder<List<CitizDynamicData>>(
            stream: BlocProvider.of<CitizBloc>(context).onDownloadCitizInfoCompleted,
            initialData: BlocProvider.of<CitizBloc>(context).citizInfo,
            builder: (context, snapshot) {
              if(!snapshot.hasData || snapshot.data == null)
                return AnimatedLoadingLogo();

              return Column(
                children: properties.vehicules.map((vehicle) {
                  return _createView(vehicle, snapshot.data, properties, context);
                }).toList(),
              );
            }
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createView(CitizVehicule vehicule, List<CitizDynamicData> data, CitizProperties properties, BuildContext context) {
    DateTime now = DateTime.now();
    DateTime nowMidnight = DateTime(now.year, now.month, now.day).add(Duration(days: 1)); // Verify if this value is equal to now + 1 (at midnight)
    CitizVehicle vehicle = data.firstWhere((v) => v.id == properties.id).vehicles.firstWhere((v) => v.id == vehicule.id.toString(), orElse: () => null);

    if (vehicle == null)
      return Container();

    String available = '';
    String availabilityDate = '';
    Color iconColor;

    String hour;
    String day;

    if(vehicle.availabilityStart.isBefore(now)) {
      hour = (vehicle.availabilityEnd.hour < 10 ? '0' : '') + vehicle.availabilityEnd.hour.toString() + 'h' + (vehicle.availabilityEnd.minute < 10 ? '0' : '') + vehicle.availabilityEnd.minute.toString();
      day = (vehicle.availabilityEnd.day < 10 ? '0' : '') + vehicle.availabilityEnd.day.toString() + '/' + (vehicle.availabilityEnd.month < 10 ? '0' : '') + vehicle.availabilityEnd.month.toString() + '/' + vehicle.availabilityEnd.year.toString();

      available = translations.text('citiz.available');
      iconColor = Colors.green.shade700;
      //TODO : HALIMA réctifier les idsponibilités
      if(vehicle.availabilityEnd.isBefore(nowMidnight)) {
        availabilityDate = Utils.formatRessource('citiz.available_until', [day, hour]);
      }
      else {
        availabilityDate = Utils.formatRessource('citiz.available_until', [day, hour]);
      }
    } else {
      // being used
      hour = (vehicle.availabilityStart.hour < 10 ? '0' : '') + vehicle.availabilityStart.hour.toString() + 'h' + (vehicle.availabilityStart.minute < 10 ? '0' : '') + vehicle.availabilityStart.minute.toString();
      day = (vehicle.availabilityStart.day < 10 ? '0' : '') + vehicle.availabilityStart.day.toString() + '/' + (vehicle.availabilityStart.month < 10 ? '0' : '') + vehicle.availabilityStart.month.toString() + '/' + vehicle.availabilityStart.year.toString();
      available = translations.text('citiz.unavailable');
      availabilityDate = Utils.formatRessource('citiz.available_date', [day, hour]);
      iconColor = Colors.redAccent.shade700;
    }

    return Padding(
      padding: const EdgeInsets.only(top : 8.0),
      child: ExpandablePanel(
        elevation: globalElevations.e02dp,
        showArrow: false,
        body: Row(
          children: <Widget>[
            ClipRect(
              child: Container(
                height: 88,
                width: 144,
                child: Image.network(
                  BlocProvider.master<WebServices>().getHost() + 'citiz/${vehicule.id}/photo',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(vehicule.name.split('/')[0].toString()),
                  SizedBox(height: 4),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.access_time,
                        color: iconColor,
                        size: 12,
                      ),
                      SizedBox(width: 4),
                      Text(
                        available,
//                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  SizedBox(height: 4),
                  Text(
                    availabilityDate,
                    //maxLines: 1,
                  ),
                ],
              ),
            ),
            SizedBox(width: 8),
          ],
        ),
        detail: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 12.0, 8.0, 8.0),
              child: Text(vehicule.name),
            ),
            /*Padding(
              padding: const EdgeInsets.fromLTRB(12.0, 8.0, 8.0, 12.0),
              child: Text('Disponible le : '),
            ),*/
            Container(
              color: globalElevations.e24dp.backgroundColor,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        _openUrl('https://citiz.coop/');
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(12, 16, 16, 16),
                        child: Text(
                          'RÉSERVER',
                          textAlign: TextAlign.start,
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            color: Theme.of(context).indicatorColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_openUrl(String url) {
  launch(url);
}
