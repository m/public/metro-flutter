import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/schedule/widgets/schedule_of_passage.dart';

class BottomSheetScheduleContent extends BottomSheetContent {
  BottomSheetScheduleContent(Point point) {
    headerKey = GlobalKey();
    header = BottomSheetScheduleHeader(key: headerKey, point: point);
    body = BottomSheetScheduleBody(point: point);
  }
}

class BottomSheetScheduleHeader extends StatelessWidget {
  final Point point;

  const BottomSheetScheduleHeader({Key key, @required this.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Required blocs
    FavoritesBloc favoritesBloc = BlocProvider.of<FavoritesBloc>(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              // Title
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.directions_bus,
                      size: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.all(2)),
                          Text(
                            point.properties.name,
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(2)),
                          Text(
                            translations.text('schedule_of_passage.stop_zone'),
                            style: TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // Legends and controls
              Container(
                alignment: Alignment.centerRight,
                child: Row(
                  children: <Widget>[
                    FutureBuilder<bool>(
                      future: favoritesBloc.isFavorite(point),
                      builder: (context, snapshot) {
                        Color iconColor = Colors.grey;
                        if (snapshot.hasError)
                          return Text('There was an error');
                        else if (snapshot.hasData) {
                          if (snapshot.data)
                            iconColor = Theme.of(context).indicatorColor;
                        }
                        return GestureDetector(
                          child: Icon(Icons.favorite_border, color: iconColor),
                          onTap: () async => snapshot.data
                              ? await favoritesBloc.removeFavorite(point)
                              : await favoritesBloc.addFavorite(point),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8),
            child: RaisedButton(
              highlightColor: Colors.white,
              color: Theme.of(context).indicatorColor,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20)),
              onPressed: () {
                BlocProvider.of<RoutesBloc>(context).setDestination(point);
                BlocProvider.of<NavigationBloc>(context)
                    .changeTab(NavigationCategory.route);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Icon(Icons.near_me, color: Colors.black),
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      translations
                          .text('modal_bottom_sheet.itinerary')
                          .toUpperCase(),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BottomSheetScheduleBody extends StatelessWidget {
  final Point point;

  const BottomSheetScheduleBody({Key key, @required this.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ClustersProperties clusterProperties =
        point.properties as ClustersProperties;
    return ScheduleOfPassage(
        point: point, code: clusterProperties.code, routeName: '');
  }
}
