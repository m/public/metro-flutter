import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/route/widgets/route_details.dart';

class BottomSheetRouteDetailsContent extends BottomSheetContent {
  BottomSheetRouteDetailsContent(Itinerary itinerary) {
    headerKey = GlobalKey();
    header = RouteDetailsBottomSheetHeader(key: headerKey, selectedRoute: itinerary);
    body = RouteDetailsBottomSheetBody(itinerary: itinerary);
  }
}
