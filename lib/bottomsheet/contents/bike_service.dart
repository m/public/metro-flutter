import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/irve_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:url_launcher/url_launcher.dart';

class BottomSheetBikeServiceContent extends BottomSheetContent {
  BottomSheetBikeServiceContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetBikeServiceBody(point: point);
  }
}

class BottomSheetBikeServiceBody extends StatelessWidget {
  final Point point;

  const BottomSheetBikeServiceBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BikeServiceProperties properties = point.properties;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 28),
          Padding(
            padding: const EdgeInsets.only(bottom: 18.0),
            child: Text(
              "Informations",
              style: TextStyle(fontSize: 20, color: globalElevations.sndTextColor.backgroundColor),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 8, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Aires de services comprenant :",
                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                ),
                SizedBox(height: 8,),
                Text(
                  "- un banc",
                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                ),
                SizedBox(height: 8,),
                Text(
                  "- une pompe à vélo",
                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                ),
                SizedBox(height: 8,),
                Text(
                  "- un plan du quartier",
                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                ),
                SizedBox(height: 8,),
                Text(
                  "- un plan du réseau Chronovélo",
                  style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left : 8.0, right: 8.0, top: 26, bottom: 26),
            child: Image.asset("assets/bikes/aire_de_service.png",
              /*scale: 0.8*/),
          ),
          FlatButton(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18), side: BorderSide(color: Color(0xFFB49BDA))),
            onPressed: () {
              Utils.launchUrl('https://www.grenoblealpesmetropole.fr/416-chronovelo.htm#par2365');
            },
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(
                      Icons.public,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      "Plus d'infos".toUpperCase(),
                      style: TextStyle(color: Theme.of(context).indicatorColor),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

_openUrl(String url) {
  launch(url);
}
