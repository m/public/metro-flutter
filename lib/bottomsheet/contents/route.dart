import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/route/widgets/route_results.dart';

class BottomSheetRouteContent extends BottomSheetContent {
  BottomSheetRouteContent() {
    headerKey = GlobalKey();
    header = RouteResultsBottomSheetHeader(key: headerKey);
    body = RouteResultsBottomSheetBody();
  }
}
