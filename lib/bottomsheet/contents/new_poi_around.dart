import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/bike_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/points.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:preferences/preference_service.dart';

class NewBottomSheetPOIAroundContent extends BottomSheetContent {
  NewBottomSheetPOIAroundContent(MapController mapController) {
    headerKey = GlobalKey();
    header = NewBottomSheetPOIAroundHeader(key: headerKey);
    body = BottomSheetPOIAroundBody(mapController: mapController);
  }
}

class NewBottomSheetPOIAroundHeader extends StatefulWidget {
  const NewBottomSheetPOIAroundHeader({Key key}) : super(key: key);

  @override
  _NewBottomSheetPOIAroundHeaderState createState() => _NewBottomSheetPOIAroundHeaderState();
}

class _NewBottomSheetPOIAroundHeaderState extends State<NewBottomSheetPOIAroundHeader> {
  bool busFilter = false;
  bool bikeFilter = false;
  bool carFilter = false;
  bool customFilter = false;

  String isFilter;

  List<int> bus = [1, 2, 4, 7, 10, 12];
  List<int> bike = [41, 42, 43, 44, 47, 48, 49, 50, 54];
  List<int> car = [14, 21, 26, 36, 37, 38, 39];
  List<int> custom = [];
  List<int> old = [];

  FiltersBloc _filterBloc;
  List<Filter> filters1;
  List<Filter> allFilters = [];

  bool isDone = false;

  @override
  Widget build(BuildContext context) {
    _filterBloc = BlocProvider.of<FiltersBloc>(context);
    filters1 = _filterBloc.filters;

    for (Filter f in filters1) {
      allFilters.add(f);
      if (f.hasSubFilters) {
        for (Filter g in f.subFilters) {
          allFilters.add(g);
        }
      }
    }

    if (PrefService.getString('preferences_filters') == null) PrefService.setString('preferences_filters', 'bus');

    customFilter ? _filterBloc.changeButtonFilterValue(true) : _filterBloc.changeButtonFilterValue(false);

    List<String> savedCustomList = PrefService.getStringList('customFiltersTest');
    if (savedCustomList != null) {
      List<int> intCustomList = savedCustomList.map((i) => int.parse(i)).toList();
      for (int f in intCustomList) {
        if (!custom.contains(f)) {
          custom.add(f);
        }
      }
    }

    custom.sort();
    if (!isDone) initialFilterValue();

    return Container(
      padding: EdgeInsets.only(bottom: 16),
      child: Column(
        children: <Widget>[
          _buildTypesHeader(context),
        ],
      ),
    );
  }

  initialFilterValue() {
    isFilter = PrefService.getString('preferences_filters');
    if (isFilter == 'bus') {
      busFilter = true;
      changeFilterBool(busFilter, false, false, false);
      BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
      BlocProvider.master<FiltersBloc>().changeFilterPageValue(false);
    } else if (isFilter == 'bike') {
      bikeFilter = true;
      changeFilterBool(false, bikeFilter, false, false);
      BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
      BlocProvider.master<FiltersBloc>().changeFilterPageValue(false);
    } else if (isFilter == 'car') {
      carFilter = true;
      changeFilterBool(false, false, carFilter, false);
      BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
      BlocProvider.master<FiltersBloc>().changeFilterPageValue(false);
    } else if (isFilter == 'custom') {
      customFilter = true;
      changeFilterBool(false, false, false, customFilter);
      BlocProvider.master<FiltersBloc>().changeButtonFilterValue(true);
      BlocProvider.master<FiltersBloc>().changeFilterPageValue(false);
    }
    _changeFilters(isFilter, true);
    isDone = true;
  }

  changeFilterBool(bool a, bool b, bool c, bool d) {
    busFilter = a;
    bikeFilter = b;
    carFilter = c;
    customFilter = d;
  }

  _changeFilters(String filterType, bool isFilter) {
    switch (filterType) {
      case "bus":
        setCustomFilterValue(bus, isFilter);
        break;
      case "bike":
        BlocProvider.master<BikeBloc>().downloadBike(isBottomFilter: true, filtersList: ["veloamenage", "tempovelo", "chronovelo", "voieverte"]);
        setCustomFilterValue(bike, isFilter);
        break;
      case "car":
        setCustomFilterValue(car, isFilter);
        break;
      case "custom":
        setCustomFilterValue(custom, isFilter);
        break;
      default:
        break;
    }
  }

  void setCustomFilterValue(List<int> filterList, bool valueFilters) async {
    for (Filter f in allFilters) {
      if (old.isNotEmpty) {
        if (old.contains(f.id)) {
          BlocProvider.master<FiltersBloc>().onValueChange(f, false, "");
        }
        if (filterList.contains(f.id)) {
          BlocProvider.master<FiltersBloc>().onValueChange(f, valueFilters, "");
        }
      } else {
        if (filterList.contains(f.id)) {
          BlocProvider.master<FiltersBloc>().onValueChange(f, valueFilters, "");
        }
      }
    }
    old = filterList;
  }

  Widget _buildTypesHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 80,
            child: Column(
              children: [
                RaisedButton(
                  padding: const EdgeInsets.all(12.0),
                  elevation: 0,
                  shape: CircleBorder(
                      side: BorderSide(
                          color: isFilter == "bus" ? Theme.of(context).indicatorColor : globalElevations.expandPanelColor.backgroundColor, width: 2)),
                  color: isFilter == "bus" ? globalElevations.inverseColor.backgroundColor : globalElevations.expandPanelColor.backgroundColor,
                  onPressed: () {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      if (mounted)
                        setState(() {
                          busFilter = !busFilter;
                          busFilter ? PrefService.setString('preferences_filters', "bus") : PrefService.setString('preferences_filters', '');
                          changeFilterBool(busFilter, false, false, false);
                          _changeFilters("bus", busFilter);
                          (busFilter == true) ? isFilter = "bus" : isFilter = "";
                          BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
                        });
                    });
                  },
                  child: Icon(
                    MetroIcons.bus,
                    color: isFilter == "bus" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor,
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  "Bus/Tram",
                  style: TextStyle(
                      fontSize: 12, color: isFilter == "bus" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor),
                )
              ],
            ),
          ),
          Container(
            width: 80,
            padding: const EdgeInsets.all(0),
            child: Column(
              children: [
                RaisedButton(
                  padding: const EdgeInsets.all(12.0),
                  elevation: 0,
                  shape: CircleBorder(
                      side: BorderSide(
                          color: isFilter == "bike" ? Theme.of(context).indicatorColor : globalElevations.expandPanelColor.backgroundColor,
                          width: 2)),
                  color: isFilter == "bike" ? globalElevations.inverseColor.backgroundColor : globalElevations.expandPanelColor.backgroundColor,
                  onPressed: () {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      if (mounted)
                        setState(() {
                          bikeFilter = !bikeFilter;
                          bikeFilter ? PrefService.setString('preferences_filters', "bike") : PrefService.setString('preferences_filters', '');
                          changeFilterBool(false, bikeFilter, false, false);
                          _changeFilters("bike", bikeFilter);
                          (bikeFilter == true) ? isFilter = "bike" : isFilter = "";
                          BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
                        });
                    });
                  },
                  child: Icon(
                    MetroIcons.velo,
                    color: isFilter == "bike" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor,
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  "Vélo",
                  style: TextStyle(
                      fontSize: 12, color: isFilter == "bike" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor),
                )
              ],
            ),
          ),
          Container(
            width: 80,
            child: Column(
              children: [
                RaisedButton(
                  padding: const EdgeInsets.all(12.0),
                  elevation: 0,
                  shape: CircleBorder(
                      side: BorderSide(
                          color: isFilter == "car" ? Theme.of(context).indicatorColor : globalElevations.expandPanelColor.backgroundColor, width: 2)),
                  color: isFilter == "car" ? globalElevations.inverseColor.backgroundColor : globalElevations.expandPanelColor.backgroundColor,
                  onPressed: () {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      if (mounted)
                        setState(() {
                          carFilter = !carFilter;
                          carFilter ? PrefService.setString('preferences_filters', "car") : PrefService.setString('preferences_filters', '');
                          _changeFilters("car", carFilter);
                          changeFilterBool(false, false, carFilter, false);
                          if (carFilter == true) isFilter = "car";
                          BlocProvider.master<FiltersBloc>().changeButtonFilterValue(false);
                        });
                    });
                  },
                  child: Icon(
                    MetroIcons.voiture,
                    color: isFilter == "car" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor,
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  "Voiture",
                  style: TextStyle(
                      fontSize: 12, color: isFilter == "car" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor),
                )
              ],
            ),
          ),
          Container(
            width: 80,
            child: Column(
              children: [
                RaisedButton(
                  padding: const EdgeInsets.all(12.0),
                  elevation: 0,
                  shape: CircleBorder(
                      side: BorderSide(
                          color: isFilter == "custom" ? Theme.of(context).indicatorColor : globalElevations.expandPanelColor.backgroundColor,
                          width: 2)),
                  color: isFilter == "custom" ? globalElevations.inverseColor.backgroundColor : globalElevations.expandPanelColor.backgroundColor,
                  onPressed: () {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      if (mounted)
                        setState(() {
                          customFilter = !customFilter;
                          customFilter ? PrefService.setString('preferences_filters', "custom") : PrefService.setString('preferences_filters', '');
                          _changeFilters("custom", customFilter);
                          changeFilterBool(false, false, false, customFilter);
                          if (customFilter == true) isFilter = "custom";
                          BlocProvider.master<FiltersBloc>().changeButtonFilterValue(true);
                        });
                    });
                  },
                  child: Icon(
                    Icons.star_border,
                    color: isFilter == "custom" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor,
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  "Personnalisé",
                  style: TextStyle(
                    fontSize: 12,
                    color: isFilter == "custom" ? Theme.of(context).indicatorColor : globalElevations.fstTextColor.backgroundColor,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BottomSheetPOIAroundBody extends StatelessWidget {
  final MapController mapController;

  const BottomSheetPOIAroundBody({Key key, @required this.mapController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Point>>(
      stream: BlocProvider.of<PointsBloc>(context).onRequestedPointsDownloaded,
      initialData: BlocProvider.of<PointsBloc>(context).pointsList,
      builder: (context, snapshot) {
        // Required blocs
        MapBloc mapBloc = BlocProvider.of<MapBloc>(context);

        Widget widget;
        //I only display visible points (=> if the map is zoomed enough to see those points)
        List<Point> data = snapshot.data?.where((point) {
          return point.properties.isVisible(mapController.zoom);
        })?.toList();

        // If the data is currently downloading, I show a progress indicator
        if (data == null) {
          widget = Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(12),
              ),
              const AnimatedLoadingLogo(),
            ],
          );
        }
        // If there is no data to display at all, I show an empty statement
        else if (snapshot.data != null && data.isEmpty) {
          widget = Padding(
            padding: const EdgeInsets.fromLTRB(34, 12, 34, 12),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.layers,
                  color: Theme.of(context).unselectedWidgetColor,
                  size: 30,
                ),
                SizedBox(
                  height: 6.0,
                ),
                StreamBuilder<bool>(
                    stream: BlocProvider.master<FiltersBloc>().onButtonFilterValueChanged,
                    initialData: false,
                    builder: (context, snapshot) {
                      if (snapshot == null || snapshot.hasError) return Container();

                      return Text(
                        snapshot.data ? translations.text('around_me.nothing_to_display_filters') : translations.text('around_me.nothing_to_display'),
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Theme.of(context).unselectedWidgetColor, fontSize: 16),
                      );
                    }),
              ],
            ),
          );
        }
        // Otherwise, I display all the points
        else {
          widget = Padding(
            padding: const EdgeInsets.only(top: 1.0),
            child: AnimationLimiter(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: AnimationConfiguration.toStaggeredList(
                  duration: const Duration(milliseconds: 375),
                  childAnimationBuilder: (widget) => SlideAnimation(
                    child: FadeInAnimation(child: widget),
                    horizontalOffset: 50.0,
                  ),
                  children: data.map((point) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8, left: 8, right: 6),
                      child: RaisedButton(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Row(
                            children: <Widget>[
                              (point.properties.iconData() is IconData)
                                  ? Icon(
                                      point.properties.iconData(),
                                      size: 36,
                                      color: Theme.of(context).unselectedWidgetColor,
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: SvgPicture.asset(
                                        point.properties.iconData(),
                                        height: 28,
                                        semanticsLabel: 'icon',
                                        color: Theme.of(context).unselectedWidgetColor,
                                      ),
                                    ),
                              SizedBox(width: 16),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 6.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        point.properties.name,
                                        style: TextStyle(
                                          color: globalElevations.fstTextColor.backgroundColor,
                                          fontSize: 15,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ),
                                      Text(
                                        PropertiesTypeExtension.getDetailsPoiType(point),
                                        style: TextStyle(fontWeight: FontWeight.normal, color: globalElevations.sndTextColor.backgroundColor),
                                        overflow: TextOverflow.ellipsis,
                                        softWrap: true,
                                      ),
                                      PropertiesTypeExtension.getThirdDetailsPoiType(point) != ''
                                          ? Text(
                                              PropertiesTypeExtension.getThirdDetailsPoiType(point),
                                              style: TextStyle(fontWeight: FontWeight.normal, color: globalElevations.sndTextColor.backgroundColor),
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: true,
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              (point.properties.type != PropertiesType.tier &&
                                      point.properties.type != PropertiesType.pony &&
                                      point.properties.type != PropertiesType.citizyea &&
                                      point.properties.type != PropertiesType.citiz &&
                                      point.properties.type != PropertiesType.veloservice)
                                  ? FavoriteButton(
                                      data: point,
                                    )
                                  : EmptyCell()
                            ],
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                        color: globalElevations.expandPanelColor.backgroundColor,
                        elevation: 0,
                        onPressed: () {
                          mapBloc.selectPOI(point.properties.id);
                          // Move view
                          mapBloc.moveTo(MapPositionAndZoom.withLatLng(Utils.coordinatesFromArray(point.geometry.coordinates), 17));
                          // Change bottomsheet content
                          mapBloc.setBottomSheetContent(BottomSheetCategory.POIDetails, arguments: point);
                        },
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),
          );
        }
        return widget;
      },
    );
  }
}
