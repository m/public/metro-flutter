import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';

class BottomSheetPoiDetailsContent extends BottomSheetContent {
  BottomSheetPoiDetailsContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetPoiDetailsBody(point: point);
  }
}

class BottomSheetPoiDetailsBody extends StatelessWidget {
  final Point point;

  const BottomSheetPoiDetailsBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
