import 'package:flutter/material.dart';
import 'package:metro/bottomsheet/contents/bottomsheet_content.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/consignesData.dart';
import 'package:metro/global/blocs/parking.dart';
import 'package:metro/global/blocs/parkingData.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/localization/translations.dart';

class BottomSheetParkingContent extends BottomSheetContent {
  BottomSheetParkingContent(Point point) {
    headerKey = GlobalKey();
    header = DefaultBottomSheetHeader(key: headerKey, point: point);
    body = BottomSheetParkingBody(point: point);
  }
}

class BottomSheetParkingBody extends StatelessWidget {
  final Point point;

  const BottomSheetParkingBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    ConsignesData consignesData = BlocProvider.master<ConsignesDataBloc>().getTypeConsignes(point.properties.id);
    ParkingProperties propertiesConsignes = point.properties;
    if (point.properties.type == PropertiesType.mvc)
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Text(
              "Informations",
              style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
            ),
          ),
          propertiesConsignes.total != null
              ? Padding(
            padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
            child: Text(consignesData.TOTAL.toString() + " places", style: TextStyle(fontSize: 16)),
          )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );

    if (point.properties.type == PropertiesType.mva)
      return Padding(
        padding: const EdgeInsets.only(top: 16),
        child: Column(
          children: <Widget>[
            BottomSheetBodyItineraryButton(point: point),
            Padding(
              padding: const EdgeInsets.only(top: 32),
              child: Align(
                alignment: Alignment.centerLeft,
                child: BottomSheetBodyNearbyLines(
                  position: Utils.coordinatesFromArray(point.geometry.coordinates),
                ),
              ),
            ),
          ],
        ),
      );

    ParkingData parkingData = BlocProvider.master<ParkingDataBloc>().getTypeParking(point.properties.id);
    ParkingProperties properties = point.properties;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          BottomSheetBodyItineraryButton(point: point),
          SizedBox(height: 20),
          BlocProvider.master<ParkingBloc>().countAvailable(point.properties.id).toString() != "null"
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 20),
                  child: RichText(
                      text: TextSpan(children: [
                    TextSpan(text: BlocProvider.master<ParkingBloc>().countAvailable(point.properties.id).toString(), style: TextStyle(fontSize: 24,
                        color: globalElevations.fstTextColor.backgroundColor)),
                    TextSpan(text: ' ' + translations.text('bottomsheet_content.parking.places'), style: TextStyle(fontSize: 24,
                        color: globalElevations.fstTextColor.backgroundColor))
                  ])),
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Text(
              "Informations",
              style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
            ),
          ),
          properties.total != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text(properties.total.toString() + " places", style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.hauteur_max != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text("Hauteur max : " + parkingData.hauteur_max.toString(), style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.hauteur_max != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text(parkingData.type_ouvrage, style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.tarif_pmr != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text(parkingData.tarif_pmr, style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.info != null
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text(parkingData.info, style: TextStyle(fontSize: 16)),
                )
              : Container(),
          SizedBox(height: 20),
          (parkingData.tarif_1h != 'null' || parkingData.tarif_3h != 'null'|| parkingData.tarif_4h != 'null'|| parkingData.tarif_24h != 'null')
              ? Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Text(
              "Tarifs",
              style: TextStyle(fontSize: 18, color: globalElevations.sndTextColor.backgroundColor),
            ),)
              : Container(),
          parkingData.tarif_1h != 'null'
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text("1h : " + parkingData.tarif_1h.toString() + "€", style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.tarif_3h != 'null'
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text("3h : " + parkingData.tarif_3h.toString() + "€", style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.tarif_4h != 'null'
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text("4h : " + parkingData.tarif_4h.toString() + "€", style: TextStyle(fontSize: 16)),
                )
              : Container(),
          parkingData.tarif_24h != 'null'
              ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 6.0),
                  child: Text("24h : " + parkingData.tarif_24h.toString() + "€", style: TextStyle(fontSize: 16)),
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Align(
              alignment: Alignment.centerLeft,
              child: BottomSheetBodyNearbyLines(
                position: Utils.coordinatesFromArray(point.geometry.coordinates),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FavoritesParkingBody extends StatelessWidget {
  final Point point;

  const FavoritesParkingBody({Key key, @required this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ParkingProperties properties = point.properties;
    String places;

    ParkingProperties propertiesParking = point.properties;
    int numberAvailable = BlocProvider.master<ParkingBloc>().countAvailable(point.properties.id);

    if (numberAvailable != null && numberAvailable == -1) {
      places = properties.total != null ? properties.total.toString() + ' ' + translations.text('bottomsheet_content.parking.places') : 'Parking';
    } else {
      places = properties.total != null
          ? numberAvailable.toString() + ' ' + translations.text('bottomsheet_content.parking.places') + ' / ' + propertiesParking.total.toString()
          : 'Parking';
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 16),
          Text(places),
          SizedBox(height: 8),
          Text(properties.address),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}
