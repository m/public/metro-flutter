import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/route/blocs/routes.dart';

class HalfBottomSheet extends StatefulWidget {
  final Widget body;
  final Widget header;

  const HalfBottomSheet({Key key, this.body, this.header}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HalfBottomSheetState(body: body, header: header);
}

class _HalfBottomSheetState extends State<HalfBottomSheet> {
  final Widget body;
  final Widget header;

  _HalfBottomSheetState({this.body, this.header});

  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = BlocProvider.of<RoutesBloc>(context).routeScrollController;
  }

  @override
  void dispose() {
    if (_controller != null && _controller.hasClients) _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      height: (BlocProvider.of<NavigationBloc>(context)
                  .getSafeAreaKey()
                  .currentContext
                  .findRenderObject() as RenderBox)
              .size
              .height * 0.65,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).bottomSheetTheme.backgroundColor,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black45,
              offset: Offset(0, 0),
              blurRadius: 4,
              spreadRadius: 1,
            )
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              child: header,
              padding: const EdgeInsets.only(top: 8),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: body,
                controller: _controller,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
