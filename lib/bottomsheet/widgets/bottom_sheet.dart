import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/widgets/route_details.dart';

/// Custom implementation of Flutter's `BottomSheet`.
///
/// Unlike Flutter's BottomSheet, this custom BottomSheet will not display above the bottom navigation bar.
///
/// The use of `CustomBottomSheetCell` with this widget is recommanded, yet not mendatory.
class CustomBottomSheet extends StatefulWidget {
  final Widget body;
  final Widget header;
  final GlobalKey headerKey;
  final bool closedByDefault;
  final CustomBottomSheetController controller;

  const CustomBottomSheet({Key key, this.body, this.header, this.headerKey, this.closedByDefault = false, this.controller}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CustomBottomSheetState();
}

class CustomBottomSheetState extends State<CustomBottomSheet> with SingleTickerProviderStateMixin {
  double _initialGesturePosition;
  double _verticalPosition = 0;
  double _verticalOffset = 0;
  double _minVerticalPosition;
  double _maxVerticalPosition;
  double _totalHeight;
  double _bodyHeight;
  double _headerHeight;

  double _storedScreenHeight;

  AnimationController _animationController;
  Tween _tween;
  Animation<double> _animation;

  bool _isVisible = true;

  bool _scrollBlocked = false;
  ScrollPhysics _scrollPhysics = NeverScrollableScrollPhysics();

  GlobalKey _headerKey = GlobalKey();

  double _previousVerticalPosition = 0;
  int _panDirection = 0;
  double _panVelocity = 0;

  @override
  void initState() {
    super.initState();

    _updateHeaderSize(after: () => _split(immediatly: false));

    // Create animation for open and close functions

    if (mounted) {
      _animationController = AnimationController(duration: Duration(milliseconds: 150), vsync: this);
    }

    // Add position callback from controller
    widget.controller?.onPositionChanged = ((value) {
      if (value == null) {
        _close();
      } else {
        if (value)
          _open();
        else
          _close();
      }
    });

    // Add visibility callback from controller
    widget.controller?.onVisibilityChanged = ((value) {
      _setVisibility(value);
    });

    widget.controller?.onHeaderResizeRequired = (() {
      if (BlocProvider.master<FiltersBloc>().buttonFilterValue)
        _updateHeaderSize(after: () => _split(immediatly: false), before: () =>
        _split());
      else _updateHeaderSize(after: () => _close(), before: () => BlocProvider.master<FiltersBloc>().buttonFilterValue ? _split() : _close());
    });

  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _isVisible,
      child: Positioned(
        left: 0,
        right: 0,
        bottom: _verticalPosition,
        height: _totalHeight,
        child: Container(
          height: _totalHeight,
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              StreamBuilder<bool>(
                  stream: BlocProvider.master<FiltersBloc>().onButtonFilterValueChanged,
                  builder: (_, snapshot) {
                    bool b;
                    snapshot.data != null ? b = snapshot.data : b = false;

                    return Visibility(
                      visible: (b && widget.body is! RouteDetailsBottomSheetBody),
                      //TODO : OVERFLOW
                      replacement: SizedBox(
                        height: 56,
                      ),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: _buildFilterButton(context),
                        ),
                      ),
                    );
                  }),
              DecoratedBox(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.8,
                    color: globalElevations.e24dp.backgroundColor,
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                  color: Theme.of(context).bottomSheetTheme.backgroundColor,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(0, 0),
                      blurRadius: 6,
                      spreadRadius: 2,
                    )
                  ],
                ),
                child: StreamBuilder<bool>(
                  stream: BlocProvider.master<FiltersBloc>().onButtonFilterValueChanged,
                  builder: (context, snapshot) {
                    double d = _totalHeight;
                    if (snapshot.data != null) {
                      d = _totalHeight - 56;
                    }
                    return Container(
                      height: d,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onPanStart: (details) {
                          // Initial touch position
                          _initialGesturePosition = details.globalPosition.dy;
                          _verticalOffset = _verticalPosition;
                        },
                        onPanUpdate: (details) {
                          // Each touch position update
                          double distance = details.globalPosition.dy - _initialGesturePosition;
                          if (mounted)
                            setState(() {
                              _previousVerticalPosition = _verticalPosition;
                              _verticalPosition = Utils.clamp(_minVerticalPosition, _maxVerticalPosition, _verticalOffset - distance);

                              _panVelocity = (_verticalPosition - _previousVerticalPosition).abs();
                              _panDirection = _verticalPosition.compareTo(_previousVerticalPosition);
                            });

                          // Block scroll if size is maximum
                          _checkScrollBlock();
                        },
                        onPanEnd: (details) {
                          // End of touch
                          _initialGesturePosition = 0;
                          _onPanEnd();
                        },
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Column(
                                key: _headerKey,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    height: 5,
                                    width: 48,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                      color: globalElevations.bottomSheetBar.backgroundColor,
                                    ),
                                  ),
                                  widget.header,
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: _bodyHeight,
                                    child: Column(
                                      // mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Container(
                                          height: _bodyHeight,
                                          child: LayoutBuilder(
                                            builder: (context, constraints) {
                                              return SingleChildScrollView(
                                                child:
                                                    /* widget.body is RouteDetailsBottomSheetBody
                                                    ? ConstrainedBox(
                                                        constraints: constraints.copyWith(
                                                          minHeight: _bodyHeight > 96 ? _bodyHeight - 96 : _bodyHeight,
                                                          maxHeight: double.infinity,
                                                        ),
                                                        child: IntrinsicHeight(
                                                          child: Column(
                                                            children: <Widget>[
                                                              widget.body,
                                                              Expanded(
                                                                child: Align(
                                                                  alignment: Alignment.bottomLeft,
                                                                  child: Image.asset(globalElevations.footerAsset),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    : */
                                                    Column(
                                                  children: [widget.body],
                                                ),
                                                physics: _scrollPhysics,
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFilterButton(BuildContext context) {
    return Container(
      width: 138,
      child: RaisedButton(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
          side: BorderSide(width: 0.6, color: globalElevations.e24dp.backgroundColor),
        ),
        color: Theme.of(context).bottomSheetTheme.backgroundColor,
        onPressed: () => BlocProvider.master<MapBloc>().setBottomSheetContent(BottomSheetCategory.filters),
        child: Container(
          padding: EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.layers,
                color: Theme.of(context).buttonTheme.colorScheme.primary,
              ),
              SizedBox(width: 8),
              Text(
                translations.text('around_me.filter'),
                style: TextStyle(color: Theme.of(context).buttonTheme.colorScheme.primary),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Build the height of the bottom sheet depending of its content
  void _defineHeight(double screenHeight, GlobalKey headerKey) {
    if (headerKey != null)
      _headerHeight = headerKey.currentContext.size.height + 56;
    else
      _headerHeight = widget.headerKey.currentContext.size.height;

    // Max height is set to the size of the screen - (search bar height + padding)
    _totalHeight = screenHeight - 64;
    _bodyHeight = _totalHeight - _headerHeight;
    _minVerticalPosition = -_bodyHeight;
    _maxVerticalPosition = 0;
  }

  void _onPanEnd() {
    double midPos = _totalHeight * 0.5;
    double endPanPos = _totalHeight + _verticalPosition;

    if (_panVelocity >= 25) {
      // If the user did a big swipe...
      if (_panDirection >= 0) {
        // ... to the top: we fully open the bottom sheet
        _open();
      } else {
        // ... to the bottom: we fully close the bottom sheet
        _close();
      }
    } else {
      // Otherwise we doa proximity check
      if (endPanPos < midPos) {
        // The pan movement ended in the bottom half of the screen
        double midBottomHalf = midPos / 1.7;

        if (_panVelocity > 5) {
          // If the user did a small swipe...
          if (_panDirection >= 0) {
            // ... to the top: we open the bottom sheet in half
            _split(immediatly: true);
          } else {
            // ... to the bottom: we fully close the bottom sheet
            _close();
          }
        } else {
          // If the user did a very small swipe (for instance a mistake)...
          if (endPanPos >= midBottomHalf) {
            // ... and is in the top half: we open the bottom sheet in half
            _close();
          } else {
            // ... and is in the bottom half: we fully close the bottom sheet
            _close();
          }
        }
      } else {
        // The pan movement ended in the top half of the screen
        // If the user did a small swipe...
        if (_panDirection >= 0) {
          // ... to the top: we fully open the bottom sheet
          _open();
        } else {
          // ... to the bottom: we open the bottom sheet in half
          _split(immediatly: true);
        }
      }
    }
  }

  void _animateTo(double position) {
    if (mounted) {
      _tween = Tween<double>(begin: _verticalPosition, end: position);
      _animation = _tween.animate(_animationController)
        ..addListener(() {
          if (mounted)
            setState(() {
              _verticalPosition = _animation.value;
            });
        });
      _animationController.reset();
      _animationController.forward();
    }
  }

  /// Animate the bottom sheet to fully open it.
  void _open() {
    //_maxVerticalPosition :0.0
    _animateTo(_maxVerticalPosition);
    _blockScroll(false);
  }

  void _split({bool immediatly = false}) {
    setState(() {
      _verticalPosition = -_storedScreenHeight * 0.40;
    });
    _blockScroll(true);
  }

  /// Animate the bottom sheet to fully close it.
  void _close() {
    _animateTo(_minVerticalPosition);
    _blockScroll(true);
  }

  /// Hide or show the bottom sheet.
  void _setVisibility(bool isVisible) {
    if (_isVisible != isVisible) {
      if (mounted)
        setState(() {
          _isVisible = isVisible;
        });
    }
  }

  void _updateHeaderSize({Function after, Function before}) {
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      if (context == null) return;

      // Once the view is created, I use the size of components to define
      // the height of the bottom sheet
      if (_storedScreenHeight == null || _storedScreenHeight == 0)
        _storedScreenHeight = (BlocProvider.of<NavigationBloc>(context).getSafeAreaKey().currentContext.findRenderObject() as RenderBox).size.height;

      setState(() {
        _defineHeight(_storedScreenHeight, _headerKey);

        if (before != null) before();
        if (after != null) after();
      });
    });
  }

  /// Check if the scroll view within the bottom sheet should be blocked
  /// or not. The scroll view should be block when the bottom sheet is
  /// not fully opened.
  void _checkScrollBlock() {
    if (mounted)
      setState(() {
        if (_verticalPosition >= _maxVerticalPosition && _scrollBlocked) {
          _blockScroll(false);
        } else if (_verticalPosition < _maxVerticalPosition && !_scrollBlocked) {
          _blockScroll(true);
        }
      });
  }

  void _blockScroll(bool block) {
    if (block) {
      _scrollBlocked = true;
      _scrollPhysics = NeverScrollableScrollPhysics();
    } else {
      _scrollBlocked = false;
      _scrollPhysics = ScrollPhysics();
    }
  }
}

/// Standard cell used with `CustomBottomSheet`, composed of one leading widget (typically an Icon),
/// a text, and a "more" button at the end.
///
/// `onTap` callback is called whenever the user tap on the cell.
///
/// `onExtraTap` callback is called whenever the user tap on the tailing button.
class CustomBottomSheetCell extends StatefulWidget {
  final Widget leading;
  final String text;
  final VoidCallback onTap;

  const CustomBottomSheetCell({Key key, this.leading, this.text, this.onTap}) : super(key: key);

  @override
  _CustomBottomSheetCellState createState() => _CustomBottomSheetCellState();
}

class _CustomBottomSheetCellState extends State<CustomBottomSheetCell> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: widget.leading,
          title: Text(widget.text),
          trailing: Stack(
            children: <Widget>[
              GestureDetector(
                onTap: widget.onTap,
                child: Container(
                  height: 32,
                  width: 32,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.white,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors.black45,
                        offset: Offset(0, 2),
                        blurRadius: 3,
                      ),
                    ],
                  ),
                  child: Icon(Icons.more_vert),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: BorderRadius.circular(16),
                  onTap: widget.onTap,
                  child: Container(
                    height: 32,
                    width: 32,
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
          color: Colors.black12,
          height: 1,
        ),
      ],
    );
  }
}

class CustomBottomSheetController {
  Function(bool) onPositionChanged;
  Function(bool) onVisibilityChanged;
  VoidCallback onHeaderResizeRequired;

  /// Open the BottomSheet with an animation. If you
  /// want to instantly show it instead, use `show()`.
  void maximize() {
    if (onPositionChanged != null) onPositionChanged(true);
  }

  /// Close the BottomSheet with an animation. If you
  /// want to instantly hide it instead, use `hide()`.
  void minimize() {
    if (onPositionChanged != null) onPositionChanged(false);
  }

  void split() {
    if (onPositionChanged != null) onPositionChanged(null);
  }

  /// Show the BottomSheet. If you want to open the BottomSheet
  /// with an animation instead, use `maximize()`.
  void show() {
    if (onVisibilityChanged != null) onVisibilityChanged(true);
  }

  /// Hide the BottomSheet. If you want to close the BottomSheet
  /// with an animation instead, use `minimize()`.
  void hide() {
    if (onVisibilityChanged != null) onVisibilityChanged(false);
  }

  void resizeHeader() {
    if (onHeaderResizeRequired != null) onHeaderResizeRequired();
  }
}
