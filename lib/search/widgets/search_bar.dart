import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:metro/disturbances/bloc/events_bloc.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/map/blocs/map_bloc.dart';

class SearchBar extends StatefulWidget {
  final String text;
  final bool hasShadow;
  final SearchBarLeading leading;
  final Widget trailing;
  final Color color;
  final Function(Point) onSearchComplete;
  final SearchBarController controller;
  final bool isSchedulePage;
  final bool isMapPage;
  final bool onlyBackButton;

  const SearchBar({
    Key key,
    this.hasShadow = false,
    this.leading = SearchBarLeading.burger,
    this.color,
    this.onSearchComplete,
    this.controller,
    @required this.text,
    this.isSchedulePage,
    this.trailing,
    this.isMapPage = false,
    this.onlyBackButton = false,
  }) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  Color _overrideBorderColor;
  String _overrideText;

  @override
  void initState() {
    super.initState();

    // Listen to controller events
    widget.controller?.onBorderColorChanged = (color) {
      _changeBorderColor(color);
    };

    widget.controller?.onTextChanged = (text) {
      _changeText(text);
    };
  }

  @override
  Widget build(BuildContext context) {
    List<BoxShadow> shadow;

    // Add shadow based on widget's "hasShadow" attribute
    if (widget.hasShadow)
      shadow = <BoxShadow>[
        BoxShadow(
          color: Colors.black45,
          offset: Offset(0, 3),
          blurRadius: 6,
        )
      ];

    Widget leadingWidget;
    VoidCallback leadingTap;
    bool event;

    // Specify leading icons & tap action based on widget's "SearchBarLeading" attribute
    switch (widget.leading) {
      case SearchBarLeading.nothing:
        // Nothing to do
        break;
      case SearchBarLeading.burger:
        leadingWidget = Icon(Icons.menu);
        leadingTap = () {
          Scaffold.of(context).openDrawer();
        };
        event = true;
        break;
      case SearchBarLeading.back:
        leadingWidget = Icon(Icons.arrow_back);
        leadingTap = () {
          BlocProvider.of<NavigationBloc>(context).backNavigation();
          BlocProvider.of<MapBloc>(context).unselectPOI();
          MapBloc.popupLayerController.hidePopup();
          //Navigator.pop(context);
        };
        event = false;
        break;
    }

    return Container(
      height: 48,
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.transparent,
//        boxShadow: shadow,
      ),
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: widget.onlyBackButton
            ? null
            : () async => widget.onSearchComplete(
                await Utils.openSearch(context, widget.isSchedulePage)),
        child: IconTheme(
            data: Theme.of(context).iconTheme.copyWith(size: 24),
            child: Row(
              children: <Widget>[
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: leadingTap,
                  child: Container(
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      border: Border.all(
                        width: 0.8,
                        color: globalElevations.e24dp.backgroundColor,
                      ),
                      color: widget.color ??
                          globalElevations.e02dp.backgroundColor,
                      boxShadow: shadow,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 50,
                            child: IconTheme(
                              child: leadingWidget,
                              data: Theme.of(context).iconTheme.copyWith(
                                    color: globalElevations
                                        .fstTextColor.backgroundColor,
                                  ),
                            ),
                          ),
                          StreamBuilder<int>(
                              stream: BlocProvider.master<EventsBloc>()
                                  .onReadExceptionnalEventsChanged,
                              initialData:
                                  BlocProvider.master<EventsBloc>().eventsRead,
                              builder: (context, snapshot) {
                                bool showBadge;
                                if (snapshot != null && event == true) {
                                  switch (snapshot.data) {
                                    case 0:
                                      showBadge = false;
                                      break;
                                    case 1:
                                      showBadge = false;
                                      break;
                                    case 2:
                                      showBadge = true;
                                      break;
                                  }
                                } else {
                                  showBadge = false;
                                }

                                return Visibility(
                                  visible: showBadge,
                                  child: Positioned(
                                    right: 0,
                                    top: 8,
                                    child: Container(
                                      height: 20,
                                      width: 9,
                                      decoration: BoxDecoration(
                                          color: globalElevations
                                              .e02dp.backgroundColor,
                                          border: Border.all(
                                              color: globalElevations
                                                  .e02dp.backgroundColor,
                                              width: 0.1),
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Text(
                                            "!",
                                            style: TextStyle(
                                                color: Color(0xFFf96c6c),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                            textAlign: TextAlign.end,
                                          )),
                                    ),
                                  ),
                                );
                              })
                        ],
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.isMapPage,
                  child: Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                            width: 0.8,
                            color: globalElevations.e24dp.backgroundColor,
                          ),
                          color: widget.color ??
                              globalElevations.e02dp.backgroundColor,
                          boxShadow: shadow,
                        ),
                        child: Row(
                          children: <Widget>[
                            widget.trailing == null
                                ? Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: Icon(
                                      Icons.search,
                                      color: globalElevations
                                          .fstTextColor.backgroundColor,
                                      size: 24,
                                    ),
                                  )
                                : widget.trailing,
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                _overrideText != null
                                    ? _overrideText
                                    : widget.text,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300,
                                  color: _overrideText != null
                                      ? globalElevations
                                          .fstTextColor.backgroundColor
                                      : globalElevations
                                          .sndTextColor.backgroundColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }

  void _changeBorderColor(Color color) {
    setState(() {
      _overrideBorderColor = color;
    });
  }

  void _changeText(String text) {
    setState(() {
      _overrideText = text;
    });
  }
}

enum SearchBarLeading { nothing, burger, back }

class SearchBarController {
  Function(Color) onBorderColorChanged;
  Function(String) onTextChanged;
  Function(String) onGetText;

  void changeBorderColor(Color color) {
    if (onBorderColorChanged != null) onBorderColorChanged(color);
  }

  void resetBorderColor() {
    if (onBorderColorChanged != null) onBorderColorChanged(null);
  }

  void changeText(String text) {
    if (onTextChanged != null) onTextChanged(text);
  }

  void resetText() {
    if (onTextChanged != null) onTextChanged(null);
  }
}
