import 'dart:async';

import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:metro/favorites/blocs/favorites.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/filters/blocs/filters_bloc.dart';
import 'package:metro/filters/filters_type.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/filters/models/filter.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/location.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/models/properties.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/widgets/route_details.dart';
import 'package:metro/search/blocs/search.dart';

/// The [CustomSearchDelegate] will handle the display and sending search
/// request to [SearchBloc] based on the used input.
///
/// Use `Utils.showSearch(context)` to launch a search page.
///
/// To get the selected point from it use `Point p = await Utils.showSearch(context);`.
class CustomSearchDelegate extends SearchDelegate<Point> {
  final bool isSchedulePage;
  bool isFavoritesRequested = false;
  bool isVisible = false;

  final GlobalKey _loadingDialogKey = GlobalKey();

  CustomSearchDelegate(this.isSchedulePage, {Key key}) : super();

  String _previousQuery;
  Timer timer;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(
          Icons.clear,
          color: globalElevations.fstTextColor.backgroundColor,
        ),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  /// Override the theme of the search input.
  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);

    final ThemeData base = theme;
    return base.copyWith(
      primaryColor: globalElevations.e02dp.backgroundColor,
      textTheme: _buildDefaultTextTheme(base.textTheme),
      iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
    );
  }

  /// Resize the displayed title.
  ///
  /// The displayed label value "Chercher un lieu, un arrêt..." is edited in CustomLocalization in main.dart.
  TextTheme _buildDefaultTextTheme(TextTheme base) {
    return base.copyWith(
      title: base.title.copyWith(
        fontSize: 16,
        fontWeight: FontWeight.w300,
        color: globalElevations.fstTextColor.backgroundColor,
        decorationColor: globalElevations.e02dp.backgroundColor,
      ),
    );
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
        color: globalElevations.fstTextColor.backgroundColor,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  /// Called when the used press enter on the keyboard.
  @override
  Widget buildResults(BuildContext context) {
    // Simply build view from previous suggestion
    return _buildResultView(context);
  }

  /// Called every time the search input change.
  @override
  Widget buildSuggestions(BuildContext context) {
    if (!isFavoritesRequested) {
      isFavoritesRequested = true;
      BlocProvider.of<FavoritesBloc>(context).requestSearchFavorites(limited: true);
    }

    if (query.isEmpty || query.length < 3 || query.length > 50) return _buildEmptyView(context);

    // Request for suggestions if the current query is different from the previous one
    if (query != _previousQuery) {
      BlocProvider.of<SearchBloc>(context).search(query.replaceAll("’", "'"), isSchedulePage);
      _previousQuery = query;
    }
    // And build the view
    return _buildResultView(context);
  }

  Widget _buildEmptyView(BuildContext context) {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                color: globalElevations.e02dp.backgroundColor,
                child: ListTile(
                    leading: Icon(
                      Icons.my_location,
                      color: globalElevations.fstTextColor.backgroundColor,
                      size: 30,
                    ),
                    title: Text(
                      translations.text('search.my_position'),
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                    onTap: () async {
                      await myPosition(context);
                    }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 14.0, 8.0, 10.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(translations.text('search.favorites'),
                      style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16, fontWeight: FontWeight.w400),
                      textAlign: TextAlign.left)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: StreamBuilder<List<Point>>(
                stream: BlocProvider.of<FavoritesBloc>(context).onFavoritesInSearchChanged,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    isVisible = false;
                    return AnimatedLoadingLogo();
                  } else {
                    if (snapshot.data.length == 3) isVisible = true;

                    return ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        final fav = snapshot.data[index];

                        return Padding(
                          padding: const EdgeInsets.fromLTRB(6.0, 1.5, 8.0, 1.5),
                          child: Container(
                            color: globalElevations.e02dp.backgroundColor,
                            child: ListTile(
                                leading: Icon(
                                  fav.properties.iconData(),
                                  color: globalElevations.fstTextColor.backgroundColor,
                                  size: 30,
                                ),
                                title: Text(
                                  fav.properties.name,
                                  style: TextStyle(fontWeight: FontWeight.w300),
                                ),
                                subtitle: Text(fav.properties.city),
                                onTap: () {
                                  if (fav.properties.type == PropertiesType.lieux) BlocProvider.of<MapBloc>(context).setSearchPoint(fav);
                                  close(context, fav);
                                }),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                isVisible = false;
                BlocProvider.of<FavoritesBloc>(context).requestSearchFavorites();
              },
              child: Visibility(
                visible: isVisible,
                child: Padding(
                  padding: const EdgeInsets.only(top: 14.0),
                  child: Text(
                    translations.text('search.more'),
                    style: TextStyle(color: Theme.of(context).indicatorColor),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(14.0, 14.0, 8.0, 10.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(translations.text('search.recent_search'),
                      style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontSize: 16, fontWeight: FontWeight.w400),
                      textAlign: TextAlign.left)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: FutureBuilder<List<LastSearchPoint>>(
                future: BlocProvider.of<SearchBloc>(context).getLastSearchPoints(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return AnimatedLoadingLogo();

                  return ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      final item = snapshot.data[index];
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(6.0, 1.5, 8.0, 1.5),
                        child: Container(
                          color: globalElevations.e02dp.backgroundColor,
                          child: ListTile(
                            leading: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Icon(
                                item.point.properties.iconData(),
                                color: globalElevations.fstTextColor.backgroundColor,
                                size: 30,
                              ),
                            ),
                            title: Text(
                              item.point.properties.name,
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            subtitle: Text(item.point.properties.city),
                            onTap: () {
                              if (item.point.properties.type == PropertiesType.lieux) BlocProvider.of<MapBloc>(context).setSearchPoint(item.point);
                              close(context, item.point);
                            },
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> myPosition(BuildContext context) async {
    showLoadingDialog(context, _loadingDialogKey);
    bool gotLocation = await BlocProvider.master<LocationBloc>().getLocation();
    // Close the loading dialog
    Navigator.of(context, rootNavigator: true).pop();
    if (gotLocation) {
      Utils.isPositionInGrenoble(_currentLatitude(context), _currentLongitude(context))
          ? close(context, _currentLocationPoint(context, _currentLatitude(context), _currentLongitude(context)))
          : alerteMessagePosition(context);
    } else {
      alerteMessagePositionNotAccepted(context);
    }
  }

  Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SimpleDialog(key: key, children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16),
              child: Center(
                child: Row(children: [
                  AnimatedLoadingLogo(
                    height: 60,
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Text(
                    translations.text('search.location_loading_message'),
                    softWrap: true,
                  ))
                ]),
              ),
            )
          ]);
        });
  }

  Future<void> alerteMessagePosition(BuildContext context) {
    // flutter defined function
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(translations.text('alert_message.title')),
          content: new Text(translations.text('alert_message.content')),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(translations.text('alert_message.close')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> alerteMessagePositionNotAccepted(BuildContext context) {
    // flutter defined function
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(translations.text('alert_message.title')),
          content: new Text("Vous ne pouvez pas utiliser votre position si vous n'accpetez pas la géolocalisation."),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(translations.text('alert_message.close')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildLoadingView() {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: const AnimatedLoadingLogo(),
      ),
    );
  }

  Widget _buildNoResultView() {
    return Container(
      color: globalElevations.e00dp.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Text(
            translations.text('search.no_result'),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
          ),
        ),
      ),
    );
  }

  List<PropertiesType> requestFilter() {
    List<PropertiesType> request = [];
    List<FiltersType> filterType = [];

    for (MapEntry<Filter, bool> filtersValue in BlocProvider.master<FiltersBloc>().filtersValue.entries) {
      if (filtersValue.value != null && filtersValue.value) {
        filterType.add(filtersValue.key.type);
      }
    }

    filterType.map((filter) {
      switch (filter) {
        case FiltersType.stops:
          request.add(PropertiesType.clusters);
          break;
        case FiltersType.parkings:
        case FiltersType.parkings_PKG:
        case FiltersType.parkings_PAR:
          request.add(PropertiesType.par);
          request.add(PropertiesType.pkg);
          break;
        case FiltersType.carpooling:
        // case FiltersType.carpooling_charging:
        case FiltersType.carpooling_parking:
        case FiltersType.carpooling_picking:
        case FiltersType.carpooling_stops:
          request.add(PropertiesType.autostop);
          break;
        case FiltersType.energy:
          request.add(PropertiesType.recharge);
          break;
        case FiltersType.energy_electric:
          request.add(PropertiesType.recharge);
          request.add(PropertiesType.irve);
          break;
        case FiltersType.energy_hydrogen:
          request.add(PropertiesType.hydrogene);
          break;
        case FiltersType.energy_GNV:
          request.add(PropertiesType.gnv);
          break;
        case FiltersType.veloservice:
          request.add(PropertiesType.veloservice);
          break;
        case FiltersType.energy_GPL:
          request.add(PropertiesType.gpl);
          break;
        case FiltersType.stations:
        case FiltersType.stations_agenceM:
        case FiltersType.stations_TAG:
        case FiltersType.stations_transisere:
          request.add(PropertiesType.depositaire);
          request.add(PropertiesType.mva);
          break;
        case FiltersType.bikes_location:
        case FiltersType.stations_bike:
          request.add(PropertiesType.mvc);
          break;
        default:
      }
    }).toList();

    return request;
  }

  List<Point> reorderFilter(List<Point> temp) {
    List<Point> reorderList = [];
    for (Point p in temp) {
      if (p.properties.type == PropertiesType.pkg || p.properties.type == PropertiesType.par) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.agenceM) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.mvc || p.properties.type == PropertiesType.mva) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.recharge ||
          p.properties.type == PropertiesType.irve ||
          p.properties.type == PropertiesType.hydrogene ||
          p.properties.type == PropertiesType.gnv ||
          p.properties.type == PropertiesType.gpl) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.clusters) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.autostop ||
          p.properties.type == PropertiesType.depositaire ||
          p.properties.type == PropertiesType.parkingCov ||
          p.properties.type == PropertiesType.pointCov ||
          p.properties.type == PropertiesType.pointService) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.lieux) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (p.properties.type == PropertiesType.rue) {
        reorderList.add(p);
      }
    }

    for (Point p in temp) {
      if (!reorderList.contains(p)) {
        reorderList.add(p);
      }
    }

    return reorderList;
  }

  Widget _buildResultView(BuildContext context) {
    SearchBloc searchBloc = BlocProvider.of<SearchBloc>(context);
    return StreamBuilder<List<Point>>(
      stream: searchBloc.onSearchFinish,
      builder: (context, snapshot) {
        // If search hasn't occured yet (not enought/too many characters), loading...
        if (snapshot.data == null)
          return _buildLoadingView();
        // Else if there is no match for the given input
        else if (snapshot.data.isEmpty) return _buildNoResultView();

        // Otherwise, we build the list of suggestion
        List<Point> results = snapshot.data;
        List<Point> temp = reorderFilter(results);
        List<Point> othersResult = [];
        List<Point> finalResult = [];
        List<PropertiesType> filters = requestFilter();

        for (Point p in temp) {
          if (filters.contains(p.properties.type)) {
            finalResult.add(p);
          }
        }

        for (Point p in temp) {
          if (!filters.contains(p.properties.type)) {
            finalResult.add(p);
          }
        }

        // finalResult += othersResult;

        return Container(
          color: globalElevations.e00dp.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ListView.builder(
              itemCount: finalResult.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(6.0, 1.5, 8.0, 1.5),
                  child: Container(
                    color: globalElevations.e02dp.backgroundColor,
                    child: ListTile(
                      leading: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Icon(
                          finalResult[index].properties.iconData(),
                          color: globalElevations.fstTextColor.backgroundColor,
                        ),
                      ),
                      title: Text(
                        finalResult[index].properties.name,
                        style: TextStyle(fontWeight: FontWeight.w300),
                      ),
                      subtitle: Text(finalResult[index].properties.city),
                      onTap: () {
                        Point selected = finalResult[index];
                        if (selected.properties.type == PropertiesType.lieux) BlocProvider.of<MapBloc>(context).setSearchPoint(selected);
                        searchBloc.addLastSearch(selected);
                        close(context, selected);
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  Point _currentLocationPoint(BuildContext context, double latitude, double longitude) {
    List<double> currentLocation = [longitude, latitude];

    Point currentLocationPoint = Point(
        properties: ClustersProperties(PropertiesType.clusters, '1', 'Ma Position', 'Grenoble', true, 1, '01234'),
        geometry: Geometry(coordinates: currentLocation));

    return currentLocationPoint;
  }

  double _currentLongitude(BuildContext context) {
    LocationData location = BlocProvider.of<LocationBloc>(context).location;
    return location?.longitude;
  }

  double _currentLatitude(BuildContext context) {
    LocationData location = BlocProvider.of<LocationBloc>(context).location;
    return location?.latitude;
  }
}
