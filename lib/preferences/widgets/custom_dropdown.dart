import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  final String initialValue;
  final List<String> items;
  final Function(String) onChanged;
  final String label;
  final IconData icon;

  CustomDropdown({
    @required this.initialValue,
    @required this.items,
    @required this.icon,
    @required this.label,
    @required this.onChanged,
  });

  @override
  State<StatefulWidget> createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropdown> {
  final _baseSize = 16.0;

  Widget _buildDropdown() {
    return DropdownButton(
      value: widget.initialValue,
      items: widget.items
          .map((elem) => DropdownMenuItem(
                child: Text(elem),
                value: elem,
              ))
          .toList(),
      onChanged: widget.onChanged,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: _baseSize, bottom: _baseSize),
      child: Row(
        children: <Widget>[
          Icon(widget.icon),
          SizedBox(width: _baseSize),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.label,
                style: TextStyle(
                  fontSize: _baseSize - 4,
                  color: Colors.grey,
                ),
              ),
              _buildDropdown(),
            ],
          ),
        ],
      ),
    );
  }
}
