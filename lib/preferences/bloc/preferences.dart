import 'dart:io';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/localization/translations.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';

class PreferencesBloc extends BlocBase {
  Stream<String> get onLanguageChanged => _languageController.stream;

  Stream<int> get onHomePageChanged => _homePageController.stream;

  BehaviorSubject<String> _languageController;

  BehaviorSubject<int> _homePageController;

  String get language => _languageController.value;

  int get homePage => _homePageController.value;

  List<String> _pages;
  List<String> get pages => _pages;

  BehaviorSubject<bool> _themeChangedController =
      BehaviorSubject<bool>.seeded(null);
  Stream<bool> get onMyThemeChanged => _themeChangedController.stream;
  bool get themeChanged => _themeChangedController.value;

  List<String> _themesLabel;
  List<String> get themesLabel => _themesLabel;
  String get selectedThemeValue =>
      _themeChangedController.value ? _themesLabel.first : _themesLabel[1];

  @override
  void initState() async {
    _languageController = BehaviorSubject<String>.seeded(
        PrefService.getString(Consts.preferencesLanguage) ??
            Platform?.localeName?.toLowerCase());
    _homePageController = BehaviorSubject<int>.seeded(
        PrefService.getInt(Consts.preferencesHome) ?? 0);
    refreshPages();
  }

  updateLanguage(String value) {
    String localeCode = replaceLanguageToLocal(value);
    PrefService.setString(Consts.preferencesLanguage, localeCode);
    translations.setNewLanguage(localeCode).then((_) {
      // replace setstate
      _languageController.sink.add(localeCode);
      refreshPages();
      _homePageController.sink.add(_homePageController.value);
    });
  }

  String selectedPageValue() {
    return _pages[_homePageController.value];
  }

  String getValue() {
    String result;
    if (language == Consts.defaultFRLanguage)
      result = Consts.languages[0];
    else if (language == Consts.defaultWorldLanguage)
      result = Consts.languages[1];
    else
      result = translations.getDeviceLanguage(1);
    return result;
  }

  changedHomePage(int index, BuildContext context) {
    PrefService.setInt(Consts.preferencesHome, index);
    BlocProvider.of<NavigationBloc>(context).changeTabByIndex(index);
    _homePageController.sink.add(index);
  }

  String replaceLanguageToLocal(String value) {
    return value == Consts.languages[0]
        ? Consts.defaultFRLanguage
        : Consts.defaultWorldLanguage;
  }

  refreshPages() {
    _pages = [
      translations.text(Consts.preferencesMap),
      translations.text(Consts.preferencesFavorite),
      translations.text(Consts.preferencesRoute),
      translations.text(Consts.preferencesSchedule)
    ];

    _themesLabel = [
      translations.text('preferences.dark_label'),
      translations.text('preferences.light_label')
    ];
  }

  void onThemeChanged(bool value, BuildContext context) {
    DynamicTheme.of(context)
        .setBrightness(value ? Brightness.dark : Brightness.light);
    _themeChangedController.sink.add(value);
  }

  void initThemeValue(bool value) {
    _themeChangedController.sink.add(value);
  }

  Color getDividerColor(BuildContext context) {
    if (Theme.of(context).brightness == Brightness.dark) {
      return Colors.white;
    } else {
      return Colors.black;
    }
  }

  @override
  void dispose() {
    _homePageController.close();
    _languageController.close();
    _themeChangedController.close();
  }
}
