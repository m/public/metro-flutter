import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';

//DARK THEME DATA
ThemeData _tempDark = ThemeData.from(colorScheme: colorSchemeDark);
final darkTheme = _tempDark.copyWith(
  bottomSheetTheme: _tempDark.bottomSheetTheme.copyWith(
      backgroundColor: Elevations.darkModeEnabled(true).e02dp.backgroundColor),
  buttonTheme: ButtonThemeData(
    colorScheme: colorSchemeDark,
    textTheme: ButtonTextTheme.primary,
  ),
  dialogBackgroundColor: Elevations.darkModeEnabled(true).e08dp.backgroundColor,
  canvasColor: Elevations.darkModeEnabled(true).e08dp.backgroundColor,
  indicatorColor: colorSchemeDark.primary,
  snackBarTheme: SnackBarThemeData(
      backgroundColor: Elevations.darkModeEnabled(true).e24dp.backgroundColor,
      actionTextColor: colorSchemeDark.primary,
      contentTextStyle: _tempDark.textTheme.title),
  chipTheme: _tempDark.chipTheme.copyWith(
    secondarySelectedColor: colorSchemeDark.primary,
    disabledColor: Elevations.darkModeEnabled(true).e08dp.backgroundColor,
  ),
    inputDecorationTheme: _tempDark.inputDecorationTheme.copyWith(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: Elevations.darkModeEnabled(true)
                    .sndTextColor
                    .backgroundColor))),);

ColorScheme colorSchemeDark = ColorScheme(
  brightness: Brightness.dark,
  primary: const Color(0xffb49bda),
  primaryVariant: const Color(0xff645bba),
  onPrimary: const Color(0xffffffff),
  secondary: const Color(0xff96dbeb),
  secondaryVariant: const Color(0xff66abcb),
  onSecondary: const Color(0xff000000),
  background: Elevations.darkModeEnabled(true).e00dp.backgroundColor,
  onBackground: const Color(0xffffffff),
  surface: Elevations.darkModeEnabled(true).e00dp.backgroundColor,
  onSurface: const Color(0xffffffff),
  error: const Color(0xffff6e6e),
  onError: const Color(0xff000000),
);

//LIGHT THEME DATA
ThemeData _tempLight = ThemeData.from(colorScheme: colorSchemeLight);
final lightTheme = _tempLight.copyWith(
  bottomSheetTheme: _tempDark.bottomSheetTheme.copyWith(
      backgroundColor: Elevations.darkModeEnabled(false).e02dp.backgroundColor),
  buttonTheme: ButtonThemeData(
    colorScheme: colorSchemeLight,
    textTheme: ButtonTextTheme.primary,
  ),
  dialogBackgroundColor:
      Elevations.darkModeEnabled(false).e08dp.backgroundColor,
  canvasColor: Elevations.darkModeEnabled(false).e08dp.backgroundColor,
  indicatorColor: colorSchemeLight.primary,
  snackBarTheme: SnackBarThemeData(
      backgroundColor: Elevations.darkModeEnabled(false).e24dp.backgroundColor,
      actionTextColor: colorSchemeLight.primary,
      contentTextStyle: _tempLight.textTheme.title),
  chipTheme: _tempLight.chipTheme.copyWith(
    secondarySelectedColor: colorSchemeLight.primary,
    disabledColor: Elevations.darkModeEnabled(false).e08dp.backgroundColor,
  ),
    inputDecorationTheme: _tempLight.inputDecorationTheme.copyWith(
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: Elevations.darkModeEnabled(false)
                    .sndTextColor
                    .backgroundColor))),);

ColorScheme colorSchemeLight = ColorScheme(
  brightness: Brightness.light,
  primary: const Color(0xff8154C0),
  primaryVariant: const Color(0xff645bba),
  onPrimary: const Color(0xffffffff),
  secondary: const Color(0xff6cc2d7),
  secondaryVariant: const Color(0xff66abcb),
  onSecondary: const Color(0xff000000),
  background: Elevations.darkModeEnabled(false).e00dp.backgroundColor,
  onBackground: const Color(0xffffffff),
  surface: Elevations.darkModeEnabled(false).e00dp.backgroundColor,
  onSurface: const Color(0xff000000),
  error: const Color(0xffff6e6e),
  onError: const Color(0xff000000),
);
