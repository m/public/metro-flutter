import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/widgets/switch.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/preferences/bloc/preferences.dart';
import 'package:metro/preferences/widgets/custom_dropdown.dart';

class PreferencesPage extends TraceableStatefulWidget {
  PreferencesPage({Key key})
      : super(key: key, name: MatomoBloc.screen_informations);

  @override
  _PreferencesPageState createState() => _PreferencesPageState();
}

class _PreferencesPageState extends State<PreferencesPage> {
  List<String> pages;
  bool _darkTheme = true;

  @override
  Widget build(BuildContext context) {
    _darkTheme = Theme.of(context).brightness == Brightness.dark;

    return StreamBuilder<String>(
        stream: BlocProvider.master<PreferencesBloc>().onLanguageChanged,
        initialData: BlocProvider.of<PreferencesBloc>(context).language,
        builder: (context, snapshot) {
          return Theme(
            data: Theme.of(context).copyWith(
                scaffoldBackgroundColor:
                    globalElevations.infoBackground.backgroundColor),
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: globalElevations.e16dp.backgroundColor,
                iconTheme: IconThemeData(
                    color: globalElevations.fstTextColor.backgroundColor),
                title: Text(
                  translations.text(Consts.drawerPreferences),
                  style: TextStyle(
                      color: globalElevations.fstTextColor.backgroundColor),
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.all(8),
                child: SingleChildScrollView(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Text(
                              translations.text(Consts.preferencesTitle),
                              style: TextStyle(color: Colors.grey),
                            )
                          ],
                        ),
                      ),
                    ),
                    CustomDropdown(
                        icon: Icons.language,
                        label: translations.text(Consts.languageTitle),
                        initialValue: BlocProvider.of<PreferencesBloc>(context)
                            .getValue(),
                        items: Consts.languages,
                        onChanged: (String value) {
                          BlocProvider.of<PreferencesBloc>(context)
                              .updateLanguage(value);
                        }),
                    StreamBuilder<int>(
                      stream: BlocProvider.of<PreferencesBloc>(context)
                          .onHomePageChanged,
                      initialData:
                          BlocProvider.of<PreferencesBloc>(context).homePage,
                      builder: (context, snapshot) {
                        return CustomDropdown(
                          icon: Icons.bookmark_border,
                          label: translations.text(Consts.homeTitle),
                          initialValue:
                              BlocProvider.of<PreferencesBloc>(context)
                                  .selectedPageValue(),
                          items:
                              BlocProvider.of<PreferencesBloc>(context).pages,
                          onChanged: (value) {
                            int _page =
                                BlocProvider.of<PreferencesBloc>(context)
                                    .pages
                                    .indexOf(value);
                            BlocProvider.of<PreferencesBloc>(context)
                                .changedHomePage(_page, context);
                          },
                        );
                      },
                    ),
                    CustomDropdown(
                      icon: Icons.color_lens,
                      label: translations.text(Consts.themeTitle),
                      initialValue: BlocProvider.of<PreferencesBloc>(context)
                          .selectedThemeValue,
                      items:
                          BlocProvider.of<PreferencesBloc>(context).themesLabel,
                      onChanged: (_) =>
                          BlocProvider.of<PreferencesBloc>(context)
                              .onThemeChanged(!_darkTheme, context),
                    ),
                  ],
                )),
              ),
            ),
          );
        });
  }
}
