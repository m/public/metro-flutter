import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/widgets/custom_drawer.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:metro/global/widgets/switch.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/models/route_preferences.dart';
import 'package:metro/route/models/transport_types.dart';
import 'package:metro/route/widgets/dropdown.dart';
import 'package:metro/route/widgets/route_search_input.dart';
import 'package:metro/search/widgets/search_bar.dart';

class RoutePage extends TraceableStatefulWidget {
  RoutePage({Key key}) : super(key: key, name: MatomoBloc.screen_route_search);

  @override
  _RoutePageState createState() => _RoutePageState();
}

class _RoutePageState extends State<RoutePage> {
  static DateTime _previousDate;
  static TimeOfDay _previousTime;

  DateTime _selectedDate;
  TimeOfDay _selectedTime;
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  TextEditingController _transportController = TextEditingController();

  List<TimePreference> _availablePreferences = [GoAfter(), ArriveBefore()];
  TimePreference _selectedTimePreference;

  List<SortPreference> _availableSorts = [
    Fastest(),
    LessSteps(),
    MostSecured(),
    LessPoluated()
  ];
  SortPreference _selectedSort;

  TextEditingController _departureController = TextEditingController();
  TextEditingController _destinationController = TextEditingController();

  GlobalKey<FormState> _formKey;
  StreamSubscription<Point> _listenerDeparture;
  StreamSubscription<Point> _listenerDestination;

  SearchBarController _departureSearchBarController = SearchBarController();
  SearchBarController _destinationSearchBarController = SearchBarController();

  String departure = '';
  String destination = '';

  TimeOfDay testTime;

  dynamic favItinerary;

  @override
  void initState() {
    super.initState();

    if (_previousDate == null) {
      _previousDate = DateTime.now();
    }
    if (_previousTime == null) {
      _previousTime = TimeOfDay.now();
    }

    _setDate(_previousDate);
    _setTime(_previousTime);
    _setTransport(null);

    bool _arriveBy = BlocProvider.of<RoutesBloc>(context).arriveBy;
    _selectedTimePreference = _arriveBy == null || !_arriveBy ? _availablePreferences[0] : _availablePreferences[1];
    _selectedSort = _availableSorts[0];

    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);
    _listenerDeparture = routesBloc.onDepartureChanged.listen((point) {
      if (point != null) {
        _departureSearchBarController.changeText(point.properties.name);
        _departureSearchBarController
            .changeBorderColor(Theme.of(context).indicatorColor);
        departure = point.properties.name;
      } else {
        _departureSearchBarController.changeBorderColor(Colors.grey);
      }
    });
    _listenerDestination = routesBloc.onDestinationChanged.listen((point) {
      if (point != null) {
        _destinationSearchBarController.changeText(point.properties.name);
        _destinationSearchBarController
            .changeBorderColor(Theme.of(context).indicatorColor);
        destination = point.properties.name;
      } else {
        _destinationSearchBarController.changeBorderColor(Colors.grey);
      }
    });

    _formKey = GlobalKey<FormState>();

    _setFavData();
  }

  Widget _buildSwapButton() {
    return GestureDetector(
      onTap: () => _swapDepartureAndDestination(),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: globalElevations.e16dp.backgroundColor,
          border:
              Border.all(width: 1.2, color: Theme.of(context).indicatorColor),
        ),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 4),
        child: Icon(
          Icons.import_export,
          color: Theme.of(context).indicatorColor,
        ),
      ),
    );
  }

  FloatingActionButton _buildFloatingActionButton() {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);
    return FloatingActionButton.extended(
        backgroundColor:
            (routesBloc.destination != null && routesBloc.departure != null)
                ? Theme.of(context).accentColor
                : globalElevations.expandPanelColor.backgroundColor,
        icon: Icon(Icons.near_me,
            color:
                (routesBloc.destination != null && routesBloc.departure != null)
                    ? Colors.black
                    : Color(0xFF7a787d)),
        onPressed: () {
          List<TransportType> _selectedTransportType = BlocProvider.of<RoutesBloc>(context).getSelectedTransports();
          // Validate forms & calculate route
          if (_selectedTransportType.isNotEmpty){
            if (routesBloc.destination != null && routesBloc.departure != null) {
              _calculateRoute();
            } else if (routesBloc.departure == null ||
                routesBloc.destination == null) {
              if (routesBloc.departure == null)
                _departureSearchBarController.changeBorderColor(Colors.red);
              if (routesBloc.destination == null)
                _destinationSearchBarController.changeBorderColor(Colors.red);
            }
          }else{
            Fluttertoast.showToast(
                msg: "Veuillez sélectionner un mode de transport",
                toastLength: Toast.LENGTH_LONG);
          }

        },
        label: Text(translations.text('search_button.SEARCH')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(),
      floatingActionButton: _buildFloatingActionButton(),
      appBar: AppBar(
          backgroundColor: globalElevations.e16dp.backgroundColor,
          iconTheme: IconThemeData(color: globalElevations.fstTextColor.backgroundColor),
          title: Text(translations.text('route.header'),
              style: TextStyle(color: globalElevations.fstTextColor.backgroundColor))),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 16),
        color: globalElevations.backgroundWhite.backgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        RouteSearchInput(
                          label: translations.text('route.start') + " *",
                          color: globalElevations.e16dp.backgroundColor,
                          controller: _departureSearchBarController,
                          isSchedulePage: false,
                          onSearchComplete: (point) async {
                            departure = point.properties.name;
                            _departureSearchBarController
                                .changeText(point.properties.name);
                            _departureSearchBarController.changeBorderColor(
                                Theme.of(context).indicatorColor);
                            BlocProvider.of<RoutesBloc>(context)
                                .setDeparture(point);
                            _setFavData();
                          },
                        ),
                        RouteSearchInput(
                          label: translations.text('route.end') + " *",
                          color: globalElevations.e16dp.backgroundColor,
                          controller: _destinationSearchBarController,
                          isSchedulePage: false,
                          onSearchComplete: (point) async {
                            destination = point.properties.name;
                            _destinationSearchBarController
                                .changeText(point.properties.name);
                            _destinationSearchBarController.changeBorderColor(
                                Theme.of(context).indicatorColor);
                            BlocProvider.of<RoutesBloc>(context)
                                .setDestination(point);
                            _setFavData();
                          },
                        ),
                      ],
                    ),
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: _buildSwapButton(),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: ExpandablePanel(
                          showShadow: false,
                          elevation: globalElevations.backgroundExpand,
                          showBorder: false,
                          isExpanded: true,
                          body: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              translations.text('route.date_heure'),
                              style: TextStyle(
                                color: globalElevations
                                    .fstTextColor.backgroundColor,
                                fontSize: 15,
                              ),
                            ),
                          ),
                          detail: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: TextField(
                                      focusNode: AlwaysDisabledFocusNode(),
                                      maxLines: 1,
                                      style: TextStyle(fontSize: 13),
                                      controller: _dateController,
                                      onTap: () => _onDateTap(),
                                      enableInteractiveSelection: false,
                                      decoration: InputDecoration(
                                          alignLabelWithHint: true),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: FixedDropdownButtonFormField<
                                        TimePreference>(
                                      value: _selectedTimePreference,
                                      decoration: InputDecoration(
                                          alignLabelWithHint: true),
                                      onChanged: _onPreferenceChange,
                                      items: _availablePreferences.map(
                                        (value) {
                                          return DropdownMenuItem<
                                              TimePreference>(
                                            value: value,
                                            child: Text(
                                              value.toString(),
                                              style: TextStyle(fontSize: 13),
                                            ),
                                          );
                                        },
                                      ).toList(),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: TextField(
                                      focusNode: AlwaysDisabledFocusNode(),
                                      maxLines: 1,
                                      style: TextStyle(fontSize: 13),
                                      controller: _timeController,
                                      onTap: () => _onTimeTap(),
                                      enableInteractiveSelection: false,
                                      decoration: InputDecoration(
                                          alignLabelWithHint: true),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: ExpandablePanel(
                          showShadow: false,
                          elevation: globalElevations.backgroundExpand,
                          showBorder: false,
                          isExpanded: true,
                          body: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Text(
                              translations.text('route.transport_types'),
                              style: TextStyle(
                                color: globalElevations
                                    .fstTextColor.backgroundColor,
                                fontSize: 15,
                              ),
                            ),
                          ),
                          detail: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                StreamBuilder<Map<TransportType, bool>>(
                                  stream: BlocProvider.of<RoutesBloc>(context)
                                      .onSelectedTransportsChanged,
                                  initialData:
                                      BlocProvider.of<RoutesBloc>(context)
                                          .selectedTransports,
                                  builder: (context, snapshot) {
                                    if (!snapshot.hasData ||
                                        snapshot.data == null)
                                      return Container();

                                    return Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.start,
                                      children:
                                          snapshot.data.entries.map((mapEntry) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(right: 8),
                                          child: ChoiceChip(
                                            selected: mapEntry.value,
                                            label: Text(
                                              mapEntry.key.shortName,
                                              style: TextStyle(
                                                color: mapEntry.value
                                                    ? globalElevations
                                                        .inverseColor
                                                        .backgroundColor
                                                    : globalElevations
                                                        .fstTextColor
                                                        .backgroundColor,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                            onSelected: (_) {
                                              setState(() {
                                                BlocProvider.of<RoutesBloc>(
                                                        context)
                                                    .setTransportTypeValue(
                                                        mapEntry.key,
                                                        !mapEntry.value);
                                                _setFavData();
                                              });
                                            },
                                          ),
                                        );
                                      }).toList(),
                                    );
                                  },
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 3),
                                        child: Icon(
                                          Icons.accessible,
                                          color: globalElevations
                                              .fstTextColor.backgroundColor,
                                        ),
                                      ),
                                      Text(translations
                                          .text('route.pmr_access')),
                                      StreamBuilder<bool>(
                                        stream:
                                            BlocProvider.of<RoutesBloc>(context)
                                                .onPmrValueChanged,
                                        initialData:
                                            BlocProvider.of<RoutesBloc>(context)
                                                .pmrValue,
                                        builder: (context, snapshot) {
                                          if (!snapshot.hasData ||
                                              snapshot.data == null)
                                            return Container();

                                          return CustomSwitch(
                                            value: snapshot.data,
                                            onChanged: (value) {
                                              BlocProvider.of<RoutesBloc>(
                                                      context)
                                                  .setPmrValue(value);
                                            },
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 30),
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              FavoriteButton(
                                data: favItinerary,
                                border: true,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _departureController.dispose();
    _destinationController.dispose();
    _listenerDeparture.cancel();
    _listenerDestination.cancel();
    super.dispose();
  }

  void _onDateTap() async {
    DateTime date = await showDatePicker(
      builder: _builderWithTheme,
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime.now().subtract(Duration(days: 1)),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );

    if (date != null && date != _selectedDate)
      setState(() {
        _setDate(date);
      });
  }

  void _setDate(DateTime date) {
    _selectedDate = date;
    _previousDate = date;
    _dateController.text = dateFormat.format(date);
  }

  void _onTimeTap() async {
    TimeOfDay time = await showTimePicker(
      builder: _builderWithTheme,
      context: context,
      initialTime: _selectedTime,
    );
    if (time != null && time != _selectedTime)
      setState(() {
        _setTime(time);
      });
  }

  void _setTime(TimeOfDay time) {
    DateTime now = DateTime.now();
    _selectedTime = time;
    _previousTime = time;
    testTime = time;
    _timeController.text = timeFormat
        .format(DateTime(now.year, now.month, now.day, time.hour, time.minute));
  }

  void _setFavData() {
    setState(() {
      favItinerary = BlocProvider.of<RoutesBloc>(context).favoriteItinerary;
    });
  }

  void _onPreferenceChange(TimePreference preference) {
    if (preference != null && preference != _selectedTimePreference) {
      setState(() {
        _setPreference(preference);
      });
    }
  }

  void _setPreference(TimePreference preference) {
    _selectedTimePreference = preference;
  }

  void _setTransport(dynamic value) {
    _transportController.text = ' ';
  }

  void _swapDepartureAndDestination() {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);

    if (routesBloc.departure != null && routesBloc.destination != null) {
      Point swapPoint = routesBloc.destination;
      routesBloc.setDestination(routesBloc.departure);
      routesBloc.setDeparture(swapPoint);
      _departureSearchBarController
          .changeBorderColor(Theme.of(context).indicatorColor);
      _destinationSearchBarController
          .changeBorderColor(Theme.of(context).indicatorColor);
    } else if (routesBloc.departure != null && routesBloc.destination == null) {
      routesBloc.setDestination(routesBloc.departure);
      routesBloc.setDeparture(null);
      _departureSearchBarController
          .changeText(translations.text('route.start'));
      _destinationSearchBarController
          .changeBorderColor(Theme.of(context).indicatorColor);
      _departureSearchBarController.changeBorderColor(Colors.red);
    } else if (routesBloc.destination != null && routesBloc.departure == null) {
      routesBloc.setDeparture(routesBloc.destination);
      routesBloc.setDestination(null);
      _destinationSearchBarController
          .changeText(translations.text('route.end'));
      _destinationSearchBarController.changeBorderColor(Colors.red);
      _departureSearchBarController
          .changeBorderColor(Theme.of(context).indicatorColor);
    }

    _setFavData();
  }

  void _calculateRoute() {
    // TODO : check for inputs
    if (_formKey.currentState.validate()) {
      List<TransportType> _selectedTransportType =
          BlocProvider.of<RoutesBloc>(context).getSelectedTransports();
      BlocProvider.of<RoutesBloc>(context).requestRoutes(
          BlocProvider.of<LinesBloc>(context).lines,
          _selectedTransportType,
          _selectedTimePreference,
          _selectedDate,
          _selectedTime,
          _selectedSort);
      BlocProvider.of<MapBloc>(context).setBottomSheetContent(
          BottomSheetCategory.routes,
          arguments: _selectedTimePreference);
      BlocProvider.of<NavigationBloc>(context)
          .changeTab(NavigationCategory.routeResult);
    }
  }
}

/// Builds the date and time picker with the appropriate theme.
Widget _builderWithTheme(BuildContext context, Widget child) => Theme(
      data: Theme.of(context)
          .copyWith(backgroundColor: globalElevations.e08dp.backgroundColor),
      child: child,
    );

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
