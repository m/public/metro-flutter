import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/line.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

import '../route_utils.dart';

class RouteDetailsBottomSheetHeader extends StatelessWidget {
  final String departureTime;
  final String destinationTime;
  final String duration;

  static const TextStyle textStyle = const TextStyle(color: Colors.white);

  const RouteDetailsBottomSheetHeader._({Key key, @required this.departureTime, @required this.destinationTime, @required this.duration})
      : super(key: key);

  factory RouteDetailsBottomSheetHeader({Key key, @required Itinerary selectedRoute}) {
    String departureTime = selectedRoute.startTimeText;
    String destinationTime = selectedRoute.endTimeText;
    String duration = selectedRoute.durationText;
    return RouteDetailsBottomSheetHeader._(key: key, departureTime: departureTime, destinationTime: destinationTime, duration: duration);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).bottomSheetTheme.backgroundColor,
          borderRadius: const BorderRadius.vertical(top: Radius.circular(5.0)),
        ),
        child: DefaultTextStyle(
          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
          child: Padding(
              padding: const EdgeInsets.fromLTRB(18.0, 12.0, 12.0, 22.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        duration,
                        style: TextStyle(fontSize: 18),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          departureTime,
                          style: TextStyle(fontSize: 16, color: globalElevations.sndTextColor.backgroundColor),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 6.0, left: 6.0),
                          child: Icon(
                            Icons.arrow_forward,
                            size: 15,
                            color: globalElevations.sndTextColor.backgroundColor,
                          ),
                        ),
                        Text(
                          destinationTime,
                          style: TextStyle(
                            fontSize: 16,
                            color: globalElevations.sndTextColor.backgroundColor,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}

class RouteDetailsBottomSheetBody extends TraceableStatelessWidget {
  final Itinerary itinerary;

  RouteDetailsBottomSheetBody({Key key, @required this.itinerary}) : super(key: key, name: MatomoBloc.screen_route_details);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: itinerary.legs.map<Widget>((leg) {
        final int index = itinerary.legs.indexOf(leg);
        bool firstLeg = index == 0;
        bool lastLeg = index == itinerary.legs.length - 1;

        if (leg.mode.isTransitMode()) {
          return Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: _LegDetails.transit(
              leg: leg,
              isFirstLeg: firstLeg,
              isLastLeg: lastLeg,
            ),
          );
        } else if (leg.mode == LegMode.WALK)
          return Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: _LegDetails.walk(
              leg: leg,
              isFirstLeg: firstLeg,
              isLastLeg: lastLeg,
            ),
          );
        else
          return EmptyCell();
        //throw Exception('not implemented exception');
      }).toList(),
    );
  }
}

class _LegDetails extends StatelessWidget {
  final Leg leg;
  final bool isFirstLeg;
  final bool isLastLeg;
  final List<Widget> children;

  const _LegDetails._({Key key, @required this.leg, @required this.isFirstLeg, @required this.isLastLeg, @required this.children}) : super(key: key);

  const _LegDetails.walk({Key key, @required this.leg, @required this.isFirstLeg, @required this.isLastLeg})
      : children = const <Widget>[_WalkLegDetailsRow.body()],
        super(key: key);

  const _LegDetails.transit({Key key, @required this.leg, @required this.isFirstLeg, @required this.isLastLeg})
      : children = const <Widget>[
          _TransitLegDetailsRow.header(),
          _TransitLegDetailsRow.departure(),
          _TransitLegDetailsRow.stops(),
          _TransitLegDetailsRow.destination()
        ],
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyInheritedLeg(
      leg: leg,
      isFirstLeg: isFirstLeg,
      isLastLeg: isLastLeg,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (isFirstLeg)
            _LegDetailsFirstAndLastRow(
              firstRow: true,
            ),
          ...children,
          if (isLastLeg)
            _LegDetailsFirstAndLastRow(
              firstRow: false,
            ),
        ],
      ),
    );
  }
}

class _LegDetailsRow extends StatelessWidget {
  final Widget leftSide;
  final Widget route;
  final Widget rightSide;

  _LegDetailsRow({@required this.leftSide, @required this.route, @required this.rightSide});

  @override
  Widget build(BuildContext context) {
    assert(leftSide != null);
    assert(route != null);
    assert(rightSide != null);

    return IntrinsicHeight(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0),
            child: SizedBox(
              width: 50,
              child: Center(child: leftSide),
            ),
          ),
          SizedBox(
            width: 34,
            child: route,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              child: rightSide,
            ),
          )
        ],
      ),
    );
  }
}

class _LegDetailsFirstAndLastRow extends StatelessWidget {
  final bool firstRow;

  _LegDetailsFirstAndLastRow({Key key, @required this.firstRow}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyInheritedLegState inheritedLeg = MyInheritedLeg.of(context);
    assert(inheritedLeg != null);
    String time = firstRow ? inheritedLeg.startTime : inheritedLeg.endTime;
    String name = firstRow ? 'Départ' : inheritedLeg.destinationName;
    IconData icon = firstRow ? Icons.my_location : Icons.flag;

    return _LegDetailsRow(
      leftSide: Text(
        time,
        style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
      ),
      route: Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: Icon(
          icon,
          size: 20,
        ),
      ),
      rightSide: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: Text(
              name,
              style: TextStyle(fontSize: 16),
            ),
          )),
    );
  }
}

class _TransitLegDetailsRow extends StatefulWidget {
  final _TransitRowType type;

  const _TransitLegDetailsRow.header({Key key})
      : this.type = _TransitRowType.header,
        super(key: key);

  const _TransitLegDetailsRow.departure({Key key})
      : this.type = _TransitRowType.departure,
        super(key: key);

  const _TransitLegDetailsRow.stops({Key key})
      : this.type = _TransitRowType.stops,
        super(key: key);

  const _TransitLegDetailsRow.destination({Key key})
      : this.type = _TransitRowType.destination,
        super(key: key);

  @override
  _TransitLegDetailsRowState createState() => _TransitLegDetailsRowState();
}

class _TransitLegDetailsRowState extends State<_TransitLegDetailsRow> with TickerProviderStateMixin {
  GlobalKey _columnKey = GlobalKey();
  GlobalKey _headerKey = GlobalKey();
  int stopsNb;
  double headerHeight = 0;
  double columnHeight = 0;

  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    if (widget.type == _TransitRowType.stops) WidgetsBinding.instance.addPostFrameCallback(_afterLayout);

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MyInheritedLegState inheritedLeg = MyInheritedLeg.of(context);
    assert(inheritedLeg != null);
    assert(debugCheckHasMaterial(context));

    stopsNb = inheritedLeg.intermediateStopsNb;

    Widget leftSide, route, rightSide;
    AnimationController c = _controller;
    switch (widget.type) {
      case _TransitRowType.header:
        leftSide = EmptyCell();
        route = _RouteCell.line();
        rightSide = Column(
          children: [
            inheritedLeg.leg?.line?.type == "MCO"
                ? ExpandablePanel(
                    elevation: globalElevations.e02dp,
                    showArrow: false,
                    showShadow: false,
                    showBorder: false,
                    body: Row(
                      children: <Widget>[
                        Container(
                          height: 26,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: SvgPicture.asset(
                              'assets/icons/m_covoit_logo.svg',
                              // height:16,
                              semanticsLabel: 'logo',
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8.0,
                        ),
                        Text(
                          "M'Covoit - " + inheritedLeg.leg?.line?.shortName,
                          style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
                        ),
                        const SizedBox(
                          width: 26.0,
                        ),
                        Container(
                          height: 26,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: SvgPicture.asset(
                              'assets/icons/ic_help_outline.svg',
                              height: 16,
                              color: Theme.of(context).indicatorColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    detail: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            child: Text(
                                "Covoiturez avec M'Covoit - Lignes+ pour vos trajets quotidiens sur le Voironnais, le Grésivaudan et la métropole Grenobloise."),
                          ),
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Row(
                          children: [
                            RaisedButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(24), side: BorderSide(color: Theme.of(context).colorScheme.primary)),
                              color: globalElevations.inverseColor.backgroundColor,
                              onPressed: () {
                                Utils.launchUrl('https://go.lignesplus-m.fr/g5gVkPVwK8');
                              },
                              child: Text(
                                "PLUS D'INFO",
                                style: TextStyle(color: Theme.of(context).colorScheme.primary),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                : inheritedLeg.leg?.line?.type == "FLEXO"
                    ? Column(
                        children: [
                          Row(
                            children: <Widget>[
                              Container(
                                height: 26,
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: LineDisplay(
                                    line: inheritedLeg.leg.line,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Transport",
                                    style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontStyle: FontStyle.italic),
                                  ),
                                  Text(
                                    "à la demande",
                                    style: TextStyle(color: globalElevations.fstTextColor.backgroundColor, fontStyle: FontStyle.italic),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(height:10.0),
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: RaisedButton(
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(16),
                                side: BorderSide(color: Theme.of(context).colorScheme.primary),
                              ),
                              color: Colors.transparent,
                              onPressed: () {
                                _launchCaller();
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.phone),
                                    SizedBox(width: 16.0,),
                                    Text("Réserver ce passage"),
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    : Row(
                        children: <Widget>[
                          inheritedLeg.leg.mode == LegMode.FUNICULAR
                              ? Container()
                              : Container(
                                  height: 30,
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: inheritedLeg.leg?.line?.type == "MCO"
                                        ? Icon(
                                            Icons.pin_drop,
                                            color: Colors.green,
                                            size: 16,
                                          )
                                        : LineDisplay(
                                            line: inheritedLeg.leg.line,
                                            isRouteDetails: true,
                                          ),
                                  ),
                                ),
                          const SizedBox(
                            width: 8.0,
                          ),
                          Text(
                            inheritedLeg.leg.mode == LegMode.FUNICULAR ? "Funiculaire" : inheritedLeg.leg.line.type == "MCO" ? "M'Covoit - M'V" : inheritedLeg.leg.mode.value,
                            style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
                          ),
                        ],
                      ),
          ],
        );
        break;

      case _TransitRowType.departure:
        leftSide = inheritedLeg.isFirstLeg
            ? EmptyCell()
            : Text(
                inheritedLeg.startTime,
                style: TextStyle(color: globalElevations.fstTextColor.backgroundColor),
              );
        route = _RouteCell.stop();
        rightSide = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              inheritedLeg.departureName,
              style: TextStyle(fontSize: 16, color: globalElevations.fstTextColor.backgroundColor),
            ),
            const SizedBox(
              height: 2.0,
            ),
            RichText(
              text: TextSpan(
                text: '${translations.text('route.direction')} ',
                style: TextStyle(fontSize: 14, color: globalElevations.sndTextColor.backgroundColor),
                children: <TextSpan>[
                  TextSpan(text: inheritedLeg.directionName, style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            )
          ],
        );
        break;

      case _TransitRowType.stops:
        leftSide = EmptyCell();
        route = inheritedLeg.intermediateStopsNb == 0
            ? _RouteCell.line()
            : _RouteCell.stops(
                controller: _controller,
                headerHeight: headerHeight,
                columnHeight: columnHeight,
              );
        rightSide = inheritedLeg.intermediateStopsNb == 0
            ? const SizedBox(height: 4)
            : _StopList(
                controller: _controller,
                columnKey: _columnKey,
                headerKey: _headerKey,
                headerHeight: headerHeight,
                columnHeight: columnHeight,
              );
        break;

      case _TransitRowType.destination:
        leftSide = Text(inheritedLeg.endTime, style: TextStyle(color: globalElevations.fstTextColor.backgroundColor));
        route = _RouteCell.stop();
        rightSide = Align(alignment: Alignment.centerLeft, child: Text(inheritedLeg.destinationName));
        break;
    }

    return _LegDetailsRow(
        leftSide: leftSide,
        route: route,
        rightSide: Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: rightSide,
        ));
  }

  _launchCaller() async {
    const url = "tel:0438703870";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _afterLayout(_) {
    if (_columnKey.currentContext != null) {
      double tempHeaderHeight = _headerKey.currentContext.size.height;
      double tempColumnHeight = _columnKey.currentContext.size.height;

      if (headerHeight != tempHeaderHeight) {
        setState(() {
          headerHeight = tempHeaderHeight;
          columnHeight = tempColumnHeight;
        });
      }
    }
  }
}

class _WalkLegDetailsRow extends StatelessWidget {
  final _WalkRowType type;

  const _WalkLegDetailsRow.body({Key key})
      : this.type = _WalkRowType.body,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    MyInheritedLegState inheritedLeg = MyInheritedLeg.of(context);
    assert(inheritedLeg != null);

    if (inheritedLeg.leg.distance > 180) {
      switch (type) {
        case _WalkRowType.body:
          return _LegDetailsRow(
            leftSide: inheritedLeg.isFirstLeg
                ? EmptyCell()
                : Text(inheritedLeg.startTime, style: TextStyle(color: globalElevations.fstTextColor.backgroundColor)),
            route: _RouteCell.line(),
            rightSide: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[Icon(Icons.directions_walk), Text(inheritedLeg.walkDistanceAndDuration)],
            ),
          );
      }
    } else {
      return EmptyCell();
    }
    throw Exception();
  }
}

class _RouteCell extends StatelessWidget {
  final _RouteCellType type;
  final AnimationController controller;
  final double headerHeight;
  final double columnHeight;

  const _RouteCell._simple({Key key, this.type})
      : this.controller = null,
        this.headerHeight = null,
        this.columnHeight = null,
        super(key: key);

  const _RouteCell.line({Key key}) : this._simple(key: key, type: _RouteCellType.line);

  const _RouteCell.stop({Key key}) : this._simple(key: key, type: _RouteCellType.stop);

  const _RouteCell.stops({Key key, @required this.controller, this.headerHeight, this.columnHeight})
      : this.type = _RouteCellType.stops,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    MyInheritedLegState inheritedLeg = MyInheritedLeg.of(context);
    assert(inheritedLeg != null);

    Widget line, stop;
    List<Widget> intermediateStops;

    if (type == _RouteCellType.line || type == _RouteCellType.stop || type == _RouteCellType.stops)
      line = Align(
        alignment: Alignment.center,
        child: Container(
          height: double.infinity,
          width: 5,
          color: inheritedLeg.leg.color,
        ),
      );

    if (type == _RouteCellType.stop)
      stop = Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Align(
          alignment: Alignment.center,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(13.0), border: Border.all(width: 3.0, color: inheritedLeg.leg.color)),
            height: 20,
            width: 20,
          ),
        ),
      );

    if (type == _RouteCellType.stops) {
      intermediateStops = List();
      if (headerHeight == 0) {
        for (var i = 0; i < inheritedLeg.intermediateStopsNb; i++) {
          intermediateStops.add(
            Positioned(
              top: topOffset * i,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(stopSize / 2),
                    color: globalElevations.fstTextColor.backgroundColor,
                    border: Border.all(width: 2.0, color: inheritedLeg.leg.color)),
                height: stopSize,
                width: stopSize,
              ),
            ),
          );
        }
      } else {
        double stopTextHeight = (columnHeight - headerHeight) / inheritedLeg.intermediateStopsNb;
        for (var i = 0; i < inheritedLeg.intermediateStopsNb; i++) {
          intermediateStops.add(
            Positioned(
              top: topOffset * i,
              child: SlideTransition(
                position: Tween<Offset>(begin: Offset.zero, end: Offset(0.0, getStopAnimationOffset(i, stopTextHeight))).animate(controller),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(stopSize / 2),
                      color: Colors.white,
                      border: Border.all(width: 3.0, color: inheritedLeg.leg.color)),
                  height: stopSize,
                  width: stopSize,
                ),
              ),
            ),
          );
        }
      }
    }

    if (line != null)
      return Stack(
        children: <Widget>[
          line,
          if (stop != null) stop else EmptyCell(),
          if (intermediateStops != null)
            Center(
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                alignment: inheritedLeg.isExpanded ? Alignment.topCenter : Alignment.center,
                height: inheritedLeg.isExpanded ? columnHeight : topOffset * inheritedLeg.intermediateStopsNb + stopSize - topOffset,
                width: stopSize,
                child: Stack(
                  children: [...intermediateStops],
                ),
              ),
            ),
        ],
      );
    else
      return EmptyCell();
  }

  double getStopAnimationOffset(int i, double stopTextHeight) =>
      (i * (stopTextHeight - topOffset) + (stopTextHeight - stopSize) / 2 + headerHeight) / stopSize;
}

class _StopList extends StatefulWidget {
  final AnimationController controller;
  final GlobalKey headerKey;
  final GlobalKey columnKey;
  final double headerHeight;
  final double columnHeight;

  _StopList(
      {Key key,
      @required this.controller,
      @required this.headerKey,
      @required this.columnKey,
      @required this.headerHeight,
      @required this.columnHeight})
      : super(key: key);

  @override
  __StopListState createState() => __StopListState();
}

class __StopListState extends State<_StopList> {
  @override
  Widget build(BuildContext context) {
    MyInheritedLegState inheritedLeg = MyInheritedLeg.of(context);
    assert(inheritedLeg != null);

    return Center(
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        height: inheritedLeg.isExpanded ? widget.columnHeight : widget.headerHeight,
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Material(
            type: MaterialType.transparency,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                key: widget.columnKey,
                children: <Widget>[
                  InkWell(
                    radius: 25,
                    onTap: () {
                      if (inheritedLeg.isExpanded) {
                        inheritedLeg.isExpanded = !inheritedLeg.isExpanded;
                        widget.controller.reverse();
                      } else {
                        inheritedLeg.isExpanded = !inheritedLeg.isExpanded;
                        widget.controller.forward();
                      }
                    },
                    child: Padding(
                      key: widget.headerKey,
                      padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, right: 8.0),
                      child: Row(
                        children: <Widget>[
                          Text(intermediateStopNumber(inheritedLeg.intermediateStopsNb),
                              style: TextStyle(fontSize: 12, color: globalElevations.sndTextColor.backgroundColor)),
                          RotationTransition(
                            turns: widget.controller.drive(_halfTween.chain(CurveTween(curve: Curves.easeIn))),
                            child: Icon(Icons.expand_more, color: globalElevations.sndTextColor.backgroundColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ...inheritedLeg.leg.intermediateStops
                      .map((s) => Padding(
                            padding: const EdgeInsets.all(4),
                            child: Align(alignment: Alignment.centerLeft, child: Text(s.name)),
                          ))
                      .toList(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _InheritedLeg extends InheritedWidget {
  _InheritedLeg({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);

  final MyInheritedLegState data;

  @override
  bool updateShouldNotify(_InheritedLeg oldWidget) {
    return true;
  }
}

class MyInheritedLeg extends StatefulWidget {
  final Leg leg;
  final bool isFirstLeg;
  final bool isLastLeg;

  MyInheritedLeg({
    Key key,
    @required this.leg,
    @required this.isFirstLeg,
    @required this.isLastLeg,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  MyInheritedLegState createState() => new MyInheritedLegState(leg: leg, isFirstLeg: isFirstLeg, isLastLeg: isLastLeg);

  static MyInheritedLegState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_InheritedLeg) as _InheritedLeg).data;
  }
}

class MyInheritedLegState extends State<MyInheritedLeg> {
  final Leg leg;
  final bool isFirstLeg;
  final bool isLastLeg;
  bool _isExpanded = false;

  MyInheritedLegState({this.leg, this.isFirstLeg, this.isLastLeg});

  bool get isExpanded => _isExpanded;

  set isExpanded(bool value) {
    if (_isExpanded != value)
      setState(() {
        _isExpanded = value;
      });
  }

  String get startTime => leg?.startTimeText ?? '';

  String get endTime => leg?.endTimeText ?? '';

  String get departureName => leg?.fromName ?? '';

  String get walkDistanceAndDuration => leg != null ? '${leg.distance} m, ${formatDuration(leg.duration)}' : 'error';

  String get destinationName => leg?.toName ?? '';

  String get directionName => leg?.lineDestination ?? '';

  int get intermediateStopsNb => leg.intermediateStopsNumber - 1 ?? 0;

  @override
  Widget build(BuildContext context) {
    return _InheritedLeg(
      data: this,
      child: widget.child,
    );
  }
}

class EmptyCell extends StatelessWidget {
  const EmptyCell();

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 0);
  }
}

const double stopSize = 12.0;
const double topOffset = 6.0;

final Animatable<double> _halfTween = Tween<double>(begin: 0.0, end: 0.5);

enum _WalkRowType { body }

enum _TransitRowType { header, departure, stops, destination }

enum _RouteCellType { line, stop, stops }
