import 'package:flutter/material.dart';
import 'package:flutter_matomo/flutter_matomo.dart';
import 'package:metro/favorites/widgets/favorite_button.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/blocs/matomo.dart';
import 'package:metro/global/widgets/animated_loading_logo.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:metro/global/widgets/rounded_button.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/route/widgets/route_result_item.dart';

class RouteResultsBottomSheetHeader extends StatefulWidget {
  final String nameDeparture;
  final String nameDestination;

  const RouteResultsBottomSheetHeader._({Key key, @required this.nameDeparture, @required this.nameDestination}) : super(key: key);

  factory RouteResultsBottomSheetHeader({Key key}) {
    String nameDeparture = BlocProvider.master<RoutesBloc>()?.departure?.properties?.name ?? 'error';
    String nameDestination = BlocProvider.master<RoutesBloc>()?.destination?.properties?.name ?? 'error';
    return RouteResultsBottomSheetHeader._(key: key, nameDeparture: nameDeparture, nameDestination: nameDestination);
  }

  @override
  State<StatefulWidget> createState() => _RouteResultsBottomSheetHeaderState(this.nameDeparture, this.nameDestination);
}

class _RouteResultsBottomSheetHeaderState extends State<RouteResultsBottomSheetHeader> {
  final String nameDeparture;
  final String nameDestination;

  _RouteResultsBottomSheetHeaderState(this.nameDeparture, this.nameDestination);

  List<BoxShadow> shadows = [];
  bool isScrolling = false;

  shadowListener() {
    ScrollController controller = BlocProvider.of<RoutesBloc>(context).routeScrollController;
    if (controller.offset > 0 && Theme.of(context).brightness==Brightness.light) {
      setState(() => isScrolling = true);
    } else if (controller.offset == 0.0) {
      setState(() => isScrolling = false);
    }
  }

  @override
  void initState() {
    super.initState();
    ScrollController controller = BlocProvider.of<RoutesBloc>(context).routeScrollController;
    controller.addListener(shadowListener);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          child: Container(
            color: Theme.of(context).bottomSheetTheme.backgroundColor,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                margin: const EdgeInsets.only(bottom: 6.0), //Same as `blurRadius` i guess
                decoration: (isScrolling ? getDecoration() : null),
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.my_location,
                                size: 25,
                              ),
                              const SizedBox(width: 4.0),
                              Container(
                                width: 170,
                                child: Text(nameDeparture, overflow: TextOverflow.ellipsis, softWrap: true, style: TextStyle(fontSize: 14.0)),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.flag,
                                size: 25,
                              ),
                              const SizedBox(width: 4.0),
                              Container(
                                width: 170,
                                child: Text(nameDestination, overflow: TextOverflow.ellipsis, softWrap: true, style: TextStyle(fontSize: 14.0)),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 0,
                      bottom: 0,
                      right: 8.0,
                      child: Row(
                        children: <Widget>[
                          RoundedButton(
                            title: translations.text('route.sort'),
                            iconData: Icons.filter_list,
                            height: 42,
                            callback: showSortBottomSheet,
                          ),
                          const SizedBox(width: 8.0),
                          FavoriteButton(
                            data: BlocProvider.of<RoutesBloc>(context).favoriteItinerary,
                            border: true,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Show a bottom sheet modal to sort the routes results.
  /// Needs the [context] to be build.
  void showSortBottomSheet(BuildContext context) {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);

    Widget getTile(OrderType orderType) {
      String title;
      Icon icon;
      switch (orderType) {
        case OrderType.byDuration:
          title = translations.text('route.sorts.by_duration');
          icon = const Icon(Icons.timer);
          break;
        case OrderType.byTime:
          if (routesBloc.arriveBy)
            title = translations.text('route.sorts.by_arrival_time');
          else
            title = translations.text('route.sorts.by_departure_time');

          icon = const Icon(Icons.access_time);
          break;
        case OrderType.byFare:
          title = translations.text('route.sorts.by_fare');
          icon = const Icon(Icons.euro_symbol);
          break;
      }

      return Container(
        color: routesBloc.sort == orderType ? Colors.white.withOpacity(0.1) : Colors.transparent,
        child: ListTile(
          leading: icon,
          title: Text(title),
          onTap: () {
            routesBloc.changeOrder(orderType);
            Navigator.pop(context);
          },
        ),
      );
    }

    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          List<Widget> tiles = List<Widget>();
          // Header
          tiles.add(Container(
            padding: EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                translations.text('route.sort'),
                style: TextStyle(fontSize: 20),
              ),
            ),
          ));

          tiles.add(getTile(OrderType.byDuration));
          tiles.add(getTile(OrderType.byTime));
          tiles.add(getTile(OrderType.byFare));

          // This empty GestureDetector is needed to avoid modalBottomSheet
          // from closing when tapping on the header
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {},
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: tiles,
            ),
          );
        });
  }

  BoxDecoration getDecoration(){
    return BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          offset: Offset(0.0, 1.0), //(x,y)
          blurRadius: 4.0,
        ),
      ],
    );
  }
}

class RouteResultsBottomSheetBody extends TraceableStatefulWidget {
  RouteResultsBottomSheetBody({Key key}) : super(key: key, name: MatomoBloc.screen_route_results);

  @override
  RouteResultsBottomSheetBodyState createState() => RouteResultsBottomSheetBodyState();
}

class RouteResultsBottomSheetBodyState extends State<RouteResultsBottomSheetBody> {
  Map<ItineraryRow, ExpandablePanelController> _itineraryControllerLink = Map();

  @override
  void initState() {
    BlocProvider.of<RoutesBloc>(context).onSelectedRouteChanged.listen((ItineraryRow itineraryRow) {
      if (itineraryRow != null) {
        for (ItineraryRow itinerary in _itineraryControllerLink.keys) {
          if (itineraryRow.itinerary != null && itineraryRow == itinerary)
            _itineraryControllerLink[itinerary].expand();
          else
            _itineraryControllerLink[itinerary].collapse();
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);
    return StreamBuilder<List<ItineraryRow>>(
      stream: routesBloc.onRoutesDownloaded,
      initialData: routesBloc.routes,
      builder: (context, snapshot) {
        // We don't already have the results for routes, show loading
        if (snapshot.data == null)
          return const AnimatedLoadingLogo(
            height: 200,
          );
        // There is no results with this parameters
        else if (snapshot.data.isEmpty) return Center(child: Text(translations.text('route.no_results')));

        return Column(
          children: <Widget>[
            _buildNextPrevBar(title: translations.text('route.results')),
            snapshot.data.length == 1 &&
                    snapshot.data.first.itinerary.requestedMode == LegMode.WALK &&
                    !BlocProvider.of<RoutesBloc>(context).walkingSelected
                ? Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(16),
                    child: Text(
                      translations.text('route.no_routes'),
                      textAlign: TextAlign.center,
                    ),
                  )
                : ListView.custom(
                    padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    childrenDelegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        // if data is null or is walking for more than 45 min, we do not display it
                        if (snapshot.data[index] == null ||
                            (snapshot.data[index].itinerary.requestedMode == LegMode.WALK && !BlocProvider.of<RoutesBloc>(context).walkingSelected))
                          return Container();

                        if (_itineraryControllerLink[snapshot.data[index]] == null)
                          _itineraryControllerLink[snapshot.data[index]] = ExpandablePanelController();

                        return RouteResultItem(
                          index: index,
                          itineraryRow: snapshot.data[index],
                          controller: _itineraryControllerLink[snapshot.data[index]],
                          initiallyOpen: index == routesBloc.selectedRouteIndex,
                        );
                      },
                      childCount: snapshot.data.length,
                      findChildIndexCallback: (Key key) {
                        final ValueKey valueKey = key;
                        final ItineraryRow data = valueKey.value;
                        return snapshot.data.indexOf(data);
                      },
                    ),
                  ),
            _buildNextPrevBar(),
          ],
        );
      },
    );
  }

  Widget _buildNextPrevBar({String title = ""}) {
    return Row(
      children: <Widget>[
        FlatButton.icon(
          icon: Icon(Icons.arrow_back_ios, color: Theme.of(context).indicatorColor),
          label: Text(
            translations.text('route.prev_15'),
            style: TextStyle(color: Theme.of(context).indicatorColor),
          ),
          onPressed: _previous15Minutes,
        ),
        Expanded(
            child: Container(
          child: Text(title),
          alignment: Alignment.center,
        )),
        FlatButton(
          child: Row(
            children: <Widget>[
              Text(
                translations.text("route.next_15"),
                textAlign: TextAlign.center,
                style: TextStyle(color: Theme.of(context).indicatorColor),
              ),
              SizedBox(width: 8),
              Icon(Icons.arrow_forward_ios, color: Theme.of(context).indicatorColor),
            ],
          ),
          onPressed: _next15Minutes,
        )
      ],
    );
  }

  void _next15Minutes() {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);
    TimeOfDay newTime = TimeOfDay.fromDateTime(DateTime(routesBloc.savedSelectedDate.year, routesBloc.savedSelectedDate.month,
            routesBloc.savedSelectedDate.day, routesBloc.savedSelectedTime.hour, routesBloc.savedSelectedTime.minute)
        .add(Duration(minutes: 15)));
    routesBloc.requestRoutes(BlocProvider.of<LinesBloc>(context).lines, routesBloc.getSelectedTransports(), routesBloc.savedSelectedTimePreference,
        routesBloc.savedSelectedDate, newTime, routesBloc.savedSelectedSort);
    BlocProvider.of<MapBloc>(context).setBottomSheetContent(BottomSheetCategory.routes, arguments: routesBloc.savedSelectedTimePreference);
  }

  void _previous15Minutes() {
    RoutesBloc routesBloc = BlocProvider.of<RoutesBloc>(context);
    TimeOfDay newTime = TimeOfDay.fromDateTime(DateTime(routesBloc.savedSelectedDate.year, routesBloc.savedSelectedDate.month,
            routesBloc.savedSelectedDate.day, routesBloc.savedSelectedTime.hour, routesBloc.savedSelectedTime.minute)
        .subtract(Duration(minutes: 15)));
    routesBloc.requestRoutes(BlocProvider.of<LinesBloc>(context).lines, routesBloc.getSelectedTransports(), routesBloc.savedSelectedTimePreference,
        routesBloc.savedSelectedDate, newTime, routesBloc.savedSelectedSort);
    BlocProvider.of<MapBloc>(context).setBottomSheetContent(BottomSheetCategory.routes, arguments: routesBloc.savedSelectedTimePreference);
  }
}
