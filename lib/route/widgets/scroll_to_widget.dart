import 'package:flutter/material.dart';

class ScrollToWidget extends StatefulWidget {
  final Widget child;

  ScrollToWidget({@required this.child});

  @override
  State<StatefulWidget> createState() => _ScrollToWidgetState();
}

class _ScrollToWidgetState extends State<ScrollToWidget> {
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
