import 'package:flutter/material.dart';
import 'package:metro/localization/translations.dart';

class SearchButton extends StatelessWidget {
  final VoidCallback onTap;

  const SearchButton({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
            color: Color(0xFF403D44),
            borderRadius: BorderRadius.circular(18.0),
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RotatedBox(
                  quarterTurns: 4,
                  child: Icon(
                    Icons.near_me,
                    size: 20,
                    color: Color(0xFF7a787d),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(2),
                ),
                Text(
                  translations.text('search_button.SEARCH'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF7a787d),
                  ),
                ),
              ],
            ),
          ),
        ),
        Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(20),
            onTap: onTap,
            child: Container(
              height: 40,
              width: 150,
            ),
          ),
        )
      ],
    );
  }
}
