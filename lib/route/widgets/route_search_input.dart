import 'package:flutter/material.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/always_disabled_focus.dart';
import 'package:metro/search/widgets/search_bar.dart';

class RouteSearchInput extends StatefulWidget {
  final String label;
  final Function(Point) onSearchComplete;
  final bool isSchedulePage;
  final Color color;
  final SearchBarController controller;

  RouteSearchInput({
    @required this.label,
    @required this.onSearchComplete,
    this.controller,
    this.isSchedulePage,
    this.color,
  });

  @override
  State<StatefulWidget> createState() => _RouteSearchInputState();
}

class _RouteSearchInputState extends State<RouteSearchInput> {
  Color _overrideBorderColor;
  TextEditingController _textController;

  void _changeBorderColor(Color color) {
    if (mounted) setState(() => _overrideBorderColor = color);
  }

  void _changeText(String text) {
    if (mounted) setState(() => _textController.text = text);
  }

  @override
  void initState() {
    super.initState();

    _textController = TextEditingController();

    widget.controller?.onBorderColorChanged =
        (color) => _changeBorderColor(color);

    widget.controller?.onTextChanged = (text) => _changeText(text);
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      child: TextField(
        focusNode: AlwaysDisabledFocusNode(),
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w300,
          color: _textController != null
              ? globalElevations.fstTextColor.backgroundColor
              : globalElevations.sndTextColor.backgroundColor,
        ),
        controller: _textController,
        onTap: () async {
          widget.onSearchComplete(
              await Utils.openSearch(context, widget.isSchedulePage));
        },
        decoration: InputDecoration(
          labelText: widget.label,
          hintText: widget.label,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: BorderSide(
              color: _overrideBorderColor != null
                  ? _overrideBorderColor
                  : Color(0xFF646267),
            ),
          ),
        ),
      ),
    );
  }
}
