import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/navigation_bloc.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/elevation.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/metro_icons.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/widgets/expandable_panel.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/map/blocs/map_bloc.dart';
import 'package:metro/route/blocs/routes.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/route/models/transport_types.dart';
import 'package:metro/route/widgets/route_details.dart';

import '../route_utils.dart';

/// Widget diplaying one route result.
class RouteResultItem extends StatefulWidget {
  final ItineraryRow itineraryRow;
  final ExpandablePanelController controller;
  final bool initiallyOpen;
  final int index;

  RouteResultItem({@required this.index, @required this.itineraryRow, this.controller, this.initiallyOpen = false})
      : super(key: ValueKey(itineraryRow));

  @override
  RouteResultItemState createState() => RouteResultItemState(itineraryRow);
}

class RouteResultItemState extends State<RouteResultItem> {
  final ItineraryRow itineraryRow;

  RouteResultItemState(this.itineraryRow);

  GlobalKey _stickyKey = GlobalKey();

  bool get showDetailsButton => itineraryRow.itinerary.showDetailsButton;

  String get durationText => itineraryRow.itinerary.durationText;

  bool get hasFare => (itineraryRow.itinerary.fare != null);

  String get fare => hasFare ? NumberFormat('##.00').format(itineraryRow.itinerary.fare.cents / 100) : translations.text('route.fare.free');

  String get timeNextTrip => itineraryRow.itinerary?.nextTrip != null ? timeFormat.format(itineraryRow.itinerary.nextTrip) : null;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<RoutesBloc>(context).addRouteKey(widget.index, _stickyKey);
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedItineraryRow(
      key: _stickyKey,
      data: this,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
        child: ExpandablePanel(
          elevation: globalElevations.e01dp,
          controller: widget.controller,
          showArrow: false,
          isExpanded: widget.initiallyOpen,
          onOpenOrClose: (isOpen) {
            if (isOpen) {
              BlocProvider.of<RoutesBloc>(context).changeSelected(itineraryRow);
              BlocProvider.of<RoutesBloc>(context).scrollToElement(widget.index);
            }
          },
          body: IntrinsicHeight(
            child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
              Expanded(
                child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[_TopPart(), itineraryRow.itinerary.showDetailsButton ? _BottomPart() : EmptyCell()],
                    )),
              )
            ]),
          ),
          detail: itineraryRow.itinerary.showDetailsButton ? _DetailPart() : EmptyCell(),
        ),
      ),
    );
  }
}

class _TopPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RouteResultItemState itemState = _InheritedItineraryRow.of(context)?.data;
    assert(itemState != null);
    IconData iconMode;
    String textMode;

    if (itemState.itineraryRow.itinerary.requestedMode == LegMode.CAR) {
      iconMode = MetroIcons.voiture;
      textMode = Car().name;
    } else if (itemState.itineraryRow.itinerary.requestedMode == LegMode.WALK) {
      iconMode = MetroIcons.pieton;
      textMode = Walk().name;
    } else if (itemState.itineraryRow.itinerary.requestedMode == LegMode.BICYCLE) {
      iconMode = MetroIcons.velo;
      textMode = Bike().name;
    } else if (itemState.itineraryRow.itinerary.requestedMode == LegMode.COV) {
      iconMode = MetroIcons.covoiturage;
      textMode = Carpooling().name;
    } else {
      textMode = '';
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                  Text(itemState.durationText, style: const TextStyle(fontSize: 16)),
                ]),
                Text(
                  textMode,
                  style: TextStyle(color: Colors.white70, fontSize: 12),
                )
              ],
            ),
          ),
          Center(
            child: (itemState.itineraryRow.itinerary.isCarPooling || !itemState.itineraryRow.itinerary.showDetailsButton)
                ? Icon(
                    iconMode,
                    color: Theme.of(context).indicatorColor,
                    size: 35,
                  )
                : EmptyCell(),
          ),
          // Fare
          Align(
            alignment: Alignment.centerRight,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    const SizedBox(width: 5),
                    Text(itemState.itineraryRow.itinerary.startTimeText),
                    Padding(
                      padding: const EdgeInsets.only(right: 6.0, left: 6.0),
                      child: Icon(Icons.arrow_forward, size: 15),
                    ),
                    Text(itemState.itineraryRow.itinerary.endTimeText),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 2.0),
                      child: Text(
                        itemState.itineraryRow.itinerary.isCarPooling
                            ? itemState.itineraryRow.itinerary.carpoolingData.price
                            : (itemState.itineraryRow.itinerary.requestedMode == LegMode.WALK ||
                                    itemState.itineraryRow.itinerary.requestedMode == LegMode.BICYCLE
                                ? ""
                                : (priceFare(itemState.fare) == translations.text('route.fare.free')
                                    ? translations.text('prices.not_specified')
                                    : priceFare(itemState.fare))),
                        style: TextStyle(color: globalElevations.sndTextColor.backgroundColor),
                      ),
                    ),
                    if (itemState.hasFare ||
                        (itemState.itineraryRow.itinerary.isCarPooling && itemState.itineraryRow.itinerary.carpoolingData.price != "Gratuit") &&
                            itemState.itineraryRow.itinerary.carpoolingData.price != "Non renseigné")
                      Icon(
                        Icons.euro_symbol,
                        size: 13,
                        color: globalElevations.sndTextColor.backgroundColor,
                      )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

String priceFare(String fare) {
  if (fare[0] == ',') fare = '0' + fare;
  return fare;
}

class _BottomPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RouteResultItemState itemState = _InheritedItineraryRow.of(context)?.data;
    assert(itemState != null);

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: itemState.itineraryRow.itinerary.isCarPooling ? _CovBottomPart() : _OthersBottomPart(),
    );
  }
}

class _CovBottomPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RouteResultItemState itemState = _InheritedItineraryRow.of(context)?.data;
    assert(itemState != null);
    String img = "";
    Color col;
    var datas = itemState.itineraryRow.itinerary.carpoolingData;

    if (datas.origin.contains('karos')) {
      img = 'assets/karos.svg';
      col = Color(0xffed2259);
    } else if (datas.origin.contains('movici')) {
      img = 'assets/movici.svg';
      col = Color(0xff5faae0);
    } else if (datas.origin.contains('klaxit')) {
      img = 'assets/klaxit.svg';
      col = Colors.orange;
    }

    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: new BorderRadius.circular(20.0),
                child: datas.driverPicture != null
                    ? FadeInImage.assetNetwork(
                        placeholder: 'assets/icons/face.png',
                        image: datas.driverPicture,
                        width: 40,
                        height: 40,
                      )
                    : Icon(
                        Icons.face,
                        color: Colors.white,
                        size: 40,
                      ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: datas.driverName != null ? Text(datas.driverName) : Text(""),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 40,
                padding: EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  color: globalElevations.e24dp.backgroundColor,
                  borderRadius: BorderRadius.circular(18.0),
                ),
                child: SvgPicture.asset(
                  img,
                  color: col,
                  height: 15,
                  width: 40,
                  semanticsLabel: 'covoit image',
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class _OthersBottomPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RouteResultItemState itemState = _InheritedItineraryRow.of(context)?.data;
    assert(itemState != null);

    return Row(
      children: <Widget>[
        Expanded(
            child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          // Legs from itinerary
          child: Row(
            children: itemState.itineraryRow.itinerary.legs
                .where((leg) =>
                    leg.mode != LegMode.WALK ||
                    !(leg.mode == LegMode.WALK && leg.duration.inMinutes < Consts.legWalkMinDurationInMinutesToShowInResults))
                .map((leg) {
              return itemState.itineraryRow.itinerary.legs.last != leg
                  ? Padding(padding: const EdgeInsets.only(right: 8), child: _RouteResultLegItem(leg))
                  : _RouteResultLegItem(leg);
            }).toList(),
          ),
        )),
      ],
    );
  }
}

class _DetailPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final RouteResultItemState itemState = _InheritedItineraryRow.of(context)?.data;
    assert(itemState != null);

    if (!itemState.itineraryRow.itinerary.isCarPooling) {
      return SizedBox(
        child: itemState.showDetailsButton
            ? Container(
                decoration: BoxDecoration(
                  color: globalElevations.expandPanelDetailsColor.backgroundColor,
                  border: Border(
                    left: BorderSide(color: Colors.black12),
                    bottom: BorderSide(color: Colors.black12),
                    right: BorderSide(color: Colors.black12),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          BlocProvider.of<RoutesBloc>(context).changeSelected(itemState.itineraryRow);
                          BlocProvider.of<NavigationBloc>(context).changeTab(NavigationCategory.routeResultDetails);
                          BlocProvider.of<MapBloc>(context)
                              .setBottomSheetContent(BottomSheetCategory.routeDetails, arguments: itemState.itineraryRow.itinerary);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            translations.text('route.show_details'),
                            textAlign: TextAlign.start,
                            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Theme.of(context).indicatorColor),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : const SizedBox(),
      );
    } else {
      return SizedBox(
          child: Container(
        decoration: BoxDecoration(
          color: globalElevations.e16dp.backgroundColor,
          border: Border(
            left: BorderSide(color: Colors.black12),
            bottom: BorderSide(color: Colors.black12),
            right: BorderSide(color: Colors.black12),
          ),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  String url = 'https://' + itemState.itineraryRow.itinerary.carpoolingData.url;
                  // Escaping the special characters for the URL to work on all platforms
                  if (url != null) {
                    List<String> splits = url.split('/');
                    if (splits != null && splits.length >= 2) {
                      String dep = Uri.encodeQueryComponent(splits[splits.length - 2]);
                      String dest = Uri.encodeQueryComponent(splits[splits.length - 1]);

                      // Rebuilding URL
                      String temp = url.substring(0, url.lastIndexOf('/'));
                      String finalUrl = url.substring(0, temp.lastIndexOf('/'));
                      finalUrl = '$finalUrl/$dep/$dest';

                      Utils.launchUrl(finalUrl);
                    }
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'RESERVER',
                    textAlign: TextAlign.start,
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16, color: Theme.of(context).indicatorColor),
                  ),
                ),
              ),
            ),
          ],
        ),
      ));
    }
  }
}

/// Widget displaying one leg in the [RouteResultItem].
class _RouteResultLegItem extends StatelessWidget {
  final Leg leg;
  final String bottomText;
  final Widget icon;
  final Color textColor;

  _RouteResultLegItem._(this.leg, this.bottomText, this.icon, this.textColor);

  factory _RouteResultLegItem(Leg leg) {
    String bottomText;
    Widget icon;
    if (leg.mode == LegMode.TRAM || leg.mode == LegMode.BUS) {
      bottomText = leg?.line?.type == 'MCO' ? " ~ " + formatDuration(leg.duration) + " " : stopNumber(leg.intermediateStopsNumber);
      if (leg.mode == LegMode.BUS)
        icon = Icon(MetroIcons.bus);
      else
        icon = Icon(MetroIcons.tram);
    } else {
      bottomText = formatDuration(leg.duration);
      if (leg.mode == LegMode.WALK)
        icon = Icon(MetroIcons.pieton);
      else if (leg.mode == LegMode.BICYCLE)
        icon = Icon(MetroIcons.velo);
      else if (leg.mode == LegMode.RAIL || leg.mode == LegMode.CABLE_CAR)
        icon = Icon(MetroIcons.train);
      else if (leg.mode == LegMode.CAR)
        icon = Icon(MetroIcons.voiture);
      else if (leg.mode == LegMode.COV)
        icon = Icon(MetroIcons.covoiturage);
      else if (leg.mode == LegMode.FUNICULAR)
        icon = SvgPicture.asset(
          'assets/icons/funicular.svg',
          height: 14,
          color: globalElevations.fstTextColor.backgroundColor,
          semanticsLabel: 'icon',
        );
    }
    Color textColor = leg.line?.textColor != null && leg.line?.textColor != '' ? Utils.colorFromHex(leg.line.textColor) : Colors.black;
    return _RouteResultLegItem._(leg, bottomText, icon, textColor);
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: leg.mode.isTransitMode() ? _TransitLegContainer(leg: leg, icon: icon) : _OthersLegContainer(icon: icon),
      ),
      Container(
          decoration: BoxDecoration(color: leg.color, borderRadius: const BorderRadius.all(Radius.circular(5.0))),
          child: Padding(padding: const EdgeInsets.all(8.0), child: Text(bottomText, style: TextStyle(color: textColor))))
    ]);
  }
}

class _TransitLegContainer extends StatelessWidget {
  final Leg leg;
  final Widget icon;

  const _TransitLegContainer({Key key, @required this.leg, @required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = leg?.line?.type == 'MCO' ? "M'Covoit" : leg?.line?.shortName;
    Widget ic = icon;
    if (name == null) {
      leg.mode == LegMode.FUNICULAR ? name = "FUN" : name = "TEST";
    }
    if (leg?.line?.type == 'MCO') {
      ic = Icon(MetroIcons.covoiturage);
    }

    return Container(height: 25, child: FittedBox(fit: BoxFit.contain, child: Row(children: <Widget>[ic, Padding(
      padding: const EdgeInsets.only(left: 4.0),
      child: Text(name),
    ), const SizedBox(width: 4.0)])));
  }
}

class _OthersLegContainer extends StatelessWidget {
  final Icon icon;

  const _OthersLegContainer({Key key, @required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(height: 25, child: FittedBox(fit: BoxFit.contain, child: icon));
  }
}

class _InheritedItineraryRow extends InheritedWidget {
  const _InheritedItineraryRow({
    Key key,
    @required Widget child,
    @required this.data,
  }) : super(key: key, child: child);

  final RouteResultItemState data;

  @override
  bool updateShouldNotify(_InheritedItineraryRow oldWidget) {
    return true;
  }

  static _InheritedItineraryRow of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(_InheritedItineraryRow);
  }
}
