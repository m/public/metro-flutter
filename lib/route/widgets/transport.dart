import 'package:flutter/material.dart';
import 'package:metro/route/models/transport_types.dart';
import 'package:preferences/preferences.dart';

class TransportDisplay extends StatelessWidget {
  final TransportType transportType;

  const TransportDisplay({Key key, @required this.transportType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Row(
            children: <Widget>[
              Container(
                height: 30,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14),
                  color: Theme.of(context).hintColor,
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 40.0, right: 14),
                  child: Center(
                    child: Text(transportType.shortName),
                  ),
                ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            height: 32,
            width: 32,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Theme.of(context).accentColor,
            ),
            child: Icon(
              transportType.icon,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }
}

class TransportPreferenceLine extends StatelessWidget {
  final TransportType transportType;

  const TransportPreferenceLine({Key key, @required this.transportType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 12),
          height: 32,
          width: 32,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.black12,
          ),
          child: Center(child: Icon(transportType.icon)),
        ),
        Expanded(
          child: SwitchPreference(
            transportType.name,
            transportType.key,
            defaultVal: true,
          ),
        ),
      ],
    );
  }
}

class TransportListDisplay extends StatelessWidget {
  final List<TransportType> transportTypes;

  const TransportListDisplay({Key key, @required this.transportTypes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> display = List<Widget>();

    for (var i = 0; i < transportTypes.length; i++) {
      display.add(
        TransportDisplay(
          transportType: transportTypes[i],
        ),
      );

      if (i < transportTypes.length - 1)
        display.add(
          Padding(
            padding: EdgeInsets.all(4),
          ),
        );
    }

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: display,
      ),
    );
  }
}
