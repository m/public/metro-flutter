import 'dart:collection';
import 'dart:convert';

import 'package:flutter_map/plugin_api.dart' show Polyline;
import 'package:intl/intl.dart';
import 'package:flutter/material.dart' hide Step;
import 'package:latlong/latlong.dart';
import 'package:metro/favorites/models/favorites.dart';
import 'package:metro/global/blocs/bloc_provider.dart';
import 'package:metro/global/blocs/lines.dart';
import 'package:metro/global/consts.dart';
import 'package:metro/global/database/database.dart' show ModeOfTransport;
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/models/point.dart';
import 'package:metro/global/utils.dart';
import 'package:metro/global/blocs/ws.dart';
import 'package:metro/localization/translations.dart';
import 'package:metro/route/models/route.dart';
import 'package:metro/route/models/route_preferences.dart';
import 'package:metro/route/models/transport_types.dart';
import 'package:preferences/preference_service.dart';
import 'package:rxdart/rxdart.dart';

import '../route_utils.dart';
import 'package:metro/extensions/datetime_extensions.dart';

class RoutesBloc extends BlocBase {
  BehaviorSubject<List<ItineraryRow>> _downloadController;
  Stream<List<ItineraryRow>> get onRoutesDownloaded =>
      _downloadController.stream;
  List<ItineraryRow> get routes => _downloadController.value;

  BehaviorSubject<Point> _departureController;
  Stream<Point> get onDepartureChanged => _departureController.stream;
  Point get departure => _departureController.value;

  BehaviorSubject<Point> _destinationController;
  Stream<Point> get onDestinationChanged => _destinationController.stream;
  Point get destination => _destinationController.value;

  BehaviorSubject<ItineraryRow> _selectedRouteController;
  Stream<ItineraryRow> get onSelectedRouteChanged =>
      _selectedRouteController.stream;
  ItineraryRow get selectedRoute => _selectedRouteController.value;

  BehaviorSubject<bool> _arriveByController;
  Stream<bool> get onArriveByChanged => _arriveByController.stream;
  bool get arriveBy => _arriveByController.value;

  BehaviorSubject<Map<TransportType, bool>> _selectedTransportsController;
  Stream<Map<TransportType, bool>> get onSelectedTransportsChanged =>
      _selectedTransportsController.stream;
  Map<TransportType, bool> get selectedTransports =>
      _selectedTransportsController.value;

  BehaviorSubject<bool> _pmrController;
  Stream<bool> get onPmrValueChanged => _pmrController.stream;
  bool get pmrValue => _pmrController.value;

  BehaviorSubject<OrderType> _sortController;
  Stream<OrderType> get onSortChange => _sortController.stream;
  OrderType get sort => _sortController.value;

  ScrollController routeScrollController = ScrollController();
  void scrollToElement(int index) {
    double offsetToScroll = 0;
    for (int currentIndex = 0; currentIndex < index; currentIndex++) {
      final keyContext = routeKeys[currentIndex.toString()].currentContext;
      if (keyContext != null) {
        final box = keyContext.findRenderObject() as RenderBox;
        offsetToScroll += box.size.height;
      }
    }
    routeScrollController.animateTo(
      offsetToScroll,
      duration: Duration(seconds: 1),
      curve: Curves.ease,
    );
  }
  Map<String, GlobalKey> routeKeys = Map();
  void addRouteKey(int index, GlobalKey dataKey) =>
      routeKeys[index.toString()] = dataKey;

  int _backedSelectedRouteIndex;
  int get selectedRouteIndex => _backedSelectedRouteIndex;
  set selectedRouteIndex(int i) {
    //assert(i >= 0);
    ItineraryRow itineraryRow = routes?.elementAt(i);
    //assert(itineraryRow != null);
    _backedSelectedRouteIndex = i;
    _selectedRouteController.sink.add(itineraryRow);
  }

  Stream<bool> onShowFavoriteIconChanged;

  Function _sortMethod;

  Map<TransportType, bool> _selectedTransports;

  // Save requestRoutes parameters to recalculate routes with +/- 15 minutes
  TimePreference _savedSelectedTimePreference;
  TimePreference get savedSelectedTimePreference =>
      _savedSelectedTimePreference;

  DateTime _savedSelectedDate;
  DateTime get savedSelectedDate => _savedSelectedDate;

  TimeOfDay _savedSelectedTime;
  TimeOfDay get savedSelectedTime => _savedSelectedTime;

  SortPreference _savedSelectedSort;
  SortPreference get savedSelectedSort => _savedSelectedSort;

  List<TransportType> _savedSelectedTransportType;
  List<TransportType> get savedSelectedTransportType =>
      _savedSelectedTransportType;

  bool _walkingSelected = false;
  bool get walkingSelected => _walkingSelected;

  @override
  void initState() {
    _downloadController = BehaviorSubject<List<ItineraryRow>>.seeded(null);
    _departureController = BehaviorSubject<Point>.seeded(null);
    _destinationController = BehaviorSubject<Point>.seeded(null);
    _selectedRouteController = BehaviorSubject<ItineraryRow>.seeded(null);
    _arriveByController = BehaviorSubject<bool>.seeded(null);
    _pmrController = BehaviorSubject<bool>.seeded(null);
    _sortController = BehaviorSubject<OrderType>.seeded(OrderType.byDuration);
    selectedRouteIndex = -1;

    _selectedTransports = Map();
    for (TransportType tp in TransportTypes.availableTransports) {
      bool storedValue = PrefService.getBool(tp.key);
      if (storedValue == null) {
        // There was no value stored in the prefs, set the default value
        // Default value is true is transportType is of type "PublicTransport", false otherwise
        storedValue = (tp.key == TransportTypes.publicTransport.key);
      }
      _selectedTransports[tp] = storedValue;
    }
    _selectedTransportsController =
        BehaviorSubject<Map<TransportType, bool>>.seeded(_selectedTransports);

    bool storedPmr = PrefService.getBool(TransportTypes.disabled.key);
    if (storedPmr == null)
      setPmrValue(false);
    else
      _pmrController.sink.add(storedPmr);
  }

  @override
  void dispose() {
    _downloadController.close();
    _departureController.close();
    _destinationController.close();
    _selectedRouteController.close();
    _arriveByController.close();
    _selectedTransportsController.close();
    _pmrController.close();
    _sortController.close();
    routeScrollController.dispose();
  }

  void setDeparture(Point point) {
    _departureController.sink.add(point);
  }

  void setDestination(Point point) {
    _destinationController.sink.add(point);
  }

  Future<void> requestRoutes(
      List<ReadyLine> lines,
      List<TransportType> transportTypes,
      TimePreference timePreference,
      DateTime date,
      TimeOfDay time,
      SortPreference sortPreference) async {
    cleanPreviousRequest();

    _savedSelectedTransportType = transportTypes;
    _savedSelectedTimePreference = timePreference;
    _savedSelectedDate = date;
    _savedSelectedTime = time;
    _savedSelectedSort = sortPreference;

    LatLng from = Utils.coordinatesFromArray(departure.geometry.coordinates);
    LatLng to = Utils.coordinatesFromArray(destination.geometry.coordinates);

    String tempTimeMinutes = time.minute.toString();
    String tempTimeHour = time.hour.toString();
    time.minute < 10
        ? tempTimeMinutes = '0' + time.minute.toString()
        : time.minute.toString();
    time.hour < 10
        ? tempTimeHour = '0' + time.hour.toString()
        : time.hour.toString();

    // TimePreference
    _arriveByController.sink.add(timePreference is ArriveBefore);
    // Time
    String finalTime = '$tempTimeHour:$tempTimeMinutes';
    // Date
    String finalDate = dateFormatWithHyphens.format(date);
    // Sort
    String finalSort = 'QUICK';
    if (sortPreference is Fastest)
      finalSort = 'QUICK';
    else if (sortPreference is LessSteps)
      finalSort = 'QUICK';
    else if (sortPreference is MostSecured)
      finalSort = 'TRIANGLE';
    else if (sortPreference is LessPoluated) finalSort = 'QUICK';

    _sortMethod = arriveBy
        ? _sortItineraryByDurationForArriveBefore
        : _sortItineraryByDurationForGoAfter;

    // Modes
    bool publicTransportSelected = false;
    bool bikeSelected = false;
    bool carSelected = false;
    bool carpoolingSelected = false;
    bool carSharingSelected = false;
    bool pmrSelected = false;
    _walkingSelected = false;
    for (TransportType mode in transportTypes) {
      if (mode is PublicTransport)
        publicTransportSelected = true;
      else if (mode is Bike)
        bikeSelected = true;
      else if (mode is Car)
        carSelected = true;
      else if (mode is Carpooling)
        carpoolingSelected = true;
      else if (mode is Carsharing)
        carSharingSelected = true;
      else if (mode is DisabledPerson)
        pmrSelected = true;
      else if (mode is Walk) _walkingSelected = true;
    }

    List<RouteResults> results = List();
    String baseUrl = _baseUrl(
        from, to, arriveBy, finalTime, finalDate, finalSort, pmrSelected);
    String quickSort = '&optimize=QUICK';
    String triangleSort = '&optimize=TRIANGLE';

    // in every cases : WALK
    results.add(await _fetchRoutes(baseUrl + _walkExtra + quickSort));
    // Public transport : TRANSIT
    if (publicTransportSelected)
      results.add(await _fetchRoutes(baseUrl + _walkTransitExtra + quickSort));
    // Public transport and bike : TRANSIT,BICYCLE
    if (publicTransportSelected && bikeSelected)
      results.add(await _fetchRoutes(baseUrl + _bikeTransitExtra + quickSort));
    // Bike : BICYCLE
    if (bikeSelected)
      results.add(await _fetchRoutes(baseUrl + _bikeExtra + triangleSort));
    // Car : CAR
    if (carSelected)
      results.add(await _fetchRoutes(baseUrl + _carExtra + quickSort));
    // Carpooling : COV
    if (carpoolingSelected)
      results.add(await _fetchRoutes(baseUrl + _carpoolingExtra + quickSort));

    List<ItineraryRow> routes = _getRouteResultsList(lines, results);
    _downloadController.sink.add(routes);
    selectedRouteIndex = 0;
  }

  void cleanPreviousRequest() {
    _downloadController.sink.add(null);
    selectedRouteIndex = -1;
  }

  /// Call to change the route selected by the [itineraryRow] provided.
  void changeSelected(ItineraryRow itineraryRow) {
    assert(routes != null);
    int index = routes?.indexOf(itineraryRow) ?? -1;
    assert(index != -1);
    if (index != selectedRouteIndex) {
      // TODO use Stream.takeLast()
      routes[selectedRouteIndex].isSelected = false;
      routes[index].isSelected = true;
      //_selectedController.sink.add(index);
      selectedRouteIndex = index;
      _downloadController.sink.add(routes);
    }
  }

  void changeOrder(OrderType type) {
    Function newSort;
    switch (type) {
      case OrderType.byTime:
        newSort = arriveBy
            ? _sortItineraryRowByArrivalTime
            : _sortItineraryRowByDepartureTime;
        break;
      case OrderType.byDuration:
        newSort = arriveBy
            ? _sortItineraryRowByDurationForArriveBefore
            : _sortItineraryRowByDurationForGoAfter;
        break;
      case OrderType.byFare:
        newSort = arriveBy
            ? _sortItineraryRowByFareForArriveBefore
            : _sortItineraryRowByFareForGoAfter;
        break;
    }

    _sortController.sink.add(type);

    if (newSort == _sortMethod) return;

    _sortMethod = newSort;
    ItineraryRow temp = routes[selectedRouteIndex];
    routes.sort(_sortMethod);
    selectedRouteIndex = routes.indexOf(temp);
    _downloadController.sink.add(routes);
  }

  FavoriteItinerary get favoriteItinerary {
    if (departure == null || destination == null) return null;

    if (selectedTransports == null)
      throw Exception('selectedTransports is null');

    ModeOfTransport modes = ModeOfTransport(
      favorite: null,
      bike: selectedTransports[TransportTypes.bike],
      car: selectedTransports[TransportTypes.car],
      carPooling: selectedTransports[TransportTypes.carpooling] ?? false,
      carSharing: selectedTransports[TransportTypes.carsharing] ?? false,
      disabledPerson: pmrValue,
      publicTransports: selectedTransports[TransportTypes.publicTransport],
    );

    return FavoriteItinerary(
        departure: departure, destination: destination, modes: modes);
  }

  Future<RouteResults> _fetchRoutes(String urlRequest) async {
    final response = await BlocProvider.master<WebServices>().get(urlRequest);
    if (response.statusCode == 200)
      return _parseRoutes(response.body);
    else
      throw Exception('Failed to fetch routes');
  }

  RouteResults _parseRoutes(String responseBody) {
    final parsed = json.decode(responseBody);
    return RouteResults.fromJson(parsed);
  }

  List<ItineraryRow> _getRouteResultsList(
      List<ReadyLine> lines, List<RouteResults> results) {
    if (results == null || lines == null) {
      return null;
    }

    SplayTreeSet<Itinerary> routes = new SplayTreeSet<Itinerary>(_sortMethod);
    bool canHaveParkPenalty = false, parkPenalty = false;

    // Park Penalty
    if (Consts.townsWithParkPenalty.contains(destination?.properties?.city))
      canHaveParkPenalty = true;

    for (var result in results) {
      if (result == null) continue;

      if (result.requestedModes != 'COV') {
        for (var itinerary in result.routes) {
          parkPenalty = false;

          DateTime tempStart = DateTime.fromMillisecondsSinceEpoch(
              itinerary.startTime,
              isUtc: false);
          DateTime start = DateTime(tempStart.year, tempStart.month,
              tempStart.day, tempStart.hour, tempStart.minute);
          DateTime tempEnd = DateTime.fromMillisecondsSinceEpoch(
              itinerary.endTime,
              isUtc: false);
          DateTime end = DateTime(tempEnd.year, tempEnd.month, tempEnd.day,
              tempEnd.hour, tempEnd.minute);

          LegModesUsed legModesUsed = LegModesUsed();

          Itinerary newItinerary = Itinerary(
            requestedMode: LegMode.parse(result.requestedModes),
            isCarPooling: false,
            startTime: start,
            departure: Utils.latLngFromString(result.fromPlace),
            destination: Utils.latLngFromString(result.toPlace),
            isThereParkTimePenalty: canHaveParkPenalty,
            fare: itinerary.fare,
          );

          newItinerary.legs = itinerary.legs.map((leg) {
            LegMode mode = LegMode.parse(leg.mode);
            _whichModesInItinerary(legModesUsed, mode);

            //Calculate transport car price
            if (legModesUsed.car == true) {
              double price = (leg.distance.round() / 1000) * 0.55;
              String stringPrice =
                  price.toStringAsFixed(2).toString().replaceAll(".", "");
              int endPrice = int.parse(stringPrice);
              newItinerary.fare = Fare(endPrice, FareCurrency('EUR', '€'));
            }

            var startTimeLeg =
                DateTime.fromMillisecondsSinceEpoch(leg.startTime);
            var endTimeLeg = DateTime.fromMillisecondsSinceEpoch(leg.endTime);

            // no need to check if we already know there is a park penality
            if (!parkPenalty) {
              bool isThereParkTimePenality =
                  canHaveParkPenalty && mode == LegMode.CAR;
              if (isThereParkTimePenality) parkPenalty = true;
            }

            Leg newLeg = Leg(
                startTimeText: formatTimeFromMilliseconds(leg.startTime),
                endTimeText: formatTimeFromMilliseconds(leg.endTime),
                duration: calculateDuration(startTimeLeg, endTimeLeg),
                from: LatLng(leg.from.lat, leg.from.lon),
                fromName: removeCityFromName(leg.from.name),
                mode: mode,
                distance: leg.distance.round(),
                toName: removeCityFromName(leg.to.name));

            if (newLeg.mode.isTransitMode()) {
              String lineCode = (leg.agencyId != null)
                  ? '${leg.agencyId}:${leg.route}'
                  : null;
              newLeg.line =
                  lines?.firstWhere((l) => l.line.id == lineCode, orElse: () {
                return null;
              })?.line;
              newLeg.intermediateStopsNumber = leg.intermediateStops.length + 1;
              newLeg.lineDestination = removeCityFromName(leg.headsign);
            }

            // For graph
            GraphData graphData = GraphData.defaultValues();
            double totalDistance = 0;
            int minElevation = Consts.startingMinElevation, maxElevation = 0;

            newLeg.steps = leg.steps.map((step) {
              // for graph
              for (int i = 0; i < step.elevations.length; i++) {
                Elevation currentElevation = step.elevations[i];

                graphData.elevations.add(Elevation(
                    currentElevation.first + totalDistance,
                    currentElevation.second));
                if (minElevation > currentElevation.second.round())
                  minElevation = currentElevation.second.round();
                if (maxElevation < currentElevation.second)
                  maxElevation = currentElevation.second.round();

                if (i > 1) {
                  var d =
                      currentElevation.second - step.elevations[i - 1].second;
                  if (d > 0)
                    graphData.positiveElevation += d;
                  else
                    graphData.negativeElevation += d.abs();
                }
              }

              // for graph
              totalDistance += (step != null && step.elevations.length > 0)
                  ? step.elevations[step.elevations.length - 1].first
                  : step.distance;

              return Step(
                  absoluteDirection:
                      AbsoluteDirection.parse(step.absoluteDirection),
                  relativeDirection:
                      RelativeDirection.parse(step.relativeDirection),
                  distance: step.distance.round(),
                  streetName: step.streetName,
                  stayOn: step.stayOn,
                  exit: step?.exit != null ? int.tryParse(step.exit) : null,
                  bogusName: step.bogusName);
            }).toList(); // map step

            // We show graph if the elevation percentage is > 10%
            if ((maxElevation - minElevation) / totalDistance >=
                Consts.minElevationPercentage) {
              newLeg.graphData = graphData;
            }

            // Intermediate stops
            newLeg.intermediateStops =
                leg.intermediateStops?.map((intermediateStop) {
              return IntermediateStop(_formatTextLeg(intermediateStop.name),
                  LatLng(intermediateStop.lat, intermediateStop.lon));
            })?.toList();

            newItinerary.polylines.add(Polyline(
              color: newLeg.color,
              points: decodeEncodedPolyline(leg.legGeometry.points),
              strokeWidth: 4.0,
            ));
            newItinerary.co2Rejection +=
                _calculateCo2(newLeg.mode, newLeg.distance);
            newItinerary.distance += newLeg.distance;
            _setTrafficRouteAlertMessage(newItinerary, leg);

            return newLeg;
          }).toList(); // map leg

          // Park Penality
          if (canHaveParkPenalty && parkPenalty) {
            end = end.add(Duration(seconds: Consts.penaltyInSeconds));

            if (newItinerary.legs.length == 1 &&
                newItinerary.legs[0].mode == LegMode.CAR) {
              newItinerary.legs[0].endTimeText = formatTimeFromDateTime(end);
            }
          }

          newItinerary.duration = Duration(
              seconds: itinerary.duration +
                  (parkPenalty ? Consts.penaltyInSeconds : 0));
          newItinerary.endTime = end;
          newItinerary.co2RejectionText =
              _getCo2RejectionText(legModesUsed, newItinerary);
          newItinerary.showDetailsButton = _showDetailsButton(legModesUsed);

          routes.add(newItinerary);
          _handleNextTrip(routes);
        }
      } else {
        // For carpooling itineraries
        if (result.routes != null && result.routes.length == 1) {
          RouteJson routeJson = result.routes.first;
          if (routeJson.legs != null && routeJson.legs.length == 1) {
            LegJson legJson = routeJson.legs.first;

            for (var carsharingData in legJson.carSharing) {
              Journey journey = carsharingData.journey;

              DateTime tempStart = DateTime.fromMillisecondsSinceEpoch(
                  routeJson.startTime,
                  isUtc: false);
              DateTime start = DateTime(tempStart.year, tempStart.month,
                  tempStart.day, tempStart.hour, tempStart.minute);
              DateTime tempEnd = DateTime.fromMillisecondsSinceEpoch(
                  routeJson.endTime,
                  isUtc: false);
              DateTime end = DateTime(tempEnd.year, tempEnd.month, tempEnd.day,
                  tempEnd.hour, tempEnd.minute);

              Itinerary itinerary = Itinerary(
                  requestedMode: LegMode.parse(result.requestedModes),
                  isCarPooling: true,
                  startTime: start,
                  endTime: end,
                  duration: Duration(seconds: routeJson.duration),
                  departure: Utils.latLngFromString(result.fromPlace),
                  destination: Utils.latLngFromString(result.toPlace),
                  distance: journey.distance.round(),
                  fare: null);

              itinerary.polylines.add(Polyline(
                color: Colors.white70,
                points: decodeEncodedPolyline(legJson.legGeometry.points),
                strokeWidth: 4.0,
              ));

              double tempPrice;
              String priceCovoit;

              if (carsharingData.journey.origin.contains('karos')) {
                priceCovoit = translations.text('prices.not_specified');
              } else if (carsharingData.journey.cost.variable.isNotEmpty) {
                tempPrice = (carsharingData.journey.distance / 1000) * double.parse(carsharingData.journey.cost.variable);
                priceCovoit = tempPrice.toStringAsFixed(2).toString();
              }else if (carsharingData.journey.cost.fixed.isNotEmpty) {
                priceCovoit = carsharingData.journey.cost.fixed;
              } else {
                priceCovoit = translations.text('prices.free');
              }

              itinerary.carpoolingData = CarpoolingData(
                url: carsharingData.journey.url,
                origin: carsharingData.journey.origin,
                driverName: carsharingData.journey.driver.alias,
                driverPicture: carsharingData.journey.driver.image,
                price: priceCovoit,
              );
              itinerary.showDetailsButton = true;

              routes.add(itinerary);
            }
          }
        }
      }
    }

    List<Itinerary> tempList = routes.toList(growable: false);
    return tempList
        .map((iti) => ItineraryRow(iti, 0 == tempList?.indexOf(iti)))
        .toList();
  }

  // Urls + extras
  String _baseUrl(from, to, arriveBy, finalTime, finalDate, finalSort, isPMR) =>
      Intl.message(
          'routers/default/plan?routerId=default&fromPlace=${from.latitude},${from.longitude}&toPlace=${to.latitude},${to.longitude}' +
              '&arriveBy=$arriveBy&time=$finalTime&date=$finalDate&locale=fr_FR&walkSpeed=1.1112&walkReluctance=5&wheelchair=$isPMR',
          name: 'baseUrl',
          args: [from, to, arriveBy, finalTime, finalDate, finalSort],
          desc: 'Construct base url for requesting routes');
  String _walkExtra = '&mode=WALK';
  String _walkTransitExtra =
      '&mode=WALK,TRANSIT&showIntermediateStops=true&minTransferTime=60&transferPenalty=60&numItineraries=6&walkBoardCost=300';
  String _bikeTransitExtra =
      '&mode=BICYCLE,TRANSIT&showIntermediateStops=true&bikeSwitchCost=300&numItineraries=2&bikeSpeed=4.44&minTransferTime=60&transferPenalty=60&walkBoardCost=300&bikeBoardCost=600&triangleSlopeFactor=0.33&triangleTimeFactor=0.33&triangleSafetyFactor=0.34&stairsReluctance=4';
  String _bikeExtra =
      '&mode=BICYCLE&bikeSpeed=4.44&triangleSlopeFactor=0.33&triangleTimeFactor=0.33&triangleSafetyFactor=0.34&stairsReluctance=4&optimize=TRIANGLE';
  String _carExtra = '&mode=CAR&carParkCost=900&carDropoffTime=120';
  String _carpoolingExtra = '&mode=COV';

  int _baseSortItinerary(Itinerary iti1, Itinerary iti2) {
    assert(iti1 != null);
    assert(iti2 != null);
    if (iti1.isCarPooling && !iti2.isCarPooling) return -1;
    if (!iti1.isCarPooling && iti2.isCarPooling) return 1;
    if (iti1.isCarPooling && iti2.isCarPooling)
      return iti1?.carpoolingData?.url?.compareTo(iti2?.carpoolingData?.url);
    if (iti1 == iti2) return 0;
    return null;
  }

  int _baseSortItineraryRow(ItineraryRow iti1, ItineraryRow iti2) {
    assert(iti1 != null);
    assert(iti2 != null);
    int compare = _baseSortItinerary(iti1.itinerary, iti2.itinerary);
    return compare != null ? compare : null;
  }

  int _baseSortByDurationAndArrivalTime(Itinerary iti1, Itinerary iti2) {
    DateTime wantedArrival = DateTime(
        _savedSelectedDate.year,
        _savedSelectedDate.month,
        _savedSelectedDate.day,
        _savedSelectedTime.hour,
        _savedSelectedTime.minute);

    if (iti1.endTime.isBeforeSameDay(wantedArrival) &&
        iti2.endTime.isBeforeSameDay(wantedArrival))
      return iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes);

    if (iti1.endTime.isBeforeSameDay(wantedArrival)) return 1;

    if (iti2.endTime.isBeforeSameDay(wantedArrival)) return -1;

    return 0;

    // if (iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes) == 0)
    //   return iti1.endTime.compareTo(iti2.endTime);
    // else
    //   return iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes);
  }

  int _baseSortByDurationAndDepartureTime(Itinerary iti1, Itinerary iti2) {
    if (iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes) == 0)
      return iti1.startTime.compareTo(iti2.startTime);
    else
      return iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes);
  }

  int _sortItineraryByArrivalTime(Itinerary iti1, Itinerary iti2) {
    int compare = _baseSortItinerary(iti1, iti2);
    if (compare != null) return compare;

    if (iti1.endTime.compareTo(iti2.endTime) == 0)
      return iti1.duration.inMinutes..compareTo(iti2.duration.inMinutes);
    else
      return iti1.endTime.compareTo(iti2.endTime);
  }

  int _sortItineraryByDepartureTime(Itinerary iti1, Itinerary iti2) {
    int compare = _baseSortItinerary(iti1, iti2);
    if (compare != null) return compare;

    if (iti1.startTime.compareTo(iti2.startTime) == 0)
      return iti1.duration.inMinutes.compareTo(iti2.duration.inMinutes);
    else
      return iti1.startTime.compareTo(iti2.startTime);
  }

  int _sortItineraryRowByArrivalTime(ItineraryRow iti1, ItineraryRow iti2) {
    int compare = _baseSortItineraryRow(iti1, iti2);
    if (compare != null) return compare;

    return _sortItineraryByArrivalTime(iti1.itinerary, iti2.itinerary);
  }

  int _sortItineraryRowByDepartureTime(ItineraryRow iti1, ItineraryRow iti2) {
    int compare = _baseSortItineraryRow(iti1, iti2);
    if (compare != null) return compare;

    return _sortItineraryByDepartureTime(iti1.itinerary, iti2.itinerary);
  }

  int _sortItineraryByDurationForArriveBefore(Itinerary iti1, Itinerary iti2) {
    int compare = _baseSortItinerary(iti1, iti2);
    if (compare != null) return compare;

    return _baseSortByDurationAndArrivalTime(iti1, iti2);
  }

  int _sortItineraryByDurationForGoAfter(Itinerary iti1, Itinerary iti2) {
    int compare = _baseSortItinerary(iti1, iti2);
    if (compare != null) return compare;

    return _baseSortByDurationAndDepartureTime(iti1, iti2);
  }

  int _sortItineraryRowByDurationForArriveBefore(
      ItineraryRow iti1, ItineraryRow iti2) {
    int compare = _baseSortItineraryRow(iti1, iti2);
    if (compare != null) return compare;

    return _baseSortByDurationAndArrivalTime(iti1.itinerary, iti2.itinerary);
  }

  int _sortItineraryRowByDurationForGoAfter(
      ItineraryRow iti1, ItineraryRow iti2) {
    int compare = _baseSortItineraryRow(iti1, iti2);
    if (compare != null) return compare;

    return _baseSortByDurationAndDepartureTime(iti1.itinerary, iti2.itinerary);
  }

  int _sortItineraryRowByFareForArriveBefore(
      ItineraryRow iti1, ItineraryRow iti2) {
    // int compare = _baseSortItineraryRow(iti1, iti2);
    // if (compare != null) return compare;

    double fare1 = _getFare(iti1);
    double fare2 = _getFare(iti2);

    if (fare1 == fare2)
      return _baseSortByDurationAndArrivalTime(iti1.itinerary, iti2.itinerary);
    else
      return fare1.compareTo(fare2);
  }

  bool _checkLegModeRail(List<Leg> legs) {
    if (legs != null) {
      for (Leg leg in legs)
        if (leg.mode == LegMode.RAIL) return true;
    }
    return false;
  }

  double _getFare(ItineraryRow iti) {
    bool hasModeRail = _checkLegModeRail(iti.itinerary.legs);
    if (iti.itinerary.isCarPooling && iti.itinerary.fare == null) {
      double price = double.tryParse(iti.itinerary.carpoolingData.price) ?? null;
      return price == null ? double.nan : (price * 100).toDouble();
    }
    else if (iti.itinerary.fare == null && hasModeRail) return double.nan;

    return iti.itinerary.fare?.cents?.toDouble() ?? 0.0;
  }

  int _sortItineraryRowByFareForGoAfter(ItineraryRow iti1, ItineraryRow iti2) {
    // int compare = _baseSortItineraryRow(iti1, iti2);
    // if (compare != null) return compare;

    double fare1 = _getFare(iti1);
    double fare2 = _getFare(iti2);

    if (fare1 == fare2)
      return _baseSortByDurationAndDepartureTime(
          iti1.itinerary, iti2.itinerary);
    else
      return fare1.compareTo(fare2);
  }

  String _getCo2RejectionText(LegModesUsed modesUsed, Itinerary itinerary) {
    if (modesUsed.car) {
      if (!modesUsed.bike && !modesUsed.bystander && !modesUsed.publicTransport)
        return Utils.formatRessource('route.co2.message_car', [
          '${itinerary.distance / 1000}',
          itinerary.co2Rejection.toString(),
          _calculateCo2(LegMode.BUS, itinerary.distance).toString()
        ]);

      if (modesUsed.publicTransport)
        return Utils.formatRessource('route.co2.message_car_public_transport', [
          itinerary.co2Rejection.toString(),
          '${_calculateCo2(LegMode.CAR, itinerary.distance) - itinerary.co2Rejection}'
        ]);
    } else {
      if (modesUsed.publicTransport)
        return Utils.formatRessource('route.co2.message_public_transport', [
          '${itinerary.distance / 1000}',
          itinerary.co2Rejection.toString(),
          '${_calculateCo2(LegMode.CAR, itinerary.distance) - itinerary.co2Rejection}'
        ]);
      else {
        if (modesUsed.bike)
          return translations.text('route.co2.message_bike');
        else
          return translations.text('route.co2.message_bystander');
      }
    }
    return "ERROR_MESSAGE_CO2";
  }

  void _whichModesInItinerary(LegModesUsed modesUsed, LegMode mode) {
    if (mode == LegMode.BICYCLE) modesUsed.bike = true;
    if (mode == LegMode.WALK) modesUsed.bystander = true;
    if (mode == LegMode.CAR) modesUsed.car = true;
    if (mode.isTransitMode()) modesUsed.publicTransport = true;
  }

  bool _showDetailsButton(LegModesUsed modesUsed) {
    return modesUsed.publicTransport;
  }

  int _calculateCo2(LegMode mode, int distance) {
    int doCalculation(double constante) {
      return (distance / 1000 * constante).round();
    }

    if (mode == LegMode.BUS) return doCalculation(Consts.busRejection);
    if (mode == LegMode.TRAM) return doCalculation(Consts.tramRejection);
    if (mode == LegMode.CABLE_CAR || mode == LegMode.RAIL)
      return doCalculation(Consts.railCableCarRejection);
    if (mode == LegMode.CAR) return doCalculation(Consts.carRejection);
    return Consts.activeMobility;
  }

  void _setTrafficRouteAlertMessage(Itinerary itinerary, LegJson leg) {
    if (leg.alerts == null) {
      itinerary.alertMessage = null;
      return;
    }

    var buffer = StringBuffer(itinerary.alertMessage ?? '');
    if (buffer.isNotEmpty) buffer.write('\n\n');

    for (var i = 0; i < leg.alerts?.length; i++) {
      buffer.write(leg.alerts[i].alertHeaderText + '\n' ?? '');
      buffer.write(leg.alerts[i].alertDescriptionText ?? '');

      if (i + 1 < leg.alerts.length) buffer.write('\n');
    }

    itinerary.alertMessage = buffer.toString();
  }

  String _formatTextLeg(String name) =>
      toUpperCaseFirstLetterLowerCaseOthers(removeCityFromName(name));

  void _handleNextTrip(SplayTreeSet<Itinerary> routes) {
    List<Itinerary> tempRoutes = List.of(routes);
    List<Itinerary> sameRoutes;
    while (tempRoutes.isNotEmpty) {
      sameRoutes =
          tempRoutes.where((i) => _sameItinerary(tempRoutes.first, i)).toList();
      if (sameRoutes.length > 1) {
        // If there is atleast two similar itineraries, we keep the earliest and save the startTime of the second.
        sameRoutes.sort(_sortItineraryByDepartureTime);
        sameRoutes.first.nextTrip = sameRoutes[1].startTime;
        routes.removeWhere(
            (i) => i != sameRoutes.first && sameRoutes.contains(i));
      }
      // Remove all the itineraries of tempRoutes because there is no other similar itinerariers for these.
      tempRoutes.removeWhere((i) => sameRoutes.contains(i));
    }
  }

  void setTransportTypeValue(TransportType transport, bool value) {
    if (transport is! DisabledPerson &&
        !TransportTypes.availableTransports.contains(transport)) return;
    if (pmrValue && transport.key == TransportTypes.bike.key && value) return;

    _selectedTransports[transport] = value;
    PrefService.setBool(transport.key, value);
    _selectedTransportsController.sink.add(_selectedTransports);
  }

  List<TransportType> getSelectedTransports() {
    return _selectedTransports.entries
        .where((entry) => entry.value)
        .map((entry) => entry.key)
        .toList();
  }

  void setPmrValue(bool value) {
    PrefService.setBool(TransportTypes.disabled.key, value);
    _pmrController.sink.add(value);

    if (value) setTransportTypeValue(TransportTypes.bike, false);
  }

  void setItineraryFromFavorites(Favorite favorite, BuildContext context) async{
    assert(favorite != null);
    assert(favorite.modes != null);

    setDeparture(favorite.point1);
    setDestination(favorite.point2);

    setTransportTypeValue(TransportTypes.bike, favorite.modes.bike);
    setTransportTypeValue(TransportTypes.car, favorite.modes.car);
    setTransportTypeValue(TransportTypes.carpooling, favorite.modes.carPooling);
    setTransportTypeValue(TransportTypes.carsharing, favorite.modes.carSharing);
    setTransportTypeValue(TransportTypes.publicTransport, favorite.modes.publicTransports);
    setPmrValue(favorite.modes.disabledPerson);
  }
}

/// Return true if [iti] is the same as [first].
/// It means that they have similar legs(same number, same modes, same lines) but at different start times.
bool _sameItinerary(Itinerary first, Itinerary iti) {
  assert(first != null);
  assert(iti != null);

  if (first.isCarPooling || iti.isCarPooling) return false;
  if (first == iti) return true;

  if (first.legs.length == iti.legs.length) {
    for (int i = 0; i < first.legs.length; i++) {
      if (first.legs[i].mode == iti.legs[i].mode) {
        if (first.legs[i].mode == TransportMode.TRANSIT) {
          if (first.legs[i].line?.shortName != iti.legs[i].line?.shortName ||
              first.legs[i].intermediateStopsNumber !=
                  iti.legs[i].intermediateStopsNumber) return false;
        }
      } else
        return false;
    }
  } else
    return false;

  return true;
}
