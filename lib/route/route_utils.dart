import 'package:flutter_map/flutter_map.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';
import 'package:metro/route/models/route.dart';


///Decode the google encoded string using Encoded Polyline Algorithm Format
///
///return [List]
List<LatLng> decodeEncodedPolyline(String encoded) {
  List<LatLng> poly = [];
  int index = 0, len = encoded.length;
  int lat = 0, lng = 0;

  while (index < len) {
    int b, shift = 0, result = 0;
    do {
      b = encoded.codeUnitAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
    lat += dlat;

    shift = 0;
    result = 0;
    do {
      b = encoded.codeUnitAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
    lng += dlng;
    LatLng p = LatLng((lat / 1E5).toDouble(), (lng / 1E5).toDouble());
    poly.add(p);
  }
  return poly;
}

///Calculates the bounding box that encapsulates this [itinerary].
LatLngBounds getItineraryFittingBounds(Itinerary itinerary) {
  if(itinerary == null || itinerary.polylines == null || itinerary.polylines.length == 0)
    return null;

  LatLngBounds bounds = LatLngBounds();
  for(var i = 0; i < itinerary.polylines.length; i++) {
    var points = itinerary.polylines[i].points;
    if(points == null)
      continue;

    for(var j =0; j < points.length; j++) {
      if(points[j] == null)
        continue;

      bounds.extend(points[j]);
    }
  }

  return bounds;
}

// -----------------------------------------------------------------------------------------------------------------
// -------------------- String -------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------

String toUpperCaseFirstLetterLowerCaseOthers(String str) {
  assert(str != null, 'toUpperCaseFirstLetterLowerCaseOthers: argument "str" can not be null');

  StringBuffer buffer = StringBuffer();
  List<String> splits = str.split(' ');
  int count = 0;

  for (var i = 0; i < splits.length; i++) {
    if (splits[i] == '')
      continue;

    buffer.write(splits[i][0].toUpperCase());
    buffer.write(splits[i].substring(1).toLowerCase());
    if (count + 1 < splits.length)
      buffer.write(' ');
  }

  return buffer.toString();
}

String removeCityFromName(String name) {
  assert(name != null, 'removeCityFromName: argument "name" can not be null');

  var splits = name.split(',');
  if (splits.length == 1)
    // no comma
    return splits[0];

  if (splits.length > 1) {
    // in case there is several commas
    StringBuffer buffer = StringBuffer();

    for (var i = 1; i < splits.length; i++)
      buffer.write(splits[i].trim());

    return buffer.toString();
  }
  return "";
}

String stopNumber(number) => Intl.plural(number,
  one: '$number arrêt',
  other: '$number arrêts',
  name: 'stopNumber',
  args: [number],
  desc: 'Show the number of intermediate stops for a leg');

String intermediateStopNumber(number) => Intl.plural(number,
  one: '$number arrêt intermédiaire',
  other: '$number arrêts intermédiaires',
  name: 'stopNumber',
  args: [number],
  desc: 'Show the number of intermediate stops for a leg');