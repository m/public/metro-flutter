import 'package:metro/localization/translations.dart';

class TimePreference {}

class ArriveBefore extends TimePreference {
  @override
  String toString() => translations.text('time_preference.arrive_before');
}

class GoAfter extends TimePreference {
  @override
  String toString() => translations.text('time_preference.go_after');
}

class SortPreference {}

class Fastest extends SortPreference {
  @override
  String toString() => translations.text('sort_preference.fastest');
}

class LessSteps extends SortPreference {
  @override
  String toString() => translations.text('sort_preference.less_steps');
}

class MostSecured extends SortPreference {
  @override
  String toString() => translations.text('sort_preference.most_secured');
}

class LessPoluated extends SortPreference {
  @override
  String toString() => translations.text('sort_preference.less_poluated');
}