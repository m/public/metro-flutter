import 'package:flutter/material.dart';
import 'package:metro/localization/translations.dart';

class TransportTypes {
  static const publicTransport = PublicTransport();
  static const car = Car();
  static const bike = Bike();
  static const carpooling = Carpooling();
  static const carsharing = Carsharing();
  static const disabled = DisabledPerson();
  static const walk = Walk();

  static const availableTransports = [publicTransport, car, bike, walk, carpooling];
}

abstract class TransportType {
  final String key;
  final IconData icon;
  final String _nameId;
  String get name => translations.text(_nameId);
  String get shortName => this is PublicTransport ? translations.text(_nameId + '_short') : name;

  const TransportType._(this.key, this.icon, this._nameId);
}

class Walk extends TransportType {
  const Walk() : super._('walk', Icons.directions_car, 'transport_type.walk');
}

class PublicTransport extends TransportType {
  const PublicTransport() : super._('car', Icons.directions_car, 'transport_type.public');
}

class Car extends TransportType {
  const Car() : super._('tc', Icons.directions_bus, 'transport_type.car');
}

class Bike extends TransportType {
  const Bike() : super._('bike', Icons.directions_bike, 'transport_type.bike');
}

class Carpooling extends TransportType {
  const Carpooling() : super._('carpooling', Icons.directions_car, 'transport_type.carpooling');
}

class Carsharing extends TransportType {
  const Carsharing() : super._('carsharing', Icons.directions_car, 'transport_type.carsharing');
}

class DisabledPerson extends TransportType {
  const DisabledPerson() : super._('pmr', Icons.accessible, 'transport_type.pmr');
}