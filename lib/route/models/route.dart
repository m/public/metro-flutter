import 'dart:ui' show Color;

import 'package:flutter/material.dart' show Colors;
import 'package:flutter_map/plugin_api.dart' show Polyline;
import 'package:latlong/latlong.dart';
import 'package:metro/global/helpers/date_utils.dart';
import 'package:metro/global/helpers/hash.dart';
import 'package:metro/global/models/line.dart';
import 'package:metro/global/utils.dart';

// JSON's results classes ------------------------------------------------------
class RouteResults {
  String fromPlace;
  String toPlace;
  String requestedModes;
  List<RouteJson> routes;

  RouteResults(this.fromPlace, this.toPlace, this.requestedModes, this.routes);

  factory RouteResults.fromJson(Map<String, dynamic> json) {
    if(!json.containsKey('plan'))
      return null;
    List<RouteJson> routeList = (json['plan']['itineraries'] as List)?.map((l) => RouteJson.fromJson(l))?.toList();
    return RouteResults(json['requestParameters']['fromPlace'] as String,
      json['requestParameters']['toPlace'] as String,
      json['requestParameters']['mode'] as String, routeList);
  }
}

class RouteJson {
  final int duration;
  final int startTime;
  final int endTime;
  final List<LegJson> legs;
  final Fare fare;

  RouteJson(this.duration, this.startTime, this.endTime, this.legs, this.fare);

  factory RouteJson.fromJson(Map<String, dynamic> json) {
    List<LegJson> temp = (json['legs'] as List)?.map((l) => LegJson.fromJson(l))?.toList();
    return RouteJson(json['duration'], json['startTime'], json['endTime'], temp, Fare.fromJson(json));
  }
}

class LegJson {
  final int startTime;
  final int endTime;
  final double distance;
  final String mode;
  final String route;
  final String routeColor;
  final String routeTextColor;
  final String headsign;
  final String agencyId;
  final LegFromTo from;
  final LegFromTo to;
  final LegGeometry legGeometry;
  final List<AlertsResults> alerts;
  final String routeShortName;
  final int duration;
  final List<LegFromTo> intermediateStops;
  final List<StepJson> steps;
  final List<CarSharingResults> carSharing;

  LegJson(this.startTime, this.endTime, this.distance, this.mode, this.route,
      this.routeColor, this.routeTextColor, this.headsign, this.agencyId, this.from, this.to,
      this.legGeometry, this.alerts, this.routeShortName, this.duration,
      this.intermediateStops, this.steps, this.carSharing);

  factory LegJson.fromJson(Map<String, dynamic> json) {
    List<AlertsResults> mAlerts = (json['alerts'] as List)?.map((l) => AlertsResults.fromJson(l))?.toList();
    List<LegFromTo> mIntermediateStops = (json['intermediateStops'] as List)?.map((l) => LegFromTo.fromJson(l))?.toList();
    List<StepJson> mSteps = (json['steps'] as List)?.map((l) => StepJson.fromJson(l))?.toList();
    List<CarSharingResults> mCarSharing = (json['carSharing'] as List)?.map((l) => CarSharingResults.fromJson(l))?.toList();

    return LegJson(json['startTime'], json['endTime'], json['distance']?.toDouble(), json['mode'],
      json['route'], json['routeColor'], json['routeTextColor'], json['headsign'], json['agencyId'],
      LegFromTo.fromJson(json['from']), LegFromTo.fromJson(json['to']),
      LegGeometry.fromJson(json['legGeometry']), mAlerts, json['routeShortName'],
      json['duration'], mIntermediateStops, mSteps,
      mCarSharing);
  }
}

class CarSharingResults {
  final Journey journey;

  CarSharingResults(this.journey);

  factory CarSharingResults.fromJson(Map<String, dynamic> json) =>
      CarSharingResults(Journey.fromJson(json['journeys']));
}

class Journey {
  final String origin;
  final String url;
  final int distance;
  final Driver driver;
  final Cost cost;

  Journey(this.origin, this.url, this.distance, this.driver, this.cost);

  factory Journey.fromJson(Map<String, dynamic> json) =>
      Journey(json['origin'], json['url'], json['distance'].toDouble().round(), Driver.fromJson(json['driver']), Cost.fromJson(json['cost']));
}

class Driver {
  final String alias;
  final String image;

  Driver(this.alias, this.image);

  factory Driver.fromJson(Map<String, dynamic> json) =>
      Driver(json['alias'], json['image']);
}

class Cost{
  final String fixed;
  final String variable;

  Cost(this.fixed, this.variable);

  factory Cost.fromJson(Map<String, dynamic> json) {
    String fixed = json['fixed'].toString();
    String variable = json['variable'].toString();
      return Cost(fixed.toString(), variable.toString());

  }

}

class Fare {
  final int cents;
  final FareCurrency currency;

  Fare(this.cents, this.currency);

  factory Fare.fromJson(Map<String, dynamic> json) {
    if (json['fare'] != null && json['fare'] is Map && (json['fare'] as Map).length > 0)
      return Fare(json['fare']['fare']['regular']['cents'] as int, FareCurrency.fromJson(json['fare']['fare']['regular']['currency']));

    return null;
  }
}

class FareCurrency {
  final String currencyCode;
  final String symbol;

  FareCurrency(this.currencyCode, this.symbol);

  factory FareCurrency.fromJson(Map<String, dynamic> json) =>
    FareCurrency(json['currencyCode'], json['symbol']);
}

class AlertsResults {
  final String alertHeaderText;
  final String alertDescriptionText;

  AlertsResults(this.alertHeaderText, this.alertDescriptionText);

  factory AlertsResults.fromJson(Map<String, dynamic> json) =>
      AlertsResults(json['alertHeaderText'], json['alertDescriptionText']);
}

class LegFromTo {
  final String name;
  final double lon;
  final double lat;

  LegFromTo(this.name, this.lon, this.lat);

  factory LegFromTo.fromJson(Map<String, dynamic> json) =>
      LegFromTo(json['name'], json['lon']?.toDouble(), json['lat']?.toDouble());
}

class StepJson {
  String streetName;
  String relativeDirection;
  String absoluteDirection;
  double distance;
  bool bogusName;
  bool stayOn;
  String exit;
  List<Elevation> elevations;

  StepJson(this.distance, this.relativeDirection, this.streetName,
    this.absoluteDirection, this.stayOn, this.exit, this.bogusName,
    this.elevations);

  factory StepJson.fromJson(Map<String, dynamic> json) {
    List<Elevation> mElevation = (json['elevation'] as List)?.map((l) => Elevation.fromJson(l))?.toList();

    return StepJson(json['distance']?.toDouble(), json['relativeDirection'], json['streetName'],
      json['absoluteDirection'], json['stayOn'], json['exit'], json['bogusName'], mElevation);
  }
}

// Common classes --------------------------------------------------------------

class LegGeometry {
  String points;
  double lenght;

  LegGeometry(this.points, this.lenght);

  factory LegGeometry.fromJson(Map<String, dynamic> json) =>
      LegGeometry(json['points'], json['lenght']?.toDouble());
}

class Elevation {
  double first;
  double second;

  Elevation(this.first, this.second);

  factory Elevation.fromJson(Map<String, dynamic> json) =>
      Elevation(json['first']?.toDouble(), json['second']?.toDouble());
}

// App classes -----------------------------------------------------------------

class CarpoolingData {
  String driverName;
  String driverPicture;
  String url;
  String origin;
  String price;

  CarpoolingData({this.driverName, this.driverPicture, this.url, this.origin, this.price});

}

class Itinerary {
  LegMode requestedMode;
  LatLng departure;
  LatLng destination;
  DateTime startTime;
  DateTime endTime;
  Duration duration;
  int distance;
  int co2Rejection;
  String co2RejectionText;
  String alertHeader;
  String alertMessage;
  bool isThereParkTimePenalty;

  bool isCarPooling;
  CarpoolingData carpoolingData;

  List<Leg> legs;
  List<Polyline> polylines;
  Fare fare;

  DateTime nextTrip;
  bool showDetailsButton;

  Itinerary({this.requestedMode, this.departure, this.destination, this.startTime, this.endTime, this.duration, this.distance = 0,
    this.co2Rejection = 0, this.co2RejectionText, this.alertHeader,
    this.alertMessage = '', this.isThereParkTimePenalty, this.isCarPooling, this.carpoolingData,
    this.legs, this.fare, this.nextTrip}) : polylines = List();

  String get startTimeText => formatTimeFromDateTime(startTime);
  String get endTimeText => formatTimeFromDateTime(endTime);
  String get durationText => formatDuration(duration);

  bool operator ==(dynamic other) => other is! Itinerary ? false : equalsItinerary(this, other);

  @override
  int get hashCode => hashObjects([departure, destination, startTime, duration, legs]);

  bool equalsItinerary(Itinerary iti1, Itinerary iti2) {
    if(iti1.isCarPooling && iti2.isCarPooling) {
      return iti1.carpoolingData.url == iti2.carpoolingData.url;
    }
    if(iti1.isCarPooling != iti2.isCarPooling) return false;

    if (iti1.departure == iti2.departure && iti1.destination == iti2.destination && iti1.startTime.compareTo(iti2.startTime) == 0
      && iti1.duration == iti2.duration && iti1.legs.length == iti2.legs.length) {
      for (int i = 0; i < iti1.legs.length; i++) {
        if (iti1.legs[i].mode == iti2.legs[i].mode) {
          if (iti1.legs[i].mode == TransportMode.TRANSIT) {
            if (iti1.legs[i].line?.shortName != iti2.legs[i].line?.shortName) return false;
          }
        } else
          return false;
      }
    } else
      return false;

    return true;
  }
}

class ItineraryRow {
  final Itinerary itinerary;
  bool isSelected;

  ItineraryRow(this.itinerary, this.isSelected);

  bool operator ==(dynamic other) => other is ItineraryRow ? this.itinerary == other.itinerary : false;

  @override
  int get hashCode => itinerary.hashCode;
}

class Leg {
  String startTimeText;
  String endTimeText;
  Duration duration;
  int distance;
  String fromName;
  LatLng from;
  String toName;

  LegMode mode;
  Line line;
  String lineDestination;

  int intermediateStopsNumber;
  List<IntermediateStop> intermediateStops;
  List<Step> steps;
  GraphData graphData;

  bool isLegVisibleOnResultsScreen;

  Leg({this.startTimeText, this.endTimeText, this.duration, this.distance, this.fromName,
    this.from, this.toName, this.mode, this.line, this.lineDestination,
    this.intermediateStopsNumber, this.intermediateStops, this.steps,
    this.graphData, this.isLegVisibleOnResultsScreen});

  bool operator ==(dynamic other) {
    if(other is Leg && fromName == other.fromName && startTimeText == other.startTimeText && duration == other.duration && mode == other.mode && line == other.line)
      return true;
    return false;
  }

  @override
  int get hashCode => hashObjects([fromName, startTimeText, duration, mode, line ?? 0]);

  Color get color => mode == LegMode.FUNICULAR
      ? Color(0xFFff00ff)
      : (mode != null)
          ? mode.isTransitMode()
              ? (line?.color == '' ? Color(0xffb49bda) : Utils.colorFromHex(line?.color))
              : Colors.grey
          : Colors.grey;
}

class GraphData {
  List<Elevation> elevations;
  double positiveElevation;
  double negativeElevation;

  GraphData({this.elevations, this.positiveElevation = 0, this.negativeElevation = 0});
  GraphData.defaultValues({this.positiveElevation = 0, this.negativeElevation = 0}) : elevations = List<Elevation>();
}

class Step {
  String streetName;
  RelativeDirection relativeDirection;
  AbsoluteDirection absoluteDirection;
  int distance;
  bool bogusName;
  bool stayOn;
  int exit;

  Step({this.streetName, this.relativeDirection, this.absoluteDirection,
    this.distance, this.bogusName, this.stayOn, this.exit});
}

class IntermediateStop {
  String name;
  LatLng coordinates;

  IntermediateStop(this.name, this.coordinates);
}

class LegModesUsed {
  bool bike;
  bool bystander;
  bool car;
  bool publicTransport;

  LegModesUsed({this.bike = false, this.bystander = false,
    this.car = false, this.publicTransport = false});
}

// enums -----------------------------------------------------------------------
/// Type of sort to apply to the results of routes query.
enum OrderType{
  byDuration, byTime, byFare
}

abstract class Enum {
  final String value;
  static get values => [];
  const Enum._(this.value);
  toString() => '${this.runtimeType.toString()}.$value';

  bool operator ==(dynamic other) => _equalOperator(other, value);

  @override
  int get hashCode => value.hashCode;

  bool _equalOperator<T extends Enum>(dynamic other, String value) {
    if (other is! T) return false;
    Enum mode = other;
    return (mode.value == value);
  }
}

T _parseString<T extends Enum>(String str, List<T> values) {
  if(str == null) return null;
  var tme = values.firstWhere((e) => e.toString().split('.')[1].toLowerCase() == str.toLowerCase(), orElse: () => null);
  return tme;
}

class TransportMode extends Enum{
  const TransportMode._(String str) : super._(str);
  static get values => [BIKE,CAR, TRANSIT, WALK];

  static const BIKE = const TransportMode._('BIKE');
  static const CAR = const TransportMode._('CAR');
  static const TRANSIT = const TransportMode._('TRANSIT');
  static const WALK = const TransportMode._('WALK');

  static TransportMode parse(String str) => _parseString<TransportMode>(str, values);

  bool operator ==(dynamic other) => super._equalOperator(other, value);

  @override
  int get hashCode => value.hashCode;
}

class LegMode extends Enum{
  const LegMode._(String str) : super._(str);
  static get values => [BICYCLE, BUS, CABLE_CAR, CAR, COV, RAIL, TRAM, WALK, FUNICULAR];

  static const BICYCLE = const LegMode._('BICYCLE');
  static const BUS = const LegMode._('BUS');
  static const CABLE_CAR = const LegMode._('CABLE_CAR');
  static const CAR = const LegMode._('CAR');
  static const COV = const LegMode._('COV');
  static const RAIL = const LegMode._('RAIL');
  static const TRAM = const LegMode._('TRAM');
  static const WALK = const LegMode._('WALK');
  static const FUNICULAR = const LegMode._('FUNICULAR');

  static LegMode parse(String str) => _parseString<LegMode>(str, values);

  bool operator ==(dynamic other) => super._equalOperator(other, value);

  @override
  int get hashCode => value.hashCode;

  bool isTransitMode() {
    assert(this != null);
    if (this == LegMode.BUS || this == LegMode.CABLE_CAR || this == LegMode.RAIL || this == LegMode.TRAM || this == LegMode.FUNICULAR) return true;
    return false;
  }
}

class RelativeDirection extends Enum {
  const RelativeDirection._(String str) : super._(str);
  static get values => [UNKNOWN, DEPART, LEFT, RIGHT, SLIGHTLY_LEFT, SLIGHTLY_RIGHT,
    HARD_LEFT, HARD_RIGHT, UTURN_LEFT, UTURN_RIGHT, CONTINUE, CIRCLE_COUNTERCLOCKWISE, CIRCLE_CLOCKWISE, ELEVATOR];

  static const UNKNOWN = const RelativeDirection._('UNKNOWN');
  static const DEPART = const RelativeDirection._('DEPART');
  static const LEFT = const RelativeDirection._('LEFT');
  static const RIGHT = const RelativeDirection._('RIGHT');
  static const SLIGHTLY_LEFT = const RelativeDirection._('SLIGHTLY_LEFT');
  static const SLIGHTLY_RIGHT = const RelativeDirection._('SLIGHTLY_RIGHT');
  static const HARD_LEFT = const RelativeDirection._('HARD_LEFT');
  static const HARD_RIGHT = const RelativeDirection._('HARD_RIGHT');
  static const UTURN_LEFT = const RelativeDirection._('UTURN_LEFT');
  static const UTURN_RIGHT = const RelativeDirection._('UTURN_RIGHT');
  static const CONTINUE = const RelativeDirection._('CONTINUE');
  static const CIRCLE_COUNTERCLOCKWISE = const RelativeDirection._('CIRCLE_COUNTERCLOCKWISE');
  static const CIRCLE_CLOCKWISE = const RelativeDirection._('CIRCLE_CLOCKWISE');
  static const ELEVATOR = const RelativeDirection._('ELEVATOR');

  static RelativeDirection parse(String str) => _parseString<RelativeDirection>(str, values);

  bool operator ==(dynamic other) => super._equalOperator(other, value);

  @override
  int get hashCode => value.hashCode;
}

class AbsoluteDirection extends Enum {
  const AbsoluteDirection._(String str) : super._(str);
  static get values => [UNKNOWN, NORTH, EAST, SOUTH, WEST, SOUTHEAST, SOUTHWEST,
    NORTHEAST, NORTHWEST];

  static const UNKNOWN = const AbsoluteDirection._('UNKNOWN');
  static const NORTH = const AbsoluteDirection._('NORTH');
  static const EAST = const AbsoluteDirection._('EAST');
  static const SOUTH = const AbsoluteDirection._('SOUTH');
  static const WEST = const AbsoluteDirection._('WEST');
  static const SOUTHEAST = const AbsoluteDirection._('SOUTHEAST');
  static const SOUTHWEST = const AbsoluteDirection._('SOUTHWEST');
  static const NORTHEAST = const AbsoluteDirection._('NORTHEAST');
  static const NORTHWEST = const AbsoluteDirection._('NORTHWEST');

  static AbsoluteDirection parse(String str) => _parseString<AbsoluteDirection>(str, values);

  bool operator ==(dynamic other) => super._equalOperator(other, value);

  @override
  int get hashCode => value.hashCode;
}