# metro

Projet flutter de l'application Métromobilité.


## Getting Started

Pour générer les fichiers générés pour la base de données entre autres, utiliser à la racine du projet soit la commande :
`flutter packages pub run build_runner build --delete-conflicting-outputs`.

Ou pour observer les changements :
`flutter packages pub run build_runner watch`.


## Déploiement iOS

Pour éviter des soucis lors de l'upload de l'application sur iTunes Connect, il est préférable d'exécuter ces comande avant de build :
```
flutter clean
rm -rf ios/Flutter/Flutter.framework
flutter pub get
flutter build ios --release
xcodebuild -workspace ios/Runner.xcworkspace -scheme Runner -sdk iphoneos -configuration Release archive -archivePath ~/Dev/Metro-Flutter/build/Runner.xcarchive

```